package com.alacritystudios.statdota.sql;

import android.provider.BaseColumns;

/**
 * Class to define the various table contracts.
 */

public abstract class StatDotaDatabaseContracts {

    public static class HeroEntry implements BaseColumns {

        public static final String TABLE_NAME = "hero_table";
        public static final String COLUMN_NAME_HERO_ID = "hero_id";
        public static final String COLUMN_NAME_HERO_NAME = "hero_name";
        public static final String COLUMN_NAME_HERO_LOCALIZED_NAME = "hero_localized_name";
        public static final String COLUMN_NAME_HERO_PRIMARY_ATTRIBUTE = "hero_primary_attribute";
    }

    public static class ItemEntry implements BaseColumns {

        public static final String TABLE_NAME = "item_table";
        public static final String COLUMN_ITEM_ID = "item_id";
        public static final String COLUMN_ITEM_NAME = "item_name";
        public static final String COLUMN_ITEM_COST = "item_cost";
        public static final String COLUMN_SECRET_SHOP = "secret_shop";
        public static final String COLUMN_SIDE_SHOP = "side_shop";
        public static final String COLUMN_RECIPE = "recipe";
    }
}