package com.alacritystudios.statdota.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.alacritystudios.statdota.models.HeroDetails;
import com.alacritystudios.statdota.models.ItemDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * A contract which defines the schema of various Tables used in the Encore system.
 */

public class StatDotaDatabaseHelper extends SQLiteOpenHelper {

    private static StatDotaDatabaseHelper mInstance = null;

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "StatDotaDatabase.db";

    private static final String SQL_CREATE_HERO_DETAILS_TABLE =
            "CREATE TABLE " + StatDotaDatabaseContracts.HeroEntry.TABLE_NAME + " (" +
                    StatDotaDatabaseContracts.HeroEntry._ID + " INTEGER PRIMARY KEY," +
                    StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_ID + " INTEGER," +
                    StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_NAME + " TEXT," +
                    StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_LOCALIZED_NAME + " TEXT," +
                    StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_PRIMARY_ATTRIBUTE + " TEXT)";

    private static final String SQL_CREATE_ITEM_DETAILS_TABLE =
            "CREATE TABLE " + StatDotaDatabaseContracts.ItemEntry.TABLE_NAME + " (" +
                    StatDotaDatabaseContracts.ItemEntry._ID + " INTEGER PRIMARY KEY," +
                    StatDotaDatabaseContracts.ItemEntry.COLUMN_ITEM_ID + " INTEGER," +
                    StatDotaDatabaseContracts.ItemEntry.COLUMN_ITEM_NAME + " TEXT," +
                    StatDotaDatabaseContracts.ItemEntry.COLUMN_ITEM_COST + " INTEGER," +
                    StatDotaDatabaseContracts.ItemEntry.COLUMN_SECRET_SHOP + " INTEGER," +
                    StatDotaDatabaseContracts.ItemEntry.COLUMN_SIDE_SHOP + " INTEGER," +
                    StatDotaDatabaseContracts.ItemEntry.COLUMN_RECIPE + " INTEGER)";

    public StatDotaDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static StatDotaDatabaseHelper getInstance(Context context) {

        if (mInstance == null) {
            mInstance = new StatDotaDatabaseHelper(context.getApplicationContext());
        }
        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_HERO_DETAILS_TABLE);
        db.execSQL(SQL_CREATE_ITEM_DETAILS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void saveOrUpdateHeroDetails(List<HeroDetails> heroDetails) {
        for (int i = 0; i < heroDetails.size(); i++) {
            SQLiteDatabase sqLiteReadableDatabase = this.getReadableDatabase();
            SQLiteDatabase sqLiteWritableDatabase;
            String[] columns = {StatDotaDatabaseContracts.HeroEntry._ID, StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_ID, StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_NAME,
                    StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_LOCALIZED_NAME, StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_PRIMARY_ATTRIBUTE};
            String selection = StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_ID + " =?";
            String[] selectionArgs = {String.valueOf(heroDetails.get(i).getId())};
            Cursor cursor = sqLiteReadableDatabase.query(StatDotaDatabaseContracts.HeroEntry.TABLE_NAME, columns, selection, selectionArgs, null, null, null);

            if (cursor != null && cursor.moveToFirst()) {
                sqLiteReadableDatabase.close();
                sqLiteWritableDatabase = this.getWritableDatabase();
                ContentValues contentValues = new ContentValues();
                contentValues.put(StatDotaDatabaseContracts.HeroEntry._ID, cursor.getInt(0));
                contentValues.put(StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_ID, cursor.getInt(1));
                contentValues.put(StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_NAME, heroDetails.get(i).getName());
                contentValues.put(StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_LOCALIZED_NAME, heroDetails.get(i).getLocalizedName());
                contentValues.put(StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_PRIMARY_ATTRIBUTE, heroDetails.get(i).getPrimaryAttr());
                sqLiteWritableDatabase.update(StatDotaDatabaseContracts.HeroEntry.TABLE_NAME, contentValues, selection, selectionArgs);
                cursor.close();
            } else {
                sqLiteReadableDatabase.close();
                sqLiteWritableDatabase = this.getWritableDatabase();
                ContentValues contentValues = new ContentValues();
                contentValues.put(StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_ID, heroDetails.get(i).getId());
                contentValues.put(StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_NAME, heroDetails.get(i).getName());
                contentValues.put(StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_LOCALIZED_NAME, heroDetails.get(i).getLocalizedName());
                contentValues.put(StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_PRIMARY_ATTRIBUTE, heroDetails.get(i).getPrimaryAttr());
                sqLiteWritableDatabase.insert(StatDotaDatabaseContracts.HeroEntry.TABLE_NAME, "", contentValues);
            }

            sqLiteWritableDatabase.close();
        }
    }

    public void saveHeroDetails(List<HeroDetails> heroDetails) {
        SQLiteDatabase sqLiteWritableDatabase = this.getWritableDatabase();
        sqLiteWritableDatabase.delete(StatDotaDatabaseContracts.HeroEntry.TABLE_NAME, null, null);
        for (int i = 0; i < heroDetails.size(); i++) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_ID, heroDetails.get(i).getId());
            contentValues.put(StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_NAME, heroDetails.get(i).getName());
            contentValues.put(StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_LOCALIZED_NAME, heroDetails.get(i).getLocalizedName());
            contentValues.put(StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_PRIMARY_ATTRIBUTE, heroDetails.get(i).getPrimaryAttr());
            sqLiteWritableDatabase.insert(StatDotaDatabaseContracts.HeroEntry.TABLE_NAME, "", contentValues);
        }
        sqLiteWritableDatabase.close();
    }

    public HeroDetails fetchHeroDetails(long heroId) {
        HeroDetails heroDetails = new HeroDetails();
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String[] columns = {StatDotaDatabaseContracts.HeroEntry._ID, StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_ID, StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_NAME,
                StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_LOCALIZED_NAME, StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_PRIMARY_ATTRIBUTE};
        String selection = StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_ID + " =?";
        String[] selectionArgs = {String.valueOf(heroId)};
        Cursor cursor = sqLiteDatabase.query(StatDotaDatabaseContracts.HeroEntry.TABLE_NAME, columns, selection, selectionArgs, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            heroDetails.setId(cursor.getInt(1));
            heroDetails.setName(cursor.getString(2));
            heroDetails.setLocalizedName(cursor.getString(3));
            heroDetails.setPrimaryAttr(cursor.getString(4));
            cursor.close();
        }
        sqLiteDatabase.close();
        return heroDetails;
    }

    public List<HeroDetails> fetchHeroDetailsList() {
        List<HeroDetails> mHeroDetailsList = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String[] columns = {StatDotaDatabaseContracts.HeroEntry._ID, StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_ID, StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_NAME,
                StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_LOCALIZED_NAME, StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_PRIMARY_ATTRIBUTE};

        Cursor cursor = sqLiteDatabase.query(StatDotaDatabaseContracts.HeroEntry.TABLE_NAME, null, null, null, null, null, StatDotaDatabaseContracts.HeroEntry.COLUMN_NAME_HERO_LOCALIZED_NAME + " ASC");

        if (cursor != null && cursor.moveToFirst()) {
            do {
                HeroDetails heroDetails = new HeroDetails();
                heroDetails.setId(cursor.getInt(1));
                heroDetails.setName(cursor.getString(2));
                heroDetails.setLocalizedName(cursor.getString(3));
                heroDetails.setPrimaryAttr(cursor.getString(4));
                mHeroDetailsList.add(heroDetails);
            } while (cursor.moveToNext());
            cursor.close();
        }
        sqLiteDatabase.close();
        return mHeroDetailsList;
    }

    public void saveOrUpdateItemDetails(List<ItemDetails> itemDetails) {
        for (int i = 0; i < itemDetails.size(); i++) {
            SQLiteDatabase sqLiteReadableDatabase = this.getReadableDatabase();
            SQLiteDatabase sqLiteWritableDatabase;
            String[] columns = {StatDotaDatabaseContracts.ItemEntry._ID, StatDotaDatabaseContracts.ItemEntry.COLUMN_ITEM_ID, StatDotaDatabaseContracts.ItemEntry.COLUMN_ITEM_NAME,
                    StatDotaDatabaseContracts.ItemEntry.COLUMN_ITEM_COST, StatDotaDatabaseContracts.ItemEntry.COLUMN_SECRET_SHOP, StatDotaDatabaseContracts.ItemEntry.COLUMN_SIDE_SHOP, StatDotaDatabaseContracts.ItemEntry.COLUMN_RECIPE};
            String selection = StatDotaDatabaseContracts.ItemEntry.COLUMN_ITEM_ID + " =?";
            String[] selectionArgs = {String.valueOf(itemDetails.get(i).getId())};
            Cursor cursor = sqLiteReadableDatabase.query(StatDotaDatabaseContracts.ItemEntry.TABLE_NAME, columns, selection, selectionArgs, null, null, null);

            if (cursor != null && cursor.moveToFirst()) {
                sqLiteReadableDatabase.close();
                sqLiteWritableDatabase = this.getWritableDatabase();
                ContentValues contentValues = new ContentValues();
                contentValues.put(StatDotaDatabaseContracts.ItemEntry._ID, cursor.getInt(0));
                contentValues.put(StatDotaDatabaseContracts.ItemEntry.COLUMN_ITEM_ID, cursor.getInt(1));
                contentValues.put(StatDotaDatabaseContracts.ItemEntry.COLUMN_ITEM_NAME, itemDetails.get(i).getName());
                contentValues.put(StatDotaDatabaseContracts.ItemEntry.COLUMN_ITEM_COST, itemDetails.get(i).getCost());
                contentValues.put(StatDotaDatabaseContracts.ItemEntry.COLUMN_SECRET_SHOP, itemDetails.get(i).getSecretShop());
                contentValues.put(StatDotaDatabaseContracts.ItemEntry.COLUMN_SIDE_SHOP, itemDetails.get(i).getSideShop());
                contentValues.put(StatDotaDatabaseContracts.ItemEntry.COLUMN_RECIPE, itemDetails.get(i).getRecipe());
                sqLiteWritableDatabase.update(StatDotaDatabaseContracts.ItemEntry.TABLE_NAME, contentValues, selection, selectionArgs);
                cursor.close();
            } else {
                sqLiteReadableDatabase.close();
                sqLiteWritableDatabase = this.getWritableDatabase();
                ContentValues contentValues = new ContentValues();
                contentValues.put(StatDotaDatabaseContracts.ItemEntry.COLUMN_ITEM_ID, itemDetails.get(i).getId());
                contentValues.put(StatDotaDatabaseContracts.ItemEntry.COLUMN_ITEM_NAME, itemDetails.get(i).getName());
                contentValues.put(StatDotaDatabaseContracts.ItemEntry.COLUMN_ITEM_COST, itemDetails.get(i).getCost());
                contentValues.put(StatDotaDatabaseContracts.ItemEntry.COLUMN_SECRET_SHOP, itemDetails.get(i).getSecretShop());
                contentValues.put(StatDotaDatabaseContracts.ItemEntry.COLUMN_SIDE_SHOP, itemDetails.get(i).getSideShop());
                contentValues.put(StatDotaDatabaseContracts.ItemEntry.COLUMN_RECIPE, itemDetails.get(i).getRecipe());
                sqLiteWritableDatabase.insert(StatDotaDatabaseContracts.ItemEntry.TABLE_NAME, "", contentValues);
            }

            sqLiteWritableDatabase.close();
        }
    }

    public void saveItemDetails(List<ItemDetails> itemDetails) {
        SQLiteDatabase sqLiteWritableDatabase = this.getWritableDatabase();
        sqLiteWritableDatabase.delete(StatDotaDatabaseContracts.ItemEntry.TABLE_NAME, null, null);
        for (int i = 0; i < itemDetails.size(); i++) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(StatDotaDatabaseContracts.ItemEntry.COLUMN_ITEM_ID, itemDetails.get(i).getId());
            contentValues.put(StatDotaDatabaseContracts.ItemEntry.COLUMN_ITEM_NAME, itemDetails.get(i).getName());
            contentValues.put(StatDotaDatabaseContracts.ItemEntry.COLUMN_ITEM_COST, itemDetails.get(i).getCost());
            contentValues.put(StatDotaDatabaseContracts.ItemEntry.COLUMN_SECRET_SHOP, itemDetails.get(i).getSecretShop());
            contentValues.put(StatDotaDatabaseContracts.ItemEntry.COLUMN_SIDE_SHOP, itemDetails.get(i).getSideShop());
            contentValues.put(StatDotaDatabaseContracts.ItemEntry.COLUMN_RECIPE, itemDetails.get(i).getRecipe());
            sqLiteWritableDatabase.insert(StatDotaDatabaseContracts.ItemEntry.TABLE_NAME, "", contentValues);
        }

        sqLiteWritableDatabase.close();
    }

    public ItemDetails fetchItemDetails(long itemId) {
        ItemDetails itemDetails = new ItemDetails();
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String[] columns = {StatDotaDatabaseContracts.ItemEntry._ID, StatDotaDatabaseContracts.ItemEntry.COLUMN_ITEM_ID, StatDotaDatabaseContracts.ItemEntry.COLUMN_ITEM_NAME,
                StatDotaDatabaseContracts.ItemEntry.COLUMN_ITEM_COST, StatDotaDatabaseContracts.ItemEntry.COLUMN_SECRET_SHOP, StatDotaDatabaseContracts.ItemEntry.COLUMN_SIDE_SHOP, StatDotaDatabaseContracts.ItemEntry.COLUMN_RECIPE};
        String selection = StatDotaDatabaseContracts.ItemEntry.COLUMN_ITEM_ID + " =?";
        String[] selectionArgs = {String.valueOf(itemId)};
        Cursor cursor = sqLiteDatabase.query(StatDotaDatabaseContracts.ItemEntry.TABLE_NAME, columns, selection, selectionArgs, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            itemDetails.setId(cursor.getInt(1));
            itemDetails.setName(cursor.getString(2));
            itemDetails.setCost(cursor.getInt(3));
            itemDetails.setSecretShop(cursor.getInt(4));
            itemDetails.setSideShop(cursor.getInt(5));
            itemDetails.setRecipe(cursor.getInt(6));
            cursor.close();
        }
        sqLiteDatabase.close();
        return itemDetails;
    }
}
