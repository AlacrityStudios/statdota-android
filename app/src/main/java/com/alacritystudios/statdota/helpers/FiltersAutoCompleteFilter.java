package com.alacritystudios.statdota.helpers;

import android.widget.Filter;

import com.alacritystudios.statdota.adapters.FiltersAutoCompleteTextViewAdapter;
import com.alacritystudios.statdota.models.FiltersAutoCompleteTextViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Custom filter for ordering elements.
 */

public class FiltersAutoCompleteFilter extends Filter {

    private FiltersAutoCompleteTextViewAdapter adapter;
    private List<FiltersAutoCompleteTextViewModel> originalList;
    private List<FiltersAutoCompleteTextViewModel> filteredList;

    public FiltersAutoCompleteFilter(FiltersAutoCompleteTextViewAdapter adapter, List<FiltersAutoCompleteTextViewModel> originalList) {
        super();
        this.adapter = adapter;
        this.originalList = originalList;
        this.filteredList = new ArrayList<>();
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        filteredList.clear();
        final FilterResults results = new FilterResults();

        if (constraint == null || constraint.length() == 0) {
            filteredList.addAll(originalList);
        } else {
            final String filterPattern = constraint.toString().toLowerCase().trim();
            // Your filtering logic goes in here
            for (final FiltersAutoCompleteTextViewModel filtersAutoCompleteTextViewModel : originalList) {
                if (filtersAutoCompleteTextViewModel.getName().toLowerCase().contains(filterPattern)) {
                    filteredList.add(filtersAutoCompleteTextViewModel);
                }
            }
        }
        results.values = filteredList;
        results.count = filteredList.size();
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.getmFilteredSuggestionsList().clear();
        adapter.getmFilteredSuggestionsList().addAll((List) results.values);
        adapter.notifyDataSetChanged();
    }
}
