package com.alacritystudios.statdota.rest;

import android.support.annotation.Nullable;

import com.alacritystudios.statdota.models.ApplicationMetadataModel;
import com.alacritystudios.statdota.models.HeroStatsModel;
import com.alacritystudios.statdota.models.HighMmrPubStarsModel;
import com.alacritystudios.statdota.models.HomeFragmentWrapperModel;
import com.alacritystudios.statdota.models.MatchFilterResultModel;
import com.alacritystudios.statdota.models.PubMatchesModel;
import com.alacritystudios.statdota.models.RecentLeagueMatchModel;
import com.alacritystudios.statdota.models.SearchResultModel;
import com.alacritystudios.statdota.models.TeamDetailsModel;
import com.alacritystudios.statdota.models.TeamsModel;
import com.alacritystudios.statdota.models.TwitchStreamsModel;
import com.alacritystudios.statdota.models.UserFriendsModel;
import com.alacritystudios.statdota.models.UserHeroSummaryModel;
import com.alacritystudios.statdota.models.UserMatchSummaryModel;
import com.alacritystudios.statdota.models.UserStatsModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Retrofit interface
 */

public interface ApiInterface {

    @GET("metadata")
    public Call<ApplicationMetadataModel> getApplicationMetadata();

    @GET("players/{account_id}")
    public Call<UserStatsModel> getUserStatsInfo(@Path("account_id") String accountId);

    //    @GET("players/{account_id}/totals")
//    public Call<Totals> getPlayerTotals(@Path("account_id") String accountId);
//
//    @GET("players/{account_id}")
//    public Call<PlayerBasicInfo> getPlayerStats(@Path("account_id") String accountId);
//
    @GET("player/{account_id}/matches")
    public Call<List<UserMatchSummaryModel>> getPlayerMatches(@Path("account_id") String accountId, @Query("limit") Integer limit, @Query("offset") Integer offset, @Query("hero_id") Integer heroId,
                                                              @Query("win") Integer win, @Query("is_radiant") Integer isRadiant, @Query("game_mode")Integer gameMode, @Query("lobby_type")Integer lobbyType,
                                                              @Query("with_hero_id")List<Integer> withHeroIds, @Query("against_hero_id")List<Integer> againstHeroIds);

    @GET("player/{account_id}/heroes")
    public Call<List<UserHeroSummaryModel>> getUserHeroSummary(@Path("account_id") String accountId);

    @GET("heroes")
    public Call<List<HeroStatsModel>> getHeroStats();

    @GET("player/{account_id}/peers")
    public Call<List<UserFriendsModel>> getUserFriends(@Path("account_id") String accountId);

//    @GET("leagues")
//    public Call<List<Leagues>> getLeagues();

    @GET("teams")
    public Call<List<TeamsModel>> getTeams(@Query("offset") int offset, @Query("count") int count, @Query("query") String query);

    @GET("teams/{teamId}")
    public Call<TeamDetailsModel> getTeamDetails(@Path("teamId") long teamId);

    @GET("mmr")
    public Call<List<HighMmrPubStarsModel>> getMmrRatings(@Query("offset") int offset, @Query("count") int count, @Query("query") String query);
//
//    @GET("proPlayers")
//    public Call<List<ProPlayers>> getProPlayers(@Query("limit") int limit, @Query("offset") int offset);

    @GET("leagues/matches")
    public Call<List<RecentLeagueMatchModel>> getRecentLeagueMatches(@Query("limit") int limit, @Query("offset") int offset);

    @GET("/pubMatches")
    public Call<List<PubMatchesModel>> getRecentPubMatches(@Query("limit") int limit, @Query("offset") int offset);
//
//    @GET("leagues/liveMatches")
//    public Call<List<LiveLeagueMatch>> getLiveLeagueMatches(@Query("limit") int limit, @Query("offset") int offset);
//
//    @GET("match/{matchId}")
//    public Call<LeagueMatchDetails> getMatchDetails(@Path("matchId") long matchId);
//
//    @POST("request/{match_id}")
//    public Call<ResponseBody> requestParse(@Path("matchId") long matchId);
//
//    @GET("leagues/liveMatch/{matchId}")
//    public Call<LiveLeagueMatch> getLiveLeagueMatchDetails(@Path("matchId") long matchId);

    // Twitch calls.
    @GET("streams/")
    public Call<TwitchStreamsModel> getLiveStreams(@Query("client_id") String clientId, @Query("game") String game);

    //Home fragment data.
    @GET("trending/")
    public Call<HomeFragmentWrapperModel> getHomeFragmentData();

    @GET("search/")
    public Call<SearchResultModel> performSearch(@Query("query") String query);
}