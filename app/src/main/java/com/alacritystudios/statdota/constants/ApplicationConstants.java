package com.alacritystudios.statdota.constants;

/**
 * Model to store some constants.
 */

public interface ApplicationConstants {

    final long weekInMillis = 604800000l;
    final String[] laneRoleArray = {"-", "Safe", "Mid", "Off", "Jungle"};
    final String[] lanePositionArray = {"-", "Bot", "Mid", "Top", "Radiant Jungle", "Dire Jungle"};
    final String[] lobbyTypeArray = {"Normal", "Practice", "Tournament", "Tutorial", "Co-Op Bots", "Ranked Team MM (Legacy)",
            "Ranked Solo MM (Legacy)", "Ranked", "1v1 Mid", "Battle Cup"};
    final String[] gameModeArray = {"-", "All Pick", "Captain\'s Mode", "Random Draft", "Single Draft", "All Random", "Intro", "Diretide", "Reverse Captains Mode", "The Greeviling",
            "Tutorial", "Mid Only", "Least Played", "Limited Heroes", "Compendium", "Custom", "Captains Draft", "Balanced Draft", "Ability Draft", "Event", "All Random Deathmatch",
            "1v1 Solo Mid", "All Draft"};
    final String[] gameTotalsArray = {"kills", "deaths", "assists", "gold_per_min", "xp_per_min", "hero_damage", "tower_damage", "hero_healing", "duration"};
    final String[] seriesTypeArray = {"BO1", "BO3", "BO5"};
    final String[] leagueTypeArray = {"Unknown", "Amateur", "Professional", "Premier"};
    final String flagsApiUrl = "https://flagpedia.net/data/flags/small/{countryCode}.png";
}