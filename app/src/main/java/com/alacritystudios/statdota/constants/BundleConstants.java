package com.alacritystudios.statdota.constants;

/**
 * Stores bundle constants.
 */

public interface BundleConstants {

    final String ACCOUNT_ID = "ACCOUNT_ID";
    final String MATCH_ID = "MATCH_ID";
    final String TEAM_ID = "TEAM_ID";
    final String LEAGUE_NAME = "LEAGUE_NAME";
    final String IS_USER_ACCOUNT = "IS_USER_ACCOUNT";
    final String STREAM_URL = "STREAM_URL";
}
