package com.alacritystudios.statdota.constants;

/**
 * An interface to store the StatDota Api constants.
 */

public interface StatDotaApiConstants {
    //final String STAT_DOTA_BASE_URL = "http://ec2-54-88-169-28.compute-1.amazonaws.com:8080/";
    final String STAT_DOTA_BASE_URL = "http://192.168.1.109:8080/";
}
