package com.alacritystudios.statdota.constants;

/**
 * An interface to store the OpenDota Api constants.
 */

public interface OpenDotaApiConstants {
    final String OPEN_DOTA_BASE_URL = "https://api.opendota.com/api/";
    final String OPEN_DOTA_FIELD_KILLS = "kills";
    final String OPEN_DOTA_FIELD_DEATHS = "deaths";
    final String OPEN_DOTA_FIELD_GPM = "gold_per_min";
    final String OPEN_DOTA_FIELD_XPM = "xp_per_min";
    final String OPEN_DOTA_FIELD_DURATION = "duration";
}
