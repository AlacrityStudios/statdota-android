package com.alacritystudios.statdota.constants;

/**
 * An interface to store the Steam Api constants.
 */

public interface SteamWebApiConstants {
    final String STEAM_API_BASE_URL = "http://api.steampowered.com/";
    final String STEAM_API_KEY = "686187C39B750916542A36907E588B4A";
    final long STEAM_API_ACCOUNT_ID_OFFSET = 76561197960265728l;
    final String STEAM_API_HERO_PORTRAIT_BASE_URL = "http://cdn.dota2.com/apps/dota2/images/heroes/{hero_name}_vert.jpg";
    final String STEAM_API_ITEM_PORTRAIT_BASE_URL = "http://cdn.dota2.com/apps/dota2/images/items/{item_name}_lg.png";
}