package com.alacritystudios.statdota.constants;

/**
 * Constants file to store fragment tags.
 */

public class FragmentTagConstants {

    public static final String HOME_FRAGMENT_TAG = "HOME_FRAGMENT";
    public static final String SEARCH_FRAGMENT_TAG = "SEARCH_FRAGMENT";
    public static final String PROFILE_FRAGMENT_TAG = "PROFILE_FRAGMENT";
    public static final String LEAGUE_MATCHES_FRAGMENT_TAG = "LEAGUE_MATCHES_FRAGMENT";
    public static final String TWITCH_FRAGMENT_TAG = "TWITCH_FRAGMENT";
    public static final String HERO_NETWORK_FRAGMENT = "HERO_NETWORK_FRAGMENT";
    public static final String HERO_PERFORMANCE_STATS_LIST_FRAGMENT = "HERO_PERFORMANCE_STATS_LIST_FRAGMENT";
    public static final String LANDING_ACTIVITY_NETWORK_FRAGMENT = "LANDING_ACTIVITY_NETWORK_FRAGMENT";
    public static final String TEAM_ACTIVITY_NETWORK_FRAGMENT = "TEAM_ACTIVITY_NETWORK_FRAGMENT";
    public static final String TEAMS_LIST_FRAGMENT = "TEAMS_LIST_FRAGMENT";
    public static final String MMR_ACTIVITY_NETWORK_FRAGMENT = "MMR_ACTIVITY_NETWORK_FRAGMENT";
    public static final String HIGH_MMR_PUB_STARS_LIST_FRAGMENT = "HIGH_MMR_PUB_STARS_LIST_FRAGMENT";
    public static final String ACCOUNT_ACTIVITY_NETWORK_FRAGMENT = "ACCOUNT_ACTIVITY_NETWORK_FRAGMENT";
}
