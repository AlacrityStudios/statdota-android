package com.alacritystudios.statdota.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.media.session.PlaybackStateCompat;

/**
 * Stores the user preferences
 */

public class PreferenceUtil {

    private static final String PREF_CURRENT_LOGGED_IN_STEAM_64_BIT_ID = "pref_current_steam_64_bit_id";
    private static final String PREF_METADATA_MODIFICATION_DATE = "pref_metadata_modification_date";
    private static final String PREF_LOGGED_IN_USER_PERSONA_NAME = "pref_logged_in_user_persona_name";

    public static Long getSteam64BitIdPreference(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getLong(PREF_CURRENT_LOGGED_IN_STEAM_64_BIT_ID, 0L);
    }

    public static void setSteam64BitIdPreference(Context mContext, long steamId) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        sharedPreferences.edit().putLong(PREF_CURRENT_LOGGED_IN_STEAM_64_BIT_ID, steamId).apply();
    }

    public static Long getMetadataModificationDatePreference(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getLong(PREF_METADATA_MODIFICATION_DATE, 0);
    }

    public static void setMetadataModificationDatePreference(Context mContext, long currentTimeInMillis) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        sharedPreferences.edit().putLong(PREF_METADATA_MODIFICATION_DATE, currentTimeInMillis).apply();
    }

    public static String getLoggedInUserPersonaNamePreference(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getString(PREF_LOGGED_IN_USER_PERSONA_NAME, "");
    }

    public static void setLoggedInUserPersonaNamePreference(Context mContext, String personaName) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        sharedPreferences.edit().putString(PREF_LOGGED_IN_USER_PERSONA_NAME, personaName).apply();
    }
}
