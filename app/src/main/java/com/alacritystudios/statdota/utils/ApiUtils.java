package com.alacritystudios.statdota.utils;

import android.util.Log;

import com.alacritystudios.statdota.constants.SteamWebApiConstants;
import com.alacritystudios.statdota.models.HeroDetails;
import com.alacritystudios.statdota.models.ItemDetails;
import com.alacritystudios.statdota.sql.StatDotaDatabaseHelper;

/**
 * A general purpose util class.
 */

public class ApiUtils {

    public static final String TAG = ApiUtils.class.getSimpleName();

    public static String getHeroImageUrl(String heroName) {
        Log.i(TAG, "getHeroImageUrl() - " + heroName);
        return SteamWebApiConstants.STEAM_API_HERO_PORTRAIT_BASE_URL.replace("{hero_name}", heroName.replace("npc_dota_hero_", ""));
    }

    public static String getItemImageUrl(StatDotaDatabaseHelper statDotaDatabaseHelper, int itemId){
        Log.i(TAG, "getItemImageUrl() - " + itemId);
        ItemDetails itemDetails = statDotaDatabaseHelper.fetchItemDetails(itemId);
        if(itemDetails.getName() == null) {
            return "";
        } else {
            return SteamWebApiConstants.STEAM_API_ITEM_PORTRAIT_BASE_URL.replace("{item_name}", itemDetails.getName().replace("item_", ""));
        }
    }

//    public static String getHeroImageUrl(StatDotaDatabaseHelper statDotaDatabaseHelper, int heroId){
//        Log.i(TAG, "getHeroImageUrl() - " + heroId);
//        HeroDetails heroDetails = statDotaDatabaseHelper.fetchHeroDetails(heroId);
//        if(heroDetails.getName() == null) {
//            return "";
//        } else {
//            return SteamWebApiConstants.STEAM_API_HERO_PORTRAIT_BASE_URL.replace("{hero_name}", heroDetails.getName().replace("npc_dota_hero_", ""));
//        }
//    }

    public static String getHeroLocalisedName(StatDotaDatabaseHelper statDotaDatabaseHelper, int heroId){
        Log.i(TAG, "getHeroLocalisedName() - " + heroId);
        HeroDetails heroDetails = statDotaDatabaseHelper.fetchHeroDetails(heroId);
        if(heroDetails.getLocalizedName() == null) {
            return "";
        } else {
            return heroDetails.getLocalizedName();
        }
    }

//    public static List<MatchPicksModel> formatPicksAndBans(List<LeagueMatchDetails.PicksBan> picksBanList, int gameMode) {
//        Log.i(TAG, "formatPicksAndBans()");
//        List<LeagueMatchDetails.PicksBan> radiantBans = new ArrayList<>();
//        List<LeagueMatchDetails.PicksBan> direBans = new ArrayList<>();
//        List<LeagueMatchDetails.PicksBan> radiantPicks = new ArrayList<>();
//        List<LeagueMatchDetails.PicksBan> direPicks = new ArrayList<>();
//        List<MatchPicksModel> mMatchPicksModels;
//        switch (gameMode) {
//            case 1:
//                mMatchPicksModels = new ArrayList<>();
//            break;
//            case 2:
//                mMatchPicksModels = new ArrayList<>(10);
//                for (LeagueMatchDetails.PicksBan picksBan : picksBanList) {
//                    if (picksBan.getTeam() == 0) {
//                        if (picksBan.getIsPick()) {
//                            radiantPicks.add(picksBan);
//                        } else {
//                            radiantBans.add(picksBan);
//                        }
//                    } else {
//                        if (picksBan.getIsPick()) {
//                            direPicks.add(picksBan);
//                        } else {
//                            direBans.add(picksBan);
//                        }
//                    }
//                }
//                mMatchPicksModels.add(0, new MatchPicksModel(1, radiantBans.get(0).getHeroId(), direBans.get(0).getHeroId(), false));
//                mMatchPicksModels.add(1, new MatchPicksModel(2, radiantBans.get(1).getHeroId(), direBans.get(1).getHeroId(), false));
//                mMatchPicksModels.add(2, new MatchPicksModel(3, radiantPicks.get(0).getHeroId(), direPicks.get(0).getHeroId(), true));
//                mMatchPicksModels.add(3, new MatchPicksModel(4, radiantPicks.get(1).getHeroId(), direPicks.get(1).getHeroId(), true));
//                mMatchPicksModels.add(4, new MatchPicksModel(5, radiantBans.get(2).getHeroId(), direBans.get(2).getHeroId(), false));
//                mMatchPicksModels.add(5, new MatchPicksModel(6, radiantBans.get(3).getHeroId(), direBans.get(3).getHeroId(), false));
//                mMatchPicksModels.add(6, new MatchPicksModel(7, radiantPicks.get(2).getHeroId(), direPicks.get(2).getHeroId(), true));
//                mMatchPicksModels.add(7, new MatchPicksModel(8, radiantPicks.get(3).getHeroId(), direPicks.get(3).getHeroId(), true));
//                mMatchPicksModels.add(8, new MatchPicksModel(9, radiantBans.get(4).getHeroId(), direBans.get(4).getHeroId(), false));
//                mMatchPicksModels.add(9, new MatchPicksModel(10, radiantPicks.get(4).getHeroId(), direPicks.get(4).getHeroId(), true));
//                break;
//            default:
//                mMatchPicksModels = new ArrayList<>();
//        }
//        return mMatchPicksModels;
//    }

}
