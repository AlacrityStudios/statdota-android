package com.alacritystudios.statdota.utils;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;

/**
 * Util class to create commonly used components
 */

public class ComponentUtil {

    public static final String TAG = LoggerUtil.makeLogTag(ComponentUtil.class);

    /**
     * Convenience method to display a snack bar with a message.
     * @param context
     * @param message
     */
    public static void createAndShowSnackBar(Context context, View parentView, String message) {
        Log.i(TAG, "createAndShowSnackBar()");
        Snackbar.make(parentView, message, 3000).show();
    }
}
