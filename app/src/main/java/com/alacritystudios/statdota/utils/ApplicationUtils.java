package com.alacritystudios.statdota.utils;

import com.alacritystudios.statdota.constants.ApplicationConstants;

/**
 * A class for commonly used application utility methods.
 */

public class ApplicationUtils
{
    private static final String TAG = LoggerUtil.makeLogTag(ApplicationUtils.class);

    public static String makePlaceholderString(String toBeShown, String placeholderString) {
        return (toBeShown == null || toBeShown.trim().isEmpty())? placeholderString : toBeShown;
    }

    public static String makePlaceholderLongString(Long toBeShown, String placeholderString) {
        return toBeShown == null? placeholderString : String.valueOf(toBeShown);
    }

    public static String makePlaceholderIntegerString(Integer toBeShown, String placeholderString) {
        return toBeShown == null? placeholderString : String.valueOf(toBeShown);
    }

    public static String makePlaceholderFloatString(Float toBeShown, String placeholderString) {
        return toBeShown == null? placeholderString : String.valueOf(toBeShown);
    }

    public static String getFlagUrl(String countryCode) {
        return ApplicationConstants.flagsApiUrl.replace("{countryCode}", countryCode.toLowerCase());
    }
}
