package com.alacritystudios.statdota.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.interfaces.FilterItemRemovalListener;
import com.alacritystudios.statdota.models.FiltersAutoCompleteTextViewModel;
import com.alacritystudios.statdota.utils.AnimationUtils;
import com.bumptech.glide.Glide;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Adapter to display selected filters.
 */

public class SelectedFilterAdapter extends RecyclerView.Adapter<SelectedFilterAdapter.ItemViewHolder> {

    private List<FiltersAutoCompleteTextViewModel> mModelList;
    private Context mContext;
    private FilterItemRemovalListener mFilterItemRemovalListener;

    public SelectedFilterAdapter(List<FiltersAutoCompleteTextViewModel> mModelList, Context mContext, FilterItemRemovalListener mFilterItemRemovalListener) {
        this.mModelList = mModelList;
        this.mContext = mContext;
        this.mFilterItemRemovalListener = mFilterItemRemovalListener;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private View mRootLayout;
        private CircleImageView logoImageView;
        private TextView nameTextView;
        private ImageView clearImageView;
        private Animation mAnimation;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mRootLayout = itemView;
            logoImageView = (CircleImageView) itemView.findViewById(R.id.selected_filter_image);
            nameTextView = (TextView) itemView.findViewById(R.id.selected_filter_name);
            clearImageView = (ImageView) itemView.findViewById(R.id.clear_filter);
        }


        public void clearAnimation() {
            mRootLayout.clearAnimation();
        }

        private void setScaleAnimation() {
            mRootLayout.setAnimation(mAnimation);
        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SelectedFilterAdapter.ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_view_selected_filter_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {
        holder.mAnimation = AnimationUtils.setAlphaAnimation(600);
        Glide.with(mContext)
                .load(mModelList.get(position).getLogoUrl())
                .centerCrop()
                .into(holder.logoImageView);
        holder.nameTextView.setText(mModelList.get(position).getName());
        holder.clearImageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mFilterItemRemovalListener.removeItemAt(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mModelList != null) {
            return mModelList.size();
        } else {
            return 0;
        }
    }

    @Override
    public void onViewDetachedFromWindow(SelectedFilterAdapter.ItemViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.clearAnimation();
    }

    @Override
    public void onViewAttachedToWindow(SelectedFilterAdapter.ItemViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.setScaleAnimation();
    }
}