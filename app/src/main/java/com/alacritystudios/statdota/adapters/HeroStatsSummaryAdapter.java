package com.alacritystudios.statdota.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.activities.AccountActivity;
import com.alacritystudios.statdota.fragments.HeroStatsDialogFragment;
import com.alacritystudios.statdota.models.HeroStatsModel;
import com.alacritystudios.statdota.utils.ApiUtils;
import com.alacritystudios.statdota.utils.ApplicationUtils;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Adapter to display hero stats summary.
 */

public class HeroStatsSummaryAdapter extends RecyclerView.Adapter<HeroStatsSummaryAdapter.ItemViewHolder> {

    private Context mContext;
    private List<HeroStatsModel> mHeroStatsModelList;

    public HeroStatsSummaryAdapter(Context mContext, List<HeroStatsModel> mHeroStatsModelList) {
        this.mContext = mContext;
        this.mHeroStatsModelList = mHeroStatsModelList;
    }

    public void setmHeroStatsModelList(List<HeroStatsModel> mHeroStatsModelList) {
        this.mHeroStatsModelList = mHeroStatsModelList;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private View mRootLayout;
        private ImageView mHeroIconImageView;
        private TextView mHeroNameTextView;
        private TextView mHeroPicksPlusBansTextView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mRootLayout = itemView;
            mHeroIconImageView = (ImageView) itemView.findViewById(R.id.player_avatar_image_view);
            mHeroNameTextView = (TextView) itemView.findViewById(R.id.hero_name_text_view);
            mHeroPicksPlusBansTextView = (TextView) itemView.findViewById(R.id.pro_picks_plus_bans_text_view);
        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_view_hero_stats_summary_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position) {
        holder.mHeroNameTextView.setText(mHeroStatsModelList.get(position).getLocalizedName());
        holder.mHeroPicksPlusBansTextView.setText(ApplicationUtils.makePlaceholderIntegerString(mHeroStatsModelList.get(position).getProPickAndBan(), "0"));
        Glide.with(mContext.getApplicationContext())
                .load(ApiUtils.getHeroImageUrl(mHeroStatsModelList.get(position).getName()))
                .centerCrop()
                .into(holder.mHeroIconImageView);
        holder.mHeroIconImageView.setClipToOutline(true);
        holder.mRootLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                HeroStatsDialogFragment newFragment = HeroStatsDialogFragment.getInstance(mHeroStatsModelList.get(position));
                newFragment.show(((AppCompatActivity)mContext).getFragmentManager(), "dialog");
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mHeroStatsModelList != null) {
            return mHeroStatsModelList.size();
        } else {
            return 0;
        }
    }
}
