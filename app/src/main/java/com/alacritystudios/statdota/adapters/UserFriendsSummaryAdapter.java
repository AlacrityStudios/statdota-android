package com.alacritystudios.statdota.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.models.UserFriendsModel;
import com.alacritystudios.statdota.utils.ApplicationUtils;
import com.bumptech.glide.Glide;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.IMarker;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * An adapter to show the friends related stats of a user.
 */

public class UserFriendsSummaryAdapter extends RecyclerView.Adapter<UserFriendsSummaryAdapter.ItemViewHolder>{

    private List<UserFriendsModel> mUserFriendsModelList;
    private Context mContext;

    public UserFriendsSummaryAdapter(List<UserFriendsModel> mUserFriendsModelList, Context mContext) {
        this.mUserFriendsModelList = mUserFriendsModelList;
        this.mContext = mContext;
    }

    public void setmUserFriendsModelList(List<UserFriendsModel> mUserFriendsModelList) {
        this.mUserFriendsModelList = mUserFriendsModelList;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private View mRootLayout;
        private ImageView mFriendAvatarImageView;
        private TextView mLastPlayedTextView;
        private TextView mSteamFriendDisplayNameTextView;
        private TextView mGamesPlayedTextView;
        private TextView mGamesWonTextView;
        private PieChart mWinPercentPieChart;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mRootLayout= itemView;
            mFriendAvatarImageView = (ImageView) itemView.findViewById(R.id.steam_friend_avatar);
            mLastPlayedTextView = (TextView) itemView.findViewById(R.id.steam_friend_last_played);
            mGamesPlayedTextView = (TextView) itemView.findViewById(R.id.games_played_text_view);
            mGamesWonTextView = (TextView) itemView.findViewById(R.id.games_won_text_view);
            mWinPercentPieChart = (PieChart) itemView.findViewById(R.id.win_percent_pie_chart);
            mSteamFriendDisplayNameTextView = (TextView) itemView.findViewById(R.id.steam_friend_display_name);
        }
    }

    @Override
    public UserFriendsSummaryAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UserFriendsSummaryAdapter.ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_view_user_friends_summary_item, parent, false));
    }

    @Override
    public void onBindViewHolder(UserFriendsSummaryAdapter.ItemViewHolder holder, final int position) {
        int winratePercent = 0;
        List<PieEntry> winrateEntries = new ArrayList<>();
        Description description = new Description();
        description.setText("");
        if (mUserFriendsModelList.get(position).getWithGames() != null && mUserFriendsModelList.get(position).getWithGames() != null) {
            if (mUserFriendsModelList.get(position).getWithGames() == 0) {
                winrateEntries.add(new PieEntry(10, mContext.getString(R.string.win)));
                winrateEntries.add(new PieEntry(10, mContext.getString(R.string.loss)));
                winratePercent = 0;
                PieDataSet winRateSet = new PieDataSet(winrateEntries, "");
                winRateSet.setColors(new int[]{R.color.colorDefaultLight, R.color.colorDefaultLight}, mContext);
                PieData winRateData = new PieData(winRateSet);
                winRateData.setDrawValues(false);
                holder.mWinPercentPieChart.setData(winRateData);

            } else {
                winrateEntries.add(new PieEntry(mUserFriendsModelList.get(position).getWithWin(), mContext.getString(R.string.win)));
                winrateEntries.add(new PieEntry(mUserFriendsModelList.get(position).getWithGames() - mUserFriendsModelList.get(position).getWithWin(), mContext.getString(R.string.loss)));
                winratePercent = (mUserFriendsModelList.get(position).getWithWin() * 100) / mUserFriendsModelList.get(position).getWithGames();
                PieDataSet winRateSet = new PieDataSet(winrateEntries, "");
                winRateSet.setColors(new int[]{R.color.colorChartFill, R.color.colorDefaultLight}, mContext);
                PieData winRateData = new PieData(winRateSet);
                winRateData.setDrawValues(false);
                holder.mWinPercentPieChart.setData(winRateData);
            }
        } else {
            winrateEntries.add(new PieEntry(10.0f, mContext.getString(R.string.win)));
            winrateEntries.add(new PieEntry(10.0f, mContext.getString(R.string.loss)));
            winratePercent = 0;
            PieDataSet winRateSet = new PieDataSet(winrateEntries, "");
            winRateSet.setColors(new int[]{R.color.colorDefaultLight, R.color.colorDefaultLight}, mContext);
            PieData winRateData = new PieData(winRateSet);
            winRateData.setDrawValues(false);
            holder.mWinPercentPieChart.setData(winRateData);
        }
        holder.mWinPercentPieChart.setBackgroundColor(ActivityCompat.getColor(mContext, R.color.colorTransparent));
        holder.mWinPercentPieChart.setHoleColor(ActivityCompat.getColor(mContext, R.color.colorTransparent));
        holder.mWinPercentPieChart.setTransparentCircleAlpha(0);
        holder.mWinPercentPieChart.setHoleRadius(70f);
        holder.mWinPercentPieChart.setRotationAngle(270f);
        holder.mWinPercentPieChart.setDrawEntryLabels(false);
        holder.mWinPercentPieChart.setDescription(description);
        holder.mWinPercentPieChart.setNoDataText("");
        holder.mWinPercentPieChart.getLegend().setEnabled(false);
        holder.mWinPercentPieChart.setCenterText(winratePercent + "%");
        holder.mWinPercentPieChart.setCenterTextColor(ActivityCompat.getColor(mContext, R.color.colorBlackText));
        holder.mWinPercentPieChart.setCenterTextSize(10);
        holder.mWinPercentPieChart.disableScroll();
        holder.mWinPercentPieChart.setTouchEnabled(false);
        holder.mWinPercentPieChart.animateXY(1000, 1000);

        holder.mGamesPlayedTextView.setText(ApplicationUtils.makePlaceholderIntegerString(mUserFriendsModelList.get(position).getWithGames(), "-"));
        holder.mGamesWonTextView.setText(ApplicationUtils.makePlaceholderIntegerString(mUserFriendsModelList.get(position).getWithWin(), "-"));
        holder.mSteamFriendDisplayNameTextView.setText(mUserFriendsModelList.get(position).getPersonaname());
        holder.mLastPlayedTextView.setText(new PrettyTime().format(new Date(System.currentTimeMillis() - (mUserFriendsModelList.get(position).getLastPlayed() * 1000))));
        Glide.with((Activity) mContext)
                .load(mUserFriendsModelList.get(position).getAvatar())
                .centerCrop()
                .into(holder.mFriendAvatarImageView);
    }

    @Override
    public int getItemCount() {
        if (mUserFriendsModelList != null) {
            return mUserFriendsModelList.size();
        } else {
            return 0;
        }
    }
}
