package com.alacritystudios.statdota.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.fragments.TeamDetailsDialogFragment;
import com.alacritystudios.statdota.models.TeamsModel;
import com.alacritystudios.statdota.utils.ApiUtils;
import com.alacritystudios.statdota.utils.ApplicationUtils;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Adapter to display teams summary.
 */

public class TeamsSummaryAdapter extends RecyclerView.Adapter<TeamsSummaryAdapter.ItemViewHolder> {

    private Context mContext;
    private List<TeamsModel> mTeamsModelList;
    private TeamsSummaryAdapterListener mTeamsSummaryAdapterListener;

    public TeamsSummaryAdapter(Context mContext, List<TeamsModel> mTeamsModelList, TeamsSummaryAdapterListener mTeamsSummaryAdapterListener) {
        this.mContext = mContext;
        this.mTeamsModelList = mTeamsModelList;
        this.mTeamsSummaryAdapterListener = mTeamsSummaryAdapterListener;
    }

    public void setmTeamsModelList(List<TeamsModel> mTeamsModelList) {
        this.mTeamsModelList = mTeamsModelList;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private View mRootLayout;
        private ImageView mTeamIconImageView;
        private TextView mTeamNameTextView;
        private TextView mTeamTagTextView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mRootLayout = itemView;
            mTeamIconImageView = (ImageView) itemView.findViewById(R.id.team_icon_image_view);
            mTeamNameTextView = (TextView) itemView.findViewById(R.id.team_name_text_view);
            mTeamTagTextView = (TextView) itemView.findViewById(R.id.team_tag_text_view);
        }
    }

    @Override
    public TeamsSummaryAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TeamsSummaryAdapter.ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_view_teams_summary_item, parent, false));
    }

    @Override
    public void onBindViewHolder(TeamsSummaryAdapter.ItemViewHolder holder, final int position) {
        holder.mTeamNameTextView.setText(ApplicationUtils.makePlaceholderString(mTeamsModelList.get(position).getName(), ""));
        holder.mTeamTagTextView.setText(ApplicationUtils.makePlaceholderString(mTeamsModelList.get(position).getTag(), ""));
        Glide.with(mContext.getApplicationContext())
                .load(mTeamsModelList.get(position).getLogoUrl())
                .fitCenter()
                .into(holder.mTeamIconImageView);
        holder.mRootLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTeamsSummaryAdapterListener.onItemTouch(mTeamsModelList.get(position).getTeamId());
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mTeamsModelList != null) {
            return mTeamsModelList.size();
        } else {
            return 0;
        }
    }

    public interface TeamsSummaryAdapterListener {
        void onItemTouch(long teamId);
    }
}