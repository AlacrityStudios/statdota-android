package com.alacritystudios.statdota.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.models.UserHeroSummaryModel;
import com.alacritystudios.statdota.utils.AnimationUtils;
import com.alacritystudios.statdota.utils.ApiUtils;
import com.alacritystudios.statdota.utils.ApplicationUtils;
import com.bumptech.glide.Glide;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter to display a list of heroes.
 */

public class UserHeroSummaryAdapter extends RecyclerView.Adapter<UserHeroSummaryAdapter.ItemViewHolder> {

    private List<UserHeroSummaryModel> mHeroSummaryList;
    private Context mContext;

    public UserHeroSummaryAdapter(List<UserHeroSummaryModel> mHeroSummaryList, Context mContext) {
        this.mHeroSummaryList = mHeroSummaryList;
        this.mContext = mContext;
    }

    public void setmHeroSummaryList(List<UserHeroSummaryModel> mHeroSummaryList) {
        this.mHeroSummaryList = mHeroSummaryList;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private View mRootLayout;
        private ImageView mHeroIconImageView;
        private TextView mHeroNameTextView;
        private LinearLayout mHeroRole1LinearLayout;
        private LinearLayout mHeroRole2LinearLayout;
        private LinearLayout mHeroRole3LinearLayout;
        private TextView mHeroRole1TextView;
        private TextView mHeroRole2TextView;
        private TextView mHeroRole3TextView;
        private TextView mGamesPlayedAsTextView;
        private TextView mGamesPlayedWithTextView;
        private TextView mGamesPlayedAgainstTextView;
        private PieChart mWinRatePieChart;
        private PieChart mWinRateWithPieChart;
        private PieChart mWinRateAgainstPieChart;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mRootLayout = itemView;
            mHeroIconImageView = (ImageView) itemView.findViewById(R.id.hero_image_icon_image_view);
            mHeroNameTextView = (TextView) itemView.findViewById(R.id.hero_name_text_view);
            mHeroRole1LinearLayout = (LinearLayout) itemView.findViewById(R.id.hero_role_1_linear_layout);
            mHeroRole2LinearLayout = (LinearLayout) itemView.findViewById(R.id.hero_role_2_linear_layout);
            mHeroRole3LinearLayout = (LinearLayout) itemView.findViewById(R.id.hero_role_3_linear_layout);
            mHeroRole1TextView = (TextView) itemView.findViewById(R.id.hero_role_1_text_view);
            mHeroRole2TextView = (TextView) itemView.findViewById(R.id.hero_role_2_text_view);
            mHeroRole3TextView = (TextView) itemView.findViewById(R.id.hero_role_3_text_view);
            mGamesPlayedAsTextView = (TextView) itemView.findViewById(R.id.games_played_as_played_text_view);
            mGamesPlayedWithTextView = (TextView) itemView.findViewById(R.id.games_played_with_text_view);
            mGamesPlayedAgainstTextView = (TextView) itemView.findViewById(R.id.games_played_against_text_view);
            mWinRatePieChart = (PieChart) itemView.findViewById(R.id.win_rate_pie_chart);
            mWinRateWithPieChart = (PieChart) itemView.findViewById(R.id.win_rate_with_pie_chart);
            mWinRateAgainstPieChart = (PieChart) itemView.findViewById(R.id.win_rate_against_pie_chart);
        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_view_user_heroes_summary_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        if(mHeroSummaryList.get(position).getHeroDetails() != null) {
            holder.mHeroNameTextView.setText(ApplicationUtils.makePlaceholderString(mHeroSummaryList.get(position).getHeroDetails().getLocalizedName(), "-"));
            try {
                holder.mHeroRole1TextView.setText(ApplicationUtils.makePlaceholderString(mHeroSummaryList.get(position).getHeroDetails().getRoles().get(0), "-"));
                holder.mHeroRole1LinearLayout.setVisibility(View.VISIBLE);
            } catch (Exception ex) {
                holder.mHeroRole1LinearLayout.setVisibility(View.GONE);
            }
            try {
                holder.mHeroRole2TextView.setText(ApplicationUtils.makePlaceholderString(mHeroSummaryList.get(position).getHeroDetails().getRoles().get(1), "-"));
                holder.mHeroRole2LinearLayout.setVisibility(View.VISIBLE);
            } catch (Exception ex) {
                holder.mHeroRole2LinearLayout.setVisibility(View.GONE);
            }
            try {
                holder.mHeroRole3TextView.setText(ApplicationUtils.makePlaceholderString(mHeroSummaryList.get(position).getHeroDetails().getRoles().get(2), "-"));
                holder.mHeroRole3LinearLayout.setVisibility(View.VISIBLE);
            } catch (Exception ex) {
                holder.mHeroRole3LinearLayout.setVisibility(View.GONE);
            }
            Glide.with((Activity) mContext)
                    .load(ApiUtils.getHeroImageUrl(mHeroSummaryList.get(position).getHeroDetails().getName()))
                    .centerCrop()
                    .into(holder.mHeroIconImageView);
        }



        setChartData(holder, position);
    }

    @Override
    public int getItemCount() {
        if (mHeroSummaryList != null) {
            return mHeroSummaryList.size();
        } else {
            return 0;
        }
    }

    /**
     * Sets the chart data to various charts
     */
    public void setChartData(ItemViewHolder holder, int position) {
        int winratePercent = 0;
        int winrateWithPercent = 0;
        int winrateAgainstPercent = 0;
        List<PieEntry> winrateEntries = new ArrayList<>();
        List<PieEntry> winRateWithEntries = new ArrayList<>();
        List<PieEntry> winRateAgainstEntries = new ArrayList<>();
        Description description = new Description();
        description.setText("");
        if (mHeroSummaryList.get(position).getGames() != null && mHeroSummaryList.get(position).getGames() != null) {
            if (mHeroSummaryList.get(position).getGames() == 0) {
                winrateEntries.add(new PieEntry(10, mContext.getString(R.string.win)));
                winrateEntries.add(new PieEntry(10, mContext.getString(R.string.loss)));
                winratePercent = 0;
                holder.mGamesPlayedAsTextView.setText("0/0");
                PieDataSet winRateSet = new PieDataSet(winrateEntries, "");
                winRateSet.setColors(new int[]{R.color.colorDefaultLight, R.color.colorDefaultLight}, mContext);
                PieData winRateData = new PieData(winRateSet);
                winRateData.setDrawValues(false);
                holder.mWinRatePieChart.setData(winRateData);

            } else {
                winrateEntries.add(new PieEntry(mHeroSummaryList.get(position).getWin(), mContext.getString(R.string.win)));
                winrateEntries.add(new PieEntry(mHeroSummaryList.get(position).getGames() - mHeroSummaryList.get(position).getWin(), mContext.getString(R.string.loss)));
                winratePercent = (mHeroSummaryList.get(position).getWin() * 100) / mHeroSummaryList.get(position).getGames();
                holder.mGamesPlayedAsTextView.setText(String.valueOf(mHeroSummaryList.get(position).getWin()) + "/" + String.valueOf(mHeroSummaryList.get(position).getGames()));
                PieDataSet winRateSet = new PieDataSet(winrateEntries, "");
                winRateSet.setColors(new int[]{R.color.colorChartFill, R.color.colorDefaultLight}, mContext);
                PieData winRateData = new PieData(winRateSet);
                winRateData.setDrawValues(false);
                holder.mWinRatePieChart.setData(winRateData);
            }
        } else {
            winrateEntries.add(new PieEntry(10.0f, mContext.getString(R.string.win)));
            winrateEntries.add(new PieEntry(10.0f, mContext.getString(R.string.loss)));
            winratePercent = 0;
            holder.mGamesPlayedAsTextView.setText("0/0");
            PieDataSet winRateSet = new PieDataSet(winrateEntries, "");
            winRateSet.setColors(new int[]{R.color.colorDefaultLight, R.color.colorDefaultLight}, mContext);
            PieData winRateData = new PieData(winRateSet);
            winRateData.setDrawValues(false);
            holder.mWinRatePieChart.setData(winRateData);
        }
        holder.mWinRatePieChart.setBackgroundColor(ActivityCompat.getColor(mContext, R.color.colorTransparent));
        holder.mWinRatePieChart.setHoleColor(ActivityCompat.getColor(mContext, R.color.colorTransparent));
        holder.mWinRatePieChart.setTransparentCircleAlpha(0);
        holder.mWinRatePieChart.setHoleRadius(70f);
        holder.mWinRatePieChart.setRotationAngle(180f);
        holder.mWinRatePieChart.setMaxAngle(180f);
        holder.mWinRatePieChart.setDrawEntryLabels(false);
        holder.mWinRatePieChart.setDescription(description);
        holder.mWinRatePieChart.setNoDataText("");
        holder.mWinRatePieChart.getLegend().setEnabled(false);
        holder.mWinRatePieChart.setCenterText(winratePercent + "%");
        holder.mWinRatePieChart.setCenterTextColor(ActivityCompat.getColor(mContext, R.color.colorBlackText));
        holder.mWinRatePieChart.setCenterTextSize(10);
        holder.mWinRatePieChart.setTouchEnabled(false);
        holder.mWinRatePieChart.animateXY(1000, 1000);

        if (mHeroSummaryList.get(position).getWithGames() != null && mHeroSummaryList.get(position).getWithWin() != null) {
            if (mHeroSummaryList.get(position).getWithGames() == 0) {
                winRateWithEntries.add(new PieEntry(10, mContext.getString(R.string.win)));
                winRateWithEntries.add(new PieEntry(10, mContext.getString(R.string.loss)));
                winrateWithPercent = 0;
                holder.mGamesPlayedWithTextView.setText("0/0");
                PieDataSet winRateWithSet = new PieDataSet(winRateWithEntries, "");
                winRateWithSet.setColors(new int[]{R.color.colorDefaultLight, R.color.colorDefaultLight}, mContext);
                PieData winRateWithData = new PieData(winRateWithSet);
                winRateWithData.setDrawValues(false);
                holder.mWinRateWithPieChart.setData(winRateWithData);
            } else {
                winRateWithEntries.add(new PieEntry(mHeroSummaryList.get(position).getWithWin(), mContext.getString(R.string.win)));
                winRateWithEntries.add(new PieEntry(mHeroSummaryList.get(position).getWithGames() - mHeroSummaryList.get(position).getWithWin(), mContext.getString(R.string.loss)));
                winrateWithPercent = (mHeroSummaryList.get(position).getWithWin() * 100) / mHeroSummaryList.get(position).getWithGames();
                holder.mGamesPlayedWithTextView.setText(String.valueOf(mHeroSummaryList.get(position).getWithWin()) + "/" + String.valueOf(mHeroSummaryList.get(position).getWithGames()));
                PieDataSet winRateWithSet = new PieDataSet(winRateWithEntries, "");
                winRateWithSet.setColors(new int[]{R.color.colorChartFill, R.color.colorDefaultLight}, mContext);
                PieData winRateWithData = new PieData(winRateWithSet);
                winRateWithData.setDrawValues(false);
                holder.mWinRateWithPieChart.setData(winRateWithData);
            }
        } else {
            winRateWithEntries.add(new PieEntry(0, mContext.getString(R.string.win)));
            winRateWithEntries.add(new PieEntry(10, mContext.getString(R.string.loss)));
            winrateWithPercent = 0;
            holder.mGamesPlayedWithTextView.setText("0/0");
            PieDataSet winRateWithSet = new PieDataSet(winRateWithEntries, "");
            winRateWithSet.setColors(new int[]{R.color.colorDefaultMedium, R.color.colorDefaultLight}, mContext);
            PieData winRateWithData = new PieData(winRateWithSet);
            winRateWithData.setDrawValues(false);
            holder.mWinRateWithPieChart.setData(winRateWithData);
        }
        holder.mWinRateWithPieChart.setBackgroundColor(ActivityCompat.getColor(mContext, R.color.colorTransparent));
        holder.mWinRateWithPieChart.setHoleColor(ActivityCompat.getColor(mContext, R.color.colorTransparent));
        holder.mWinRateWithPieChart.setTransparentCircleAlpha(0);
        holder.mWinRateWithPieChart.setHoleRadius(70f);
        holder.mWinRateWithPieChart.setRotationAngle(180f);
        holder.mWinRateWithPieChart.setMaxAngle(180f);
        holder.mWinRateWithPieChart.setDrawEntryLabels(false);
        holder.mWinRateWithPieChart.setDescription(description);
        holder.mWinRateWithPieChart.setNoDataText("");
        holder.mWinRateWithPieChart.getLegend().setEnabled(false);
        holder.mWinRateWithPieChart.setCenterText(winrateWithPercent + "%");
        holder.mWinRateWithPieChart.setCenterTextColor(ActivityCompat.getColor(mContext, R.color.colorBlackText));
        holder.mWinRateWithPieChart.setCenterTextSize(10);
        holder.mWinRateWithPieChart.setTouchEnabled(false);
        holder.mWinRateWithPieChart.animateXY(1000, 1000);

        if (mHeroSummaryList.get(position).getAgainstGames() != null && mHeroSummaryList.get(position).getAgainstWin() != null) {
            if (mHeroSummaryList.get(position).getAgainstGames() == 0) {
                winRateAgainstEntries.add(new PieEntry(10, mContext.getString(R.string.win)));
                winRateAgainstEntries.add(new PieEntry(10, mContext.getString(R.string.loss)));
                winrateAgainstPercent = 0;
                holder.mGamesPlayedAgainstTextView.setText("0/0");
                PieDataSet winRateAgainstSet = new PieDataSet(winRateAgainstEntries, "");
                winRateAgainstSet.setColors(new int[]{R.color.colorDefaultLight, R.color.colorDefaultLight}, mContext);
                PieData winRateAgainstData = new PieData(winRateAgainstSet);
                winRateAgainstData.setDrawValues(false);
                holder.mWinRateAgainstPieChart.setData(winRateAgainstData);
            } else {
                winRateAgainstEntries.add(new PieEntry(mHeroSummaryList.get(position).getAgainstWin(), mContext.getString(R.string.win)));
                winRateAgainstEntries.add(new PieEntry(mHeroSummaryList.get(position).getAgainstGames() - mHeroSummaryList.get(position).getAgainstWin(), mContext.getString(R.string.loss)));
                winrateAgainstPercent = (mHeroSummaryList.get(position).getAgainstWin() * 100) / mHeroSummaryList.get(position).getAgainstGames();
                holder.mGamesPlayedAgainstTextView.setText(String.valueOf(mHeroSummaryList.get(position).getAgainstWin()) + "/" + String.valueOf(mHeroSummaryList.get(position).getAgainstGames()));
                PieDataSet winRateAgainstSet = new PieDataSet(winRateAgainstEntries, "");
                winRateAgainstSet.setColors(new int[]{R.color.colorChartFill, R.color.colorDefaultLight}, mContext);
                PieData winRateAgainstData = new PieData(winRateAgainstSet);
                winRateAgainstData.setDrawValues(false);
                holder.mWinRateAgainstPieChart.setData(winRateAgainstData);
            }
        } else {
            winRateAgainstEntries.add(new PieEntry(10.0f, mContext.getString(R.string.win)));
            winRateAgainstEntries.add(new PieEntry(0.0f, mContext.getString(R.string.loss)));
            winrateAgainstPercent = 0;
            holder.mGamesPlayedAgainstTextView.setText("0/0");
            PieDataSet winRateAgainstSet = new PieDataSet(winRateAgainstEntries, "");
            winRateAgainstSet.setColors(new int[]{R.color.colorDefaultLight, R.color.colorDefaultLight}, mContext);
            PieData winRateAgainstData = new PieData(winRateAgainstSet);
            winRateAgainstData.setDrawValues(false);
            holder.mWinRateAgainstPieChart.setData(winRateAgainstData);
        }
        holder.mWinRateAgainstPieChart.setBackgroundColor(ActivityCompat.getColor(mContext, R.color.colorTransparent));
        holder.mWinRateAgainstPieChart.setHoleColor(ActivityCompat.getColor(mContext, R.color.colorTransparent));
        holder.mWinRateAgainstPieChart.setTransparentCircleAlpha(0);
        holder.mWinRateAgainstPieChart.setHoleRadius(70f);
        holder.mWinRateAgainstPieChart.setRotationAngle(180f);
        holder.mWinRateAgainstPieChart.setMaxAngle(180f);
        holder.mWinRateAgainstPieChart.setDrawEntryLabels(false);
        holder.mWinRateAgainstPieChart.setDescription(description);
        holder.mWinRateAgainstPieChart.setNoDataText("");
        holder.mWinRateAgainstPieChart.getLegend().setEnabled(false);
        holder.mWinRateAgainstPieChart.setCenterText(winrateAgainstPercent + "%");
        holder.mWinRateAgainstPieChart.setCenterTextColor(ActivityCompat.getColor(mContext, R.color.colorBlackText));
        holder.mWinRateAgainstPieChart.setCenterTextSize(10);
        holder.mWinRateAgainstPieChart.setTouchEnabled(false);
        holder.mWinRateAgainstPieChart.animateXY(1000, 1000);
    }
}
