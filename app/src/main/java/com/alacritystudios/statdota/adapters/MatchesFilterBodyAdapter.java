package com.alacritystudios.statdota.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.enums.FilterType;
import com.alacritystudios.statdota.models.MatchesFilterBodyModel;

import java.util.List;

/**
 * Adapter to display all the suggestions for a filter.
 */

public class MatchesFilterBodyAdapter extends RecyclerView.Adapter<MatchesFilterBodyAdapter.ItemViewHolder> {

    private Context mContext;
    private List<MatchesFilterBodyModel> mMatchesFilterBodyModelOriginalList;
    private boolean isSingleUserChoice;
    private FilterType mFilterType;
    private MatchesFilterBodyAdapterInterface mMatchesFilterBodyAdapterInterface;

    public MatchesFilterBodyAdapter(Context mContext, List<MatchesFilterBodyModel> mMatchesFilterBodyModelOriginalList,
                                    boolean isSingleUserChoice, FilterType mFilterType, MatchesFilterBodyAdapterInterface mMatchesFilterBodyAdapterInterface) {
        this.mContext = mContext;
        this.mMatchesFilterBodyModelOriginalList = mMatchesFilterBodyModelOriginalList;
        this.isSingleUserChoice = isSingleUserChoice;
        this.mFilterType = mFilterType;
        this.mMatchesFilterBodyAdapterInterface = mMatchesFilterBodyAdapterInterface;
    }

    public List<MatchesFilterBodyModel> getmMatchesFilterBodyModelOriginalList() {
        return mMatchesFilterBodyModelOriginalList;
    }

    public void setmMatchesFilterBodyModelOriginalList(List<MatchesFilterBodyModel> mMatchesFilterBodyModelOriginalList) {
        this.mMatchesFilterBodyModelOriginalList = mMatchesFilterBodyModelOriginalList;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private TextView mFilterDisplayNameTextView;
        private CheckBox mSelectFilterCheckBox;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mFilterDisplayNameTextView = itemView.findViewById(R.id.filter_display_name_text_view);
            mSelectFilterCheckBox = itemView.findViewById(R.id.filter_selection_state_checkbox);
        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MatchesFilterBodyAdapter.ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_view_filters_body_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position) {
        holder.mFilterDisplayNameTextView.setText(mMatchesFilterBodyModelOriginalList.get(position).getDisplayName());
        holder.mSelectFilterCheckBox.setOnCheckedChangeListener(null);
        holder.mSelectFilterCheckBox.setChecked(mMatchesFilterBodyModelOriginalList.get(position).isSelected());
        holder.mSelectFilterCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            mMatchesFilterBodyAdapterInterface.onItemChecked(position, mMatchesFilterBodyModelOriginalList.get(position).getValue(), mFilterType, isSingleUserChoice, b);

            }
        });
    }

    @Override
    public int getItemCount() {
        if (mMatchesFilterBodyModelOriginalList != null) {
            return mMatchesFilterBodyModelOriginalList.size();
        } else {
            return 0;
        }
    }

    public interface MatchesFilterBodyAdapterInterface {

        void onItemChecked(int position, String value, FilterType filterType, boolean isSingleChoiceFilter, boolean isSelected);
    }
}
