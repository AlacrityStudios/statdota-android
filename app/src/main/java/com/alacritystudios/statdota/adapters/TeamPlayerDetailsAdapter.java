package com.alacritystudios.statdota.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.activities.AccountActivity;
import com.alacritystudios.statdota.constants.BundleConstants;
import com.alacritystudios.statdota.models.ProPlayersModel;
import com.alacritystudios.statdota.utils.ApplicationUtils;
import com.bumptech.glide.Glide;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * An adapter to show team player details.
 */

public class TeamPlayerDetailsAdapter extends RecyclerView.Adapter<TeamPlayerDetailsAdapter.ItemViewHolder> {

    private List<ProPlayersModel> mProPlayersModelList;
    private Context mContext;
    private Drawable mPlaceholderDrawable;

    public TeamPlayerDetailsAdapter(List<ProPlayersModel> mProPlayersModelList, Context mContext, Drawable mPlaceholderDrawable) {
        this.mProPlayersModelList = mProPlayersModelList;
        this.mContext = mContext;
        this.mPlaceholderDrawable = mPlaceholderDrawable;
    }

    public List<ProPlayersModel> getmProPlayersModelList() {
        return mProPlayersModelList;
    }

    public void setmProPlayersModelList(List<ProPlayersModel> mProPlayersModelList) {
        this.mProPlayersModelList = mProPlayersModelList;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private View mRootLayout;
        private CircleImageView mPlayerAvatarImageView;
        private TextView mPlayerNameTextView;
        private TextView mPlayerPersonaNameTextView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mRootLayout = itemView;
            mPlayerAvatarImageView = itemView.findViewById(R.id.player_avatar_image_view);
            mPlayerNameTextView = itemView.findViewById(R.id.player_name_text_view);
            mPlayerPersonaNameTextView = itemView.findViewById(R.id.player_persona_name_text_view);
        }
    }

    @Override
    public TeamPlayerDetailsAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TeamPlayerDetailsAdapter.ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_view_team_players_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final TeamPlayerDetailsAdapter.ItemViewHolder holder, final int position) {
        holder.mPlayerNameTextView.setText(ApplicationUtils.makePlaceholderString(mProPlayersModelList.get(position).getName(), "-"));
        holder.mPlayerPersonaNameTextView.setText(ApplicationUtils.makePlaceholderString(mProPlayersModelList.get(position).getPersonaname(), "-"));
        Glide.with((Activity) mContext)
                .load(mProPlayersModelList.get(position).getAvatarfull())
                .fitCenter()
                .error(mPlaceholderDrawable)
                .into(holder.mPlayerAvatarImageView);
        holder.mPlayerAvatarImageView.setClipToOutline(true);
        holder.mRootLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, AccountActivity.class);
                intent.putExtra(BundleConstants.ACCOUNT_ID, mProPlayersModelList.get(position).getAccountId().longValue());
                mContext.startActivity(intent);
                ((AppCompatActivity) mContext).overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mProPlayersModelList != null) {
            return mProPlayersModelList.size();
        } else {
            return 0;
        }
    }
}