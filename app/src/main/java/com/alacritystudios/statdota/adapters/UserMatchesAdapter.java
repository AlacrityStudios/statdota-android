package com.alacritystudios.statdota.adapters;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.constants.ApplicationConstants;
import com.alacritystudios.statdota.models.UserMatchSummaryModel;
import com.alacritystudios.statdota.utils.ApplicationUtils;
import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Adapter to show a list of matches.
 */

public class UserMatchesAdapter extends RecyclerView.Adapter<UserMatchesAdapter.ItemViewHolder> {

    private List<UserMatchSummaryModel> mUserMatchSummaryModelList;
    private Context mContext;

    public UserMatchesAdapter(List<UserMatchSummaryModel> mUserMatchSummaryModelList, Context mContext) {
        this.mUserMatchSummaryModelList = mUserMatchSummaryModelList;
        this.mContext = mContext;
    }

    public void setmUserMatchSummaryModelList(List<UserMatchSummaryModel> mUserMatchSummaryModelList) {
        this.mUserMatchSummaryModelList = mUserMatchSummaryModelList;
    }

    public List<UserMatchSummaryModel> getmUserMatchSummaryModelList() {
        return mUserMatchSummaryModelList;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private View mRootLayout;
        private TextView mHeroNameTextView;
        private TextView mDurationTextView;
        private TextView mDateTextView;
        private TextView mSideTextView;
        private TextView mRoleTextView;
        private TextView mModeTextView;
        private TextView mResultTextView;
        private TextView mKillsTextView;
        private TextView mDeathsTextView;
        private TextView mAssistsTextView;
        private TextView mNetWorthTextView;
        private CircleImageView mRadiantHero1IconImageView;
        private CircleImageView mRadiantHero2IconImageView;
        private CircleImageView mRadiantHero3IconImageView;
        private CircleImageView mRadiantHero4IconImageView;
        private CircleImageView mRadiantHero5IconImageView;
        private CircleImageView mDireHero1IconImageView;
        private CircleImageView mDireHero2IconImageView;
        private CircleImageView mDireHero3IconImageView;
        private CircleImageView mDireHero4IconImageView;
        private CircleImageView mDireHero5IconImageView;
        private CircleImageView mHeroPortraitImageView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mRootLayout = itemView;
            mHeroNameTextView = (TextView) itemView.findViewById(R.id.hero_name_text_view);
            mDateTextView = (TextView) itemView.findViewById(R.id.match_date_text_view);
            mDurationTextView = (TextView) itemView.findViewById(R.id.match_duration_text_view);
            mSideTextView = (TextView) itemView.findViewById(R.id.side_text_view);
            mRoleTextView = (TextView) itemView.findViewById(R.id.role_text_view);
            mModeTextView = (TextView) itemView.findViewById(R.id.match_mode_text_view);
            mResultTextView = (TextView) itemView.findViewById(R.id.match_result_text_view);
            mKillsTextView = (TextView) itemView.findViewById(R.id.kills_text_view);
            mDeathsTextView = (TextView) itemView.findViewById(R.id.deaths_text_view);
            mAssistsTextView = (TextView) itemView.findViewById(R.id.assists_text_view);
            mNetWorthTextView = (TextView) itemView.findViewById(R.id.net_worth_text_view);
            mHeroPortraitImageView = (CircleImageView) itemView.findViewById(R.id.hero_image_icon_image_view);
            mRadiantHero1IconImageView = (CircleImageView) itemView.findViewById(R.id.radiant_1_hero_icon_image_view);
            mRadiantHero2IconImageView = (CircleImageView) itemView.findViewById(R.id.radiant_2_hero_icon_image_view);
            mRadiantHero3IconImageView = (CircleImageView) itemView.findViewById(R.id.radiant_3_hero_icon_image_view);
            mRadiantHero4IconImageView = (CircleImageView) itemView.findViewById(R.id.radiant_4_hero_icon_image_view);
            mRadiantHero5IconImageView = (CircleImageView) itemView.findViewById(R.id.radiant_5_hero_icon_image_view);
            mDireHero1IconImageView = (CircleImageView) itemView.findViewById(R.id.dire_1_hero_icon_image_view);
            mDireHero2IconImageView = (CircleImageView) itemView.findViewById(R.id.dire_2_hero_icon_image_view);
            mDireHero3IconImageView = (CircleImageView) itemView.findViewById(R.id.dire_3_hero_icon_image_view);
            mDireHero4IconImageView = (CircleImageView) itemView.findViewById(R.id.dire_4_hero_icon_image_view);
            mDireHero5IconImageView = (CircleImageView) itemView.findViewById(R.id.dire_5_hero_icon_image_view);
        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_view_user_matches_summary_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {
        holder.mDurationTextView.setText(DateUtils.formatElapsedTime(mUserMatchSummaryModelList.get(position).getDuration()));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM YYYY");
        holder.mDateTextView.setText(simpleDateFormat.format(new Date(mUserMatchSummaryModelList.get(position).getStartTime() * 1000)));
        if (mUserMatchSummaryModelList.get(position).getGameMode() != null) {
            holder.mModeTextView.setText(ApplicationConstants.gameModeArray[mUserMatchSummaryModelList.get(position).getGameMode()]);
        } else {
            holder.mModeTextView.setText(ApplicationConstants.gameModeArray[0]);
        }

        if (mUserMatchSummaryModelList.get(position).getRadiantWin() && mUserMatchSummaryModelList.get(position).getPlayerSlot() <= 64) {
            holder.mResultTextView.setTextColor(ActivityCompat.getColor(mContext, R.color.colorWin));
            holder.mResultTextView.setText(mContext.getString(R.string.victory));
            holder.mSideTextView.setText(mContext.getString(R.string.radiant));
        } else if (mUserMatchSummaryModelList.get(position).getRadiantWin() && mUserMatchSummaryModelList.get(position).getPlayerSlot() > 64) {
            holder.mResultTextView.setTextColor(ActivityCompat.getColor(mContext, R.color.colorLoss));
            holder.mResultTextView.setText(mContext.getString(R.string.defeat));
            holder.mSideTextView.setText(mContext.getString(R.string.dire));
        } else if (!mUserMatchSummaryModelList.get(position).getRadiantWin() && mUserMatchSummaryModelList.get(position).getPlayerSlot() <= 64) {
            holder.mResultTextView.setTextColor(ActivityCompat.getColor(mContext, R.color.colorLoss));
            holder.mResultTextView.setText(mContext.getString(R.string.defeat));
            holder.mSideTextView.setText(mContext.getString(R.string.radiant));
        } else {
            holder.mResultTextView.setText(mContext.getString(R.string.victory));
            holder.mResultTextView.setTextColor(ActivityCompat.getColor(mContext, R.color.colorWin));
            holder.mSideTextView.setText(mContext.getString(R.string.dire));
        }

        switch (mUserMatchSummaryModelList.get(position).getPlayerSlot()) {
            case 0:
                holder.mSideTextView.setTextColor(ActivityCompat.getColor(mContext, R.color.radiant_slot_1));
                break;
            case 1:
                holder.mSideTextView.setTextColor(ActivityCompat.getColor(mContext, R.color.radiant_slot_2));
                break;
            case 2:
                holder.mSideTextView.setTextColor(ActivityCompat.getColor(mContext, R.color.radiant_slot_3));
                break;
            case 3:
                holder.mSideTextView.setTextColor(ActivityCompat.getColor(mContext, R.color.radiant_slot_4));
                break;
            case 4:
                holder.mSideTextView.setTextColor(ActivityCompat.getColor(mContext, R.color.radiant_slot_5));
                break;
            case 128:
                holder.mSideTextView.setTextColor(ActivityCompat.getColor(mContext, R.color.dire_slot_1));
                break;
            case 129:
                holder.mSideTextView.setTextColor(ActivityCompat.getColor(mContext, R.color.dire_slot_2));
                break;
            case 130:
                holder.mSideTextView.setTextColor(ActivityCompat.getColor(mContext, R.color.dire_slot_3));
                break;
            case 131:
                holder.mSideTextView.setTextColor(ActivityCompat.getColor(mContext, R.color.dire_slot_4));
                break;
            case 132:
                holder.mSideTextView.setTextColor(ActivityCompat.getColor(mContext, R.color.dire_slot_5));
                break;

        }

        //TODO : NULL CHECKS
        holder.mRoleTextView.setText(mUserMatchSummaryModelList.get(position).getLaneRole() != null ? ApplicationConstants.laneRoleArray[mUserMatchSummaryModelList.get(position).getLaneRole()] : ApplicationConstants.laneRoleArray[0]);
        holder.mNetWorthTextView.setText(String.valueOf(mUserMatchSummaryModelList.get(position).getDuration() * (mUserMatchSummaryModelList.get(position).getGoldPerMin() / 60)));
        holder.mKillsTextView.setText(ApplicationUtils.makePlaceholderIntegerString(mUserMatchSummaryModelList.get(position).getKills(), "0"));
        holder.mDeathsTextView.setText(ApplicationUtils.makePlaceholderIntegerString(mUserMatchSummaryModelList.get(position).getDeaths(), "0"));
        holder.mAssistsTextView.setText(ApplicationUtils.makePlaceholderIntegerString(mUserMatchSummaryModelList.get(position).getAssists(), "0"));

        if (mUserMatchSummaryModelList.get(position).getPlayerSlots() != null) {
            for (UserMatchSummaryModel.PlayerSlot playerSlot : mUserMatchSummaryModelList.get(position).getPlayerSlots()) {
                switch (playerSlot.getPlayerSlot()) {
                    case 0:
                        holder.mRadiantHero1IconImageView.setBorderWidth(4);
                        holder.mRadiantHero1IconImageView.setBorderColor(ActivityCompat.getColor(mContext, R.color.radiant_slot_1));
                        Glide.with(mContext.getApplicationContext())
                                .load(playerSlot.getHeroImageUrl())
                                .centerCrop()
                                .into(holder.mRadiantHero1IconImageView);
                        if (mUserMatchSummaryModelList.get(position).getPlayerSlot() == 0) {
                            Glide.with(mContext.getApplicationContext())
                                    .load(playerSlot.getHeroImageUrl())
                                    .centerCrop()
                                    .into(holder.mHeroPortraitImageView);
                            holder.mHeroNameTextView.setText(playerSlot.getHeroLocalisedName());
                        }
                        break;
                    case 1:
                        holder.mRadiantHero2IconImageView.setBorderWidth(4);
                        holder.mRadiantHero2IconImageView.setBorderColor(ActivityCompat.getColor(mContext, R.color.radiant_slot_2));
                        Glide.with(mContext.getApplicationContext())
                                .load(playerSlot.getHeroImageUrl())
                                .centerCrop()
                                .into(holder.mRadiantHero2IconImageView);
                        if (mUserMatchSummaryModelList.get(position).getPlayerSlot() == 1) {
                            Glide.with(mContext.getApplicationContext())
                                    .load(playerSlot.getHeroImageUrl())
                                    .centerCrop()
                                    .into(holder.mHeroPortraitImageView);
                            holder.mHeroNameTextView.setText(playerSlot.getHeroLocalisedName());
                        }
                        break;
                    case 2:
                        holder.mRadiantHero3IconImageView.setBorderWidth(4);
                        holder.mRadiantHero3IconImageView.setBorderColor(ActivityCompat.getColor(mContext, R.color.radiant_slot_3));
                        Glide.with(mContext.getApplicationContext())
                                .load(playerSlot.getHeroImageUrl())
                                .centerCrop()
                                .into(holder.mRadiantHero3IconImageView);
                        if (mUserMatchSummaryModelList.get(position).getPlayerSlot() == 2) {
                            Glide.with(mContext.getApplicationContext())
                                    .load(playerSlot.getHeroImageUrl())
                                    .centerCrop()
                                    .into(holder.mHeroPortraitImageView);
                            holder.mHeroNameTextView.setText(playerSlot.getHeroLocalisedName());
                        }
                        break;
                    case 3:
                        holder.mRadiantHero4IconImageView.setBorderWidth(4);
                        holder.mRadiantHero4IconImageView.setBorderColor(ActivityCompat.getColor(mContext, R.color.radiant_slot_4));
                        Glide.with(mContext.getApplicationContext())
                                .load(playerSlot.getHeroImageUrl())
                                .centerCrop()
                                .into(holder.mRadiantHero4IconImageView);
                        if (mUserMatchSummaryModelList.get(position).getPlayerSlot() == 3) {
                            Glide.with(mContext.getApplicationContext())
                                    .load(playerSlot.getHeroImageUrl())
                                    .centerCrop()
                                    .into(holder.mHeroPortraitImageView);
                            holder.mHeroNameTextView.setText(playerSlot.getHeroLocalisedName());
                        }
                        break;
                    case 4:
                        holder.mRadiantHero5IconImageView.setBorderWidth(4);
                        holder.mRadiantHero5IconImageView.setBorderColor(ActivityCompat.getColor(mContext, R.color.radiant_slot_5));
                        Glide.with(mContext.getApplicationContext())
                                .load(playerSlot.getHeroImageUrl())
                                .centerCrop()
                                .into(holder.mRadiantHero5IconImageView);
                        if (mUserMatchSummaryModelList.get(position).getPlayerSlot() == 4) {
                            Glide.with(mContext.getApplicationContext())
                                    .load(playerSlot.getHeroImageUrl())
                                    .centerCrop()
                                    .into(holder.mHeroPortraitImageView);
                            holder.mHeroNameTextView.setText(playerSlot.getHeroLocalisedName());
                        }
                        break;
                    case 128:
                        holder.mDireHero1IconImageView.setBorderWidth(4);
                        holder.mDireHero1IconImageView.setBorderColor(ActivityCompat.getColor(mContext, R.color.dire_slot_1));
                        Glide.with(mContext.getApplicationContext())
                                .load(playerSlot.getHeroImageUrl())
                                .centerCrop()
                                .into(holder.mDireHero1IconImageView);
                        if (mUserMatchSummaryModelList.get(position).getPlayerSlot() == 128) {
                            Glide.with(mContext.getApplicationContext())
                                    .load(playerSlot.getHeroImageUrl())
                                    .centerCrop()
                                    .into(holder.mHeroPortraitImageView);
                            holder.mHeroNameTextView.setText(playerSlot.getHeroLocalisedName());
                        }
                        break;
                    case 129:
                        holder.mDireHero2IconImageView.setBorderWidth(4);
                        holder.mDireHero2IconImageView.setBorderColor(ActivityCompat.getColor(mContext, R.color.dire_slot_2));
                        Glide.with(mContext.getApplicationContext())
                                .load(playerSlot.getHeroImageUrl())
                                .centerCrop()
                                .into(holder.mDireHero2IconImageView);
                        if (mUserMatchSummaryModelList.get(position).getPlayerSlot() == 129) {
                            Glide.with(mContext.getApplicationContext())
                                    .load(playerSlot.getHeroImageUrl())
                                    .centerCrop()
                                    .into(holder.mHeroPortraitImageView);
                            holder.mHeroNameTextView.setText(playerSlot.getHeroLocalisedName());
                        }
                        break;
                    case 130:
                        holder.mDireHero3IconImageView.setBorderWidth(4);
                        holder.mDireHero3IconImageView.setBorderColor(ActivityCompat.getColor(mContext, R.color.dire_slot_3));
                        Glide.with(mContext.getApplicationContext())
                                .load(playerSlot.getHeroImageUrl())
                                .centerCrop()
                                .into(holder.mDireHero3IconImageView);
                        if (mUserMatchSummaryModelList.get(position).getPlayerSlot() == 130) {
                            Glide.with(mContext.getApplicationContext())
                                    .load(playerSlot.getHeroImageUrl())
                                    .centerCrop()
                                    .into(holder.mHeroPortraitImageView);
                            holder.mHeroNameTextView.setText(playerSlot.getHeroLocalisedName());
                        }
                        break;
                    case 131:
                        holder.mDireHero4IconImageView.setBorderWidth(4);
                        holder.mDireHero4IconImageView.setBorderColor(ActivityCompat.getColor(mContext, R.color.dire_slot_4));
                        Glide.with(mContext.getApplicationContext())
                                .load(playerSlot.getHeroImageUrl())
                                .centerCrop()
                                .into(holder.mDireHero4IconImageView);
                        if (mUserMatchSummaryModelList.get(position).getPlayerSlot() == 131) {
                            Glide.with(mContext.getApplicationContext())
                                    .load(playerSlot.getHeroImageUrl())
                                    .centerCrop()
                                    .into(holder.mHeroPortraitImageView);
                            holder.mHeroNameTextView.setText(playerSlot.getHeroLocalisedName());
                        }
                        break;
                    case 132:
                        holder.mDireHero5IconImageView.setBorderWidth(4);
                        holder.mDireHero5IconImageView.setBorderColor(ActivityCompat.getColor(mContext, R.color.dire_slot_5));
                        Glide.with(mContext.getApplicationContext())
                                .load(playerSlot.getHeroImageUrl())
                                .centerCrop()
                                .into(holder.mDireHero5IconImageView);
                        if (mUserMatchSummaryModelList.get(position).getPlayerSlot() == 132) {
                            Glide.with(mContext.getApplicationContext())
                                    .load(playerSlot.getHeroImageUrl())
                                    .centerCrop()
                                    .into(holder.mHeroPortraitImageView);
                            holder.mHeroNameTextView.setText(playerSlot.getHeroLocalisedName());
                        }
                        break;
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        if (mUserMatchSummaryModelList != null) {
            return mUserMatchSummaryModelList.size();
        } else {
            return 0;
        }
    }
}
