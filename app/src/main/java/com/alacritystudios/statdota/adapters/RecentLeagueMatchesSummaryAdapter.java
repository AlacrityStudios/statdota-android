package com.alacritystudios.statdota.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.models.RecentLeagueMatchModel;
import com.alacritystudios.statdota.utils.ApplicationUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.Date;
import java.util.List;

/**
 * Adapter to display recent league matches summary.
 */

public class RecentLeagueMatchesSummaryAdapter extends RecyclerView.Adapter<RecentLeagueMatchesSummaryAdapter.ItemViewHolder> {

    private Context mContext;
    private List<RecentLeagueMatchModel> mRecentLeagueMatchModelList;
    private Drawable mPlaceholderDrawable;

    public RecentLeagueMatchesSummaryAdapter(Context mContext, List<RecentLeagueMatchModel> mRecentLeagueMatchModelList, Drawable mPlaceholderDrawable) {
        this.mContext = mContext;
        this.mRecentLeagueMatchModelList = mRecentLeagueMatchModelList;
        this.mPlaceholderDrawable = mPlaceholderDrawable;
    }

    public void setmRecentLeagueMatchModelList(List<RecentLeagueMatchModel> mRecentLeagueMatchModelList) {
        this.mRecentLeagueMatchModelList = mRecentLeagueMatchModelList;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private View mRootLayout;
        private TextView mLeagueNameTextView;
        private TextView mElapsedTimeTextView;
        private TextView mRadiantTeamTagTextView;
        private TextView mDireTeamTagTextView;
        private ImageView mRadiantTeamLogoImageView;
        private ImageView mDireTeamLogoImageView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mRootLayout = itemView;
            mRadiantTeamLogoImageView = itemView.findViewById(R.id.radiant_team_logo_image_view);
            mDireTeamLogoImageView = itemView.findViewById(R.id.dire_team_logo_image_view);
            mRadiantTeamTagTextView = itemView.findViewById(R.id.radiant_team_tag_text_view);
            mDireTeamTagTextView = itemView.findViewById(R.id.dire_team_tag_text_view);
            mLeagueNameTextView = itemView.findViewById(R.id.league_name_text_view);
            mElapsedTimeTextView = itemView.findViewById(R.id.elapsed_time_text_view);
        }
    }

    @Override
    public RecentLeagueMatchesSummaryAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecentLeagueMatchesSummaryAdapter.ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_view_recent_league_matches_summary_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecentLeagueMatchesSummaryAdapter.ItemViewHolder holder, int position) {
        holder.mLeagueNameTextView.setText(ApplicationUtils.makePlaceholderString(mRecentLeagueMatchModelList.get(position).getLeagueName(), "-"));
        if (mRecentLeagueMatchModelList.get(position).getStartTime() != null) {
            holder.mElapsedTimeTextView.setText(new PrettyTime().format(new Date(mRecentLeagueMatchModelList.get(position).getStartTime() * 1000)));
        }

        Glide.with((Activity) mContext)
                .load(mRecentLeagueMatchModelList.get(position).getRadiantTeamLogo())
                .fitCenter()
                .error(mPlaceholderDrawable)
                .into(holder.mRadiantTeamLogoImageView);
        holder.mRadiantTeamLogoImageView.setClipToOutline(true);
        holder.mRadiantTeamTagTextView.setText(ApplicationUtils.makePlaceholderString(mRecentLeagueMatchModelList.get(position).getRadiantTeamTag(), "-"));

        Glide.with((Activity) mContext)
                .load(mRecentLeagueMatchModelList.get(position).getDireTeamLogo())
                .fitCenter()
                .error(mPlaceholderDrawable)
                .into(holder.mDireTeamLogoImageView);
        holder.mDireTeamLogoImageView.setClipToOutline(true);
        holder.mDireTeamTagTextView.setText(ApplicationUtils.makePlaceholderString(mRecentLeagueMatchModelList.get(position).getDireTeamTag(), "-"));
    }

    @Override
    public int getItemCount() {
        if (mRecentLeagueMatchModelList != null) {
            return mRecentLeagueMatchModelList.size();
        } else {
            return 0;
        }
    }
}
