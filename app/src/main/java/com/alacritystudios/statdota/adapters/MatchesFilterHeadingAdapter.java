package com.alacritystudios.statdota.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.enums.FilterType;
import com.alacritystudios.statdota.models.MatchFilterHeadingModel;

import java.util.List;

/**
 * Adapter to display list of available filters.
 */

public class MatchesFilterHeadingAdapter extends RecyclerView.Adapter<MatchesFilterHeadingAdapter.ItemViewHolder> {

    private Context mContext;
    private List<MatchFilterHeadingModel> mMatchFilterHeadingModelList;
    private MatchesFilterHeadingAdapterListener mMatchesFilterHeadingAdapterListener;

    public MatchesFilterHeadingAdapter(Context mContext, List<MatchFilterHeadingModel> mMatchFilterHeadingModelList, MatchesFilterHeadingAdapterListener mMatchesFilterHeadingAdapterListener) {
        this.mContext = mContext;
        this.mMatchFilterHeadingModelList = mMatchFilterHeadingModelList;
        this.mMatchesFilterHeadingAdapterListener = mMatchesFilterHeadingAdapterListener;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private View mRootLayout;
        private TextView mFilterNameTextView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mRootLayout = itemView;
            mFilterNameTextView = itemView.findViewById(R.id.filter_name_text_view);
        }
    }


    @Override
    public MatchesFilterHeadingAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MatchesFilterHeadingAdapter.ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_view_filters_headings_item, parent, false));
    }

    @Override
    public void onBindViewHolder(MatchesFilterHeadingAdapter.ItemViewHolder holder, final int position) {
        holder.mFilterNameTextView.setText(mMatchFilterHeadingModelList.get(position).getFilterGeneralName());
        holder.mFilterNameTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMatchesFilterHeadingAdapterListener.onItemClick(mMatchFilterHeadingModelList.get(position).getFilterType());
            }
        });
        if(mMatchFilterHeadingModelList.get(position).isHasUserChoices()) {
            holder.mFilterNameTextView.setTypeface(Typeface.DEFAULT_BOLD);
        } else {
            holder.mFilterNameTextView.setTypeface(Typeface.DEFAULT);
        }
    }

    @Override
    public int getItemCount() {
        if (mMatchFilterHeadingModelList != null) {
            return mMatchFilterHeadingModelList.size();
        } else {
            return 0;
        }
    }

    public interface MatchesFilterHeadingAdapterListener {
        void onItemClick(FilterType filterType);
    }
}
