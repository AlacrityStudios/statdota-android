package com.alacritystudios.statdota.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.activities.AccountActivity;
import com.alacritystudios.statdota.constants.BundleConstants;
import com.alacritystudios.statdota.fragments.HeroStatsDialogFragment;
import com.alacritystudios.statdota.models.HighMmrPubStarsModel;
import com.alacritystudios.statdota.utils.ApplicationUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

/**
 * Adapter to display a list of high mmr pub stars.
 */

public class HighMmrPubStarsListAdapter extends RecyclerView.Adapter<HighMmrPubStarsListAdapter.ItemViewHolder> {

    private List<HighMmrPubStarsModel> mHighMmrPubStarsModelList;
    private Context mContext;
    private Drawable mPlaceholderDrawable;

    public HighMmrPubStarsListAdapter(List<HighMmrPubStarsModel> mHighMmrPubStarsModelList, Context mContext, Drawable mPlaceholderDrawable) {
        this.mHighMmrPubStarsModelList = mHighMmrPubStarsModelList;
        this.mContext = mContext;
        this.mPlaceholderDrawable = mPlaceholderDrawable;
    }

    public List<HighMmrPubStarsModel> getmHighMmrPubStarsModelList() {
        return mHighMmrPubStarsModelList;
    }

    public void setmHighMmrPubStarsModelList(List<HighMmrPubStarsModel> mHighMmrPubStarsModelList) {
        this.mHighMmrPubStarsModelList = mHighMmrPubStarsModelList;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private View mRootLayout;
        private ImageView mPlayerAvatarImageView;
        private TextView mPlayerPersonaNameTextView;
        private TextView mPlayerMmrTextView;
        private ImageView mPlayerCountryFlagImageView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mRootLayout = itemView;
            mPlayerAvatarImageView = itemView.findViewById(R.id.player_avatar_image_view);
            mPlayerPersonaNameTextView = itemView.findViewById(R.id.hero_name_text_view);
            mPlayerMmrTextView = itemView.findViewById(R.id.player_mmr_text_view);
            mPlayerCountryFlagImageView = itemView.findViewById(R.id.player_location_flag_image_view);
        }
    }

    @Override
    public HighMmrPubStarsListAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new HighMmrPubStarsListAdapter.ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_view_high_mmr_pub_stars_details_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final HighMmrPubStarsListAdapter.ItemViewHolder holder, final int position) {
        holder.mPlayerPersonaNameTextView.setText(ApplicationUtils.makePlaceholderString(mHighMmrPubStarsModelList.get(position).getName(), ApplicationUtils.makePlaceholderString(mHighMmrPubStarsModelList.get(position).getPersonaname(), "-")));
        holder.mPlayerMmrTextView.setText(ApplicationUtils.makePlaceholderIntegerString(mHighMmrPubStarsModelList.get(position).getRating(), "-"));
        Glide.with((Activity) mContext)
                .load(mHighMmrPubStarsModelList.get(position).getAvatarfull())
                .fitCenter()
                .error(mPlaceholderDrawable)
                .into(holder.mPlayerAvatarImageView);
        holder.mPlayerAvatarImageView.setClipToOutline(true);
        Glide.with((Activity) mContext)
                .load(ApplicationUtils.getFlagUrl(mHighMmrPubStarsModelList.get(position).getLocCountryCode()))
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        return false;
                    }
                })
                .fitCenter()
                .into(holder.mPlayerCountryFlagImageView);
        holder.mRootLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, AccountActivity.class);
                intent.putExtra(BundleConstants.ACCOUNT_ID, mHighMmrPubStarsModelList.get(position).getAccountId().longValue());
                mContext.startActivity(intent);
                ((AppCompatActivity) mContext).overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mHighMmrPubStarsModelList != null) {
            return mHighMmrPubStarsModelList.size();
        } else {
            return 0;
        }
    }
}