package com.alacritystudios.statdota.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.models.RecentLeagueMatchModel;
import com.alacritystudios.statdota.utils.ApplicationUtils;
import com.bumptech.glide.Glide;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.Date;
import java.util.List;

/**
 * Adapter to display a list of leagues.
 */

public class RecentLeagueMatchesListAdapter extends RecyclerView.Adapter<RecentLeagueMatchesListAdapter.ItemViewHolder> {

    private List<RecentLeagueMatchModel> mRecentLeagueMatchList;
    private Context mContext;
    private Drawable mPlaceholderDrawable;

    public RecentLeagueMatchesListAdapter(List<RecentLeagueMatchModel> mRecentLeagueMatchList, Context mContext, Drawable mPlaceholderDrawable) {
        this.mRecentLeagueMatchList = mRecentLeagueMatchList;
        this.mContext = mContext;
        this.mPlaceholderDrawable = mPlaceholderDrawable;
    }

    public List<RecentLeagueMatchModel> getmRecentLeagueMatchList() {
        return mRecentLeagueMatchList;
    }

    public void setmRecentLeagueMatchList(List<RecentLeagueMatchModel> mRecentLeagueMatchList) {
        this.mRecentLeagueMatchList = mRecentLeagueMatchList;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private View mRootLayout;
        private TextView mTournamentNameTextView;
        private TextView mTimeElapsedTextView;
        private ImageView mRadiantTeamLogoImageView;
        private ImageView mDireTeamLogoImageView;
        private TextView mRadiantTeamNameTextView;
        private TextView mDireTeamNameTextView;
        private TextView mDurationTextView;
        private TextView mRadiantScoreTextView;
        private TextView mDireScoreTextView;
        private TextView mResultTextView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mRootLayout = itemView;
            mTimeElapsedTextView = (TextView) itemView.findViewById(R.id.time_elapsed_text_view);
            mTournamentNameTextView = (TextView) itemView.findViewById(R.id.tournament_name_text_view);
            mRadiantTeamNameTextView = (TextView) itemView.findViewById(R.id.radiant_team_name_text_view);
            mDireTeamNameTextView = (TextView) itemView.findViewById(R.id.dire_team_name_text_view);
            mDurationTextView = (TextView) itemView.findViewById(R.id.match_duration_text_view);
            mResultTextView = (TextView) itemView.findViewById(R.id.match_result_text_view);
            mRadiantTeamLogoImageView = (ImageView) itemView.findViewById(R.id.radiant_team_logo_image_view);
            mDireTeamLogoImageView = (ImageView) itemView.findViewById(R.id.dire_team_logo_image_view);
            mRadiantScoreTextView = (TextView) itemView.findViewById(R.id.radiant_score_text_view);
            mDireScoreTextView = (TextView) itemView.findViewById(R.id.dire_score_text_view);
        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_view_recent_league_matches_details_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {
        holder.mTimeElapsedTextView.setText(new PrettyTime().format(new Date(mRecentLeagueMatchList.get(position).getStartTime() * 1000)));
        holder.mTournamentNameTextView.setText(ApplicationUtils.makePlaceholderString(mRecentLeagueMatchList.get(position).getLeagueName(), "-"));
        holder.mRadiantTeamNameTextView.setText(ApplicationUtils.makePlaceholderString(mRecentLeagueMatchList.get(position).getRadiantName(), mContext.getString(R.string.radiant)));
        holder.mDireTeamNameTextView.setText(ApplicationUtils.makePlaceholderString(mRecentLeagueMatchList.get(position).getDireName(), mContext.getString(R.string.dire)));
        holder.mDurationTextView.setText(DateUtils.formatElapsedTime(mRecentLeagueMatchList.get(position).getDuration()));
        holder.mRadiantScoreTextView.setText(ApplicationUtils.makePlaceholderIntegerString(mRecentLeagueMatchList.get(position).getRadiantScore(), "0"));
        holder.mDireScoreTextView.setText(ApplicationUtils.makePlaceholderIntegerString(mRecentLeagueMatchList.get(position).getDireScore(), "0"));
        if (mRecentLeagueMatchList.get(position).getRadiantWin()) {
            holder.mResultTextView.setText(ApplicationUtils.makePlaceholderString(mRecentLeagueMatchList.get(position).getRadiantName(), mContext.getString(R.string.radiant)) + " " + mContext.getString(R.string.victory));
        } else {
            holder.mResultTextView.setText(ApplicationUtils.makePlaceholderString(mRecentLeagueMatchList.get(position).getDireName(), mContext.getString(R.string.dire)) + " " + mContext.getString(R.string.victory));
        }
        Glide.with((Activity) mContext)
                .load(mRecentLeagueMatchList.get(position).getRadiantTeamLogo())
                .fitCenter()
                .error(mPlaceholderDrawable)
                .into(holder.mRadiantTeamLogoImageView);

        Glide.with((Activity) mContext)
                .load(mRecentLeagueMatchList.get(position).getDireTeamLogo())
                .fitCenter()
                .error(mPlaceholderDrawable)
                .into(holder.mDireTeamLogoImageView);
    }

    @Override
    public int getItemCount() {
        if (mRecentLeagueMatchList != null) {
            return mRecentLeagueMatchList.size();
        } else {
            return 0;
        }
    }
}
