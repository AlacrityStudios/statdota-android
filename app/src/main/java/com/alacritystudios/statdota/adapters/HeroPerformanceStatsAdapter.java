package com.alacritystudios.statdota.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.components.HeroStatsMarkerView;
import com.alacritystudios.statdota.models.HeroStatsModel;
import com.alacritystudios.statdota.utils.ApiUtils;
import com.alacritystudios.statdota.utils.ApplicationUtils;
import com.bumptech.glide.Glide;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.IMarker;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter to display detailed hero stats about their performances in the past 30 days.
 */

public class HeroPerformanceStatsAdapter extends RecyclerView.Adapter<HeroPerformanceStatsAdapter.ItemViewHolder> {

    private List<HeroStatsModel> mHeroStatsModelList;
    private Context mContext;

    public HeroPerformanceStatsAdapter(List<HeroStatsModel> mHeroStatsModelList, Context mContext) {
        this.mHeroStatsModelList = mHeroStatsModelList;
        this.mContext = mContext;
    }

    public void setmHeroStatsModelList(List<HeroStatsModel> mHeroStatsModelList) {
        this.mHeroStatsModelList = mHeroStatsModelList;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private View mRootLayout;
        private ImageView mHeroIconImageView;
        private TextView mHeroNameTextView;
        private LinearLayout mHeroRole1LinearLayout;
        private LinearLayout mHeroRole2LinearLayout;
        private LinearLayout mHeroRole3LinearLayout;
        private TextView mHeroRole1TextView;
        private TextView mHeroRole2TextView;
        private TextView mHeroRole3TextView;
        private BarChart mHeroStatsBarChart;
        private TextView mPrimaryAttributeTextView;
        private TextView mAttackTypeTextView;
        private TextView mProjectileSpeedTextView;
        private TextView mCmEnabledTextView;
        private TextView mMovementSpeedTextView;
        private TextView mTurnRateTextView;
        private TextView mAttackRateTextView;
        private TextView mAttackRangeTextView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mRootLayout = itemView;
            mHeroIconImageView = (ImageView) itemView.findViewById(R.id.hero_image_icon_image_view);
            mHeroNameTextView = (TextView) itemView.findViewById(R.id.hero_name_text_view);
            mHeroRole1TextView = (TextView) itemView.findViewById(R.id.hero_role_1_text_view);
            mHeroRole2TextView = (TextView) itemView.findViewById(R.id.hero_role_2_text_view);
            mHeroRole3TextView = (TextView) itemView.findViewById(R.id.hero_role_3_text_view);
            mHeroRole1LinearLayout = (LinearLayout) itemView.findViewById(R.id.hero_role_1_linear_layout);
            mHeroRole2LinearLayout = (LinearLayout) itemView.findViewById(R.id.hero_role_2_linear_layout);
            mHeroRole3LinearLayout = (LinearLayout) itemView.findViewById(R.id.hero_role_3_linear_layout);
            mPrimaryAttributeTextView = (TextView) itemView.findViewById(R.id.primary_attr_text_view);
            mAttackTypeTextView = (TextView) itemView.findViewById(R.id.attack_type_text_view);
            mProjectileSpeedTextView = (TextView) itemView.findViewById(R.id.projectile_speed_text_view);
            mCmEnabledTextView = (TextView) itemView.findViewById(R.id.cm_enabled_text_view);
            mMovementSpeedTextView = (TextView) itemView.findViewById(R.id.movement_speed_text_view);
            mTurnRateTextView = (TextView) itemView.findViewById(R.id.turn_rate_text_view);
            mAttackRateTextView = (TextView) itemView.findViewById(R.id.attack_rate_text_view);
            mAttackRangeTextView = (TextView) itemView.findViewById(R.id.attack_range_text_view);
            mHeroStatsBarChart = (BarChart) itemView.findViewById(R.id.hero_stats_bar_chart);
        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new HeroPerformanceStatsAdapter.ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_view_hero_stats_details_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, int position) {
        holder.mHeroNameTextView.setText(ApplicationUtils.makePlaceholderString(mHeroStatsModelList.get(position).getLocalizedName(), "-"));
        try {
            holder.mHeroRole1TextView.setText(ApplicationUtils.makePlaceholderString(mHeroStatsModelList.get(position).getRoles().get(0), "-"));
            holder.mHeroRole1LinearLayout.setVisibility(View.VISIBLE);
        } catch (Exception ex) {
            holder.mHeroRole1LinearLayout.setVisibility(View.GONE);
        }
        try {
            holder.mHeroRole2TextView.setText(ApplicationUtils.makePlaceholderString(mHeroStatsModelList.get(position).getRoles().get(1), "-"));
            holder.mHeroRole2LinearLayout.setVisibility(View.VISIBLE);
        } catch (Exception ex) {
            holder.mHeroRole2LinearLayout.setVisibility(View.GONE);
        }
        try {
            holder.mHeroRole3TextView.setText(ApplicationUtils.makePlaceholderString(mHeroStatsModelList.get(position).getRoles().get(2), "-"));
            holder.mHeroRole3LinearLayout.setVisibility(View.VISIBLE);
        } catch (Exception ex) {
            holder.mHeroRole3LinearLayout.setVisibility(View.GONE);
        }
        holder.mPrimaryAttributeTextView.setText(ApplicationUtils.makePlaceholderString(mHeroStatsModelList.get(position).getPrimaryAttr(), "-"));
        holder.mAttackTypeTextView.setText(ApplicationUtils.makePlaceholderString(mHeroStatsModelList.get(position).getAttackType(), "-"));
        holder.mProjectileSpeedTextView.setText(ApplicationUtils.makePlaceholderFloatString(mHeroStatsModelList.get(position).getProjectileSpeed() != null ? mHeroStatsModelList.get(position).getProjectileSpeed().floatValue() : 0f, "-"));
        holder.mCmEnabledTextView.setText(mHeroStatsModelList.get(position).getCmEnabled() != null? String.valueOf(mHeroStatsModelList.get(position).getCmEnabled()): String.valueOf(false));
        holder.mMovementSpeedTextView.setText(ApplicationUtils.makePlaceholderFloatString(mHeroStatsModelList.get(position).getMoveSpeed() != null ? mHeroStatsModelList.get(position).getMoveSpeed().floatValue() : 0f, "-"));
        holder.mTurnRateTextView.setText(ApplicationUtils.makePlaceholderFloatString(mHeroStatsModelList.get(position).getTurnRate() != null ? mHeroStatsModelList.get(position).getTurnRate().floatValue() : 0f, "-"));
        holder.mAttackRateTextView.setText(ApplicationUtils.makePlaceholderFloatString(mHeroStatsModelList.get(position).getAttackRate() != null ? mHeroStatsModelList.get(position).getAttackRate().floatValue() : 0f, "-"));
        holder.mAttackRangeTextView.setText(ApplicationUtils.makePlaceholderFloatString(mHeroStatsModelList.get(position).getAttackRange() != null ? mHeroStatsModelList.get(position).getAttackRange().floatValue() : 0f, "-"));
        Glide.with((Activity) mContext)
                .load(ApiUtils.getHeroImageUrl(mHeroStatsModelList.get(position).getName()))
                .centerCrop()
                .into(holder.mHeroIconImageView);

        setChartData(holder, position);
    }

    @Override
    public int getItemCount() {
        if (mHeroStatsModelList != null) {
            return mHeroStatsModelList.size();
        } else {
            return 0;
        }
    }

    @Override
    public void onViewDetachedFromWindow(HeroPerformanceStatsAdapter.ItemViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        //holder.clearAnimation();
    }

    @Override
    public void onViewAttachedToWindow(HeroPerformanceStatsAdapter.ItemViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        //holder.setAlphaAnimation();
    }

    /**
     * Sets the chart data to various charts
     */
    public void setChartData(HeroPerformanceStatsAdapter.ItemViewHolder holder, int position) {
        List<BarEntry> winRateEntries = new ArrayList<>();
        if(mHeroStatsModelList.get(position).getProPick() != null && mHeroStatsModelList.get(position).getProPick() != 0) {
            winRateEntries.add(new BarEntry(0f, (mHeroStatsModelList.get(position).getProWin() != null? mHeroStatsModelList.get(position).getProWin().floatValue() : 0f) * 100 / mHeroStatsModelList.get(position).getProPick()));
        } else {
            winRateEntries.add(new BarEntry(0f, 0f));
        }
        if(mHeroStatsModelList.get(position).get1000Pick() != null && mHeroStatsModelList.get(position).get1000Pick() != 0) {
            winRateEntries.add(new BarEntry(1f, (mHeroStatsModelList.get(position).get1000Win() != null? mHeroStatsModelList.get(position).get1000Win().floatValue() : 0f) * 100 / mHeroStatsModelList.get(position).get1000Pick()));
        } else {
            winRateEntries.add(new BarEntry(1f, 0f));
        }
        if(mHeroStatsModelList.get(position).get2000Pick() != null && mHeroStatsModelList.get(position).get2000Pick() != 0) {
            winRateEntries.add(new BarEntry(2f, (mHeroStatsModelList.get(position).get2000Win() != null? mHeroStatsModelList.get(position).get2000Win().floatValue() : 0f) * 100 / mHeroStatsModelList.get(position).get2000Pick()));
        } else {
            winRateEntries.add(new BarEntry(2f, 0f));
        }
        if(mHeroStatsModelList.get(position).get3000Pick() != null && mHeroStatsModelList.get(position).get3000Pick() != 0) {
            winRateEntries.add(new BarEntry(3f, (mHeroStatsModelList.get(position).get3000Win() != null? mHeroStatsModelList.get(position).get3000Win().floatValue() : 0f) * 100 / mHeroStatsModelList.get(position).get3000Pick()));
        } else {
            winRateEntries.add(new BarEntry(3f, 0f));
        }
        if(mHeroStatsModelList.get(position).get4000Pick() != null && mHeroStatsModelList.get(position).get4000Pick() != 0) {
            winRateEntries.add(new BarEntry(4f, (mHeroStatsModelList.get(position).get4000Win() != null? mHeroStatsModelList.get(position).get4000Win().floatValue() : 0f) * 100 / mHeroStatsModelList.get(position).get4000Pick()));
        } else {
            winRateEntries.add(new BarEntry(4f, 0f));
        }
        if(mHeroStatsModelList.get(position).get5000Pick() != null && mHeroStatsModelList.get(position).get5000Pick() != 0) {
            winRateEntries.add(new BarEntry(5f, (mHeroStatsModelList.get(position).get5000Win() != null? mHeroStatsModelList.get(position).get5000Win().floatValue() : 0f) * 100 / mHeroStatsModelList.get(position).get5000Pick()));
        } else {
            winRateEntries.add(new BarEntry(5f, 0f));
        }

        Description description = new Description();
        description.setText("");
        BarDataSet winRateBarDataSet = new BarDataSet(winRateEntries, "");
        winRateBarDataSet.setValueTextColor(ActivityCompat.getColor(mContext, R.color.colorBlackText));
        winRateBarDataSet.setValueTextSize(8);
        winRateBarDataSet.setValueTypeface(ResourcesCompat.getFont(mContext, R.font.noto_sans_ui_bold));
        winRateBarDataSet.setValueFormatter(new IValueFormatter() {

            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                return String.format("%.01f", value) + "%";
            }
        });
        BarData winRateBarData = new BarData(winRateBarDataSet);
        XAxis xAxis = holder.mHeroStatsBarChart.getXAxis();
        xAxis.setDrawGridLines(false);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawAxisLine(false);
        xAxis.setTextColor(ActivityCompat.getColor(mContext, R.color.colorBlackText));
        xAxis.setTextSize(8);
        xAxis.setTypeface(ResourcesCompat.getFont(mContext, R.font.noto_sans_ui_bold));
        xAxis.setValueFormatter(new IAxisValueFormatter() {

            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                if(value == 0f) {
                    return mContext.getString(R.string.pro).toUpperCase();
                } else if (value == 1f) {
                    return mContext.getString(R.string._1_thousand_mmr).toUpperCase();
                } else if (value == 2f) {
                    return mContext.getString(R.string._2_thousand_mmr).toUpperCase();
                } else if (value == 3f) {
                    return mContext.getString(R.string._3_thousand_mmr).toUpperCase();
                } else if (value == 4f) {
                    return mContext.getString(R.string._4_thousand_mmr).toUpperCase();
                } else if (value == 5f) {
                    return mContext.getString(R.string._5_thousand_mmr).toUpperCase();
                } else {
                    return mContext.getString(R.string._5_thousand_mmr).toUpperCase();
                }
            }
        });

        YAxis axisLeft = holder.mHeroStatsBarChart.getAxisLeft();
        axisLeft.setAxisMaximum(100f);
        axisLeft.setAxisMinimum(0f);
        axisLeft.setDrawGridLines(true);
        axisLeft.setDrawAxisLine(false);
        axisLeft.setGridColor(ActivityCompat.getColor(mContext, R.color.colorDefaultMedium));
        axisLeft.setTextColor(ActivityCompat.getColor(mContext, R.color.colorBlackText));
        axisLeft.setTextSize(8);
        axisLeft.setTypeface(ResourcesCompat.getFont(mContext, R.font.noto_sans_ui_bold));
        YAxis axisRight = holder.mHeroStatsBarChart.getAxisRight();
        axisRight.setEnabled(false);
        IMarker marker = new HeroStatsMarkerView(mContext, R.layout.layout_hero_stats_marker_view, mHeroStatsModelList.get(position));
        holder.mHeroStatsBarChart.setMarker(marker);
        holder.mHeroStatsBarChart.getLegend().setEnabled(false);
        holder.mHeroStatsBarChart.setDescription(description);
        holder.mHeroStatsBarChart.setData(winRateBarData);
        holder.mHeroStatsBarChart.setPinchZoom(false);
        holder.mHeroStatsBarChart.disableScroll();
        holder.mHeroStatsBarChart.setScaleEnabled(false);
        holder.mHeroStatsBarChart.setDoubleTapToZoomEnabled(false);
        holder.mHeroStatsBarChart.animateXY(1000, 1000);
    }
}
