package com.alacritystudios.statdota.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.helpers.FiltersAutoCompleteFilter;
import com.alacritystudios.statdota.models.FiltersAutoCompleteTextViewModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter to display suggestions in auto complete text view.
 */

public class FiltersAutoCompleteTextViewAdapter extends ArrayAdapter<FiltersAutoCompleteTextViewModel> {

    private List<FiltersAutoCompleteTextViewModel> mSuggestionsList;
    private List<FiltersAutoCompleteTextViewModel> mFilteredSuggestionsList;
    private Context mContext;
    private int mLayoutResourceId;

    public FiltersAutoCompleteTextViewAdapter(@NonNull Context context, @NonNull List<FiltersAutoCompleteTextViewModel> objects) {
        super(context, 0, objects);
        this.mSuggestionsList = objects;
        this.mContext = context;
        this.mLayoutResourceId = R.layout.layout_auto_complete_text_view_suggestion;
        this.mFilteredSuggestionsList = new ArrayList<>();
    }

    public List<FiltersAutoCompleteTextViewModel> getmFilteredSuggestionsList() {
        return mFilteredSuggestionsList;
    }

    public void setmFilteredSuggestionsList(List<FiltersAutoCompleteTextViewModel> mFilteredSuggestionsList) {
        this.mFilteredSuggestionsList = mFilteredSuggestionsList;
    }

    @Override
    public Filter getFilter() {
        return new FiltersAutoCompleteFilter(this, mSuggestionsList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(mLayoutResourceId, parent, false);
        }
        TextView mTextTextView = (TextView) convertView.findViewById(R.id.text1);
        ImageView mImageImageView = (ImageView) convertView.findViewById(R.id.suggestion_image_image_view);

        Glide.with((Activity) mContext)
                .load(mFilteredSuggestionsList.get(position).getLogoUrl())
                .centerCrop()
                .into(mImageImageView);
        mImageImageView.setClipToOutline(true);

        mTextTextView.setText(mFilteredSuggestionsList.get(position).getName());

        return convertView;
    }

    @Override
    public int getCount() {
        return mFilteredSuggestionsList.size();
    }
}
