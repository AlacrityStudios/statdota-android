package com.alacritystudios.statdota.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.models.TwitchClipsModel;
import com.alacritystudios.statdota.utils.ApplicationUtils;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Adapter to display a summary of popular Twitch clips in the past week.
 */

public class TwitchClipsSummaryAdapter extends RecyclerView.Adapter<TwitchClipsSummaryAdapter.ItemViewHolder> {

    private Context mContext;
    private List<TwitchClipsModel> mTwitchClipsModelList;

    public TwitchClipsSummaryAdapter(Context mContext, List<TwitchClipsModel> mTwitchClipsModelList) {
        this.mContext = mContext;
        this.mTwitchClipsModelList = mTwitchClipsModelList;
    }

    public void setmTwitchClipsModelList(List<TwitchClipsModel> mTwitchClipsModelList) {
        this.mTwitchClipsModelList = mTwitchClipsModelList;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private ImageView mClipIconImageView;
        private TextView mClipNameTextView;
        private TextView mClipBroadcasterNameTextView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mClipIconImageView = (ImageView) itemView.findViewById(R.id.twitch_clip_icon_image_view);
            mClipNameTextView = (TextView) itemView.findViewById(R.id.twitch_clip_name_text_view);
            mClipBroadcasterNameTextView = (TextView) itemView.findViewById(R.id.twitch_clip_broadcaster_name_text_view);
        }
    }

    @Override
    public TwitchClipsSummaryAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TwitchClipsSummaryAdapter.ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_view_twitch_clips_summary_item, parent, false));
    }

    @Override
    public void onBindViewHolder(TwitchClipsSummaryAdapter.ItemViewHolder holder, int position) {
        holder.mClipNameTextView.setText(ApplicationUtils.makePlaceholderString(mTwitchClipsModelList.get(position).getTitle(), ""));
        holder.mClipBroadcasterNameTextView.setText(ApplicationUtils.makePlaceholderString(mTwitchClipsModelList.get(position).getBroadcasterName(), ""));
        Glide.with(mContext.getApplicationContext())
                .load(mTwitchClipsModelList.get(position).getThumbnail())
                .centerCrop()
                .into(holder.mClipIconImageView);
        holder.mClipIconImageView.setClipToOutline(true);
    }

    @Override
    public int getItemCount() {
        if (mTwitchClipsModelList != null) {
            return mTwitchClipsModelList.size();
        } else {
            return 0;
        }
    }
}
