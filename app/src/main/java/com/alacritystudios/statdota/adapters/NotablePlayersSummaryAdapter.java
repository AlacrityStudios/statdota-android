package com.alacritystudios.statdota.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.activities.AccountActivity;
import com.alacritystudios.statdota.constants.BundleConstants;
import com.alacritystudios.statdota.models.ProPlayersModel;
import com.alacritystudios.statdota.utils.ApplicationUtils;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 * An adapter to display a summary of the list of recent league matches.
 */

public class NotablePlayersSummaryAdapter extends RecyclerView.Adapter<NotablePlayersSummaryAdapter.ItemViewHolder> {

    private List<ProPlayersModel> mProPlayersModelList;
    private Context mContext;

    public NotablePlayersSummaryAdapter(Context mContext, List<ProPlayersModel> mProPlayersModelList) {
        this.mProPlayersModelList = mProPlayersModelList;
        this.mContext = mContext;
    }

    public void setmProPlayersModelList(List<ProPlayersModel> mProPlayersModelList) {
        this.mProPlayersModelList = mProPlayersModelList;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private View mRootLayout;
        private TextView mPlayerPersonaNameTextView;
        private TextView mPlayerMmrTextView;
        private ImageView mPlayerAvatarImageView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mRootLayout = itemView;
            mPlayerAvatarImageView = itemView.findViewById(R.id.player_avatar_image_view);
            mPlayerPersonaNameTextView = itemView.findViewById(R.id.hero_name_text_view);
            mPlayerMmrTextView = itemView.findViewById(R.id.player_mmr_text_view);
        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_view_high_mmr_pub_stars_summary_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position) {
        holder.mPlayerPersonaNameTextView.setText(ApplicationUtils.makePlaceholderString(ApplicationUtils.makePlaceholderString(mProPlayersModelList.get(position).getName(),
                mProPlayersModelList.get(position).getPersonaname()), mContext.getString(R.string.anonymous)));
        holder.mPlayerMmrTextView.setText(ApplicationUtils.makePlaceholderIntegerString(mProPlayersModelList.get(position).getSoloCompetitiveRank(), "-"));
        Glide.with(mContext.getApplicationContext())
                .load(mProPlayersModelList.get(position).getAvatarfull())
                .centerCrop()
                .into(holder.mPlayerAvatarImageView);
        holder.mPlayerAvatarImageView.setClipToOutline(true);
        holder.mRootLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, AccountActivity.class);
                intent.putExtra(BundleConstants.ACCOUNT_ID, mProPlayersModelList.get(position).getAccountId().longValue());
                mContext.startActivity(intent);
                ((AppCompatActivity) mContext).overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mProPlayersModelList != null) {
            return mProPlayersModelList.size();
        } else {
            return 0;
        }
    }
}
