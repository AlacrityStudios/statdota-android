package com.alacritystudios.statdota.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.activities.AccountActivity;
import com.alacritystudios.statdota.constants.BundleConstants;
import com.alacritystudios.statdota.models.PlayerSearchResultModel;
import com.alacritystudios.statdota.utils.ApplicationUtils;
import com.bumptech.glide.Glide;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * An adapter to display player search results.
 */

public class PlayerSearchResultAdapter extends RecyclerView.Adapter<PlayerSearchResultAdapter.ItemViewHolder> {

    private List<PlayerSearchResultModel> mPlayerSearchResultModelList;
    private Context mContext;

    public PlayerSearchResultAdapter(List<PlayerSearchResultModel> mPlayerSearchResultModelList, Context mContext) {
        this.mPlayerSearchResultModelList = mPlayerSearchResultModelList;
        this.mContext = mContext;
    }

    public void setmPlayerSearchResultModelList(List<PlayerSearchResultModel> mPlayerSearchResultModelList) {
        this.mPlayerSearchResultModelList = mPlayerSearchResultModelList;
    }

    public List<PlayerSearchResultModel> getmPlayerSearchResultModelList() {
        return mPlayerSearchResultModelList;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        public View mRootLayout;
        public TextView mPlayerPersonaNameTextView;
        public TextView mPlayerLastPlayedTextView;
        public ImageView mPlayerAvatarImageView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mRootLayout = itemView;
            mPlayerAvatarImageView = (ImageView) itemView.findViewById(R.id.player_avatar_image_view);
            mPlayerPersonaNameTextView = (TextView) itemView.findViewById(R.id.hero_name_text_view);
            mPlayerLastPlayedTextView = (TextView) itemView.findViewById(R.id.player_last_played_text_view);
        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_view_player_search_result_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position) {
        holder.mPlayerPersonaNameTextView.setText(ApplicationUtils.makePlaceholderString(mPlayerSearchResultModelList.get(position).getPersonaname(), mContext.getString(R.string.anonymous)));
        if (mPlayerSearchResultModelList.get(position).getLastMatchTime() != null) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            try {
                Date date = format.parse(mPlayerSearchResultModelList.get(position).getLastMatchTime());
                holder.mPlayerLastPlayedTextView.setText(new PrettyTime().format(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        Glide.with(mContext.getApplicationContext())
                .load(mPlayerSearchResultModelList.get(position).getAvatarfull())
                .centerCrop()
                .into(holder.mPlayerAvatarImageView);
        holder.mRootLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, AccountActivity.class);
                intent.putExtra(BundleConstants.ACCOUNT_ID, mPlayerSearchResultModelList.get(position).getAccountId().longValue());
                mContext.startActivity(intent);
                ((AppCompatActivity) mContext).overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mPlayerSearchResultModelList != null) {
            return mPlayerSearchResultModelList.size();
        } else {
            return 0;
        }
    }
}
