package com.alacritystudios.statdota.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.interfaces.TwitchLiveStreamsItemClickListener;
import com.alacritystudios.statdota.models.TwitchStreamsModel;
import com.alacritystudios.statdota.utils.AnimationUtils;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 * An adapter to display Twitch live streams.
 */

public class TwitchStreamsAdapter extends RecyclerView.Adapter<TwitchStreamsAdapter.ItemViewHolder>{

    private List<TwitchStreamsModel.Stream> streamList;
    private Context mContext;
    private TwitchLiveStreamsItemClickListener mTwitchLiveStreamsItemClickListener;

    public TwitchStreamsAdapter(List<TwitchStreamsModel.Stream> streamList, Context mContext, TwitchLiveStreamsItemClickListener mTwitchLiveStreamsItemClickListener) {
        this.streamList = streamList;
        this.mContext = mContext;
        this.mTwitchLiveStreamsItemClickListener = mTwitchLiveStreamsItemClickListener;
    }

    public void setStreamList(List<TwitchStreamsModel.Stream> streamList) {
        this.streamList = streamList;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private View mRootLayout;
        private ImageView mStreamThumbnailImageView;
        private ImageView mChannelLogoImageView;
        private TextView mChannelStatusTextView;
        private TextView mChannelDisplayNameTextView;
        private TextView mSpectatorsTextView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mRootLayout= itemView;
            mStreamThumbnailImageView = (ImageView) itemView.findViewById(R.id.stream_thumbnail_image_view);
            mChannelLogoImageView = (ImageView) itemView.findViewById(R.id.channel_logo_image_view);
            mChannelStatusTextView = (TextView) itemView.findViewById(R.id.channel_status_text_view);
            mChannelDisplayNameTextView = (TextView) itemView.findViewById(R.id.channel_display_name_text_view);
            mSpectatorsTextView = (TextView) itemView.findViewById(R.id.spectators_text_view);
        }
    }

    @Override
    public TwitchStreamsAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TwitchStreamsAdapter.ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_view_twitch_streams_item, parent, false));
    }

    @Override
    public void onBindViewHolder(TwitchStreamsAdapter.ItemViewHolder holder, final int position) {
        holder.mChannelStatusTextView.setText(streamList.get(position).getChannel().getStatus());
        holder.mChannelDisplayNameTextView.setText(streamList.get(position).getChannel().getDisplayName());
        holder.mSpectatorsTextView.setText(String.valueOf(streamList.get(position).getViewers()));
        Glide.with((Activity) mContext)
                .load(streamList.get(position).getChannel().getLogo())
                .centerCrop()
                .into(holder.mChannelLogoImageView);
        holder.mChannelLogoImageView.setClipToOutline(true);
        Glide.with((Activity) mContext)
                .load(streamList.get(position).getPreview().getLarge())
                .fitCenter()
                .into(holder.mStreamThumbnailImageView);
        holder.mRootLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mTwitchLiveStreamsItemClickListener.onItemClick(streamList.get(position).getChannel().getName());
            }
        });
    }

    @Override
    public int getItemCount() {
        if (streamList != null) {
            return streamList.size();
        } else {
            return 0;
        }
    }
}
