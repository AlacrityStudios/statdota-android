package com.alacritystudios.statdota.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.models.TeamsModel;
import com.alacritystudios.statdota.utils.AnimationUtils;
import com.alacritystudios.statdota.utils.ApplicationUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Adapter to display a list of TeamsModel.
 */

public class TeamsListAdapter extends RecyclerView.Adapter<TeamsListAdapter.ItemViewHolder> {

    private List<TeamsModel> mTeamsModelList;
    private Context mContext;
    private Drawable mPlaceholderDrawable;
    private TeamsListAdapterListener mTeamsListAdapterListener;

    public TeamsListAdapter(List<TeamsModel> mTeamsModelList, Context mContext, Drawable mPlaceholderDrawable, TeamsListAdapterListener mTeamsListAdapterListener) {
        this.mTeamsModelList = mTeamsModelList;
        this.mContext = mContext;
        this.mPlaceholderDrawable = mPlaceholderDrawable;
        this.mTeamsListAdapterListener = mTeamsListAdapterListener;
    }

    public List<TeamsModel> getmTeamsModelList() {
        return mTeamsModelList;
    }

    public void setmTeamsModelList(List<TeamsModel> mTeamsModelList) {
        this.mTeamsModelList = mTeamsModelList;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private View mRootLayout;
        private ImageView mTeamLogoImageView;
        private TextView mTeamNameTextView;
        private TextView mTeamTagTextView;
        private TextView mTeamRatingTextView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mRootLayout = itemView;
            mTeamLogoImageView = itemView.findViewById(R.id.team_logo_image_view);
            mTeamNameTextView = itemView.findViewById(R.id.team_name_text_view);
            mTeamTagTextView = itemView.findViewById(R.id.team_tag_text_view);
            mTeamRatingTextView = itemView.findViewById(R.id.team_rating_text_view);
        }
    }

    @Override
    public TeamsListAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TeamsListAdapter.ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_view_teams_details_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final TeamsListAdapter.ItemViewHolder holder, final int position) {
        holder.mTeamNameTextView.setText(ApplicationUtils.makePlaceholderString(mTeamsModelList.get(position).getName(), "-"));
        holder.mTeamTagTextView.setText(ApplicationUtils.makePlaceholderString(mTeamsModelList.get(position).getTag(), "-"));
        holder.mTeamRatingTextView.setText(ApplicationUtils.makePlaceholderIntegerString(mTeamsModelList.get(position).getRating() != null? mTeamsModelList.get(position).getRating().intValue() : 0, "-"));
        Glide.with((Activity) mContext)
                .load(mTeamsModelList.get(position).getLogoUrl())
                .fitCenter()
                .error(mPlaceholderDrawable)
                .into(holder.mTeamLogoImageView);
        holder.mTeamLogoImageView.setClipToOutline(true);
        holder.mRootLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTeamsListAdapterListener.onItemTouch(mTeamsModelList.get(position).getTeamId());
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mTeamsModelList != null) {
            return mTeamsModelList.size();
        } else {
            return 0;
        }
    }

    public interface TeamsListAdapterListener {
        void onItemTouch(long teamId);
    }
}
