package com.alacritystudios.statdota.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.models.PubMatchesModel;
import com.alacritystudios.statdota.sql.StatDotaDatabaseHelper;
import com.alacritystudios.statdota.utils.ApiUtils;
import com.alacritystudios.statdota.utils.ApplicationUtils;
import com.bumptech.glide.Glide;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.Date;
import java.util.List;

/**
 * Adapter to display a list of recent high MMR pub games.
 */

public class PubMatchesSummaryAdapter extends RecyclerView.Adapter<PubMatchesSummaryAdapter.ItemViewHolder> {

    private Context mContext;
    private List<PubMatchesModel> mPubMatchesModelList;

    public PubMatchesSummaryAdapter(Context mContext, List<PubMatchesModel> mPubMatchesModelList) {
        this.mContext = mContext;
        this.mPubMatchesModelList = mPubMatchesModelList;
    }

    public void setmPubMatchesModelList(List<PubMatchesModel> mPubMatchesModelList) {
        this.mPubMatchesModelList = mPubMatchesModelList;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private ImageView mRadiantHero1IconImageView;
        private ImageView mRadiantHero2IconImageView;
        private ImageView mRadiantHero3IconImageView;
        private ImageView mRadiantHero4IconImageView;
        private ImageView mRadiantHero5IconImageView;
        private ImageView mDireHero1IconImageView;
        private ImageView mDireHero2IconImageView;
        private ImageView mDireHero3IconImageView;
        private ImageView mDireHero4IconImageView;
        private ImageView mDireHero5IconImageView;
        private TextView mAverageMmrTextView;
        private TextView mElapsedTimeTextView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mRadiantHero1IconImageView = (ImageView) itemView.findViewById(R.id.radiant_1_hero_icon_image_view);
            mRadiantHero2IconImageView = (ImageView) itemView.findViewById(R.id.radiant_2_hero_icon_image_view);
            mRadiantHero3IconImageView = (ImageView) itemView.findViewById(R.id.radiant_3_hero_icon_image_view);
            mRadiantHero4IconImageView = (ImageView) itemView.findViewById(R.id.radiant_4_hero_icon_image_view);
            mRadiantHero5IconImageView = (ImageView) itemView.findViewById(R.id.radiant_5_hero_icon_image_view);
            mDireHero1IconImageView = (ImageView) itemView.findViewById(R.id.dire_1_hero_icon_image_view);
            mDireHero2IconImageView = (ImageView) itemView.findViewById(R.id.dire_2_hero_icon_image_view);
            mDireHero3IconImageView = (ImageView) itemView.findViewById(R.id.dire_3_hero_icon_image_view);
            mDireHero4IconImageView = (ImageView) itemView.findViewById(R.id.dire_4_hero_icon_image_view);
            mDireHero5IconImageView = (ImageView) itemView.findViewById(R.id.dire_5_hero_icon_image_view);
            mAverageMmrTextView = (TextView) itemView.findViewById(R.id.average_mmr_text_view);
            mElapsedTimeTextView = (TextView) itemView.findViewById(R.id.elapsed_time_text_view);
        }
    }

    @Override
    public PubMatchesSummaryAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PubMatchesSummaryAdapter.ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_view_pub_matches_summary_item, parent, false));
    }

    @Override
    public void onBindViewHolder(PubMatchesSummaryAdapter.ItemViewHolder holder, int position) {
        holder.mAverageMmrTextView.setText(mContext.getString(R.string.avg_mmr)+ ": " + ApplicationUtils.makePlaceholderIntegerString(mPubMatchesModelList.get(position).getAvgMmr(), "0"));
        holder.mElapsedTimeTextView.setText(new PrettyTime().format(new Date((mPubMatchesModelList.get(position).getStartTime() != null ? mPubMatchesModelList.get(position).getStartTime() * 1000 : 0))));
        Glide.with(mContext.getApplicationContext())
                .load(ApiUtils.getHeroImageUrl(mPubMatchesModelList.get(position).getmRadiantHeroDetails().get(0) != null ? mPubMatchesModelList.get(position).getmRadiantHeroDetails().get(0).getName() : ""))
                .centerCrop()
                .into(holder.mRadiantHero1IconImageView);
        Glide.with(mContext.getApplicationContext())
                .load(ApiUtils.getHeroImageUrl(mPubMatchesModelList.get(position).getmRadiantHeroDetails().get(1) != null ? mPubMatchesModelList.get(position).getmRadiantHeroDetails().get(1).getName() : ""))
                .centerCrop()
                .into(holder.mRadiantHero2IconImageView);
        Glide.with(mContext.getApplicationContext())
                .load(ApiUtils.getHeroImageUrl(mPubMatchesModelList.get(position).getmRadiantHeroDetails().get(2) != null ? mPubMatchesModelList.get(position).getmRadiantHeroDetails().get(2).getName() : ""))
                .centerCrop()
                .into(holder.mRadiantHero3IconImageView);
        Glide.with(mContext.getApplicationContext())
                .load(ApiUtils.getHeroImageUrl(mPubMatchesModelList.get(position).getmRadiantHeroDetails().get(3) != null ? mPubMatchesModelList.get(position).getmRadiantHeroDetails().get(3).getName() : ""))
                .centerCrop()
                .into(holder.mRadiantHero4IconImageView);
        Glide.with(mContext.getApplicationContext())
                .load(ApiUtils.getHeroImageUrl(mPubMatchesModelList.get(position).getmRadiantHeroDetails().get(4) != null ? mPubMatchesModelList.get(position).getmRadiantHeroDetails().get(4).getName() : ""))
                .centerCrop()
                .into(holder.mRadiantHero5IconImageView);
        Glide.with(mContext.getApplicationContext())
                .load(ApiUtils.getHeroImageUrl(mPubMatchesModelList.get(position).getmDireHeroDetails().get(0) != null ? mPubMatchesModelList.get(position).getmDireHeroDetails().get(0).getName() : ""))
                .centerCrop()
                .into(holder.mDireHero1IconImageView);
        Glide.with(mContext.getApplicationContext())
                .load(ApiUtils.getHeroImageUrl(mPubMatchesModelList.get(position).getmDireHeroDetails().get(1) != null ? mPubMatchesModelList.get(position).getmDireHeroDetails().get(1).getName() : ""))
                .centerCrop()
                .into(holder.mDireHero2IconImageView);
        Glide.with(mContext.getApplicationContext())
                .load(ApiUtils.getHeroImageUrl(mPubMatchesModelList.get(position).getmDireHeroDetails().get(2) != null ? mPubMatchesModelList.get(position).getmDireHeroDetails().get(2).getName() : ""))
                .centerCrop()
                .into(holder.mDireHero3IconImageView);
        Glide.with(mContext.getApplicationContext())
                .load(ApiUtils.getHeroImageUrl(mPubMatchesModelList.get(position).getmDireHeroDetails().get(3) != null ? mPubMatchesModelList.get(position).getmDireHeroDetails().get(3).getName() : ""))
                .centerCrop()
                .into(holder.mDireHero4IconImageView);
        Glide.with(mContext.getApplicationContext())
                .load(ApiUtils.getHeroImageUrl(mPubMatchesModelList.get(position).getmDireHeroDetails().get(4) != null ? mPubMatchesModelList.get(position).getmDireHeroDetails().get(4).getName() : ""))
                .centerCrop()
                .into(holder.mDireHero5IconImageView);
    }

    @Override
    public int getItemCount() {
        if (mPubMatchesModelList != null) {
            return mPubMatchesModelList.size();
        } else {
            return 0;
        }
    }
}