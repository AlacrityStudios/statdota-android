package com.alacritystudios.statdota.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.models.TwitchClipsModel;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 * An adapter to display Twitch top clips of the week.
 */

public class TwitchClipsAdapter extends RecyclerView.Adapter<TwitchClipsAdapter.ItemViewHolder> {

    private Context mContext;
    private List<TwitchClipsModel> mTwitchClipsModelList;

    public TwitchClipsAdapter(Context mContext, List<TwitchClipsModel> mTwitchClipsModelList) {
        this.mContext = mContext;
        this.mTwitchClipsModelList = mTwitchClipsModelList;
    }

    public List<TwitchClipsModel> getmTwitchClipsModelList() {
        return mTwitchClipsModelList;
    }

    public void setmTwitchClipsModelList(List<TwitchClipsModel> mTwitchClipsModelList) {
        this.mTwitchClipsModelList = mTwitchClipsModelList;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private View mRootLayout;
        private ImageView mClipThumbnailImageView;
        private TextView mBroadcasterNameTextView;
        private TextView mClipNameTextView;
        private ImageView mClipOptionsImageView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mRootLayout = itemView;
            mClipThumbnailImageView = (ImageView) itemView.findViewById(R.id.stream_thumbnail_image_view);
            mClipOptionsImageView = (ImageView) itemView.findViewById(R.id.twitch_clips_options_image_view);
            mBroadcasterNameTextView = (TextView) itemView.findViewById(R.id.twitch_clip_broadcaster_name_text_view);
            mClipNameTextView = (TextView) itemView.findViewById(R.id.twitch_clip_name_text_view);
        }
    }

    @Override
    public TwitchClipsAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TwitchClipsAdapter.ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_view_twitch_clips_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final TwitchClipsAdapter.ItemViewHolder holder, final int position) {
        holder.mBroadcasterNameTextView.setText(mTwitchClipsModelList.get(position).getBroadcasterName());
        holder.mClipNameTextView.setText(mTwitchClipsModelList.get(position).getTitle());
        Glide.with((Activity) mContext)
                .load(mTwitchClipsModelList.get(position).getThumbnail())
                .fitCenter()
                .into(holder.mClipThumbnailImageView);
        holder.mClipThumbnailImageView.setClipToOutline(true);
        holder.mRootLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                PopupMenu twitchClipsPopUpMenu = new PopupMenu(mContext, holder.mRootLayout);
                //Inflating the Popup using xml file
                twitchClipsPopUpMenu.getMenuInflater()
                        .inflate(R.menu.twitch_clip_item_menu, twitchClipsPopUpMenu.getMenu());
            }
        });
        holder.mClipOptionsImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public int getItemCount() {
        if (mTwitchClipsModelList != null) {
            return mTwitchClipsModelList.size();
        } else {
            return 0;
        }
    }
}