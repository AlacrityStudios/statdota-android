package com.alacritystudios.statdota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Displays the details of each team.
 */

public class TeamDetailsModel {

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("country_code")
    @Expose
    private String countryCode;
    @SerializedName("ugc_sponsor_logo")
    @Expose
    private String ugcSponsorLogo;
    @SerializedName("ugc_banner_logo")
    @Expose
    private String ugcBannerLogo;
    @SerializedName("ugc_base_logo")
    @Expose
    private String ugcBaseLogo;
    @SerializedName("ugc_logo")
    @Expose
    private String ugcLogo;
    @SerializedName("locked")
    @Expose
    private Boolean locked;
    @SerializedName("pro")
    @Expose
    private Boolean pro;
    @SerializedName("time_created")
    @Expose
    private Integer timeCreated;
    @SerializedName("tag")
    @Expose
    private String tag;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("losses")
    @Expose
    private Integer losses;
    @SerializedName("wins")
    @Expose
    private Integer wins;
    @SerializedName("rating")
    @Expose
    private Double rating;
    @SerializedName("team_id")
    @Expose
    private Long teamId;

    @SerializedName("logo_url")
    @Expose
    private String logoUrl;

    @SerializedName("player_details")
    @Expose
    private List<ProPlayersModel> playerDetails;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getUgcSponsorLogo() {
        return ugcSponsorLogo;
    }

    public void setUgcSponsorLogo(String ugcSponsorLogo) {
        this.ugcSponsorLogo = ugcSponsorLogo;
    }

    public String getUgcBannerLogo() {
        return ugcBannerLogo;
    }

    public void setUgcBannerLogo(String ugcBannerLogo) {
        this.ugcBannerLogo = ugcBannerLogo;
    }

    public String getUgcBaseLogo() {
        return ugcBaseLogo;
    }

    public void setUgcBaseLogo(String ugcBaseLogo) {
        this.ugcBaseLogo = ugcBaseLogo;
    }

    public String getUgcLogo() {
        return ugcLogo;
    }

    public void setUgcLogo(String ugcLogo) {
        this.ugcLogo = ugcLogo;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public Boolean getPro() {
        return pro;
    }

    public void setPro(Boolean pro) {
        this.pro = pro;
    }

    public Integer getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(Integer timeCreated) {
        this.timeCreated = timeCreated;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLosses() {
        return losses;
    }

    public void setLosses(Integer losses) {
        this.losses = losses;
    }

    public Integer getWins() {
        return wins;
    }

    public void setWins(Integer wins) {
        this.wins = wins;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public List<ProPlayersModel> getPlayerDetails() {
        return playerDetails;
    }

    public void setPlayerDetails(List<ProPlayersModel> playerDetails) {
        this.playerDetails = playerDetails;
    }
}
