package com.alacritystudios.statdota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Model to hold twitch clips details.
 */

public class TwitchClipsModel {

    @SerializedName("broadcaster_name")
    @Expose
    private String broadcasterName;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("embed_url")
    @Expose
    private String embed_url;

    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;

    @SerializedName("views")
    @Expose
    private Long views;

    @SerializedName("duration")
    @Expose
    private Float duration;

    public String getBroadcasterName() {
        return broadcasterName;
    }

    public void setBroadcasterName(String broadcasterName) {
        this.broadcasterName = broadcasterName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmbed_url() {
        return embed_url;
    }

    public void setEmbed_url(String embed_url) {
        this.embed_url = embed_url;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Long getViews() {
        return views;
    }

    public void setViews(Long views) {
        this.views = views;
    }

    public Float getDuration() {
        return duration;
    }

    public void setDuration(Float duration) {
        this.duration = duration;
    }
}
