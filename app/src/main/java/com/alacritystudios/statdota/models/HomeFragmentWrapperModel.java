package com.alacritystudios.statdota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * A wrapper model to hold all data required by the home fragment.
 */

public class HomeFragmentWrapperModel {

    @SerializedName("teams")
    @Expose
    private List<TeamsModel> teams;

    @SerializedName("proPlayers")
    @Expose
    private List<ProPlayersModel> proPlayersModels;

    @SerializedName("recentLeagueMatches")
    @Expose
    private List<RecentLeagueMatchModel> recentLeagueMatchModels;

    @SerializedName("pubMatches")
    @Expose
    private List<PubMatchesModel> pubMatches;

    @SerializedName("twitchClips")
    @Expose
    private List<TwitchClipsModel> twitchClips;

    @SerializedName("heroStats")
    @Expose
    private List<HeroStatsModel> heroStats;

    public List<TeamsModel> getTeams() {
        return teams;
    }

    public void setTeams(List<TeamsModel> teams) {
        this.teams = teams;
    }

    public List<PubMatchesModel> getPubMatches() {
        return pubMatches;
    }

    public void setPubMatches(List<PubMatchesModel> pubMatches) {
        this.pubMatches = pubMatches;
    }

    public List<TwitchClipsModel> getTwitchClips() {
        return twitchClips;
    }

    public void setTwitchClips(List<TwitchClipsModel> twitchClips) {
        this.twitchClips = twitchClips;
    }

    public List<HeroStatsModel> getHeroStats() {
        return heroStats;
    }

    public void setHeroStats(List<HeroStatsModel> heroStats) {
        this.heroStats = heroStats;
    }

    public List<ProPlayersModel> getProPlayersModels() {
        return proPlayersModels;
    }

    public void setProPlayersModels(List<ProPlayersModel> proPlayersModels) {
        this.proPlayersModels = proPlayersModels;
    }

    public List<RecentLeagueMatchModel> getRecentLeagueMatchModels() {
        return recentLeagueMatchModels;
    }

    public void setRecentLeagueMatchModels(List<RecentLeagueMatchModel> recentLeagueMatchModels) {
        this.recentLeagueMatchModels = recentLeagueMatchModels;
    }
}
