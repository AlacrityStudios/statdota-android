package com.alacritystudios.statdota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Model to store hero summary
 */

public class UserHeroSummaryModel {

    @SerializedName("hero_id")
    @Expose
    private String heroId;
    @SerializedName("last_played")
    @Expose
    private Integer lastPlayed;
    @SerializedName("games")
    @Expose
    private Integer games;
    @SerializedName("win")
    @Expose
    private Integer win;
    @SerializedName("with_games")
    @Expose
    private Integer withGames;
    @SerializedName("with_win")
    @Expose
    private Integer withWin;
    @SerializedName("against_games")
    @Expose
    private Integer againstGames;
    @SerializedName("against_win")
    @Expose
    private Integer againstWin;
    @SerializedName("hero_details")
    @Expose
    private HeroDetails heroDetails;
    @SerializedName("heroStats")
    @Expose
    private HeroStatsModel heroStatsModel;

    public String getHeroId() {
        return heroId;
    }

    public void setHeroId(String heroId) {
        this.heroId = heroId;
    }

    public Integer getLastPlayed() {
        return lastPlayed;
    }

    public void setLastPlayed(Integer lastPlayed) {
        this.lastPlayed = lastPlayed;
    }

    public Integer getGames() {
        return games;
    }

    public void setGames(Integer games) {
        this.games = games;
    }

    public Integer getWin() {
        return win;
    }

    public void setWin(Integer win) {
        this.win = win;
    }

    public Integer getWithGames() {
        return withGames;
    }

    public void setWithGames(Integer withGames) {
        this.withGames = withGames;
    }

    public Integer getWithWin() {
        return withWin;
    }

    public void setWithWin(Integer withWin) {
        this.withWin = withWin;
    }

    public Integer getAgainstGames() {
        return againstGames;
    }

    public void setAgainstGames(Integer againstGames) {
        this.againstGames = againstGames;
    }

    public Integer getAgainstWin() {
        return againstWin;
    }

    public void setAgainstWin(Integer againstWin) {
        this.againstWin = againstWin;
    }

    public HeroDetails getHeroDetails() {
        return heroDetails;
    }

    public void setHeroDetails(HeroDetails heroDetails) {
        this.heroDetails = heroDetails;
    }

    public HeroStatsModel getHeroStatsModel() {
        return heroStatsModel;
    }

    public void setHeroStatsModel(HeroStatsModel heroStatsModel) {
        this.heroStatsModel = heroStatsModel;
    }
}
