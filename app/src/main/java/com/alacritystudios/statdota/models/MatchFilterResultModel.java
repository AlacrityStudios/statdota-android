package com.alacritystudios.statdota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Holds the filters selected by the user.
 */

public class MatchFilterResultModel {

    @SerializedName("heroPlayedId")
    @Expose
    private Integer heroPlayedId;
    @SerializedName("winCondition")
    @Expose
    private Integer winCondition;
    @SerializedName("isRadiant")
    @Expose
    private Integer isRadiant;
    @SerializedName("alliedHeroIdList")
    @Expose
    private List<Integer> alliedHeroIdList;
    @SerializedName("againstHeroIdList")
    @Expose
    private List<Integer> againstHeroIdList;
    @SerializedName("lobbyType")
    @Expose
    private Integer lobbyType;
    @SerializedName("gameMode")
    @Expose
    private Integer gameMode;

    public MatchFilterResultModel() {

        this.heroPlayedId = -1;
        this.winCondition = -1;
        this.isRadiant = -1;
        this.lobbyType = -1;
        this.gameMode = -1;
        this.alliedHeroIdList = new ArrayList<>();
        this.againstHeroIdList = new ArrayList<>();
    }

    public Integer getHeroPlayedId() {
        return heroPlayedId;
    }

    public void setHeroPlayedId(Integer heroPlayedId) {
        this.heroPlayedId = heroPlayedId;
    }

    public Integer getWinCondition() {
        return winCondition;
    }

    public void setWinCondition(Integer winCondition) {
        this.winCondition = winCondition;
    }

    public List<Integer> getAlliedHeroIdList() {
        return alliedHeroIdList;
    }

    public void setAlliedHeroIdList(List<Integer> alliedHeroIdList) {
        this.alliedHeroIdList = alliedHeroIdList;
    }

    public List<Integer> getAgainstHeroIdList() {
        return againstHeroIdList;
    }

    public void setAgainstHeroIdList(List<Integer> againstHeroIdList) {
        this.againstHeroIdList = againstHeroIdList;
    }

    public Integer getLobbyType() {
        return lobbyType;
    }

    public void setLobbyType(Integer lobbyType) {
        this.lobbyType = lobbyType;
    }

    public Integer getGameMode() {
        return gameMode;
    }

    public void setGameMode(Integer gameMode) {
        this.gameMode = gameMode;
    }

    public Integer getIsRadiant() {
        return isRadiant;
    }

    public void setIsRadiant(Integer isRadiant) {
        this.isRadiant = isRadiant;
    }
}
