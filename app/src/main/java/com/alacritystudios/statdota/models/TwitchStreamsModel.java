package com.alacritystudios.statdota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Model to hold twitch streams data.
 */

public class TwitchStreamsModel {

    @SerializedName("_total")
    @Expose
    private Integer total;
    @SerializedName("streams")
    @Expose
    private List<Stream> streams = null;

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<Stream> getStreams() {
        return streams;
    }

    public void setStreams(List<Stream> streams) {
        this.streams = streams;
    }

    public class Stream {

        @SerializedName("_id")
        @Expose
        private Long id;
        @SerializedName("game")
        @Expose
        private String game;
        @SerializedName("viewers")
        @Expose
        private Integer viewers;
        @SerializedName("video_height")
        @Expose
        private Integer videoHeight;
        @SerializedName("average_fps")
        @Expose
        private Float averageFps;
        @SerializedName("delay")
        @Expose
        private Integer delay;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("is_playlist")
        @Expose
        private Boolean isPlaylist;
        @SerializedName("stream_type")
        @Expose
        private String streamType;
        @SerializedName("preview")
        @Expose
        private Preview preview;
        @SerializedName("channel")
        @Expose
        private Channel channel;
        @SerializedName("_links")
        @Expose
        private Links_ links;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getGame() {
            return game;
        }

        public void setGame(String game) {
            this.game = game;
        }

        public Integer getViewers() {
            return viewers;
        }

        public void setViewers(Integer viewers) {
            this.viewers = viewers;
        }

        public Integer getVideoHeight() {
            return videoHeight;
        }

        public void setVideoHeight(Integer videoHeight) {
            this.videoHeight = videoHeight;
        }

        public Float getAverageFps() {
            return averageFps;
        }

        public void setAverageFps(Float averageFps) {
            this.averageFps = averageFps;
        }

        public Integer getDelay() {
            return delay;
        }

        public void setDelay(Integer delay) {
            this.delay = delay;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public Boolean getIsPlaylist() {
            return isPlaylist;
        }

        public void setIsPlaylist(Boolean isPlaylist) {
            this.isPlaylist = isPlaylist;
        }

        public String getStreamType() {
            return streamType;
        }

        public void setStreamType(String streamType) {
            this.streamType = streamType;
        }

        public Preview getPreview() {
            return preview;
        }

        public void setPreview(Preview preview) {
            this.preview = preview;
        }

        public Channel getChannel() {
            return channel;
        }

        public void setChannel(Channel channel) {
            this.channel = channel;
        }

        public Links_ getLinks() {
            return links;
        }

        public void setLinks(Links_ links) {
            this.links = links;
        }
    }

    public class Links_ {

        @SerializedName("self")
        @Expose
        private String self;

        public String getSelf() {
            return self;
        }

        public void setSelf(String self) {
            this.self = self;
        }
    }

    public class Preview {

        @SerializedName("small")
        @Expose
        private String small;
        @SerializedName("medium")
        @Expose
        private String medium;
        @SerializedName("large")
        @Expose
        private String large;
        @SerializedName("template")
        @Expose
        private String template;

        public String getSmall() {
            return small;
        }

        public void setSmall(String small) {
            this.small = small;
        }

        public String getMedium() {
            return medium;
        }

        public void setMedium(String medium) {
            this.medium = medium;
        }

        public String getLarge() {
            return large;
        }

        public void setLarge(String large) {
            this.large = large;
        }

        public String getTemplate() {
            return template;
        }

        public void setTemplate(String template) {
            this.template = template;
        }

    }

    public class Channel {

        @SerializedName("mature")
        @Expose
        private Boolean mature;
        @SerializedName("partner")
        @Expose
        private Boolean partner;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("broadcaster_language")
        @Expose
        private String broadcasterLanguage;
        @SerializedName("display_name")
        @Expose
        private String displayName;
        @SerializedName("game")
        @Expose
        private String game;
        @SerializedName("language")
        @Expose
        private String language;
        @SerializedName("_id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("delay")
        @Expose
        private Object delay;
        @SerializedName("logo")
        @Expose
        private String logo;
        @SerializedName("banner")
        @Expose
        private Object banner;
        @SerializedName("video_banner")
        @Expose
        private String videoBanner;
        @SerializedName("background")
        @Expose
        private Object background;
        @SerializedName("profile_banner")
        @Expose
        private String profileBanner;
        @SerializedName("profile_banner_background_color")
        @Expose
        private String profileBannerBackgroundColor;
        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("views")
        @Expose
        private Integer views;
        @SerializedName("followers")
        @Expose
        private Integer followers;
        @SerializedName("_links")
        @Expose
        private Links links;

        public Boolean getMature() {
            return mature;
        }

        public void setMature(Boolean mature) {
            this.mature = mature;
        }

        public Boolean getPartner() {
            return partner;
        }

        public void setPartner(Boolean partner) {
            this.partner = partner;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getBroadcasterLanguage() {
            return broadcasterLanguage;
        }

        public void setBroadcasterLanguage(String broadcasterLanguage) {
            this.broadcasterLanguage = broadcasterLanguage;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

        public String getGame() {
            return game;
        }

        public void setGame(String game) {
            this.game = game;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getDelay() {
            return delay;
        }

        public void setDelay(Object delay) {
            this.delay = delay;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public Object getBanner() {
            return banner;
        }

        public void setBanner(Object banner) {
            this.banner = banner;
        }

        public String getVideoBanner() {
            return videoBanner;
        }

        public void setVideoBanner(String videoBanner) {
            this.videoBanner = videoBanner;
        }

        public Object getBackground() {
            return background;
        }

        public void setBackground(Object background) {
            this.background = background;
        }

        public String getProfileBanner() {
            return profileBanner;
        }

        public void setProfileBanner(String profileBanner) {
            this.profileBanner = profileBanner;
        }

        public String getProfileBannerBackgroundColor() {
            return profileBannerBackgroundColor;
        }

        public void setProfileBannerBackgroundColor(String profileBannerBackgroundColor) {
            this.profileBannerBackgroundColor = profileBannerBackgroundColor;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public Integer getViews() {
            return views;
        }

        public void setViews(Integer views) {
            this.views = views;
        }

        public Integer getFollowers() {
            return followers;
        }

        public void setFollowers(Integer followers) {
            this.followers = followers;
        }

        public Links getLinks() {
            return links;
        }

        public void setLinks(Links links) {
            this.links = links;
        }

    }

    public class Links {

        @SerializedName("self")
        @Expose
        private String self;
        @SerializedName("follows")
        @Expose
        private String follows;
        @SerializedName("commercial")
        @Expose
        private String commercial;
        @SerializedName("stream_key")
        @Expose
        private String streamKey;
        @SerializedName("chat")
        @Expose
        private String chat;
        @SerializedName("features")
        @Expose
        private String features;
        @SerializedName("subscriptions")
        @Expose
        private String subscriptions;
        @SerializedName("editors")
        @Expose
        private String editors;
        @SerializedName("teams")
        @Expose
        private String teams;
        @SerializedName("videos")
        @Expose
        private String videos;

        public String getSelf() {
            return self;
        }

        public void setSelf(String self) {
            this.self = self;
        }

        public String getFollows() {
            return follows;
        }

        public void setFollows(String follows) {
            this.follows = follows;
        }

        public String getCommercial() {
            return commercial;
        }

        public void setCommercial(String commercial) {
            this.commercial = commercial;
        }

        public String getStreamKey() {
            return streamKey;
        }

        public void setStreamKey(String streamKey) {
            this.streamKey = streamKey;
        }

        public String getChat() {
            return chat;
        }

        public void setChat(String chat) {
            this.chat = chat;
        }

        public String getFeatures() {
            return features;
        }

        public void setFeatures(String features) {
            this.features = features;
        }

        public String getSubscriptions() {
            return subscriptions;
        }

        public void setSubscriptions(String subscriptions) {
            this.subscriptions = subscriptions;
        }

        public String getEditors() {
            return editors;
        }

        public void setEditors(String editors) {
            this.editors = editors;
        }

        public String getTeams() {
            return teams;
        }

        public void setTeams(String teams) {
            this.teams = teams;
        }

        public String getVideos() {
            return videos;
        }

        public void setVideos(String videos) {
            this.videos = videos;
        }

    }
}
