package com.alacritystudios.statdota.models;

import com.alacritystudios.statdota.enums.FilterType;

/**
 * Model to store match filter details.
 */

public class MatchFilterHeadingModel {

    private String filterGeneralName;
    private FilterType filterType;
    private boolean hasUserChoices;

    public MatchFilterHeadingModel(String filterGeneralName, FilterType filterType, boolean hasUserChoices) {
        this.filterGeneralName = filterGeneralName;
        this.filterType = filterType;
        this.hasUserChoices = hasUserChoices;
    }

    public String getFilterGeneralName() {
        return filterGeneralName;
    }

    public void setFilterGeneralName(String filterGeneralName) {
        this.filterGeneralName = filterGeneralName;
    }

    public FilterType getFilterType() {
        return filterType;
    }

    public void setFilterType(FilterType filterType) {
        this.filterType = filterType;
    }

    public boolean isHasUserChoices() {
        return hasUserChoices;
    }

    public void setHasUserChoices(boolean hasUserChoices) {
        this.hasUserChoices = hasUserChoices;
    }
}
