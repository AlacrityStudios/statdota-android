package com.alacritystudios.statdota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 *  Model to store friends data.
 */

public class UserFriendsModel {
    @SerializedName("account_id")
    @Expose
    private Long accountId;
    @SerializedName("last_played")
    @Expose
    private Integer lastPlayed;
    @SerializedName("win")
    @Expose
    private Integer win;
    @SerializedName("games")
    @Expose
    private Integer games;
    @SerializedName("with_win")
    @Expose
    private Integer withWin;
    @SerializedName("with_games")
    @Expose
    private Integer withGames;
    @SerializedName("against_win")
    @Expose
    private Integer againstWin;
    @SerializedName("against_games")
    @Expose
    private Integer againstGames;
    @SerializedName("with_gpm_sum")
    @Expose
    private Integer withGpmSum;
    @SerializedName("with_xpm_sum")
    @Expose
    private Integer withXpmSum;
    @SerializedName("personaname")
    @Expose
    private String personaname;
    @SerializedName("last_login")
    @Expose
    private Object lastLogin;
    @SerializedName("avatar")
    @Expose
    private String avatar;

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Integer getLastPlayed() {
        return lastPlayed;
    }

    public void setLastPlayed(Integer lastPlayed) {
        this.lastPlayed = lastPlayed;
    }

    public Integer getWin() {
        return win;
    }

    public void setWin(Integer win) {
        this.win = win;
    }

    public Integer getGames() {
        return games;
    }

    public void setGames(Integer games) {
        this.games = games;
    }

    public Integer getWithWin() {
        return withWin;
    }

    public void setWithWin(Integer withWin) {
        this.withWin = withWin;
    }

    public Integer getWithGames() {
        return withGames;
    }

    public void setWithGames(Integer withGames) {
        this.withGames = withGames;
    }

    public Integer getAgainstWin() {
        return againstWin;
    }

    public void setAgainstWin(Integer againstWin) {
        this.againstWin = againstWin;
    }

    public Integer getAgainstGames() {
        return againstGames;
    }

    public void setAgainstGames(Integer againstGames) {
        this.againstGames = againstGames;
    }

    public Integer getWithGpmSum() {
        return withGpmSum;
    }

    public void setWithGpmSum(Integer withGpmSum) {
        this.withGpmSum = withGpmSum;
    }

    public Integer getWithXpmSum() {
        return withXpmSum;
    }

    public void setWithXpmSum(Integer withXpmSum) {
        this.withXpmSum = withXpmSum;
    }

    public String getPersonaname() {
        return personaname;
    }

    public void setPersonaname(String personaname) {
        this.personaname = personaname;
    }

    public Object getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Object lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
