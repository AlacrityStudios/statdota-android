package com.alacritystudios.statdota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Model to hold hero stats.
 */

public class HeroStatsModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("localized_name")
    @Expose
    private String localizedName;
    @SerializedName("primary_attr")
    @Expose
    private String primaryAttr;
    @SerializedName("attack_type")
    @Expose
    private String attackType;
    @SerializedName("roles")
    @Expose
    private List<String> roles = null;
    @SerializedName("img")
    @Expose
    private String img;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("base_health")
    @Expose
    private Double baseHealth;
    @SerializedName("base_health_regen")
    @Expose
    private Double baseHealthRegen;
    @SerializedName("base_mana")
    @Expose
    private Double baseMana;
    @SerializedName("base_mana_regen")
    @Expose
    private Double baseManaRegen;
    @SerializedName("base_armor")
    @Expose
    private Double baseArmor;
    @SerializedName("base_mr")
    @Expose
    private Double baseMr;
    @SerializedName("base_attack_min")
    @Expose
    private Double baseAttackMin;
    @SerializedName("base_attack_max")
    @Expose
    private Double baseAttackMax;
    @SerializedName("base_str")
    @Expose
    private Double baseStr;
    @SerializedName("base_agi")
    @Expose
    private Double baseAgi;
    @SerializedName("base_int")
    @Expose
    private Double baseInt;
    @SerializedName("str_gain")
    @Expose
    private Double strGain;
    @SerializedName("agi_gain")
    @Expose
    private Double agiGain;
    @SerializedName("int_gain")
    @Expose
    private Double intGain;
    @SerializedName("attack_range")
    @Expose
    private Double attackRange;
    @SerializedName("projectile_speed")
    @Expose
    private Double projectileSpeed;
    @SerializedName("attack_rate")
    @Expose
    private Double attackRate;
    @SerializedName("move_speed")
    @Expose
    private Double moveSpeed;
    @SerializedName("turn_rate")
    @Expose
    private Double turnRate;
    @SerializedName("cm_enabled")
    @Expose
    private Boolean cmEnabled;
    @SerializedName("legs")
    @Expose
    private Integer legs;
    @SerializedName("pro_win")
    @Expose
    private Integer proWin;
    @SerializedName("pro_pick")
    @Expose
    private Integer proPick;
    @SerializedName("pro_ban")
    @Expose
    private Integer proBan;
    @SerializedName("pro_pick_and_ban")
    @Expose
    private Integer proPickAndBan;
    @SerializedName("hero_id")
    @Expose
    private Integer heroId;
    @SerializedName("2000_pick")
    @Expose
    private Integer _2000Pick;
    @SerializedName("2000_win")
    @Expose
    private Integer _2000Win;
    @SerializedName("5000_pick")
    @Expose
    private Integer _5000Pick;
    @SerializedName("5000_win")
    @Expose
    private Integer _5000Win;
    @SerializedName("1000_pick")
    @Expose
    private Integer _1000Pick;
    @SerializedName("1000_win")
    @Expose
    private Integer _1000Win;
    @SerializedName("3000_pick")
    @Expose
    private Integer _3000Pick;
    @SerializedName("3000_win")
    @Expose
    private Integer _3000Win;
    @SerializedName("4000_pick")
    @Expose
    private Integer _4000Pick;
    @SerializedName("4000_win")
    @Expose
    private Integer _4000Win;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocalizedName() {
        return localizedName;
    }

    public void setLocalizedName(String localizedName) {
        this.localizedName = localizedName;
    }

    public String getPrimaryAttr() {
        return primaryAttr;
    }

    public void setPrimaryAttr(String primaryAttr) {
        this.primaryAttr = primaryAttr;
    }

    public String getAttackType() {
        return attackType;
    }

    public void setAttackType(String attackType) {
        this.attackType = attackType;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Double getBaseHealth() {
        return baseHealth;
    }

    public void setBaseHealth(Double baseHealth) {
        this.baseHealth = baseHealth;
    }

    public Double getBaseHealthRegen() {
        return baseHealthRegen;
    }

    public void setBaseHealthRegen(Double baseHealthRegen) {
        this.baseHealthRegen = baseHealthRegen;
    }

    public Double getBaseMana() {
        return baseMana;
    }

    public void setBaseMana(Double baseMana) {
        this.baseMana = baseMana;
    }

    public Double getBaseManaRegen() {
        return baseManaRegen;
    }

    public void setBaseManaRegen(Double baseManaRegen) {
        this.baseManaRegen = baseManaRegen;
    }

    public Double getBaseArmor() {
        return baseArmor;
    }

    public void setBaseArmor(Double baseArmor) {
        this.baseArmor = baseArmor;
    }

    public Double getBaseMr() {
        return baseMr;
    }

    public void setBaseMr(Double baseMr) {
        this.baseMr = baseMr;
    }

    public Double getBaseAttackMin() {
        return baseAttackMin;
    }

    public void setBaseAttackMin(Double baseAttackMin) {
        this.baseAttackMin = baseAttackMin;
    }

    public Double getBaseAttackMax() {
        return baseAttackMax;
    }

    public void setBaseAttackMax(Double baseAttackMax) {
        this.baseAttackMax = baseAttackMax;
    }

    public Double getBaseStr() {
        return baseStr;
    }

    public void setBaseStr(Double baseStr) {
        this.baseStr = baseStr;
    }

    public Double getBaseAgi() {
        return baseAgi;
    }

    public void setBaseAgi(Double baseAgi) {
        this.baseAgi = baseAgi;
    }

    public Double getBaseInt() {
        return baseInt;
    }

    public void setBaseInt(Double baseInt) {
        this.baseInt = baseInt;
    }

    public Double getStrGain() {
        return strGain;
    }

    public void setStrGain(Double strGain) {
        this.strGain = strGain;
    }

    public Double getAgiGain() {
        return agiGain;
    }

    public void setAgiGain(Double agiGain) {
        this.agiGain = agiGain;
    }

    public Double getIntGain() {
        return intGain;
    }

    public void setIntGain(Double intGain) {
        this.intGain = intGain;
    }

    public Double getAttackRange() {
        return attackRange;
    }

    public void setAttackRange(Double attackRange) {
        this.attackRange = attackRange;
    }

    public Double getProjectileSpeed() {
        return projectileSpeed;
    }

    public void setProjectileSpeed(Double projectileSpeed) {
        this.projectileSpeed = projectileSpeed;
    }

    public Double getAttackRate() {
        return attackRate;
    }

    public void setAttackRate(Double attackRate) {
        this.attackRate = attackRate;
    }

    public Double getMoveSpeed() {
        return moveSpeed;
    }

    public void setMoveSpeed(Double moveSpeed) {
        this.moveSpeed = moveSpeed;
    }

    public Double getTurnRate() {
        return turnRate;
    }

    public void setTurnRate(Double turnRate) {
        this.turnRate = turnRate;
    }

    public Boolean getCmEnabled() {
        return cmEnabled;
    }

    public void setCmEnabled(Boolean cmEnabled) {
        this.cmEnabled = cmEnabled;
    }

    public Integer getLegs() {
        return legs;
    }

    public void setLegs(Integer legs) {
        this.legs = legs;
    }

    public Integer getProWin() {
        return proWin;
    }

    public void setProWin(Integer proWin) {
        this.proWin = proWin;
    }

    public Integer getProPick() {
        return proPick;
    }

    public void setProPick(Integer proPick) {
        this.proPick = proPick;
    }

    public Integer getHeroId() {
        return heroId;
    }

    public void setHeroId(Integer heroId) {
        this.heroId = heroId;
    }

    public Integer getProBan() {
        return proBan;
    }

    public void setProBan(Integer proBan) {
        this.proBan = proBan;
    }

    public Integer getProPickAndBan() {
        return proPickAndBan;
    }

    public void setProPickAndBan(Integer proPickAndBan) {
        this.proPickAndBan = proPickAndBan;
    }

    public Integer get2000Pick() {
        return _2000Pick;
    }

    public void set2000Pick(Integer _2000Pick) {
        this._2000Pick = _2000Pick;
    }

    public Integer get2000Win() {
        return _2000Win;
    }

    public void set2000Win(Integer _2000Win) {
        this._2000Win = _2000Win;
    }

    public Integer get5000Pick() {
        return _5000Pick;
    }

    public void set5000Pick(Integer _5000Pick) {
        this._5000Pick = _5000Pick;
    }

    public Integer get5000Win() {
        return _5000Win;
    }

    public void set5000Win(Integer _5000Win) {
        this._5000Win = _5000Win;
    }

    public Integer get1000Pick() {
        return _1000Pick;
    }

    public void set1000Pick(Integer _1000Pick) {
        this._1000Pick = _1000Pick;
    }

    public Integer get1000Win() {
        return _1000Win;
    }

    public void set1000Win(Integer _1000Win) {
        this._1000Win = _1000Win;
    }

    public Integer get3000Pick() {
        return _3000Pick;
    }

    public void set3000Pick(Integer _3000Pick) {
        this._3000Pick = _3000Pick;
    }

    public Integer get3000Win() {
        return _3000Win;
    }

    public void set3000Win(Integer _3000Win) {
        this._3000Win = _3000Win;
    }

    public Integer get4000Pick() {
        return _4000Pick;
    }

    public void set4000Pick(Integer _4000Pick) {
        this._4000Pick = _4000Pick;
    }

    public Integer get4000Win() {
        return _4000Win;
    }

    public void set4000Win(Integer _4000Win) {
        this._4000Win = _4000Win;
    }
}
