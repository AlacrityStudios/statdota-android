package com.alacritystudios.statdota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Model to store details about high MMR pub stars.
 */

public class HighMmrPubStarsModel {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("personaname")
    @Expose
    private String personaname;
    @SerializedName("loccountrycode")
    @Expose
    private String locCountryCode;
    @SerializedName("avatarfull")
    @Expose
    private String avatarfull;
    @SerializedName("rating")
    @Expose
    private Integer rating;
    @SerializedName("account_id")
    @Expose
    private Integer accountId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPersonaname() {
        return personaname;
    }

    public void setPersonaname(String personaname) {
        this.personaname = personaname;
    }

    public String getLocCountryCode() {
        return locCountryCode;
    }

    public void setLocCountryCode(String locCountryCode) {
        this.locCountryCode = locCountryCode;
    }

    public String getAvatarfull() {
        return avatarfull;
    }

    public void setAvatarfull(String avatarfull) {
        this.avatarfull = avatarfull;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }
}
