package com.alacritystudios.statdota.models;

/**
 * Model to hold body details
 */

public class MatchesFilterBodyModel {

    private String displayName;
    private String value;
    private boolean isSelected;


    public MatchesFilterBodyModel(String displayName, String value, boolean isSelected) {
        this.displayName = displayName;
        this.value = value;
        this.isSelected = isSelected;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
