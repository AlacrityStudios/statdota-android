package com.alacritystudios.statdota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Stores details about
 */

public class RecentLeagueMatchModel {

    @SerializedName("match_id")
    @Expose
    private Long matchId;
    @SerializedName("duration")
    @Expose
    private Long duration;
    @SerializedName("start_time")
    @Expose
    private Long startTime;
    @SerializedName("radiant_team_id")
    @Expose
    private Long radiantTeamId;
    @SerializedName("radiant_name")
    @Expose
    private String radiantName;
    @SerializedName("dire_team_id")
    @Expose
    private Long direTeamId;
    @SerializedName("dire_name")
    @Expose
    private String direName;
    @SerializedName("leagueid")
    @Expose
    private Long leagueid;
    @SerializedName("league_name")
    @Expose
    private String leagueName;
    @SerializedName("series_id")
    @Expose
    private Long seriesId;
    @SerializedName("series_type")
    @Expose
    private Integer seriesType;
    @SerializedName("radiant_win")
    @Expose
    private Boolean radiantWin;
    @SerializedName("game_number")
    @Expose
    private Integer gameNumber;
    @SerializedName("radiant_team_tag")
    @Expose
    private String radiantTeamTag;
    @SerializedName("radiant_team_logo")
    @Expose
    private String radiantTeamLogo;
    @SerializedName("dire_team_tag")
    @Expose
    private String direTeamTag;
    @SerializedName("dire_team_logo")
    @Expose
    private String direTeamLogo;
    @SerializedName("radiant_score")
    @Expose
    private Integer radiantScore;
    @SerializedName("dire_score")
    @Expose
    private Integer direScore;

    public Long getMatchId() {
        return matchId;
    }

    public void setMatchId(Long matchId) {
        this.matchId = matchId;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getRadiantTeamId() {
        return radiantTeamId;
    }

    public void setRadiantTeamId(Long radiantTeamId) {
        this.radiantTeamId = radiantTeamId;
    }

    public String getRadiantName() {
        return radiantName;
    }

    public void setRadiantName(String radiantName) {
        this.radiantName = radiantName;
    }

    public Long getDireTeamId() {
        return direTeamId;
    }

    public void setDireTeamId(Long direTeamId) {
        this.direTeamId = direTeamId;
    }

    public String getDireName() {
        return direName;
    }

    public void setDireName(String direName) {
        this.direName = direName;
    }

    public Long getLeagueid() {
        return leagueid;
    }

    public void setLeagueid(Long leagueid) {
        this.leagueid = leagueid;
    }

    public String getLeagueName() {
        return leagueName;
    }

    public void setLeagueName(String leagueName) {
        this.leagueName = leagueName;
    }

    public Long getSeriesId() {
        return seriesId;
    }

    public void setSeriesId(Long seriesId) {
        this.seriesId = seriesId;
    }

    public Integer getSeriesType() {
        return seriesType;
    }

    public void setSeriesType(Integer seriesType) {
        this.seriesType = seriesType;
    }

    public Boolean getRadiantWin() {
        return radiantWin;
    }

    public void setRadiantWin(Boolean radiantWin) {
        this.radiantWin = radiantWin;
    }

    public Integer getGameNumber() {
        return gameNumber;
    }

    public void setGameNumber(Integer gameNumber) {
        this.gameNumber = gameNumber;
    }

    public String getRadiantTeamTag() {
        return radiantTeamTag;
    }

    public void setRadiantTeamTag(String radiantTeamTag) {
        this.radiantTeamTag = radiantTeamTag;
    }

    public String getRadiantTeamLogo() {
        return radiantTeamLogo;
    }

    public void setRadiantTeamLogo(String radiantTeamLogo) {
        this.radiantTeamLogo = radiantTeamLogo;
    }

    public String getDireTeamTag() {
        return direTeamTag;
    }

    public void setDireTeamTag(String direTeamTag) {
        this.direTeamTag = direTeamTag;
    }

    public String getDireTeamLogo() {
        return direTeamLogo;
    }

    public void setDireTeamLogo(String direTeamLogo) {
        this.direTeamLogo = direTeamLogo;
    }

    public Integer getRadiantScore() {
        return radiantScore;
    }

    public void setRadiantScore(Integer radiantScore) {
        this.radiantScore = radiantScore;
    }

    public Integer getDireScore() {
        return direScore;
    }

    public void setDireScore(Integer direScore) {
        this.direScore = direScore;
    }
}
