package com.alacritystudios.statdota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Container model to store application data that is pulled from backend.
 */

public class ApplicationMetadataModel {

    @SerializedName("hero_details")
    @Expose
    private List<HeroDetails> heroDetailsList;
    @SerializedName("item_details")
    @Expose
    private List<ItemDetails> itemDetailsList;

    public List<HeroDetails> getHeroDetailsList() {
        return heroDetailsList;
    }

    public void setHeroDetailsList(List<HeroDetails> heroDetailsList) {
        this.heroDetailsList = heroDetailsList;
    }

    public List<ItemDetails> getItemDetailsList() {
        return itemDetailsList;
    }

    public void setItemDetailsList(List<ItemDetails> itemDetailsList) {
        this.itemDetailsList = itemDetailsList;
    }
}
