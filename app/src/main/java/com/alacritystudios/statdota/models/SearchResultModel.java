package com.alacritystudios.statdota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Model to hold search results returned by statDota back-end.
 */

public class SearchResultModel {

    @SerializedName("playerSearchResults")
    @Expose
    private List<PlayerSearchResultModel> playersSearchResultModelList;

    @SerializedName("teamSearchResults")
    @Expose
    private List<TeamsModel> teamsSearchResultModelList;

    public List<PlayerSearchResultModel> getPlayersSearchResultModelList() {
        return playersSearchResultModelList;
    }

    public void setPlayersSearchResultModelList(List<PlayerSearchResultModel> playersSearchResultModelList) {
        this.playersSearchResultModelList = playersSearchResultModelList;
    }

    public List<TeamsModel> getTeamsSearchResultModelList() {
        return teamsSearchResultModelList;
    }

    public void setTeamsSearchResultModelList(List<TeamsModel> teamsSearchResultModelList) {
        this.teamsSearchResultModelList = teamsSearchResultModelList;
    }
}
