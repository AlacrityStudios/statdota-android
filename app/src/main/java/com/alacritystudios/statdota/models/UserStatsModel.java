package com.alacritystudios.statdota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Model to store all user stats.
 */

public class UserStatsModel {
    @SerializedName("mean_gpm")
    @Expose
    private Double meanGpm;
    @SerializedName("mean_xppm")
    @Expose
    private Double meanXppm;
    @SerializedName("mean_lasthits")
    @Expose
    private Integer meanLasthits;
    @SerializedName("rampages")
    @Expose
    private Integer rampages;
    @SerializedName("first_blood_claimed")
    @Expose
    private Long firstBloodClaimed;
    @SerializedName("first_blood_given")
    @Expose
    private Long firstBloodGiven;
    @SerializedName("couriers_killed")
    @Expose
    private Integer couriersKilled;
    @SerializedName("aegises_snatched")
    @Expose
    private Integer aegisesSnatched;
    @SerializedName("creeps_stacked")
    @Expose
    private Integer creepsStacked;
    @SerializedName("cheeses_eaten")
    @Expose
    private Integer cheesesEaten;
    @SerializedName("fight_score")
    @Expose
    private Double fightScore;
    @SerializedName("farm_score")
    @Expose
    private Double farmScore;
    @SerializedName("support_score")
    @Expose
    private Double supportScore;
    @SerializedName("push_score")
    @Expose
    private Double pushScore;
    @SerializedName("versatility_score")
    @Expose
    private Double versatilityScore;
    @SerializedName("solo_competitive_rank")
    @Expose
    private String soloCompetitiveRank;
    @SerializedName("competitive_rank")
    @Expose
    private String competitiveRank;
    @SerializedName("mmr_estimate")
    @Expose
    private MmrEstimate mmrEstimate;
    @SerializedName("matches_won")
    @Expose
    private Integer matchesWon;
    @SerializedName("matches_lost")
    @Expose
    private Integer matchesLost;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("persona_name")
    @Expose
    private String personaName;
    @SerializedName("avatar_full")
    @Expose
    private String avatarFull;
    @SerializedName("persona_state")
    @Expose
    private Integer personaState;
    @SerializedName("profile_url")
    @Expose
    private String profileUrl;
    @SerializedName("twitch_channel_name")
    @Expose
    private String twitchChannelName;
    @SerializedName("twitch_clips")
    @Expose
    private List<TwitchClipsModel> twitchClips;

    public Double getMeanGpm() {
        return meanGpm;
    }

    public void setMeanGpm(Double meanGpm) {
        this.meanGpm = meanGpm;
    }

    public Double getMeanXppm() {
        return meanXppm;
    }

    public void setMeanXppm(Double meanXppm) {
        this.meanXppm = meanXppm;
    }

    public Integer getMeanLasthits() {
        return meanLasthits;
    }

    public void setMeanLasthits(Integer meanLasthits) {
        this.meanLasthits = meanLasthits;
    }

    public Integer getRampages() {
        return rampages;
    }

    public void setRampages(Integer rampages) {
        this.rampages = rampages;
    }

    public Long getFirstBloodClaimed() {
        return firstBloodClaimed;
    }

    public void setFirstBloodClaimed(Long firstBloodClaimed) {
        this.firstBloodClaimed = firstBloodClaimed;
    }

    public Long getFirstBloodGiven() {
        return firstBloodGiven;
    }

    public void setFirstBloodGiven(Long firstBloodGiven) {
        this.firstBloodGiven = firstBloodGiven;
    }

    public Integer getCouriersKilled() {
        return couriersKilled;
    }

    public void setCouriersKilled(Integer couriersKilled) {
        this.couriersKilled = couriersKilled;
    }

    public Integer getAegisesSnatched() {
        return aegisesSnatched;
    }

    public void setAegisesSnatched(Integer aegisesSnatched) {
        this.aegisesSnatched = aegisesSnatched;
    }

    public Integer getCreepsStacked() {
        return creepsStacked;
    }

    public void setCreepsStacked(Integer creepsStacked) {
        this.creepsStacked = creepsStacked;
    }

    public Integer getCheesesEaten() {
        return cheesesEaten;
    }

    public void setCheesesEaten(Integer cheesesEaten) {
        this.cheesesEaten = cheesesEaten;
    }

    public Double getFightScore() {
        return fightScore;
    }

    public void setFightScore(Double fightScore) {
        this.fightScore = fightScore;
    }

    public Double getFarmScore() {
        return farmScore;
    }

    public void setFarmScore(Double farmScore) {
        this.farmScore = farmScore;
    }

    public Double getSupportScore() {
        return supportScore;
    }

    public void setSupportScore(Double supportScore) {
        this.supportScore = supportScore;
    }

    public Double getPushScore() {
        return pushScore;
    }

    public void setPushScore(Double pushScore) {
        this.pushScore = pushScore;
    }

    public Double getVersatilityScore() {
        return versatilityScore;
    }

    public void setVersatilityScore(Double versatilityScore) {
        this.versatilityScore = versatilityScore;
    }

    public String getSoloCompetitiveRank() {
        return soloCompetitiveRank;
    }

    public void setSoloCompetitiveRank(String soloCompetitiveRank) {
        this.soloCompetitiveRank = soloCompetitiveRank;
    }

    public String getCompetitiveRank() {
        return competitiveRank;
    }

    public void setCompetitiveRank(String competitiveRank) {
        this.competitiveRank = competitiveRank;
    }

    public MmrEstimate getMmrEstimate() {
        return mmrEstimate;
    }

    public void setMmrEstimate(MmrEstimate mmrEstimate) {
        this.mmrEstimate = mmrEstimate;
    }

    public Integer getMatchesLost() {
        return matchesLost;
    }

    public void setMatchesLost(Integer matchesLost) {
        this.matchesLost = matchesLost;
    }

    public Integer getMatchesWon() {
        return matchesWon;
    }

    public void setMatchesWon(Integer matchesWon) {
        this.matchesWon = matchesWon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPersonaName() {
        return personaName;
    }

    public void setPersonaName(String personaName) {
        this.personaName = personaName;
    }

    public String getAvatarFull() {
        return avatarFull;
    }

    public void setAvatarFull(String avatarFull) {
        this.avatarFull = avatarFull;
    }

    public Integer getPersonaState() {
        return personaState;
    }

    public void setPersonaState(Integer personaState) {
        this.personaState = personaState;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getTwitchChannelName() {
        return twitchChannelName;
    }

    public void setTwitchChannelName(String twitchChannelName) {
        this.twitchChannelName = twitchChannelName;
    }

    public List<TwitchClipsModel> getTwitchClips() {
        return twitchClips;
    }

    public void setTwitchClips(List<TwitchClipsModel> twitchClips) {
        this.twitchClips = twitchClips;
    }

    public class MmrEstimate {

        @SerializedName("estimate")
        @Expose
        private Double estimate;

        public Double getEstimate() {
            return estimate;
        }

        public void setEstimate(Double estimate) {
            this.estimate = estimate;
        }
    }
}
