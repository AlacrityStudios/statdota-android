package com.alacritystudios.statdota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Model to store pub matches related data.
 */

public class PubMatchesModel {

    @SerializedName("match_id")
    @Expose
    private Long matchId;
    @SerializedName("match_seq_num")
    @Expose
    private Long matchSeqNum;
    @SerializedName("radiant_win")
    @Expose
    private Boolean radiantWin;
    @SerializedName("start_time")
    @Expose
    private Long startTime;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("avg_mmr")
    @Expose
    private Integer avgMmr;
    @SerializedName("num_mmr")
    @Expose
    private Integer numMmr;
    @SerializedName("lobby_type")
    @Expose
    private Integer lobbyType;
    @SerializedName("game_mode")
    @Expose
    private Integer gameMode;
    @SerializedName("radiant_team")
    @Expose
    private String radiantTeam;
    @SerializedName("dire_team")
    @Expose
    private String direTeam;

    private List<HeroDetails> mRadiantHeroDetails;

    private List<HeroDetails> mDireHeroDetails;

    public Long getMatchId() {
        return matchId;
    }

    public void setMatchId(Long matchId) {
        this.matchId = matchId;
    }

    public Long getMatchSeqNum() {
        return matchSeqNum;
    }

    public void setMatchSeqNum(Long matchSeqNum) {
        this.matchSeqNum = matchSeqNum;
    }

    public Boolean getRadiantWin() {
        return radiantWin;
    }

    public void setRadiantWin(Boolean radiantWin) {
        this.radiantWin = radiantWin;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getAvgMmr() {
        return avgMmr;
    }

    public void setAvgMmr(Integer avgMmr) {
        this.avgMmr = avgMmr;
    }

    public Integer getNumMmr() {
        return numMmr;
    }

    public void setNumMmr(Integer numMmr) {
        this.numMmr = numMmr;
    }

    public Integer getLobbyType() {
        return lobbyType;
    }

    public void setLobbyType(Integer lobbyType) {
        this.lobbyType = lobbyType;
    }

    public Integer getGameMode() {
        return gameMode;
    }

    public void setGameMode(Integer gameMode) {
        this.gameMode = gameMode;
    }

    public String getRadiantTeam() {
        return radiantTeam;
    }

    public void setRadiantTeam(String radiantTeam) {
        this.radiantTeam = radiantTeam;
    }

    public String getDireTeam() {
        return direTeam;
    }

    public void setDireTeam(String direTeam) {
        this.direTeam = direTeam;
    }

    public List<HeroDetails> getmRadiantHeroDetails() {
        return mRadiantHeroDetails;
    }

    public void setmRadiantHeroDetails(List<HeroDetails> mRadiantHeroDetails) {
        this.mRadiantHeroDetails = mRadiantHeroDetails;
    }

    public List<HeroDetails> getmDireHeroDetails() {
        return mDireHeroDetails;
    }

    public void setmDireHeroDetails(List<HeroDetails> mDireHeroDetails) {
        this.mDireHeroDetails = mDireHeroDetails;
    }
}
