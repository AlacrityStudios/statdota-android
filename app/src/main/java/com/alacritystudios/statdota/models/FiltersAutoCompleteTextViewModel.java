package com.alacritystudios.statdota.models;

/**
 * Model to hold suggestions info.
 */

public class FiltersAutoCompleteTextViewModel {

    private String name;
    private String logoUrl;
    private Long id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public FiltersAutoCompleteTextViewModel(String name, String logoUrl, Long id) {
        this.name = name;
        this.logoUrl = logoUrl;
        this.id = id;
    }
}
