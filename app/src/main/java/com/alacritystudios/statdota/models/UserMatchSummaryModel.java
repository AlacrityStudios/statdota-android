package com.alacritystudios.statdota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Holds match summary information
 */

public class UserMatchSummaryModel {

    @SerializedName("match_id")
    @Expose
    private Long matchId;
    @SerializedName("player_slot")
    @Expose
    private Integer playerSlot;
    @SerializedName("radiant_win")
    @Expose
    private Boolean radiantWin;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("game_mode")
    @Expose
    private Integer gameMode;
    @SerializedName("lobby_type")
    @Expose
    private Integer lobbyType;
    @SerializedName("kills")
    @Expose
    private Integer kills;
    @SerializedName("deaths")
    @Expose
    private Integer deaths;
    @SerializedName("assists")
    @Expose
    private Integer assists;
    @SerializedName("gold_per_min")
    @Expose
    private Integer goldPerMin;
    @SerializedName("xp_per_min")
    @Expose
    private Integer xpPerMin;
    @SerializedName("lane_role")
    @Expose
    private Integer laneRole;
    @SerializedName("start_time")
    @Expose
    private Long startTime;
    @SerializedName("player_slots")
    @Expose
    private List<PlayerSlot> playerSlots;

    public Long getMatchId() {
        return matchId;
    }

    public void setMatchId(Long matchId) {
        this.matchId = matchId;
    }

    public Integer getPlayerSlot() {
        return playerSlot;
    }

    public void setPlayerSlot(Integer playerSlot) {
        this.playerSlot = playerSlot;
    }

    public Boolean getRadiantWin() {
        return radiantWin;
    }

    public void setRadiantWin(Boolean radiantWin) {
        this.radiantWin = radiantWin;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getGameMode() {
        return gameMode;
    }

    public void setGameMode(Integer gameMode) {
        this.gameMode = gameMode;
    }

    public Integer getLobbyType() {
        return lobbyType;
    }

    public void setLobbyType(Integer lobbyType) {
        this.lobbyType = lobbyType;
    }

    public Integer getKills() {
        return kills;
    }

    public void setKills(Integer kills) {
        this.kills = kills;
    }

    public Integer getDeaths() {
        return deaths;
    }

    public void setDeaths(Integer deaths) {
        this.deaths = deaths;
    }

    public Integer getAssists() {
        return assists;
    }

    public void setAssists(Integer assists) {
        this.assists = assists;
    }

    public Integer getGoldPerMin() {
        return goldPerMin;
    }

    public void setGoldPerMin(Integer goldPerMin) {
        this.goldPerMin = goldPerMin;
    }

    public Integer getXpPerMin() {
        return xpPerMin;
    }

    public void setXpPerMin(Integer xpPerMin) {
        this.xpPerMin = xpPerMin;
    }

    public Integer getLaneRole() {
        return laneRole;
    }

    public void setLaneRole(Integer laneRole) {
        this.laneRole = laneRole;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public List<PlayerSlot> getPlayerSlots() {
        return playerSlots;
    }

    public void setPlayerSlots(List<PlayerSlot> playerSlots) {
        this.playerSlots = playerSlots;
    }

    public class PlayerSlot {


        @SerializedName("account_id")
        @Expose
        private Long accountId;
        @SerializedName("hero_id")
        @Expose
        private Integer heroId;
        @SerializedName("player_slot")
        @Expose
        private Integer playerSlot;
        @SerializedName("hero_localised_name")
        @Expose
        private String heroLocalisedName;
        @SerializedName("hero_image_url")
        @Expose
        private String heroImageUrl;

        public Long getAccountId() {
            return accountId;
        }

        public void setAccountId(Long accountId) {
            this.accountId = accountId;
        }

        public Integer getHeroId() {
            return heroId;
        }

        public void setHeroId(Integer heroId) {
            this.heroId = heroId;
        }

        public Integer getPlayerSlot() {
            return playerSlot;
        }

        public void setPlayerSlot(Integer playerSlot) {
            this.playerSlot = playerSlot;
        }

        public String getHeroLocalisedName() {
            return heroLocalisedName;
        }

        public void setHeroLocalisedName(String heroLocalisedName) {
            this.heroLocalisedName = heroLocalisedName;
        }

        public String getHeroImageUrl() {
            return heroImageUrl;
        }

        public void setHeroImageUrl(String heroImageUrl) {
            this.heroImageUrl = heroImageUrl;
        }
    }
}
