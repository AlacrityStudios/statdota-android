package com.alacritystudios.statdota.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Model to hold pro players information.
 */

public class ProPlayersModel {

    @SerializedName("solo_competitive_rank")
    @Expose
    private Integer soloCompetitiveRank;
    @SerializedName("twitch_channel_name")
    @Expose
    private String twitchChannelName;
    @SerializedName("locked_until")
    @Expose
    private Integer lockedUntil;
    @SerializedName("is_pro")
    @Expose
    private Boolean isPro;
    @SerializedName("is_locked")
    @Expose
    private Boolean isLocked;
    @SerializedName("team_tag")
    @Expose
    private String teamTag;
    @SerializedName("team_name")
    @Expose
    private String teamName;
    @SerializedName("team_id")
    @Expose
    private Integer teamId;
    @SerializedName("country_code")
    @Expose
    private String countryCode;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("full_history_time")
    @Expose
    private Object fullHistoryTime;
    @SerializedName("last_login")
    @Expose
    private Object lastLogin;
    @SerializedName("personaname")
    @Expose
    private String personaname;
    @SerializedName("profileurl")
    @Expose
    private String profileurl;
    @SerializedName("avatarfull")
    @Expose
    private String avatarfull;
    @SerializedName("steamid")
    @Expose
    private String steamid;
    @SerializedName("account_id")
    @Expose
    private Integer accountId;

    public Integer getSoloCompetitiveRank() {
        return soloCompetitiveRank;
    }

    public void setSoloCompetitiveRank(Integer soloCompetitiveRank) {
        this.soloCompetitiveRank = soloCompetitiveRank;
    }

    public String getTwitchChannelName() {
        return twitchChannelName;
    }

    public void setTwitchChannelName(String twitchChannelName) {
        this.twitchChannelName = twitchChannelName;
    }

    public Integer getLockedUntil() {
        return lockedUntil;
    }

    public void setLockedUntil(Integer lockedUntil) {
        this.lockedUntil = lockedUntil;
    }

    public Boolean getIsPro() {
        return isPro;
    }

    public void setIsPro(Boolean isPro) {
        this.isPro = isPro;
    }

    public Boolean getIsLocked() {
        return isLocked;
    }

    public void setIsLocked(Boolean isLocked) {
        this.isLocked = isLocked;
    }

    public String getTeamTag() {
        return teamTag;
    }

    public void setTeamTag(String teamTag) {
        this.teamTag = teamTag;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getFullHistoryTime() {
        return fullHistoryTime;
    }

    public void setFullHistoryTime(Object fullHistoryTime) {
        this.fullHistoryTime = fullHistoryTime;
    }

    public Object getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Object lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getPersonaname() {
        return personaname;
    }

    public void setPersonaname(String personaname) {
        this.personaname = personaname;
    }

    public String getProfileurl() {
        return profileurl;
    }

    public void setProfileurl(String profileurl) {
        this.profileurl = profileurl;
    }

    public String getAvatarfull() {
        return avatarfull;
    }

    public void setAvatarfull(String avatarfull) {
        this.avatarfull = avatarfull;
    }

    public String getSteamid() {
        return steamid;
    }

    public void setSteamid(String steamid) {
        this.steamid = steamid;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }
}
