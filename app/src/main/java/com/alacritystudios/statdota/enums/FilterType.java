package com.alacritystudios.statdota.enums;

/**
 * Enum to store match filter types.
 */

public enum FilterType {

    MATCH_FILTER_HERO_PLAYED,
    MATCH_FILTER_RESULT,
    MATCH_FILTER_ALLIED_HERO,
    MATCH_FILTER_AGAINST_HERO,
    MATCH_FILTER_SIDE,
    MATCH_FILTER_GAME_MODE,
    MATCH_FILTER_LOBBY_TYPE
}
