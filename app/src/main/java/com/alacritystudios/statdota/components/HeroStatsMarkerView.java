package com.alacritystudios.statdota.components;

import android.content.Context;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.models.HeroStatsModel;
import com.alacritystudios.statdota.utils.ApplicationUtils;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;

/**
 * Hard coded marker view for hero stats.
 */

public class HeroStatsMarkerView extends MarkerView {

    private TextView mMatchesPlayedTextView;
    private TextView mMatchesWonTextView;
    private TextView mMatchesLostTextView;
    private TextView mHeadingTextView;
    private HeroStatsModel mHeroStatsModel;
    private MPPointF mOffset;
    private Context mContext;

    public HeroStatsMarkerView(Context context, int layoutResource, HeroStatsModel heroStatsModel) {
        super(context, layoutResource);
        mContext = context;
        mHeroStatsModel = heroStatsModel;
        mHeadingTextView = (TextView) findViewById(R.id.marker_view_heading_text_view);
        mMatchesPlayedTextView = (TextView) findViewById(R.id.matches_played_text_view);
        mMatchesWonTextView = (TextView) findViewById(R.id.matches_won_text_view);
        mMatchesLostTextView = (TextView) findViewById(R.id.matches_lost_text_view);
    }

    @Override
    public void refreshContent(Entry e, Highlight highlight) {
        if(e.getX() == 0f) {
            mHeadingTextView.setText(mContext.getString(R.string.pro));
            mMatchesPlayedTextView.setText(ApplicationUtils.makePlaceholderIntegerString(mHeroStatsModel.getProPick(), "0"));
            mMatchesWonTextView.setText(ApplicationUtils.makePlaceholderIntegerString(mHeroStatsModel.getProWin(), "0"));
            mMatchesLostTextView.setText(String.valueOf((mHeroStatsModel.getProPick() != null? mHeroStatsModel.getProPick() : 0) - (mHeroStatsModel.getProWin() != null? mHeroStatsModel.getProWin() : 0)));
        } else if(e.getX() == 1f) {
            mHeadingTextView.setText(mContext.getString(R.string._1_thousand_mmr));
            mMatchesPlayedTextView.setText(ApplicationUtils.makePlaceholderIntegerString(mHeroStatsModel.get1000Pick(), "0"));
            mMatchesWonTextView.setText(ApplicationUtils.makePlaceholderIntegerString(mHeroStatsModel.get1000Win(), "0"));
            mMatchesLostTextView.setText(String.valueOf((mHeroStatsModel.get1000Pick() != null? mHeroStatsModel.get1000Pick() : 0) - (mHeroStatsModel.get1000Win() != null? mHeroStatsModel.get1000Win() : 0)));
        } else if(e.getX() == 2f) {
            mHeadingTextView.setText(mContext.getString(R.string._2_thousand_mmr));
            mMatchesPlayedTextView.setText(ApplicationUtils.makePlaceholderIntegerString(mHeroStatsModel.get2000Pick(), "0"));
            mMatchesWonTextView.setText(ApplicationUtils.makePlaceholderIntegerString(mHeroStatsModel.get2000Win(), "0"));
            mMatchesLostTextView.setText(String.valueOf((mHeroStatsModel.get2000Pick() != null? mHeroStatsModel.get2000Pick() : 0) - (mHeroStatsModel.get2000Win() != null? mHeroStatsModel.get2000Win() : 0)));
        } else if(e.getX() == 3f) {
            mHeadingTextView.setText(mContext.getString(R.string._3_thousand_mmr));
            mMatchesPlayedTextView.setText(ApplicationUtils.makePlaceholderIntegerString(mHeroStatsModel.get3000Pick(), "0"));
            mMatchesWonTextView.setText(ApplicationUtils.makePlaceholderIntegerString(mHeroStatsModel.get3000Win(), "0"));
            mMatchesLostTextView.setText(String.valueOf((mHeroStatsModel.get3000Pick() != null? mHeroStatsModel.get3000Pick() : 0) - (mHeroStatsModel.get3000Win() != null? mHeroStatsModel.get3000Win() : 0)));
        } else if(e.getX() == 4f) {
            mHeadingTextView.setText(mContext.getString(R.string._4_thousand_mmr));
            mMatchesPlayedTextView.setText(ApplicationUtils.makePlaceholderIntegerString(mHeroStatsModel.get4000Pick(), "0"));
            mMatchesWonTextView.setText(ApplicationUtils.makePlaceholderIntegerString(mHeroStatsModel.get4000Win(), "0"));
            mMatchesLostTextView.setText(String.valueOf((mHeroStatsModel.get4000Pick() != null? mHeroStatsModel.get4000Pick() : 0) - (mHeroStatsModel.get4000Win() != null? mHeroStatsModel.get4000Win() : 0)));
        } else if(e.getX() == 5f) {
            mHeadingTextView.setText(mContext.getString(R.string._5_thousand_mmr));
            mMatchesPlayedTextView.setText(ApplicationUtils.makePlaceholderIntegerString(mHeroStatsModel.get5000Pick(), "0"));
            mMatchesWonTextView.setText(ApplicationUtils.makePlaceholderIntegerString(mHeroStatsModel.get5000Win(), "0"));
            mMatchesLostTextView.setText(String.valueOf((mHeroStatsModel.get5000Pick() != null? mHeroStatsModel.get5000Pick() : 0) - (mHeroStatsModel.get5000Win() != null? mHeroStatsModel.get5000Win() : 0)));
        }
        super.refreshContent(e, highlight);
    }

    @Override
    public MPPointF getOffset() {

        if(mOffset == null) {
            // center the marker horizontally and vertically
            mOffset = new MPPointF(-(getWidth() / 2), -getHeight() - 12);
        }

        return mOffset;
    }
}
