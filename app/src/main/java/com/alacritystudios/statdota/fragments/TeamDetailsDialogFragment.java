package com.alacritystudios.statdota.fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.adapters.TeamPlayerDetailsAdapter;
import com.alacritystudios.statdota.adapters.UserFriendsSummaryAdapter;
import com.alacritystudios.statdota.constants.BundleConstants;
import com.alacritystudios.statdota.models.ProPlayersModel;
import com.alacritystudios.statdota.models.TeamDetailsModel;
import com.alacritystudios.statdota.models.UserFriendsModel;
import com.alacritystudios.statdota.utils.ApplicationUtils;
import com.alacritystudios.statdota.utils.LoggerUtil;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Fragment to display team details.
 */

public class TeamDetailsDialogFragment extends DialogFragment {

    protected View mFragmentView;
    private RecyclerView mPlayerDetailsRecyclerView;
    private TextView mTeamNameTextView;
    private TextView mTeamTagTextView;
    private TextView mTeamUrlTextView;
    private ImageView mTeamLogoImageView;
    private TextView mCloseDialogTextView;
    private TextView mTeamMatchesPlayedTextView;
    private TextView mTeamMatchesWonNameTextView;
    private TextView mTeamRatingTextView;

    private TeamPlayerDetailsAdapter mTeamPlayerDetailsAdapter;
    private LinearLayoutManager mTeamPlayerDetailsLinearLayoutManager;

    protected Context mContext;
    protected String TAG;
    private TeamDetailsModel mTeamDetailsModel;

    public static TeamDetailsDialogFragment getInstance(TeamDetailsModel mTeamDetailsModel) {
        TeamDetailsDialogFragment teamDetailsFragment = new TeamDetailsDialogFragment();
        Bundle bundle = new Bundle();
        teamDetailsFragment.setArguments(bundle);
        teamDetailsFragment.setmTeamDetailsModel(mTeamDetailsModel);
        return teamDetailsFragment;
    }

    public void setmTeamDetailsModel(TeamDetailsModel mTeamDetailsModel) {
        this.mTeamDetailsModel = mTeamDetailsModel;
    }

    public void initialiseUI() {
        mPlayerDetailsRecyclerView = (RecyclerView) mFragmentView.findViewById(R.id.recycler_view_team_player_details);
        mTeamNameTextView = (TextView) mFragmentView.findViewById(R.id.team_name_text_view);
        mTeamTagTextView = (TextView) mFragmentView.findViewById(R.id.team_tag_text_view);
        mTeamUrlTextView = (TextView) mFragmentView.findViewById(R.id.team_url_text_view);
        mTeamLogoImageView = (ImageView) mFragmentView.findViewById(R.id.team_logo_image_view);
        mCloseDialogTextView = (TextView) mFragmentView.findViewById(R.id.close_dialog_button);
        mTeamMatchesPlayedTextView = (TextView) mFragmentView.findViewById(R.id.team_matches_played_text_view);
        mTeamMatchesWonNameTextView = (TextView) mFragmentView.findViewById(R.id.team_matches_won_text_view);
        mTeamRatingTextView = (TextView) mFragmentView.findViewById(R.id.team_rating_text_view);
        mPlayerDetailsRecyclerView.setLayoutManager(mTeamPlayerDetailsLinearLayoutManager);
        mPlayerDetailsRecyclerView.setAdapter(mTeamPlayerDetailsAdapter);
        fillTeamDetailsData();
    }

    public void setListeners() {
        mCloseDialogTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    public void setContextAndTags() {
        mContext = getActivity();
        TAG = LoggerUtil.makeLogTag(getClass());
    }

    public void initialiseOtherComponents() {
        Drawable drawable = ActivityCompat.getDrawable(mContext, R.drawable.ic_person);
        drawable = DrawableCompat.wrap(drawable);
        drawable.mutate().setColorFilter(ActivityCompat.getColor(mContext, R.color.colorBlackFill), PorterDuff.Mode.SRC_IN);
        mTeamPlayerDetailsAdapter = new TeamPlayerDetailsAdapter(new ArrayList<ProPlayersModel>(), mContext, drawable);
        mTeamPlayerDetailsLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.dialog_team_details, container, false);
        setContextAndTags();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
        return mFragmentView;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    /**
     * Fill teams details data int he dialog fragment.
     */
    private void fillTeamDetailsData() {
        mTeamNameTextView.setText(ApplicationUtils.makePlaceholderString(mTeamDetailsModel.getName(), mContext.getString(R.string.anonymous)));
        mTeamTagTextView.setText(ApplicationUtils.makePlaceholderString(mTeamDetailsModel.getTag(), "-"));
        mTeamUrlTextView.setText(ApplicationUtils.makePlaceholderString(mTeamDetailsModel.getUrl(), "-"));
        mTeamMatchesPlayedTextView.setText(String.valueOf(mTeamDetailsModel.getWins() + mTeamDetailsModel.getLosses()));
        mTeamMatchesWonNameTextView.setText(String.valueOf(mTeamDetailsModel.getWins()));
        mTeamRatingTextView.setText(String.valueOf(mTeamDetailsModel.getRating()));
        Glide.with(mContext)
                .load(mTeamDetailsModel.getLogoUrl())
                .into(mTeamLogoImageView);
        mTeamPlayerDetailsAdapter.setmProPlayersModelList(mTeamDetailsModel.getPlayerDetails());
        mTeamPlayerDetailsAdapter.notifyDataSetChanged();
    }
}
