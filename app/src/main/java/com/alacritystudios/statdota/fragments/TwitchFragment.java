package com.alacritystudios.statdota.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.adapters.ViewPagerAdapter;
import com.alacritystudios.statdota.fragments.base.BaseFragment;
import com.alacritystudios.statdota.models.PubMatchesModel;
import com.alacritystudios.statdota.models.RecentLeagueMatchModel;
import com.alacritystudios.statdota.models.TwitchClipsModel;
import com.alacritystudios.statdota.models.TwitchStreamsModel;
import com.alacritystudios.statdota.utils.LoggerUtil;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import java.util.List;

/**
 * Container fragment to view twitch data.
 */

public class TwitchFragment extends BaseFragment implements BaseFragment.BaseFragmentImpl {

    private Toolbar mToolbar;
    private SmartTabLayout mTwitchFragmentSmartTabLayout;
    private ViewPager mTwitchFragmentViewPager;

    private ViewPagerAdapter mViewPagerAdapter;
    private TwitchClipsFragment mTwitchClipsFragment;
    private TwitchStreamsFragment mTwitchStreamsFragment;


    public static TwitchFragment newInstance() {
        TwitchFragment twitchFragment = new TwitchFragment();
        Bundle bundle = new Bundle();
        twitchFragment.setArguments(bundle);
        return twitchFragment;
    }

    @Override
    public void initialiseUI() {
        mToolbar = (Toolbar) mFragmentView.findViewById(R.id.twitch_fragment_toolbar);
        mTwitchFragmentSmartTabLayout = (SmartTabLayout) mFragmentView.findViewById(R.id.twitch_fragment_smart_tab_layout);
        mTwitchFragmentViewPager = (ViewPager) mFragmentView.findViewById(R.id.twitch_fragment_view_pager);
        setupToolbar();
        setupViewPager();
    }

    @Override
    public void setListeners() {

    }

    @Override
    public void setContextAndTags() {
        TAG = LoggerUtil.makeLogTag(getClass());
        mContext = getActivity();
    }

    @Override
    public void initialiseOtherComponents() {
        mViewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        mTwitchStreamsFragment = TwitchStreamsFragment.newInstance();
        mTwitchClipsFragment = TwitchClipsFragment.newInstance();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_twitch, container, false);
        setContextAndTags();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
        return mFragmentView;
    }
    /**
     * Sets up the toolbar for the activity.
     */
    private void setupToolbar() {
        Log.i(TAG, "setupToolbar()");
        mToolbar.setTitleTextAppearance(mContext, R.style.ToolbarTitleAppearance);
        mToolbar.setTitle(R.string.twitch);
        ((AppCompatActivity) mContext).setSupportActionBar(mToolbar);
    }

    /**
     * Sets up the view pager.
     */
    private void setupViewPager() {
        Log.i(TAG, "setupViewPager()");
        mViewPagerAdapter.addFragment(mTwitchClipsFragment, mContext.getString(R.string.top_clips));
        mViewPagerAdapter.addFragment(mTwitchStreamsFragment, mContext.getString(R.string.live_streams));
        mTwitchFragmentViewPager.setOffscreenPageLimit(1);
        mTwitchFragmentViewPager.setAdapter(mViewPagerAdapter);
        mTwitchFragmentSmartTabLayout.setViewPager(mTwitchFragmentViewPager);
    }

    /**
     * To be called when the query succeeds.
     *
     * @param twitchClipsModelList - The resulting list to be shown.
     */
    public void onTopTwitchClipsQuerySuccess(List<TwitchClipsModel> twitchClipsModelList) {
        Log.i(TAG, "onTopTwitchClipsQuerySuccess()");
        mTwitchClipsFragment.onFetchResultSuccess(twitchClipsModelList);
    }

    /**
     * To be called when the query fails.
     */
    public void onTopTwitchClipsQueryFailure(boolean isCancelled) {
        Log.i(TAG, "onTopTwitchClipsQueryFailure()");
        mTwitchClipsFragment.onFetchResultFailure(isCancelled);
    }

    /**
     * To be called when the query succeeds.
     *
     * @param twitchStreamsModel - The containing model for resulting list to be shown.
     */
    public void onLiveTwitchStreamsQuerySuccess(TwitchStreamsModel twitchStreamsModel) {
        Log.i(TAG, "onLiveTwitchStreamsQuerySuccess()");
        mTwitchStreamsFragment.onFetchResultSuccess(twitchStreamsModel);
    }

    /**
     * To be called when the query fails.
     */
    public void onLiveTwitchStreamsQueryFailure(boolean isCancelled) {
        Log.i(TAG, "onLiveTwitchStreamsQueryFailure()");
        mTwitchStreamsFragment.onFetchResultFailure(isCancelled);
    }
}
