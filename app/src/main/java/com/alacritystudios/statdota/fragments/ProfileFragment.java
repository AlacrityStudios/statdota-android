package com.alacritystudios.statdota.fragments;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.adapters.ViewPagerAdapter;
import com.alacritystudios.statdota.constants.SteamWebApiConstants;
import com.alacritystudios.statdota.fragments.base.BaseFragment;
import com.alacritystudios.statdota.models.UserFriendsModel;
import com.alacritystudios.statdota.models.UserHeroSummaryModel;
import com.alacritystudios.statdota.models.UserMatchSummaryModel;
import com.alacritystudios.statdota.models.UserStatsModel;
import com.alacritystudios.statdota.utils.LoggerUtil;
import com.alacritystudios.statdota.utils.PreferenceUtil;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import java.util.List;

/**
 * Shows the currently logged in user profile.
 */

public class ProfileFragment extends BaseFragment implements BaseFragment.BaseFragmentImpl {

    private Toolbar mToolbar;
    private SmartTabLayout mProfileFragmentSmartTabLayout;
    private ViewPager mProfileFragmentViewPager;
    private SwipeRefreshLayout mProfileFragmentErrorLayout;
    private SwipeRefreshLayout mProfileFragmentWebViewLayout;
    private AppCompatButton mLoginButton;

    private ViewPagerAdapter mViewPagerAdapter;
    private UserMatchesFragment mUserMatchesFragment;
    private UserStatsFragment mUserStatsFragment;
    private UserHeroesFragment mUserHeroesFragment;
    private UserFriendsFragment mUserFriendsFragment;
    private Menu mFragmentMenu;

    private String mSteamLoginUrl;
    private String REALM_PARAM = "statDota";
    private WebView mWebView;


    public static ProfileFragment newInstance() {
        ProfileFragment profileFragment = new ProfileFragment();
        Bundle bundle = new Bundle();
        profileFragment.setArguments(bundle);
        return profileFragment;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.profile_fragment_menu, menu);
        mFragmentMenu = menu;
        if (PreferenceUtil.getSteam64BitIdPreference(mContext) == 0L) {
            mFragmentMenu.setGroupVisible(R.id.user_profile_menu_items, false);
        } else {
            mFragmentMenu.setGroupVisible(R.id.user_profile_menu_items, true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_future_updates:
                return true;
            case R.id.menu_toggle_login_status:
                return true;
        }
        return false;
    }

    @Override
    public void initialiseUI() {
        mToolbar = (Toolbar) mFragmentView.findViewById(R.id.profile_fragment_toolbar);
        mProfileFragmentSmartTabLayout = (SmartTabLayout) mFragmentView.findViewById(R.id.profile_fragment_smart_tab_layout);
        mProfileFragmentViewPager = (ViewPager) mFragmentView.findViewById(R.id.profile_fragment_view_pager);
        mProfileFragmentErrorLayout = (SwipeRefreshLayout) mFragmentView.findViewById(R.id.profile_fragment_error_layout);
        mProfileFragmentWebViewLayout = (SwipeRefreshLayout) mFragmentView.findViewById(R.id.profile_fragment_web_view_layout);
        mLoginButton = (AppCompatButton) mFragmentView.findViewById(R.id.login_button);
        setupToolbar();
        checkUserLoginStatus();
    }

    @Override
    public void setListeners() {
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mProfileFragmentWebViewLayout.addView(mWebView);
                mProfileFragmentErrorLayout.setVisibility(View.GONE);
                mProfileFragmentWebViewLayout.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void setContextAndTags() {
        TAG = LoggerUtil.makeLogTag(getClass());
        mContext = getActivity();
    }

    @Override
    public void initialiseOtherComponents() {
        mSteamLoginUrl = "https://steamcommunity.com/openid/login?" +
                "openid.claimed_id=http://specs.openid.net/auth/2.0/identifier_select&" +
                "openid.identity=http://specs.openid.net/auth/2.0/identifier_select&" +
                "openid.mode=checkid_setup&" +
                "openid.ns=http://specs.openid.net/auth/2.0&" +
                "openid.realm=https://" + REALM_PARAM + "&" +
                "openid.return_to=https://" + REALM_PARAM + "/signin/";

        mWebView = new WebView(mContext);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                //mContext.setTitle(url);
                Uri Url = Uri.parse(url);
                if (Url.getAuthority().equals(REALM_PARAM.toLowerCase())) {
                    mWebView.stopLoading();
                    Uri userAccountUrl = Uri.parse(Url.getQueryParameter("openid.identity"));
                    String userId = userAccountUrl.getLastPathSegment();
                    PreferenceUtil.setSteam64BitIdPreference(mContext, Long.parseLong(userId));
                    checkUserLoginStatus();
                }
            }
        });
        mWebView.loadUrl(mSteamLoginUrl);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        mFragmentView = inflater.inflate(R.layout.fragment_profile, container, false);
        setContextAndTags();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
        return mFragmentView;
    }

    /**
     * Sets up the toolbar for the activity.
     */
    private void setupToolbar() {
        Log.i(TAG, "setupToolbar()");
        mToolbar.setTitleTextAppearance(mContext, R.style.ToolbarTitleAppearance);
        ((AppCompatActivity) mContext).setSupportActionBar(mToolbar);
    }

    /**
     * Checks if user is logged on or not.
     */
    private void checkUserLoginStatus() {
        Log.i(TAG, "checkUserLoginStatus()");
        if (PreferenceUtil.getSteam64BitIdPreference(mContext) == 0L) {
            mProfileFragmentSmartTabLayout.setVisibility(View.GONE);
            mProfileFragmentViewPager.setVisibility(View.GONE);
            mProfileFragmentWebViewLayout.setVisibility(View.GONE);
            mProfileFragmentErrorLayout.setVisibility(View.VISIBLE);
            if (mFragmentMenu != null) {
                mFragmentMenu.setGroupVisible(R.id.user_profile_menu_items, false);
            }
        } else {
            mProfileFragmentSmartTabLayout.setVisibility(View.VISIBLE);
            mProfileFragmentViewPager.setVisibility(View.VISIBLE);
            mProfileFragmentErrorLayout.setVisibility(View.GONE);
            mProfileFragmentWebViewLayout.setVisibility(View.GONE);
            if (mFragmentMenu != null) {
                mFragmentMenu.setGroupVisible(R.id.user_profile_menu_items, true);
            }
            setupViewPager(PreferenceUtil.getSteam64BitIdPreference(mContext) - SteamWebApiConstants.STEAM_API_ACCOUNT_ID_OFFSET);
            mToolbar.setTitle(PreferenceUtil.getLoggedInUserPersonaNamePreference(mContext).equals("") ? mContext.getString(R.string.app_name) : PreferenceUtil.getLoggedInUserPersonaNamePreference(mContext));
        }
    }

    /**
     * Sets up the view pager.
     */
    private void setupViewPager(long accountId) {
        Log.i(TAG, "setupViewPager()");
        mViewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        mUserStatsFragment = UserStatsFragment.newInstance(accountId, true);
        mUserMatchesFragment = UserMatchesFragment.newInstance(accountId, true);
        mUserHeroesFragment = UserHeroesFragment.newInstance(accountId, true);
        mUserFriendsFragment = UserFriendsFragment.newInstance(accountId, true);
        mViewPagerAdapter.addFragment(mUserStatsFragment, mContext.getString(R.string.stats));
        mViewPagerAdapter.addFragment(mUserMatchesFragment, mContext.getString(R.string.matches));
        mViewPagerAdapter.addFragment(mUserHeroesFragment, mContext.getString(R.string.heroes));
        mViewPagerAdapter.addFragment(mUserFriendsFragment, mContext.getString(R.string.friends));
        mProfileFragmentViewPager.setOffscreenPageLimit(3);
        mProfileFragmentViewPager.setAdapter(mViewPagerAdapter);
        mProfileFragmentSmartTabLayout.setViewPager(mProfileFragmentViewPager);
    }

    public void onUserMatchesQuerySuccessCallback(List<UserMatchSummaryModel> userMatchSummaryModelList) {
        mUserMatchesFragment.onUserMatchesQuerySuccess(userMatchSummaryModelList);
    }

    public void onUserMatchesQueryFailureCallback(boolean isCancelled) {
        mUserMatchesFragment.onUserMatchesQueryFailure(isCancelled);
    }


    public void onUserStatsQuerySuccessCallback(UserStatsModel userStatsModel) {

    }

    public void onUserStatsQueryFailureCallback(boolean isCancelled) {

    }

    public void onUserHeroesQuerySuccessCallback(List<UserHeroSummaryModel> userHeroSummaryModelList) {
        mUserHeroesFragment.onUserHeroesQuerySuccess(userHeroSummaryModelList);
    }

    public void onUserHeroesQueryFailureCallback(boolean isCancelled) {
        mUserHeroesFragment.onUserHeroesQueryFailure(isCancelled);
    }

    public void onUserFriendsQuerySuccessCallback(List<UserFriendsModel> userFriendsModelList) {
        mUserFriendsFragment.onUserFriendsQuerySuccess(userFriendsModelList);
    }

    public void onUserFriendsQueryFailureCallback(boolean isCancelled) {
        mUserFriendsFragment.onUserFriendsQueryFailure(isCancelled);
    }
}
