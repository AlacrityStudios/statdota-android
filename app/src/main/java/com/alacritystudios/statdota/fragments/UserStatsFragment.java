package com.alacritystudios.statdota.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.adapters.TwitchClipsSummaryAdapter;
import com.alacritystudios.statdota.constants.BundleConstants;
import com.alacritystudios.statdota.constants.StatDotaApiConstants;
import com.alacritystudios.statdota.fragments.base.BaseFragment;
import com.alacritystudios.statdota.models.TwitchClipsModel;
import com.alacritystudios.statdota.models.UserStatsModel;
import com.alacritystudios.statdota.rest.ApiClient;
import com.alacritystudios.statdota.rest.ApiInterface;
import com.alacritystudios.statdota.utils.ApplicationUtils;
import com.alacritystudios.statdota.utils.LoggerUtil;
import com.bumptech.glide.Glide;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Displays major in game stats of the users.
 */

public class UserStatsFragment extends BaseFragment implements BaseFragment.BaseFragmentImpl {

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayout mPlaceholderLayout;
    private TextView mPlaceholderMessage;
    private NestedScrollView mContentScrollView;
    private ImageView mUserAvatar;
    private TextView mUserMatchesPlayedTextView;
    private TextView mUserMatchesWonTextView;
    private TextView mUserDisplayNameTextView;
    private TextView mSoloMMRTextView;
    private TextView mPartyMMRTextView;
    private TextView mEstimatedMMRTextView;
    private TextView mAverageLastHitsTextView;
    private TextView mRampagesTextView;
    private TextView mFirstBloodClaimedTextView;
    private TextView mAvgGpmTextView;
    private TextView mAvgXpmTextView;
    private TextView mAegisSnatchedTextView;
    private TextView mCheesesEatenTextView;
    private TextView mCouriersKilledTextView;
    private TextView mCreepsStackedTextView;
    private TextView mFirstBloodGivenTextView;
    private RadarChart mPlayerScoresRadarChart;
    private RecyclerView mPopularClipsRecyclerView;

    private long mAccountId;
    private LinearLayoutManager mPopularClipsLinearLayoutManager;
    private TwitchClipsSummaryAdapter mTwitchClipsSummaryAdapter;

    public static UserStatsFragment newInstance(long accountId, boolean isUserAccount) {
        UserStatsFragment userStatsFragment = new UserStatsFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(BundleConstants.ACCOUNT_ID, accountId);
        bundle.putBoolean(BundleConstants.IS_USER_ACCOUNT, isUserAccount);
        userStatsFragment.setArguments(bundle);
        return userStatsFragment;
    }

    @Override
    public void initialiseUI() {
        mSwipeRefreshLayout = (SwipeRefreshLayout) mFragmentView.findViewById(R.id.user_stats_fragment_swipe_refresh_layout);
        mPlaceholderLayout = (LinearLayout) mFragmentView.findViewById(R.id.user_stats_fragment_placeholder_layout);
        mPlaceholderMessage = (TextView) mFragmentView.findViewById(R.id.user_stats_fragment_placeholder_error_message);
        mContentScrollView = (NestedScrollView) mFragmentView.findViewById(R.id.user_stats_fragment_nested_scroll_view);
        mUserAvatar = (ImageView) mFragmentView.findViewById(R.id.user_profile_picture_image_view);
        mUserDisplayNameTextView = (TextView) mFragmentView.findViewById(R.id.user_display_name_text_view);
        mUserMatchesPlayedTextView = (TextView) mFragmentView.findViewById(R.id.user_matches_played_text_view);
        mUserMatchesWonTextView = (TextView) mFragmentView.findViewById(R.id.user_matches_won_text_view);
        mSoloMMRTextView = (TextView) mFragmentView.findViewById(R.id.player_solo_mmr);
        mPartyMMRTextView = (TextView) mFragmentView.findViewById(R.id.player_party_mmr);
        mEstimatedMMRTextView = (TextView) mFragmentView.findViewById(R.id.player_est_mmr);
        mAverageLastHitsTextView = (TextView) mFragmentView.findViewById(R.id.avg_last_hits_text_view);
        mAvgGpmTextView = (TextView) mFragmentView.findViewById(R.id.avg_gpm_text_view);
        mAvgXpmTextView = (TextView) mFragmentView.findViewById(R.id.avg_xpm_text_view);
        mRampagesTextView = (TextView) mFragmentView.findViewById(R.id.rampages_text_view);
        mFirstBloodClaimedTextView = (TextView) mFragmentView.findViewById(R.id.first_blood_claimed_text_view);
        mFirstBloodGivenTextView = (TextView) mFragmentView.findViewById(R.id.first_blood_given_text_view);
        mAegisSnatchedTextView = (TextView) mFragmentView.findViewById(R.id.aegises_snatched_text_view);
        mCheesesEatenTextView = (TextView) mFragmentView.findViewById(R.id.cheeses_eaten_text_view);
        mCouriersKilledTextView = (TextView) mFragmentView.findViewById(R.id.couriers_killed_text_view);
        mCreepsStackedTextView = (TextView) mFragmentView.findViewById(R.id.creeps_stacked_text_view);
        mPlayerScoresRadarChart = (RadarChart) mFragmentView.findViewById(R.id.player_scores_radar_chart);
        mPopularClipsRecyclerView = (RecyclerView) mFragmentView.findViewById(R.id.popular_clips_recycler_view);
        mPopularClipsRecyclerView.setLayoutManager(mPopularClipsLinearLayoutManager);
        mPopularClipsRecyclerView.setAdapter(mTwitchClipsSummaryAdapter);
        setupSwipeRefreshLayout();
        mSwipeRefreshLayout.setRefreshing(true);
        findUserStatsInfo(String.valueOf(mAccountId));
    }

    @Override
    public void setListeners() {

    }

    @Override
    public void setContextAndTags() {
        mContext = getActivity();
        TAG = LoggerUtil.makeLogTag(getClass());
    }

    @Override
    public void initialiseOtherComponents() {
        mAccountId = getArguments().getLong(BundleConstants.ACCOUNT_ID);
        mPopularClipsLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        mTwitchClipsSummaryAdapter = new TwitchClipsSummaryAdapter(mContext, new ArrayList<TwitchClipsModel>());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_user_stats, container, false);
        setContextAndTags();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
        return mFragmentView;
    }

    /**
     * Setup the swipe refresh layout behaviour.
     */
    private void setupSwipeRefreshLayout() {
        Log.i(TAG, "setupSwipeRefreshLayout()");
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                findUserStatsInfo(String.valueOf(mAccountId));
            }
        });
    }

    /**
     * Display user information fetched from back end.
     */
    private void setUserInformation(UserStatsModel userStatsModel) {
        Log.i(TAG, "setUserInformation()");
        if (userStatsModel.getMmrEstimate() != null) {
            mEstimatedMMRTextView.setText(ApplicationUtils.makePlaceholderIntegerString(userStatsModel.getMmrEstimate().getEstimate() != null ? userStatsModel.getMmrEstimate().getEstimate().intValue() : 0, "-"));
        } else {
            mEstimatedMMRTextView.setText("-");
        }
        mUserMatchesPlayedTextView.setText(String.valueOf(userStatsModel.getMatchesWon() + userStatsModel.getMatchesLost()));
        mUserMatchesWonTextView.setText(String.valueOf(userStatsModel.getMatchesWon()));
        mSoloMMRTextView.setText(ApplicationUtils.makePlaceholderString(userStatsModel.getSoloCompetitiveRank(), "-"));
        mPartyMMRTextView.setText(ApplicationUtils.makePlaceholderString(userStatsModel.getCompetitiveRank(), "-"));
        mUserDisplayNameTextView.setText(userStatsModel.getName() != null ? userStatsModel.getName() : ApplicationUtils.makePlaceholderString(userStatsModel.getPersonaName(), mContext.getString(R.string.anonymous)));
        Glide.with(mContext.getApplicationContext())
                .load(userStatsModel.getAvatarFull())
                .centerCrop()
                .into(mUserAvatar);
        mUserAvatar.setClipToOutline(true);
        mTwitchClipsSummaryAdapter.setmTwitchClipsModelList(userStatsModel.getTwitchClips());
        mTwitchClipsSummaryAdapter.notifyDataSetChanged();
    }

    /**
     * Setup the basic info panel.
     */
    private void setupTotalsInfo(UserStatsModel userStatsModel) {
        Log.i(TAG, "setupKeyInfo()");
        mAverageLastHitsTextView.setText(ApplicationUtils.makePlaceholderIntegerString(userStatsModel.getMeanLasthits(), "0"));
        mAvgGpmTextView.setText(ApplicationUtils.makePlaceholderIntegerString(userStatsModel.getMeanGpm() != null ? userStatsModel.getMeanGpm().intValue() : 0, "0"));
        mAvgXpmTextView.setText(ApplicationUtils.makePlaceholderIntegerString(userStatsModel.getMeanXppm() != null ? userStatsModel.getMeanXppm().intValue() : 0, "0"));
        mRampagesTextView.setText(ApplicationUtils.makePlaceholderIntegerString(userStatsModel.getRampages(), "0"));
        mFirstBloodClaimedTextView.setText(ApplicationUtils.makePlaceholderLongString(userStatsModel.getFirstBloodClaimed(), "0"));
        mFirstBloodGivenTextView.setText(ApplicationUtils.makePlaceholderLongString(userStatsModel.getFirstBloodGiven(), "0"));
        mAegisSnatchedTextView.setText(ApplicationUtils.makePlaceholderIntegerString(userStatsModel.getAegisesSnatched(), "0"));
        mCheesesEatenTextView.setText(ApplicationUtils.makePlaceholderIntegerString(userStatsModel.getCheesesEaten(), "0"));
        mCouriersKilledTextView.setText(ApplicationUtils.makePlaceholderIntegerString(userStatsModel.getCouriersKilled(), "0"));
        mCreepsStackedTextView.setText(ApplicationUtils.makePlaceholderIntegerString(userStatsModel.getCreepsStacked(), "0"));
    }

    /**
     * Populates the radar chart with player scores.
     */
    public void setupScoresChart(UserStatsModel userStatsModel) {
        Log.i(TAG, "setChartData()");
        List<RadarEntry> radarEntryList = new ArrayList<>();
        Description description = new Description();
        description.setText("");

        if (userStatsModel.getFightScore() != null) {
            radarEntryList.add(new RadarEntry(userStatsModel.getFightScore().floatValue() * 10, ""));
        } else {
            radarEntryList.add(new RadarEntry(0f, ""));
        }

        if (userStatsModel.getFarmScore() != null) {
            radarEntryList.add(new RadarEntry(userStatsModel.getFarmScore().floatValue() * 10, ""));
        } else {
            radarEntryList.add(new RadarEntry(0f, ""));
        }
        if (userStatsModel.getSupportScore() != null) {
            radarEntryList.add(new RadarEntry(userStatsModel.getSupportScore().floatValue() * 10, ""));
        } else {
            radarEntryList.add(new RadarEntry(0f, ""));
        }
        if (userStatsModel.getPushScore() != null) {
            radarEntryList.add(new RadarEntry(userStatsModel.getPushScore().floatValue() * 10, ""));
        } else {
            radarEntryList.add(new RadarEntry(0f, ""));
        }
        if (userStatsModel.getVersatilityScore() != null) {
            radarEntryList.add(new RadarEntry(userStatsModel.getVersatilityScore().floatValue() * 10, ""));
        } else {
            radarEntryList.add(new RadarEntry(0f, ""));
        }
        RadarDataSet radarDataSet = new RadarDataSet(radarEntryList, "");
        radarDataSet.setColors(new int[]{R.color.colorBlack}, mContext);
        radarDataSet.setValueTypeface(ResourcesCompat.getFont(mContext, R.font.noto_sans_ui_bold));
        radarDataSet.setValueTextColor(ActivityCompat.getColor(mContext, R.color.colorBlackText));
        radarDataSet.setValueTextSize(8f);
        radarDataSet.setHighlightCircleFillColor(ActivityCompat.getColor(mContext, R.color.colorFocus));
        radarDataSet.setFillDrawable(ActivityCompat.getDrawable(mContext, R.drawable.radar_chart_primary_gradient_background));
        radarDataSet.setDrawFilled(true);
        radarDataSet.setLineWidth(1f);
        RadarData radarData = new RadarData(radarDataSet);
        mPlayerScoresRadarChart.getYAxis().setDrawLabels(false);
        mPlayerScoresRadarChart.getYAxis().setAxisMaximum(10f);
        mPlayerScoresRadarChart.getYAxis().setAxisMinimum(0f);
        mPlayerScoresRadarChart.getXAxis().setTextSize(12f);
        mPlayerScoresRadarChart.getXAxis().setTypeface(ResourcesCompat.getFont(mContext, R.font.noto_sans_ui_regular));
        mPlayerScoresRadarChart.getXAxis().setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                switch (Float.valueOf(value).intValue()) {
                    case 0:
                        return mContext.getString(R.string.fighting);
                    case 1:
                        return mContext.getString(R.string.farming);
                    case 2:
                        return mContext.getString(R.string.supporting);
                    case 3:
                        return mContext.getString(R.string.pushing);
                    case 4:
                        return mContext.getString(R.string.versatility);
                    default:
                        return mContext.getString(R.string.app_name);
                }
            }
        });
        mPlayerScoresRadarChart.setWebColor(ActivityCompat.getColor(mContext, R.color.colorDefaultMedium));
        mPlayerScoresRadarChart.setWebColorInner(ActivityCompat.getColor(mContext, R.color.colorDefaultMedium));
        mPlayerScoresRadarChart.setWebAlpha(60);
        mPlayerScoresRadarChart.setDescription(description);
        mPlayerScoresRadarChart.getLegend().setEnabled(false);
        mPlayerScoresRadarChart.setData(radarData);
        mPlayerScoresRadarChart.setTouchEnabled(false);
        mPlayerScoresRadarChart.animateXY(0, 400);
    }

    /**
     * Find basic info about user.
     */
    private void findUserStatsInfo(String accountId) {
        Log.i(TAG, "findUserStatsInfo()");
        Retrofit retrofit = ApiClient.getClient(StatDotaApiConstants.STAT_DOTA_BASE_URL);
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<UserStatsModel> playerMatches = apiInterface.getUserStatsInfo(accountId);
        playerMatches.enqueue(new Callback<UserStatsModel>() {

            @Override
            public void onResponse(Call<UserStatsModel> call, Response<UserStatsModel> response) {
                if (response.isSuccessful()) {
                    UserStatsModel userStatsModel = response.body();
                    if (userStatsModel == null) {
                        mContentScrollView.setVisibility(View.GONE);
                        mPlaceholderLayout.setVisibility(View.VISIBLE);
                    } else {
                        mTwitchClipsSummaryAdapter.setmTwitchClipsModelList(userStatsModel.getTwitchClips());
                        mTwitchClipsSummaryAdapter.notifyDataSetChanged();
                        setUserInformation(userStatsModel);
                        setupTotalsInfo(userStatsModel);
                        setupScoresChart(userStatsModel);
                        mContentScrollView.setVisibility(View.VISIBLE);
                        mPlaceholderLayout.setVisibility(View.GONE);
                    }
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<UserStatsModel> call, Throwable t) {
                mSwipeRefreshLayout.setRefreshing(false);
                mContentScrollView.setVisibility(View.GONE);
                mPlaceholderLayout.setVisibility(View.VISIBLE);
            }
        });
    }

    public interface UserStatsFragmentInterface {

        void fetchUserStats(long accountId);

        void fetchUserStatsFromBackEnd(long accountId);
    }
}
