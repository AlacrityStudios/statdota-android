package com.alacritystudios.statdota.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.activities.TwitchPlayerActivity;
import com.alacritystudios.statdota.adapters.TwitchStreamsAdapter;
import com.alacritystudios.statdota.constants.BundleConstants;
import com.alacritystudios.statdota.fragments.base.BaseFragment;
import com.alacritystudios.statdota.interfaces.TwitchLiveStreamsItemClickListener;
import com.alacritystudios.statdota.models.TwitchStreamsModel;
import com.alacritystudios.statdota.utils.LoggerUtil;

import java.util.ArrayList;

/**
 * Fragment to display twitch live streams.
 */

public class TwitchStreamsFragment extends BaseFragment implements BaseFragment.BaseFragmentImpl, TwitchLiveStreamsItemClickListener {

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayout mPlaceholderLayout;
    private LinearLayout mErrorLayout;
    private RecyclerView mTwitchStreamsRecyclerView;

    private TwitchStreamsAdapter mTwitchStreamsAdapter;
    private LinearLayoutManager mLiveStreamLinearLayoutManager;
    private TwitchStreamsFragmentInterface mTwitchStreamsFragmentInterface;

    public static TwitchStreamsFragment newInstance() {
        TwitchStreamsFragment twitchStreamsFragment = new TwitchStreamsFragment();
        Bundle bundle = new Bundle();
        twitchStreamsFragment.setArguments(bundle);
        return twitchStreamsFragment;
    }

    @Override
    public void initialiseUI() {
        mSwipeRefreshLayout = (SwipeRefreshLayout) mFragmentView.findViewById(R.id.twitch_streams_fragment_swipe_refresh_layout);
        mPlaceholderLayout = (LinearLayout) mFragmentView.findViewById(R.id.twitch_streams_fragment_placeholder_layout);
        mErrorLayout = (LinearLayout) mFragmentView.findViewById(R.id.twitch_streams_fragment_error_layout);
        mTwitchStreamsRecyclerView = (RecyclerView) mFragmentView.findViewById(R.id.recycler_view_twitch_streams);
        setupLiveStreamRecyclerView();
        setupSwipeRefreshLayout();
        mTwitchStreamsFragmentInterface.fetchTwitchFragmentLiveStreams();
    }

    @Override
    public void setListeners() {

    }

    @Override
    public void setContextAndTags() {
        mContext = getActivity();
        TAG = LoggerUtil.makeLogTag(getClass());
    }

    @Override
    public void initialiseOtherComponents() {
        mTwitchStreamsAdapter = new TwitchStreamsAdapter(new ArrayList<TwitchStreamsModel.Stream>(), mContext, this);
        mLiveStreamLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_twitch_streams, container, false);
        setContextAndTags();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
        return mFragmentView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SearchFragment.SearchFragmentListenerInterface) {
            mTwitchStreamsFragmentInterface = (TwitchStreamsFragmentInterface) context;
        } else {
            throw new ClassCastException("LandingActivity must implement TwitchStreamsFragmentInterface.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mTwitchStreamsFragmentInterface = null;
    }

    @Override
    public void onItemClick(String streamUrl) {
        Intent intent = new Intent(mContext, TwitchPlayerActivity.class);
        intent.putExtra(BundleConstants.STREAM_URL, streamUrl);
        startActivity(intent);
    }

    /**
     * Setup the swipe refresh layout behaviour.
     */
    private void setupSwipeRefreshLayout() {
        Log.i(TAG, "setupSwipeRefreshLayout()");
        mPlaceholderLayout.setVisibility(View.GONE);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                mTwitchStreamsFragmentInterface.refreshTwitchFragmentLiveStreams();
            }
        });
    }

    /**
     * Setup the matches recycler view.
     */
    private void setupLiveStreamRecyclerView() {
        Log.i(TAG, "setupLiveStreamRecyclerView()");
        mTwitchStreamsRecyclerView.setLayoutManager(mLiveStreamLinearLayoutManager);
        mTwitchStreamsRecyclerView.setAdapter(mTwitchStreamsAdapter);
    }


    /**
     * Success case of fetching stats.
     *
     * @param twitchStreamsModel - Container model to hold the result.
     */
    public void onFetchResultSuccess(TwitchStreamsModel twitchStreamsModel) {
        Log.i(TAG, "onFetchResultSuccess()");
        mTwitchStreamsAdapter.setStreamList(twitchStreamsModel.getStreams());
        mTwitchStreamsAdapter.notifyDataSetChanged();
        if (twitchStreamsModel.getStreams().size() == 0) {
            mTwitchStreamsRecyclerView.setVisibility(View.GONE);
            mErrorLayout.setVisibility(View.GONE);
            mPlaceholderLayout.setVisibility(View.VISIBLE);
        } else {
            mTwitchStreamsRecyclerView.setVisibility(View.VISIBLE);
            mErrorLayout.setVisibility(View.GONE);
            mPlaceholderLayout.setVisibility(View.GONE);
        }
        mSwipeRefreshLayout.setRefreshing(false);
    }

    /**
     * Failure case of fetching stats.
     */
    public void onFetchResultFailure(boolean isCancelled) {
        Log.i(TAG, "onFetchResultFailure()");
        mTwitchStreamsAdapter.setStreamList(new ArrayList<TwitchStreamsModel.Stream>());
        mTwitchStreamsAdapter.notifyDataSetChanged();
        if(!isCancelled) {
            mTwitchStreamsRecyclerView.setVisibility(View.GONE);
            mErrorLayout.setVisibility(View.VISIBLE);
            mPlaceholderLayout.setVisibility(View.GONE);
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    public interface TwitchStreamsFragmentInterface {

        void fetchTwitchFragmentLiveStreams();

        void refreshTwitchFragmentLiveStreams();
    }
}
