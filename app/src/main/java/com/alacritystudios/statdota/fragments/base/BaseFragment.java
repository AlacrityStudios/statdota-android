package com.alacritystudios.statdota.fragments.base;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;

/**
 * A general implementation of a base fragment from which other fragments defined in this application will inherit from.
 */

public class BaseFragment extends Fragment {

    protected Context mContext;
    protected String TAG;
    protected View mFragmentView;

    public interface BaseFragmentImpl {

        void initialiseUI();

        void setListeners();

        void setContextAndTags();

        void initialiseOtherComponents();
    }
}
