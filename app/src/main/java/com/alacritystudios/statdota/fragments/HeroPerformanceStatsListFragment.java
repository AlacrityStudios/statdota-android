package com.alacritystudios.statdota.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.adapters.HeroPerformanceStatsAdapter;
import com.alacritystudios.statdota.fragments.base.BaseFragment;
import com.alacritystudios.statdota.models.HeroStatsModel;
import com.alacritystudios.statdota.utils.LoggerUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment to display the hero performance stats in the past 30 days.
 */

public class HeroPerformanceStatsListFragment extends BaseFragment implements BaseFragment.BaseFragmentImpl {

    private RecyclerView mHeroStatsRecyclerView;
    private SwipeRefreshLayout mHeroPerformanceStatsSwipeRefreshLayout;
    private LinearLayout mHeroPerformanceStatsPlaceholderLayout;
    private TextView mHeroPerformanceStatsPlaceholderMessage;

    private List<HeroStatsModel> mHeroStatsModelList;
    private LinearLayoutManager mHeroPerformanceStatsLinearLayoutManager;
    private HeroPerformanceStatsAdapter mHeroPerformanceStatsAdapter;

    private HeroPerformanceStatsListFragmentInterface mHeroPerformanceStatsListFragmentInterface;

    public static HeroPerformanceStatsListFragment getInstance() {
        HeroPerformanceStatsListFragment heroPerformanceStatsListFragment = new HeroPerformanceStatsListFragment();
        Bundle bundle = new Bundle();
        heroPerformanceStatsListFragment.setArguments(bundle);
        return heroPerformanceStatsListFragment;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.hero_list_sort_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                ((Activity) mContext).onBackPressed();
                break;
            case R.id.menu_sort_order:
                break;
        }
        return true;
    }

    @Override
    public void initialiseUI() {
        mHeroStatsRecyclerView = mFragmentView.findViewById(R.id.hero_performance_stats_recycler_view);
        mHeroPerformanceStatsSwipeRefreshLayout = mFragmentView.findViewById(R.id.hero_performance_stats_fragment_swipe_refresh_layout);
        mHeroPerformanceStatsPlaceholderLayout = mFragmentView.findViewById(R.id.hero_performance_stats_fragment_placeholder_layout);
        mHeroPerformanceStatsPlaceholderMessage = mFragmentView.findViewById(R.id.hero_performance_stats_fragment_placeholder_error_message);
        mHeroStatsRecyclerView.setLayoutManager(mHeroPerformanceStatsLinearLayoutManager);
        mHeroStatsRecyclerView.setAdapter(mHeroPerformanceStatsAdapter);
        setupToolbar();
        setupSwipeRefreshLayout();
        mHeroPerformanceStatsSwipeRefreshLayout.setRefreshing(true);
        mHeroPerformanceStatsListFragmentInterface.fetchHeroPerformanceStats();
    }

    @Override
    public void setListeners() {

    }

    @Override
    public void setContextAndTags() {
        mContext = getActivity();
        TAG = LoggerUtil.makeLogTag(getClass());
    }

    @Override
    public void initialiseOtherComponents() {
        mHeroStatsModelList = new ArrayList<>();
        mHeroPerformanceStatsAdapter = new HeroPerformanceStatsAdapter(mHeroStatsModelList, mContext);
        mHeroPerformanceStatsLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_hero_performance_stats_list, container, false);
        setContextAndTags();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
        return mFragmentView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof HeroPerformanceStatsListFragmentInterface) {
            mHeroPerformanceStatsListFragmentInterface = (HeroPerformanceStatsListFragmentInterface) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement HeroPerformanceStatsListFragment.HeroPerformanceStatsListFragmentInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mHeroPerformanceStatsListFragmentInterface = null;
    }

    /**
     * Setup the custom toolbar.
     */
    private void setupToolbar() {
        Log.i(TAG, "setupToolbar()");
        ((AppCompatActivity) mContext).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) mContext).getSupportActionBar().setTitle(R.string.heroes);
        setHasOptionsMenu(true);
    }

    /**
     * Setup the swipe refresh layout behaviour.
     */
    private void setupSwipeRefreshLayout() {
        Log.i(TAG, "setupSwipeRefreshLayout()");
        mHeroPerformanceStatsPlaceholderLayout.setVisibility(View.GONE);
        mHeroPerformanceStatsSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                mHeroStatsModelList.clear();
                mHeroPerformanceStatsListFragmentInterface.refreshHeroPerformanceStats();
            }
        });
    }

    /**
     * Called when data is available to be shown.
     */
    public void onDataFetchSuccess(List<HeroStatsModel> heroStatsModelList) {
        Log.i(TAG, "onDataFetchSuccess()");
        mHeroStatsModelList = heroStatsModelList;
        mHeroPerformanceStatsAdapter.setmHeroStatsModelList(mHeroStatsModelList);
        mHeroPerformanceStatsAdapter.notifyDataSetChanged();
        if (mHeroPerformanceStatsSwipeRefreshLayout.isRefreshing()) {
            mHeroPerformanceStatsSwipeRefreshLayout.setRefreshing(false);
        }
        if (mHeroStatsModelList.size() == 0) {
            mHeroPerformanceStatsPlaceholderMessage.setText(R.string.no_records_to_be_shown);
            mHeroStatsRecyclerView.setVisibility(View.GONE);
            mHeroPerformanceStatsPlaceholderLayout.setVisibility(View.VISIBLE);
        } else {
            mHeroStatsRecyclerView.setVisibility(View.VISIBLE);
            mHeroPerformanceStatsPlaceholderLayout.setVisibility(View.GONE);
        }
    }

    /**
     * Called when data is not available to be shown.
     *
     * @param isCancelled - Checks if the call was cancelled.
     */
    public void onDataFetchFailure(boolean isCancelled) {
        Log.i(TAG, "onDataFetchFailure()");
        mHeroStatsModelList = new ArrayList<>();
        mHeroPerformanceStatsAdapter.setmHeroStatsModelList(mHeroStatsModelList);
        mHeroPerformanceStatsAdapter.notifyDataSetChanged();
        if (mHeroPerformanceStatsSwipeRefreshLayout.isRefreshing()) {
            mHeroPerformanceStatsSwipeRefreshLayout.setRefreshing(false);
        }

        if (! isCancelled) {
            mHeroPerformanceStatsPlaceholderMessage.setText(R.string.fragment_heroes_error_message);
            mHeroStatsRecyclerView.setVisibility(View.GONE);
            mHeroPerformanceStatsPlaceholderLayout.setVisibility(View.VISIBLE);
        } else if (mHeroStatsModelList.size() == 0) {
            mHeroStatsRecyclerView.setVisibility(View.GONE);
            mHeroPerformanceStatsPlaceholderLayout.setVisibility(View.GONE);
        } else {
            mHeroStatsRecyclerView.setVisibility(View.VISIBLE);
            mHeroPerformanceStatsPlaceholderLayout.setVisibility(View.GONE);
        }
    }

    public interface HeroPerformanceStatsListFragmentInterface {

        void fetchHeroPerformanceStats();

        void refreshHeroPerformanceStats();
    }
}
