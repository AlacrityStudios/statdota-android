package com.alacritystudios.statdota.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.adapters.UserFriendsSummaryAdapter;
import com.alacritystudios.statdota.adapters.UserFriendsSummaryAdapter;
import com.alacritystudios.statdota.constants.BundleConstants;
import com.alacritystudios.statdota.fragments.base.BaseFragment;
import com.alacritystudios.statdota.models.UserFriendsModel;
import com.alacritystudios.statdota.models.UserHeroSummaryModel;
import com.alacritystudios.statdota.models.UserMatchSummaryModel;
import com.alacritystudios.statdota.utils.ComponentUtil;
import com.alacritystudios.statdota.utils.LoggerUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Displays a list of user's friends.
 */

public class UserFriendsFragment extends BaseFragment implements BaseFragment.BaseFragmentImpl{

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayout mPlaceholderLayout;
    private LinearLayout mErrorLayout;
    private RecyclerView mFriendsSummaryRecyclerView;

    private UserFriendsSummaryAdapter mUserFriendsSummaryAdapter;
    private LinearLayoutManager mUserFriendsListLinearLayoutManager;

    private UserFriendsFragment.UserFriendsFragmentInterface mUserFriendsFragmentInterface;
    private long mAccountId;
    private boolean mIsUserAccount;

    public static UserFriendsFragment newInstance(long accountId, boolean isUserAccount) {
        UserFriendsFragment userFriendsFragment = new UserFriendsFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(BundleConstants.ACCOUNT_ID, accountId);
        bundle.putBoolean(BundleConstants.IS_USER_ACCOUNT, isUserAccount);
        userFriendsFragment.setArguments(bundle);
        return userFriendsFragment;
    }

    @Override
    public void initialiseUI() {
        mFriendsSummaryRecyclerView = (RecyclerView) mFragmentView.findViewById(R.id.recycler_view_user_friends_summary);
        mSwipeRefreshLayout = (SwipeRefreshLayout) mFragmentView.findViewById(R.id.fragment_user_friends_swipe_refresh_layout);
        mPlaceholderLayout = (LinearLayout) mFragmentView.findViewById(R.id.user_friends_fragment_placeholder_layout);
        mErrorLayout = (LinearLayout) mFragmentView.findViewById(R.id.user_friends_fragment_error_layout);
        mFriendsSummaryRecyclerView.setLayoutManager(mUserFriendsListLinearLayoutManager);
        mFriendsSummaryRecyclerView.setAdapter(mUserFriendsSummaryAdapter);
        setupSwipeRefreshLayout();
        mSwipeRefreshLayout.setRefreshing(true);
        mUserFriendsFragmentInterface.fetchFriendFragmentStats(String.valueOf(mAccountId));
    }

    @Override
    public void setListeners() {

    }

    @Override
    public void setContextAndTags() {
        mContext = getActivity();
        TAG = LoggerUtil.makeLogTag(getClass());
    }

    @Override
    public void initialiseOtherComponents() {
        mAccountId = getArguments().getLong(BundleConstants.ACCOUNT_ID);
        mIsUserAccount = getArguments().getBoolean(BundleConstants.IS_USER_ACCOUNT);
        mUserFriendsSummaryAdapter = new UserFriendsSummaryAdapter(new ArrayList<UserFriendsModel>(), mContext);
        mUserFriendsListLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_user_friends, container, false);
        setContextAndTags();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
        return mFragmentView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof UserFriendsFragmentInterface) {
            mUserFriendsFragmentInterface = (UserFriendsFragmentInterface) context;
        } else {
            throw new ClassCastException("LandingActivity must implement UserFriendsFragmentInterface.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mUserFriendsFragmentInterface = null;
    }

    /**
     * Setup the swipe refresh layout behaviour.
     */
    private void setupSwipeRefreshLayout() {
        Log.i(TAG, "setupSwipeRefreshLayout()");
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                mUserFriendsFragmentInterface.refreshFriendFragmentStats(String.valueOf(mAccountId));
            }
        });
    }

    /**
     * To be called when the query succeeds.
     * @param userFriendsModelList - The resulting list to be shown.
     */
    public void onUserFriendsQuerySuccess(List<UserFriendsModel> userFriendsModelList) {
        Log.i(TAG, "onTeamDetailsQuerySuccess()");
        mUserFriendsSummaryAdapter.setmUserFriendsModelList(userFriendsModelList);
        mUserFriendsSummaryAdapter.notifyDataSetChanged();
        if (userFriendsModelList.size() == 0) {
            mFriendsSummaryRecyclerView.setVisibility(View.GONE);
            mErrorLayout.setVisibility(View.GONE);
            mPlaceholderLayout.setVisibility(View.VISIBLE);
        } else {
            mFriendsSummaryRecyclerView.setVisibility(View.VISIBLE);
            mErrorLayout.setVisibility(View.GONE);
            mPlaceholderLayout.setVisibility(View.GONE);
        }
        mSwipeRefreshLayout.setRefreshing(false);
    }

    /**
     * To be called when the query fails.
     */
    public void onUserFriendsQueryFailure(boolean isCancelled) {
        Log.i(TAG, "onUserFriendsQueryFailure()");
        if(!isCancelled) {
            mFriendsSummaryRecyclerView.setVisibility(View.GONE);
            mErrorLayout.setVisibility(View.VISIBLE);
            mPlaceholderLayout.setVisibility(View.GONE);
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    public interface UserFriendsFragmentInterface {

        void fetchFriendFragmentStats(String accountId);

        void refreshFriendFragmentStats(String accountId);
    }
}
