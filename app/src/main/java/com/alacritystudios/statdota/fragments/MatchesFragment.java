package com.alacritystudios.statdota.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.adapters.ViewPagerAdapter;
import com.alacritystudios.statdota.fragments.base.BaseFragment;
import com.alacritystudios.statdota.models.PubMatchesModel;
import com.alacritystudios.statdota.models.RecentLeagueMatchModel;
import com.alacritystudios.statdota.utils.LoggerUtil;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import java.util.List;

/**
 * Container fragment to view league matches.
 */

public class MatchesFragment extends BaseFragment implements BaseFragment.BaseFragmentImpl {

    private Toolbar mToolbar;
    private SmartTabLayout mMatchesFragmentSmartTabLayout;
    private ViewPager mMatchesFragmentViewPager;

    private ViewPagerAdapter mViewPagerAdapter;
    private RecentLeagueMatchesFragment mRecentLeagueMatchesFragment;
    private RecentPubMatchesFragment mRecentPubMatchesFragment;
    private LiveLeagueMatchesFragment mLiveLeagueMatchesFragment;


    public static MatchesFragment newInstance() {
        MatchesFragment matchesFragment = new MatchesFragment();
        Bundle bundle = new Bundle();
        matchesFragment.setArguments(bundle);
        return matchesFragment;
    }

    @Override
    public void initialiseUI() {
        mToolbar = (Toolbar) mFragmentView.findViewById(R.id.matches_fragment_toolbar);
        mMatchesFragmentSmartTabLayout = (SmartTabLayout) mFragmentView.findViewById(R.id.matches_fragment_smart_tab_layout);
        mMatchesFragmentViewPager = (ViewPager) mFragmentView.findViewById(R.id.matches_fragment_view_pager);
        setupToolbar();
        setupViewPager();
    }

    @Override
    public void setListeners() {

    }

    @Override
    public void setContextAndTags() {
        TAG = LoggerUtil.makeLogTag(getClass());
        mContext = getActivity();
    }

    @Override
    public void initialiseOtherComponents() {
        mViewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        mRecentPubMatchesFragment = RecentPubMatchesFragment.newInstance();
        mRecentLeagueMatchesFragment = RecentLeagueMatchesFragment.newInstance();
        mLiveLeagueMatchesFragment = LiveLeagueMatchesFragment.newInstance();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_matches, container, false);
        setContextAndTags();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
        return mFragmentView;
    }
    /**
     * Sets up the toolbar for the activity.
     */
    private void setupToolbar() {
        Log.i(TAG, "setupToolbar()");
        mToolbar.setTitleTextAppearance(mContext, R.style.ToolbarTitleAppearance);
        mToolbar.setTitle(R.string.matches);
        ((AppCompatActivity) mContext).setSupportActionBar(mToolbar);
    }

    /**
     * Sets up the view pager.
     */
    private void setupViewPager() {
        Log.i(TAG, "setupViewPager()");
        mViewPagerAdapter.addFragment(mRecentPubMatchesFragment, mContext.getString(R.string.recent_pub));
        mViewPagerAdapter.addFragment(mRecentLeagueMatchesFragment, mContext.getString(R.string.recent_league));
        mViewPagerAdapter.addFragment(mLiveLeagueMatchesFragment, mContext.getString(R.string.live_league));
        mMatchesFragmentViewPager.setOffscreenPageLimit(2);
        mMatchesFragmentViewPager.setAdapter(mViewPagerAdapter);
        mMatchesFragmentSmartTabLayout.setViewPager(mMatchesFragmentViewPager);
    }

    /**
     * To be called when the query succeeds.
     *
     * @param pubMatchesModelList - The resulting list to be shown.
     */
    public void onRecentPubMatchesQuerySuccess(List<PubMatchesModel> pubMatchesModelList) {
        Log.i(TAG, "onRecentPubMatchesQuerySuccess()");
        mRecentPubMatchesFragment.onRecentPubMatchesQuerySuccess(pubMatchesModelList);
    }

    /**
     * To be called when the query fails.
     */
    public void onRecentPubMatchesQueryFailure(boolean isCancelled) {
        Log.i(TAG, "onRecentPubMatchesQueryFailure()");
        mRecentPubMatchesFragment.onRecentPubMatchesQueryFailure(isCancelled);
    }

    /**
     * To be called when the query succeeds.
     *
     * @param recentLeagueMatchModelList - The resulting list to be shown.
     */
    public void onRecentLeagueMatchesQuerySuccess(List<RecentLeagueMatchModel> recentLeagueMatchModelList) {
        Log.i(TAG, "onRecentLeagueMatchesQuerySuccess()");
        mRecentLeagueMatchesFragment.onRecentLeagueMatchesQuerySuccess(recentLeagueMatchModelList);
    }

    /**
     * To be called when the query fails.
     */
    public void onRecentLeagueMatchesQueryFailure(boolean isCancelled) {
        Log.i(TAG, "onRecentLeagueMatchesQueryFailure()");
        mRecentLeagueMatchesFragment.onRecentLeagueMatchesQueryFailure(isCancelled);
    }
}
