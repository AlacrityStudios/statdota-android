package com.alacritystudios.statdota.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.adapters.FiltersAutoCompleteTextViewAdapter;
import com.alacritystudios.statdota.adapters.MatchesFilterHeadingAdapter;
import com.alacritystudios.statdota.adapters.SelectedFilterAdapter;
import com.alacritystudios.statdota.adapters.UserMatchesAdapter;
import com.alacritystudios.statdota.constants.ApplicationConstants;
import com.alacritystudios.statdota.constants.BundleConstants;
import com.alacritystudios.statdota.enums.FilterType;
import com.alacritystudios.statdota.fragments.base.BaseFragment;
import com.alacritystudios.statdota.interfaces.FilterItemRemovalListener;
import com.alacritystudios.statdota.models.FiltersAutoCompleteTextViewModel;
import com.alacritystudios.statdota.models.HeroDetails;
import com.alacritystudios.statdota.models.MatchFilterHeadingModel;
import com.alacritystudios.statdota.models.MatchFilterResultModel;
import com.alacritystudios.statdota.models.MatchesFilterBodyModel;
import com.alacritystudios.statdota.models.UserMatchSummaryModel;
import com.alacritystudios.statdota.sql.StatDotaDatabaseHelper;
import com.alacritystudios.statdota.utils.ApiUtils;
import com.alacritystudios.statdota.utils.ComponentUtil;
import com.alacritystudios.statdota.utils.LoggerUtil;
import com.bumptech.glide.Glide;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Displays a list of the summary of user's matches.
 */

public class UserMatchesFragment extends BaseFragment implements BaseFragment.BaseFragmentImpl, MatchesFilterHeadingAdapter.MatchesFilterHeadingAdapterListener, UserMatchesFilterDialogFragment.UserMatchesFilterDialogFragmentInterface {

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayout mPlaceholderLayout;
    private LinearLayout mErrorLayout;
    private TextView mSeeAllFiltersTextView;
    private ExpandableLayout mFiltersExpandableLayout;
    private RecyclerView mFiltersHeadingRecyclerView;
    private RecyclerView mMatchesSummaryRecyclerView;
    private UserMatchesAdapter mUserMatchesAdapter;
    private MatchesFilterHeadingAdapter mMatchesFilterHeadingAdapter;

    private List<MatchFilterHeadingModel> mMatchFilterHeadingModelList;
    private StaggeredGridLayoutManager matchesFilterHeadingGridLayoutManager;

    private LinearLayoutManager mUserMatchListLinearLayoutManager;
    private List<UserMatchSummaryModel> mUserMatchSummaryModelList;

    private UserMatchesFragmentInterface mUserMatchesFragmentInterface;

    private List<FiltersAutoCompleteTextViewModel> mHeroList;
    private List<FiltersAutoCompleteTextViewModel> mFriendsList;
    private List<FiltersAutoCompleteTextViewModel> mResultList;
    private FiltersAutoCompleteTextViewModel mSelectedHeroFilter;
    private FiltersAutoCompleteTextViewModel mSelectedResultsFilter;
    private List<FiltersAutoCompleteTextViewModel> mSelectedAlliedHeroFilter;
    private List<FiltersAutoCompleteTextViewModel> mSelectedAgainstHeroFilter;
    private SelectedFilterAdapter mAlliedHeroesAdapter;
    private SelectedFilterAdapter mAgainstHeroesAdapter;
    private LinearLayoutManager mAlliedHeroesLinearLayoutManager;
    private LinearLayoutManager mAgainstHeroesLinearLayoutManager;

    private long mAccountId;
    private boolean mIsUserAccount;

    private int mPageNumber = 0;
    private int mPreviousTotal = 0;
    private boolean mLoading = true;
    private int mVisibleThreshold = 10;
    private int mVisibleItemCount;
    private int mTotalItemCount;
    private int mFirstVisibleItem;


    private MatchFilterResultModel mMatchFilterResultModel;

    public static UserMatchesFragment newInstance(long accountId, boolean isUserAccount) {
        UserMatchesFragment userMatchesFragment = new UserMatchesFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(BundleConstants.ACCOUNT_ID, accountId);
        bundle.putBoolean(BundleConstants.IS_USER_ACCOUNT, isUserAccount);
        userMatchesFragment.setArguments(bundle);
        return userMatchesFragment;
    }

    @Override
    public void initialiseUI() {
        mFiltersHeadingRecyclerView = (RecyclerView) mFragmentView.findViewById(R.id.filter_heading_recycler_view);
        mMatchesSummaryRecyclerView = (RecyclerView) mFragmentView.findViewById(R.id.recycler_view_matches);
        mSwipeRefreshLayout = (SwipeRefreshLayout) mFragmentView.findViewById(R.id.user_matches_fragment_swipe_refresh_layout);
        mPlaceholderLayout = (LinearLayout) mFragmentView.findViewById(R.id.user_matches_fragment_placeholder_layout);
        mErrorLayout = (LinearLayout) mFragmentView.findViewById(R.id.user_matches_fragment_error_layout);
        mSeeAllFiltersTextView = (TextView) mFragmentView.findViewById(R.id.see_all_filters_text_view);
        mFiltersExpandableLayout = (ExpandableLayout) mFragmentView.findViewById(R.id.filters_expandable_layout);
        setupFilterHeader();
        setupMatchesRecyclerView();
        setupSwipeRefreshLayout();
        mSwipeRefreshLayout.setRefreshing(true);
        mUserMatchesFragmentInterface.fetchUserMatches(String.valueOf(mAccountId), 50, mPageNumber * 50, mMatchFilterResultModel);
    }

    @Override
    public void setListeners() {
        mMatchesSummaryRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                mVisibleItemCount = mMatchesSummaryRecyclerView.getChildCount();
                mTotalItemCount = mUserMatchListLinearLayoutManager.getItemCount();
                mFirstVisibleItem = mUserMatchListLinearLayoutManager.findFirstVisibleItemPosition();

                if (mLoading) {
                    if (mTotalItemCount > mPreviousTotal) {
                        mLoading = false;
                        mPreviousTotal = mTotalItemCount;
                    }
                }
                if (!mLoading && (mTotalItemCount - mVisibleItemCount) <= (mFirstVisibleItem + mVisibleThreshold)) {
                    Log.d(TAG, "Detected need to fetch new details from backend");
                    mPageNumber++;
                    mSwipeRefreshLayout.setRefreshing(true);
                    mUserMatchesFragmentInterface.fetchUserMatches(String.valueOf(mAccountId), 50, mPageNumber * 50, mMatchFilterResultModel);
                    mLoading = true;
                }
            }
        });
        mSeeAllFiltersTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mFiltersExpandableLayout.isExpanded()) {
                    mFiltersExpandableLayout.collapse(true);
                } else {
                    mFiltersExpandableLayout.expand(true);
                }
            }
        });
    }

    @Override
    public void setContextAndTags() {
        mContext = getActivity();
        TAG = LoggerUtil.makeLogTag(getClass());
    }

    @Override
    public void initialiseOtherComponents() {
        mPageNumber = 0;
        mPreviousTotal = 0;
        mAccountId = getArguments().getLong(BundleConstants.ACCOUNT_ID);
        mIsUserAccount = getArguments().getBoolean(BundleConstants.IS_USER_ACCOUNT);
        mUserMatchSummaryModelList = new ArrayList<>();
        mUserMatchesAdapter = new UserMatchesAdapter(mUserMatchSummaryModelList, mContext);
        mUserMatchListLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);

        //Filters related.
        mHeroList = new ArrayList<>();
        StatDotaDatabaseHelper statDotaDatabaseHelper = new StatDotaDatabaseHelper(mContext);
        List<HeroDetails> heroDetails = statDotaDatabaseHelper.fetchHeroDetailsList();
        for (HeroDetails hero : heroDetails) {
            mHeroList.add(new FiltersAutoCompleteTextViewModel(hero.getLocalizedName(), ApiUtils.getHeroImageUrl(hero.getName()), hero.getId().longValue()));
        }
        mResultList = new ArrayList<>();
        mResultList.add(new FiltersAutoCompleteTextViewModel(mContext.getString(R.string.win), "", 1l));
        mResultList.add(new FiltersAutoCompleteTextViewModel(mContext.getString(R.string.loss), "", 0l));
        mSelectedAlliedHeroFilter = new ArrayList<FiltersAutoCompleteTextViewModel>();
        mSelectedAgainstHeroFilter = new ArrayList<FiltersAutoCompleteTextViewModel>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_user_matches, container, false);
        setContextAndTags();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
        return mFragmentView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof UserMatchesFragmentInterface) {
            mUserMatchesFragmentInterface = (UserMatchesFragmentInterface) context;
        } else {
            throw new ClassCastException("LandingActivity must implement UserMatchesFragmentInterface.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mUserMatchesFragmentInterface = null;
    }

    @Override
    public void onItemClick(FilterType filterType) {
        List<MatchesFilterBodyModel> matchesFilterBodyModels;
        UserMatchesFilterDialogFragment userMatchesFilterDialogFragment;
        StatDotaDatabaseHelper statDotaDatabaseHelper = new StatDotaDatabaseHelper(mContext);
        List<HeroDetails> heroDetails = statDotaDatabaseHelper.fetchHeroDetailsList();
        switch (filterType) {
            case MATCH_FILTER_HERO_PLAYED:
                matchesFilterBodyModels = new ArrayList<>();
                for (HeroDetails hero : heroDetails) {
                    if (hero.getId() == mMatchFilterResultModel.getHeroPlayedId()) {
                        matchesFilterBodyModels.add(new MatchesFilterBodyModel(hero.getLocalizedName(), String.valueOf(hero.getId()), true));
                    } else {
                        matchesFilterBodyModels.add(new MatchesFilterBodyModel(hero.getLocalizedName(), String.valueOf(hero.getId()), false));
                    }
                }
                userMatchesFilterDialogFragment = UserMatchesFilterDialogFragment.newInstance(matchesFilterBodyModels, mMatchFilterResultModel, FilterType.MATCH_FILTER_HERO_PLAYED, true, this);
                userMatchesFilterDialogFragment.show(getFragmentManager(), "dialog");
                break;

            case MATCH_FILTER_LOBBY_TYPE:
                matchesFilterBodyModels = new ArrayList<>();
                for (int i = 0; i < ApplicationConstants.lobbyTypeArray.length; i++) {
                    if (mMatchFilterResultModel.getLobbyType() == i) {
                        matchesFilterBodyModels.add(new MatchesFilterBodyModel(ApplicationConstants.lobbyTypeArray[i], String.valueOf(i), true));
                    } else {
                        matchesFilterBodyModels.add(new MatchesFilterBodyModel(ApplicationConstants.lobbyTypeArray[i], String.valueOf(i), false));
                    }
                }
                userMatchesFilterDialogFragment = UserMatchesFilterDialogFragment.newInstance(matchesFilterBodyModels, mMatchFilterResultModel, FilterType.MATCH_FILTER_LOBBY_TYPE, false, this);
                userMatchesFilterDialogFragment.show(getFragmentManager(), "dialog");
                break;

            case MATCH_FILTER_ALLIED_HERO:
                matchesFilterBodyModels = new ArrayList<>();
                for (HeroDetails hero : heroDetails) {
                    for (Integer alliedHeroId : mMatchFilterResultModel.getAlliedHeroIdList()) {
                        if (alliedHeroId == hero.getId()) {
                            matchesFilterBodyModels.add(new MatchesFilterBodyModel(hero.getLocalizedName(), String.valueOf(hero.getId()), true));
                            continue;
                        }
                    }
                    matchesFilterBodyModels.add(new MatchesFilterBodyModel(hero.getLocalizedName(), String.valueOf(hero.getId()), false));
                }
                userMatchesFilterDialogFragment = UserMatchesFilterDialogFragment.newInstance(matchesFilterBodyModels, mMatchFilterResultModel, FilterType.MATCH_FILTER_ALLIED_HERO, false, this);
                userMatchesFilterDialogFragment.show(getFragmentManager(), "dialog");
                break;

            case MATCH_FILTER_AGAINST_HERO:
                matchesFilterBodyModels = new ArrayList<>();
                for (HeroDetails hero : heroDetails) {
                    for (Integer againstHeroId : mMatchFilterResultModel.getAgainstHeroIdList()) {
                        if (againstHeroId == hero.getId()) {
                            matchesFilterBodyModels.add(new MatchesFilterBodyModel(hero.getLocalizedName(), String.valueOf(hero.getId()), true));
                            continue;
                        }
                    }
                    matchesFilterBodyModels.add(new MatchesFilterBodyModel(hero.getLocalizedName(), String.valueOf(hero.getId()), false));
                }
                userMatchesFilterDialogFragment = UserMatchesFilterDialogFragment.newInstance(matchesFilterBodyModels, mMatchFilterResultModel, FilterType.MATCH_FILTER_AGAINST_HERO, false, this);
                userMatchesFilterDialogFragment.show(getFragmentManager(), "dialog");
                break;

            case MATCH_FILTER_GAME_MODE:
                matchesFilterBodyModels = new ArrayList<>();
                for (int i = 0; i < ApplicationConstants.gameModeArray.length; i++) {
                    if (mMatchFilterResultModel.getGameMode() == i) {
                        matchesFilterBodyModels.add(new MatchesFilterBodyModel(ApplicationConstants.gameModeArray[i], String.valueOf(i), true));
                    } else {
                        matchesFilterBodyModels.add(new MatchesFilterBodyModel(ApplicationConstants.gameModeArray[i], String.valueOf(i), false));
                    }
                }
                userMatchesFilterDialogFragment = UserMatchesFilterDialogFragment.newInstance(matchesFilterBodyModels, mMatchFilterResultModel, FilterType.MATCH_FILTER_GAME_MODE, true, this);
                userMatchesFilterDialogFragment.show(getFragmentManager(), "dialog");
                break;


            case MATCH_FILTER_RESULT:
                matchesFilterBodyModels = new ArrayList<>();
                matchesFilterBodyModels.add(new MatchesFilterBodyModel(mContext.getString(R.string.win), String.valueOf(1), false));
                matchesFilterBodyModels.add(new MatchesFilterBodyModel(mContext.getString(R.string.loss), String.valueOf(0), false));
                userMatchesFilterDialogFragment = UserMatchesFilterDialogFragment.newInstance(matchesFilterBodyModels, mMatchFilterResultModel, FilterType.MATCH_FILTER_RESULT, true, this);
                userMatchesFilterDialogFragment.show(getFragmentManager(), "dialog");
                break;

            case MATCH_FILTER_SIDE:
                matchesFilterBodyModels = new ArrayList<>();
                matchesFilterBodyModels.add(new MatchesFilterBodyModel(mContext.getString(R.string.radiant), String.valueOf(1), false));
                matchesFilterBodyModels.add(new MatchesFilterBodyModel(mContext.getString(R.string.dire), String.valueOf(0), false));
                userMatchesFilterDialogFragment = UserMatchesFilterDialogFragment.newInstance(matchesFilterBodyModels, mMatchFilterResultModel, FilterType.MATCH_FILTER_SIDE, true, this);
                userMatchesFilterDialogFragment.show(getFragmentManager(), "dialog");
                break;
        }
    }

    @Override
    public void onFiltersChanged(MatchFilterResultModel matchFilterResultModel) {
        for (MatchFilterHeadingModel matchFilterHeadingModel : mMatchFilterHeadingModelList) {
            switch (matchFilterHeadingModel.getFilterType()) {
                case MATCH_FILTER_HERO_PLAYED:
                    if (matchFilterResultModel.getHeroPlayedId() != -1) {
                        matchFilterHeadingModel.setHasUserChoices(true);
                    } else {
                        matchFilterHeadingModel.setHasUserChoices(false);
                    }
                    break;

                case MATCH_FILTER_LOBBY_TYPE:
                    if (matchFilterResultModel.getLobbyType() != -1) {
                        matchFilterHeadingModel.setHasUserChoices(true);
                    } else {
                        matchFilterHeadingModel.setHasUserChoices(false);
                    }
                    break;

                case MATCH_FILTER_ALLIED_HERO:
                    if (matchFilterResultModel.getAlliedHeroIdList().size() != 0) {
                        matchFilterHeadingModel.setHasUserChoices(true);
                    } else {
                        matchFilterHeadingModel.setHasUserChoices(false);
                    }
                    break;

                case MATCH_FILTER_AGAINST_HERO:
                    if (matchFilterResultModel.getAgainstHeroIdList().size() != 0) {
                        matchFilterHeadingModel.setHasUserChoices(true);
                    } else {
                        matchFilterHeadingModel.setHasUserChoices(false);
                    }
                    break;

                case MATCH_FILTER_GAME_MODE:
                    if (matchFilterResultModel.getGameMode() != -1) {
                        matchFilterHeadingModel.setHasUserChoices(true);
                    } else {
                        matchFilterHeadingModel.setHasUserChoices(false);
                    }
                    break;

                case MATCH_FILTER_RESULT:
                    if (matchFilterResultModel.getWinCondition() != -1) {
                        matchFilterHeadingModel.setHasUserChoices(true);
                    } else {
                        matchFilterHeadingModel.setHasUserChoices(false);
                    }
                    break;

                case MATCH_FILTER_SIDE:
                    if (matchFilterResultModel.getIsRadiant() != -1) {
                        matchFilterHeadingModel.setHasUserChoices(true);
                    } else {
                        matchFilterHeadingModel.setHasUserChoices(false);
                    }
                    break;
            }
        }
        mMatchesFilterHeadingAdapter.notifyDataSetChanged();
        mPageNumber = 0;
        mSwipeRefreshLayout.setRefreshing(true);
        mUserMatchesFragmentInterface.fetchUserMatches(String.valueOf(mAccountId), 50, mPageNumber * 50, mMatchFilterResultModel);
        mLoading = true;
    }

    /**
     * Setup the filter heading.
     */
    private void setupFilterHeader() {
        Log.i(TAG, "setupFilterHeader()");
        mMatchFilterResultModel = new MatchFilterResultModel();
        mMatchFilterHeadingModelList = new ArrayList<>();
        mMatchFilterHeadingModelList.add(new MatchFilterHeadingModel(mContext.getString(R.string.hero_played), FilterType.MATCH_FILTER_HERO_PLAYED, false));
        mMatchFilterHeadingModelList.add(new MatchFilterHeadingModel(mContext.getString(R.string.result), FilterType.MATCH_FILTER_RESULT, false));
        mMatchFilterHeadingModelList.add(new MatchFilterHeadingModel(mContext.getString(R.string.game_mode), FilterType.MATCH_FILTER_GAME_MODE, false));
        mMatchFilterHeadingModelList.add(new MatchFilterHeadingModel(mContext.getString(R.string.lobby_type), FilterType.MATCH_FILTER_LOBBY_TYPE, false));
        mMatchFilterHeadingModelList.add(new MatchFilterHeadingModel(mContext.getString(R.string.side), FilterType.MATCH_FILTER_SIDE, false));
        mMatchFilterHeadingModelList.add(new MatchFilterHeadingModel(mContext.getString(R.string.allied_heroes), FilterType.MATCH_FILTER_ALLIED_HERO, false));
        mMatchFilterHeadingModelList.add(new MatchFilterHeadingModel(mContext.getString(R.string.against_heroes), FilterType.MATCH_FILTER_AGAINST_HERO, false));
        matchesFilterHeadingGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.HORIZONTAL);
        mMatchesFilterHeadingAdapter = new MatchesFilterHeadingAdapter(mContext, mMatchFilterHeadingModelList, this);
        mFiltersHeadingRecyclerView.setLayoutManager(matchesFilterHeadingGridLayoutManager);
        mFiltersHeadingRecyclerView.setAdapter(mMatchesFilterHeadingAdapter);
    }

    /**
     * Setup the matches recycler view.
     */
    private void setupMatchesRecyclerView() {
        Log.i(TAG, "setupMatchesRecyclerView()");
        mMatchesSummaryRecyclerView.setLayoutManager(mUserMatchListLinearLayoutManager);
        mMatchesSummaryRecyclerView.setAdapter(mUserMatchesAdapter);
    }

    /**
     * Setup the swipe refresh layout behaviour.
     */
    private void setupSwipeRefreshLayout() {
        Log.i(TAG, "setupSwipeRefreshLayout()");
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                mUserMatchesAdapter.notifyDataSetChanged();
                mPageNumber = 0;
                mPreviousTotal = 0;
                mSwipeRefreshLayout.setRefreshing(true);
                mUserMatchesFragmentInterface.fetchUserMatches(String.valueOf(mAccountId), 50, mPageNumber * 50, mMatchFilterResultModel);
            }
        });
    }

    /**
     * To be called when the query succeeds.
     *
     * @param userMatchSummaryModelList - The resulting list to be shown.
     */
    public void onUserMatchesQuerySuccess(List<UserMatchSummaryModel> userMatchSummaryModelList) {
        Log.i(TAG, "onUserMatchesQuerySuccess()");
        mUserMatchesAdapter.setmUserMatchSummaryModelList(userMatchSummaryModelList);
        mUserMatchesAdapter.notifyDataSetChanged();
        if (userMatchSummaryModelList.size() == 0) {
            mMatchesSummaryRecyclerView.setVisibility(View.GONE);
            mErrorLayout.setVisibility(View.GONE);
            mPlaceholderLayout.setVisibility(View.VISIBLE);
        } else {
            mMatchesSummaryRecyclerView.setVisibility(View.VISIBLE);
            mErrorLayout.setVisibility(View.GONE);
            mPlaceholderLayout.setVisibility(View.GONE);
        }
        mLoading = false;
        mSwipeRefreshLayout.setRefreshing(false);
    }

    /**
     * To be called when the query fails.
     */
    public void onUserMatchesQueryFailure(boolean isCancelled) {
        Log.i(TAG, "onUserMatchesQueryFailure()");
        if (!isCancelled && mUserMatchesAdapter.getmUserMatchSummaryModelList().size() != 0) {
            ComponentUtil.createAndShowSnackBar(mContext, mSwipeRefreshLayout, mContext.getString(R.string.fragment_matches_error_message));
            mMatchesSummaryRecyclerView.setVisibility(View.VISIBLE);
            mErrorLayout.setVisibility(View.GONE);
            mPlaceholderLayout.setVisibility(View.GONE);
            mSwipeRefreshLayout.setRefreshing(false);
        } else if (!isCancelled && mUserMatchesAdapter.getmUserMatchSummaryModelList().size() == 0) {
            mMatchesSummaryRecyclerView.setVisibility(View.GONE);
            mErrorLayout.setVisibility(View.VISIBLE);
            mPlaceholderLayout.setVisibility(View.GONE);
            mSwipeRefreshLayout.setRefreshing(false);
        }
        mPageNumber--;
        mLoading = false;
    }


    /**
     * Show the filters layout.
     */
    private void setupAndShowFilters() {
        Log.i(TAG, "setupAndShowFilters()");
        mAlliedHeroesAdapter = new SelectedFilterAdapter(mSelectedAlliedHeroFilter, mContext, new FilterItemRemovalListener() {

            @Override
            public void removeItemAt(int position) {
                mSelectedAlliedHeroFilter.remove(position);
                mAlliedHeroesAdapter.notifyDataSetChanged();
            }
        });
        mAgainstHeroesAdapter = new SelectedFilterAdapter(mSelectedAgainstHeroFilter, mContext, new FilterItemRemovalListener() {

            @Override
            public void removeItemAt(int position) {
                mSelectedAgainstHeroFilter.remove(position);
                mAgainstHeroesAdapter.notifyDataSetChanged();
            }
        });
        mAlliedHeroesLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        mAgainstHeroesLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        final View contentView = LayoutInflater.from(mContext).inflate(R.layout.layout_match_filters, mSwipeRefreshLayout, false);
        final AutoCompleteTextView mHeroTextView = (AutoCompleteTextView) contentView.findViewById(R.id.hero_auto_complete_text_view);
        final AutoCompleteTextView mResultTextView = (AutoCompleteTextView) contentView.findViewById(R.id.result_type_auto_complete_text_view);
        final AutoCompleteTextView mAlliedHeroesTextView = (AutoCompleteTextView) contentView.findViewById(R.id.allied_heroes_auto_complete_text_view);
        final AutoCompleteTextView mAgainstHeroesTextView = (AutoCompleteTextView) contentView.findViewById(R.id.against_heroes_auto_complete_text_view);
        final AutoCompleteTextView mFriendsTextView = (AutoCompleteTextView) contentView.findViewById(R.id.friends_auto_complete_text_view);
        final RecyclerView mAlliedHeroesFilterRecyclerView = (RecyclerView) contentView.findViewById(R.id.allied_heroes_filter_recycler_view);
        final RecyclerView mAgainstHeroesFilterRecyclerView = (RecyclerView) contentView.findViewById(R.id.against_heroes_filter_recycler_view);
        mHeroTextView.setThreshold(0);
        mResultTextView.setThreshold(0);
        mAlliedHeroesTextView.setThreshold(0);
        mAgainstHeroesTextView.setThreshold(0);
        mFriendsTextView.setThreshold(0);
        final FiltersAutoCompleteTextViewAdapter mHeroTextViewAdapter = new FiltersAutoCompleteTextViewAdapter(mContext, mHeroList);
        final FiltersAutoCompleteTextViewAdapter mAlliedHeroTextViewAdapter = new FiltersAutoCompleteTextViewAdapter(mContext, mHeroList);
        final FiltersAutoCompleteTextViewAdapter mAgainstHeroTextViewAdapter = new FiltersAutoCompleteTextViewAdapter(mContext, mHeroList);
        final FiltersAutoCompleteTextViewAdapter mResultTextViewAdapter = new FiltersAutoCompleteTextViewAdapter(mContext, mResultList);
        mHeroTextView.setAdapter(mHeroTextViewAdapter);
        mAlliedHeroesTextView.setAdapter(mAlliedHeroTextViewAdapter);
        mAgainstHeroesTextView.setAdapter(mAgainstHeroTextViewAdapter);
        mResultTextView.setAdapter(mResultTextViewAdapter);

        mAlliedHeroesFilterRecyclerView.setLayoutManager(mAlliedHeroesLinearLayoutManager);
        mAlliedHeroesFilterRecyclerView.setAdapter(mAlliedHeroesAdapter);
        mAgainstHeroesFilterRecyclerView.setLayoutManager(mAgainstHeroesLinearLayoutManager);
        mAgainstHeroesFilterRecyclerView.setAdapter(mAgainstHeroesAdapter);
        mHeroTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setHeroFilterView(contentView, mHeroTextViewAdapter.getmFilteredSuggestionsList().get(position));
                mHeroTextView.setText("");
            }
        });

        mResultTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setResultFilterView(contentView, mResultTextViewAdapter.getmFilteredSuggestionsList().get(position));
                mResultTextView.setText("");
            }
        });

        mAlliedHeroesTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setAlliedHeroesFilterView(mAlliedHeroesFilterRecyclerView, mAlliedHeroTextViewAdapter.getmFilteredSuggestionsList().get(position));
                mAlliedHeroesTextView.setText("");
            }
        });

        mAgainstHeroesTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setAgainstHeroesFilterView(mAgainstHeroesFilterRecyclerView, mAgainstHeroTextViewAdapter.getmFilteredSuggestionsList().get(position));
                mAgainstHeroesTextView.setText("");
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(contentView);
        builder.setPositiveButton(R.string.filter, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                triggerQueryWithParams();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        alertDialog.show();
    }

    /**
     * Change filter selected
     *
     * @param heroFilterView - filter selected.
     */
    private void setHeroFilterView(View view, FiltersAutoCompleteTextViewModel heroFilterView) {
        Log.i(TAG, "setHeroFilterView()");
        mSelectedHeroFilter = heroFilterView;
        final CircleImageView logoImageView = (CircleImageView) view.findViewById(R.id.selected_hero_filter_image);
        final TextView nameTextView = (TextView) view.findViewById(R.id.selected_hero_filter_name);
        ImageView clearImageView = (ImageView) view.findViewById(R.id.clear_hero_filter);
        final LinearLayout mSelectedHeroFilterLayout = (LinearLayout) view.findViewById(R.id.selected_hero_filter_layout);
        mSelectedHeroFilterLayout.setVisibility(View.VISIBLE);
        Glide.with(mContext)
                .load(mSelectedHeroFilter.getLogoUrl())
                .centerCrop()
                .into(logoImageView);
        nameTextView.setText(mSelectedHeroFilter.getName());
        clearImageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mSelectedHeroFilter = null;
                mSelectedHeroFilterLayout.setVisibility(View.INVISIBLE);
                nameTextView.setText("");
                logoImageView.setImageDrawable(null);
            }
        });
    }

    /**
     * Change filter selected
     *
     * @param resultsFilterView - filter selected.
     */
    private void setResultFilterView(View view, FiltersAutoCompleteTextViewModel resultsFilterView) {
        Log.i(TAG, "setResultFilterView()");
        mSelectedResultsFilter = resultsFilterView;
        final CircleImageView logoImageView = (CircleImageView) view.findViewById(R.id.selected_result_filter_image);
        final TextView nameTextView = (TextView) view.findViewById(R.id.selected_result_filter_name);
        ImageView clearImageView = (ImageView) view.findViewById(R.id.clear_result_filter);
        final LinearLayout mSelectedresultFilterLayout = (LinearLayout) view.findViewById(R.id.selected_result_filter_layout);
        mSelectedresultFilterLayout.setVisibility(View.VISIBLE);
        Glide.with(mContext)
                .load(mSelectedResultsFilter.getLogoUrl())
                .centerCrop()
                .into(logoImageView);
        nameTextView.setText(mSelectedResultsFilter.getName());
        clearImageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mSelectedResultsFilter = null;
                mSelectedresultFilterLayout.setVisibility(View.INVISIBLE);
                nameTextView.setText("");
                logoImageView.setImageDrawable(null);
            }
        });
    }

    /**
     * Change filter selected
     *
     * @param alliedHeroesFilterView - filter selected.
     */
    private void setAlliedHeroesFilterView(RecyclerView view, FiltersAutoCompleteTextViewModel alliedHeroesFilterView) {
        Log.i(TAG, "setAlliedHeroesFilterView()");
        if (!mSelectedAlliedHeroFilter.contains(alliedHeroesFilterView)) {
            mSelectedAlliedHeroFilter.add(alliedHeroesFilterView);
            mAlliedHeroesAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Change filter selected
     *
     * @param againstHeroesFilterView - filter selected.
     */
    private void setAgainstHeroesFilterView(RecyclerView view, FiltersAutoCompleteTextViewModel againstHeroesFilterView) {
        Log.i(TAG, "setAgainstHeroesFilterView()");
        if (!mSelectedAgainstHeroFilter.contains(againstHeroesFilterView)) {
            mSelectedAgainstHeroFilter.add(againstHeroesFilterView);
            mAgainstHeroesAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Applies the query params to the query and triggers it again.
     */
    private void triggerQueryWithParams() {
        Log.i(TAG, "setAgainstHeroesFilterView()");

    }


    public interface UserMatchesFragmentInterface {

        void fetchUserMatches(String accountId, int limit, int offset, MatchFilterResultModel matchFilterResultModel);
    }
}
