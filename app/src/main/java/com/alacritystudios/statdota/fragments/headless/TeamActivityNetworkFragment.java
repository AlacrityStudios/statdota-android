package com.alacritystudios.statdota.fragments.headless;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.alacritystudios.statdota.constants.StatDotaApiConstants;
import com.alacritystudios.statdota.models.TeamDetailsModel;
import com.alacritystudios.statdota.models.TeamsModel;
import com.alacritystudios.statdota.rest.ApiClient;
import com.alacritystudios.statdota.rest.ApiInterface;
import com.alacritystudios.statdota.utils.LoggerUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Headless fragment to persist data fetched from network calls.
 */

public class TeamActivityNetworkFragment extends Fragment {

    private final String TAG = LoggerUtil.makeLogTag(TeamActivityNetworkFragment.class);

    private List<TeamsModel> mTeamsModelList;

    private Call<List<TeamsModel>> mTeamsModelListCall;

    private Call<TeamDetailsModel> mTeamDetailsModelCall;

    private TeamActivityNetworkFragmentInterface mTeamActivityNetworkFragmentInterface;

    public static TeamActivityNetworkFragment getInstance() {
        TeamActivityNetworkFragment teamActivityNetworkFragment = new TeamActivityNetworkFragment();
        Bundle bundle = new Bundle();
        teamActivityNetworkFragment.setArguments(bundle);
        return teamActivityNetworkFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TeamActivityNetworkFragmentInterface) {
            mTeamActivityNetworkFragmentInterface = (TeamActivityNetworkFragmentInterface) context;
        } else {
            throw new ClassCastException("TeamActivity must implement TeamActivityNetworkFragmentInterface");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mTeamActivityNetworkFragmentInterface = null;
    }

    /**
     * Fetches the records from the backend.
     *
     * @param offset - The number of records to skip.
     * @param count  - The count of the results.
     * @param query  - The teams to query.
     */
    public void fetchTeamsListFromBackend(int offset, int count, String query) {
        Log.i(TAG, "fetchTeamsListFromBackend()");
        if (mTeamsModelListCall != null) {
            mTeamsModelListCall.cancel();
        }
        Retrofit retrofit = ApiClient.getClient(StatDotaApiConstants.STAT_DOTA_BASE_URL);
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        mTeamsModelListCall = apiInterface.getTeams(offset, count, query);
        mTeamsModelListCall.enqueue(new Callback<List<TeamsModel>>() {

            @Override
            public void onResponse(Call<List<TeamsModel>> call, Response<List<TeamsModel>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if(mTeamActivityNetworkFragmentInterface != null) {
                        mTeamActivityNetworkFragmentInterface.onFetchTeamsListSuccessCallback(response.body());
                    }
                } else {
                    if(mTeamActivityNetworkFragmentInterface != null) {
                        mTeamActivityNetworkFragmentInterface.onFetchTeamsListFailureCallback(false);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<TeamsModel>> call, Throwable t) {
                if (call.isCanceled()) {
                    if(mTeamActivityNetworkFragmentInterface != null) {
                        mTeamActivityNetworkFragmentInterface.onFetchTeamsListFailureCallback(true);
                    }
                } else {
                    if(mTeamActivityNetworkFragmentInterface != null) {
                        mTeamActivityNetworkFragmentInterface.onFetchTeamsListFailureCallback(false);
                    }
                }
            }
        });
    }

    /**
     * Fetches the team details from the backend.
     * @param teamId - The team Id of the team who's details are to be fetched.
     */
    public void fetchTeamDetailsFromBackend(long teamId) {
        Log.i(TAG, "fetchTeamDetailsFromBackend()");
        if (mTeamDetailsModelCall != null) {
            mTeamDetailsModelCall.cancel();
        }
        Retrofit retrofit = ApiClient.getClient(StatDotaApiConstants.STAT_DOTA_BASE_URL);
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        mTeamDetailsModelCall = apiInterface.getTeamDetails(teamId);
        mTeamDetailsModelCall.enqueue(new Callback<TeamDetailsModel>() {

            @Override
            public void onResponse(Call<TeamDetailsModel> call, Response<TeamDetailsModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if(mTeamActivityNetworkFragmentInterface != null) {
                        mTeamActivityNetworkFragmentInterface.onFetchTeamDetailsSuccessCallback(response.body());
                    }
                } else {
                    if(mTeamActivityNetworkFragmentInterface != null) {
                        mTeamActivityNetworkFragmentInterface.onFetchTeamDetailsFailureCallback(false);
                    }
                }
            }

            @Override
            public void onFailure(Call<TeamDetailsModel> call, Throwable t) {
                if (call.isCanceled()) {
                    if(mTeamActivityNetworkFragmentInterface != null) {
                        mTeamActivityNetworkFragmentInterface.onFetchTeamDetailsFailureCallback(true);
                    }
                } else {
                    if(mTeamActivityNetworkFragmentInterface != null) {
                        mTeamActivityNetworkFragmentInterface.onFetchTeamDetailsFailureCallback(false);
                    }
                }
            }
        });
    }

    public interface TeamActivityNetworkFragmentInterface {

        void onFetchTeamsListSuccessCallback(List<TeamsModel> teamsModelList);

        void onFetchTeamsListFailureCallback(boolean isCancelled);

        void onFetchTeamDetailsSuccessCallback(TeamDetailsModel teamDetailsModel);

        void onFetchTeamDetailsFailureCallback(boolean isCancelled);
    }
}