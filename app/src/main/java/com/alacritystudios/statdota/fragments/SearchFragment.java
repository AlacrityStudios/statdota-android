package com.alacritystudios.statdota.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.adapters.PlayerSearchResultAdapter;
import com.alacritystudios.statdota.fragments.base.BaseFragment;
import com.alacritystudios.statdota.models.PlayerSearchResultModel;
import com.alacritystudios.statdota.utils.ComponentUtil;
import com.alacritystudios.statdota.utils.LoggerUtil;
import com.lapism.searchview.SearchEditText;
import com.lapism.searchview.SearchView;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment to search for other users based on their persona name.
 */

public class SearchFragment extends BaseFragment implements BaseFragment.BaseFragmentImpl {

    private Toolbar mSearchFragmentToolbar;
    private SearchView mSearchView;
    private SwipeRefreshLayout mSearchFragmentSwipeRefreshLayout;

    private LinearLayoutManager mPlayersLinearLayoutManager;
    private PlayerSearchResultAdapter mPlayerSearchResultAdapter;

    private RecyclerView mPlayerSearchRecyclerView;
    private LinearLayout mPlayerSearchResultsPlaceholderLayout;
    private LinearLayout mPlayerSearchResultsErrorLayout;

    private SearchFragmentListenerInterface mSearchFragmentListenerInterface;

    public static SearchFragment newInstance() {
        SearchFragment searchFragment = new SearchFragment();
        Bundle bundle = new Bundle();
        searchFragment.setArguments(bundle);
        return searchFragment;
    }

    @Override
    public void initialiseUI() {
        mSearchFragmentToolbar = mFragmentView.findViewById(R.id.search_fragment_toolbar);
        mSearchFragmentSwipeRefreshLayout = mFragmentView.findViewById(R.id.search_fragment_swipe_refresh_layout);
        mSearchView = mFragmentView.findViewById(R.id.search_fragment_search_view);
        mPlayerSearchRecyclerView = mFragmentView.findViewById(R.id.recycler_view_players_search_results);
        mPlayerSearchResultsPlaceholderLayout = mFragmentView.findViewById(R.id.players_search_results_placeholder_layout);
        mPlayerSearchRecyclerView.setLayoutManager(mPlayersLinearLayoutManager);
        mPlayerSearchRecyclerView.setAdapter(mPlayerSearchResultAdapter);
        setupSearchView();
    }

    @Override
    public void setListeners() {
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mSearchFragmentSwipeRefreshLayout.setRefreshing(true);
                mSearchFragmentListenerInterface.performSearch(newText);
                return false;
            }
        });
    }

    @Override
    public void setContextAndTags() {
        TAG = LoggerUtil.makeLogTag(getClass());
        mContext = getActivity();
    }

    @Override
    public void initialiseOtherComponents() {
        mPlayerSearchResultAdapter = new PlayerSearchResultAdapter(new ArrayList<PlayerSearchResultModel>(), mContext);
        mPlayersLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_search, container, false);
        setContextAndTags();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
        return mFragmentView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SearchFragmentListenerInterface) {
            mSearchFragmentListenerInterface = (SearchFragmentListenerInterface) context;
        } else {
            throw new ClassCastException("LandingActivity must implement SearchFragmentListenerInterface.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mSearchFragmentListenerInterface = null;
    }

    /**
     * Sets up the search view.
     */
    private void setupSearchView() {
        Log.i(TAG, "setupSearchView()");
        mSearchFragmentToolbar.setTitleTextAppearance(mContext, R.style.ToolbarTitleAppearance);
        mSearchFragmentToolbar.setTitle(R.string.search);
        ((AppCompatActivity) mContext).setSupportActionBar(mSearchFragmentToolbar);
        LinearLayout linearLayout = mSearchView.findViewById(com.lapism.searchview.R.id.linearLayout);
        ImageView imageView =  mSearchView.findViewById(com.lapism.searchview.R.id.imageView_arrow_back);
        imageView.setLayoutParams(new LinearLayout.LayoutParams((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics()), (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics())));
        imageView =  mSearchView.findViewById(com.lapism.searchview.R.id.imageView_clear);
        imageView.setImageDrawable(ActivityCompat.getDrawable(mContext, R.drawable.ic_cancel));
        imageView.setLayoutParams(new FrameLayout.LayoutParams((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics()), (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics()), Gravity.CENTER));
        ViewGroup.LayoutParams params = linearLayout.getLayoutParams();
        params.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 44, getResources().getDisplayMetrics());
        params.width = LinearLayout.LayoutParams.MATCH_PARENT;
        linearLayout.setLayoutParams(params);
        mSearchView.findViewById(com.lapism.searchview.R.id.view_divider).setVisibility(View.GONE);
        SearchEditText searchEditText = mSearchView.findViewById(com.lapism.searchview.R.id.searchEditText_input);
        searchEditText.setPadding(0, 0, 0, 0);

        CardView.LayoutParams cardViewParams = new CardView.LayoutParams(
                CardView.LayoutParams.MATCH_PARENT,
                CardView.LayoutParams.WRAP_CONTENT
        );
        cardViewParams.setMargins((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics()), (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()), (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics()), (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));
        mSearchView.findViewById(com.lapism.searchview.R.id.cardView).setLayoutParams(cardViewParams);
    }

    /**
     * Called when data is available to be shown.
     */
    public void onDataFetchSuccess(List<PlayerSearchResultModel> playerSearchResultModelList) {
        Log.i(TAG, "onDataFetchSuccess()");
        mPlayerSearchResultAdapter.setmPlayerSearchResultModelList(playerSearchResultModelList);
        mPlayerSearchResultAdapter.notifyDataSetChanged();
        if (mPlayerSearchResultAdapter.getmPlayerSearchResultModelList().size() == 0) {
            mPlayerSearchRecyclerView.setVisibility(View.GONE);
            mPlayerSearchResultsPlaceholderLayout.setVisibility(View.VISIBLE);
        } else {
            mPlayerSearchRecyclerView.setVisibility(View.VISIBLE);
            mPlayerSearchResultsPlaceholderLayout.setVisibility(View.GONE);
        }
        mSearchFragmentSwipeRefreshLayout.setRefreshing(false);
    }

    /**
     * Called when data is not available to be shown.
     */
    public void onDataFetchFailure(boolean isCancelled) {
        Log.i(TAG, "onDataFetchFailure()");
        mPlayerSearchResultAdapter.setmPlayerSearchResultModelList(new ArrayList<PlayerSearchResultModel>());
        mPlayerSearchResultAdapter.notifyDataSetChanged();
        if (!isCancelled) {
            ComponentUtil.createAndShowSnackBar(mContext, mFragmentView, mContext.getString(R.string.fragment_search_error_message));
            mPlayerSearchRecyclerView.setVisibility(View.GONE);
            mPlayerSearchResultsPlaceholderLayout.setVisibility(View.VISIBLE);
            mSearchFragmentSwipeRefreshLayout.setRefreshing(false);
        }
    }

    public interface SearchFragmentListenerInterface {

        void performSearch(String query);
    }
}
