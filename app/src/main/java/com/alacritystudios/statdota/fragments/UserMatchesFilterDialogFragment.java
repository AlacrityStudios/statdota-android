package com.alacritystudios.statdota.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.adapters.MatchesFilterBodyAdapter;
import com.alacritystudios.statdota.enums.FilterType;
import com.alacritystudios.statdota.models.MatchFilterResultModel;
import com.alacritystudios.statdota.models.MatchesFilterBodyModel;

import java.util.List;

/**
 * A dialog fragment to display the filter selection dialog.
 */

public class UserMatchesFilterDialogFragment extends DialogFragment implements MatchesFilterBodyAdapter.MatchesFilterBodyAdapterInterface {

    private View mFilterView;
    private TextView mHeadingTextView;
    private RecyclerView mFilterRecyclerView;
    private LinearLayoutManager mFiltersBodyLinearLayoutManager;
    private MatchesFilterBodyAdapter mMatchesFilterBodyAdapter;
    private TextView mCloseDialogButton;

    private List<MatchesFilterBodyModel> mMatchesFilterBodyModelList;
    private MatchFilterResultModel mMatchFilterResultModel;
    private FilterType mMatchFilterType;
    private boolean mIsSingleUserChoice;
    private UserMatchesFilterDialogFragmentInterface mUserMatchesFilterDialogFragmentInterface;


    public static UserMatchesFilterDialogFragment newInstance(List<MatchesFilterBodyModel> matchesFilterBodyModelList, MatchFilterResultModel matchFilterResultModel, FilterType matchFilterType, boolean isSingleUserChoice, UserMatchesFilterDialogFragmentInterface userMatchesFilterDialogFragmentInterface) {
        UserMatchesFilterDialogFragment userMatchesFilterDialogFragment = new UserMatchesFilterDialogFragment();
        userMatchesFilterDialogFragment.setmMatchesFilterBodyModelList(matchesFilterBodyModelList);
        userMatchesFilterDialogFragment.setmMatchFilterResultModel(matchFilterResultModel);
        userMatchesFilterDialogFragment.setmMatchFilterType(matchFilterType);
        userMatchesFilterDialogFragment.setmIsSingleUserChoice(isSingleUserChoice);
        userMatchesFilterDialogFragment.setmUserMatchesFilterDialogFragmentInterface(userMatchesFilterDialogFragmentInterface);
        return userMatchesFilterDialogFragment;
    }


    public List<MatchesFilterBodyModel> getmMatchesFilterBodyModelList() {
        return mMatchesFilterBodyModelList;
    }

    public void setmMatchesFilterBodyModelList(List<MatchesFilterBodyModel> mMatchesFilterBodyModelList) {
        this.mMatchesFilterBodyModelList = mMatchesFilterBodyModelList;
    }

    public MatchFilterResultModel getmMatchFilterResultModel() {
        return mMatchFilterResultModel;
    }

    public void setmMatchFilterResultModel(MatchFilterResultModel mMatchFilterResultModel) {
        this.mMatchFilterResultModel = mMatchFilterResultModel;
    }

    public FilterType getmMatchFilterType() {
        return mMatchFilterType;
    }

    public void setmMatchFilterType(FilterType mMatchFilterType) {
        this.mMatchFilterType = mMatchFilterType;
    }

    public boolean ismIsSingleUserChoice() {
        return mIsSingleUserChoice;
    }

    public void setmIsSingleUserChoice(boolean mIsSingleUserChoice) {
        this.mIsSingleUserChoice = mIsSingleUserChoice;
    }

    public UserMatchesFilterDialogFragmentInterface getmUserMatchesFilterDialogFragmentInterface() {
        return mUserMatchesFilterDialogFragmentInterface;
    }

    public void setmUserMatchesFilterDialogFragmentInterface(UserMatchesFilterDialogFragmentInterface mUserMatchesFilterDialogFragmentInterface) {
        this.mUserMatchesFilterDialogFragmentInterface = mUserMatchesFilterDialogFragmentInterface;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mFilterView = ((Activity) getActivity()).getLayoutInflater().inflate(R.layout.dialog_match_filters_item, null, false);
        mHeadingTextView = mFilterView.findViewById(R.id.match_filter_heading_text_view);
        mCloseDialogButton = mFilterView.findViewById(R.id.close_dialog_button);
        mFiltersBodyLinearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        mFilterRecyclerView = mFilterView.findViewById(R.id.filters_body_recycler_view);

        switch (mMatchFilterType) {
            case MATCH_FILTER_HERO_PLAYED:
                mMatchesFilterBodyAdapter = new MatchesFilterBodyAdapter(getActivity(), mMatchesFilterBodyModelList, true, FilterType.MATCH_FILTER_HERO_PLAYED, this);
                mHeadingTextView.setText(getActivity().getText(R.string.hero_played));
                mFilterRecyclerView.setLayoutManager(mFiltersBodyLinearLayoutManager);
                mFilterRecyclerView.setAdapter(mMatchesFilterBodyAdapter);
                break;

            case MATCH_FILTER_LOBBY_TYPE:
                mMatchesFilterBodyAdapter = new MatchesFilterBodyAdapter(getActivity(), mMatchesFilterBodyModelList, false, FilterType.MATCH_FILTER_LOBBY_TYPE, this);
                mHeadingTextView.setText(getActivity().getText(R.string.lobby_type));
                mFilterRecyclerView.setLayoutManager(mFiltersBodyLinearLayoutManager);
                mFilterRecyclerView.setAdapter(mMatchesFilterBodyAdapter);
                break;
            case MATCH_FILTER_ALLIED_HERO:
                mMatchesFilterBodyAdapter = new MatchesFilterBodyAdapter(getActivity(), mMatchesFilterBodyModelList, false, FilterType.MATCH_FILTER_ALLIED_HERO, this);
                mHeadingTextView.setText(getActivity().getText(R.string.allied_heroes));
                mFilterRecyclerView.setLayoutManager(mFiltersBodyLinearLayoutManager);
                mFilterRecyclerView.setAdapter(mMatchesFilterBodyAdapter);
                break;
            case MATCH_FILTER_AGAINST_HERO:
                mMatchesFilterBodyAdapter = new MatchesFilterBodyAdapter(getActivity(), mMatchesFilterBodyModelList, false, FilterType.MATCH_FILTER_AGAINST_HERO, this);
                mHeadingTextView.setText(getActivity().getText(R.string.against_heroes));
                mFilterRecyclerView.setLayoutManager(mFiltersBodyLinearLayoutManager);
                mFilterRecyclerView.setAdapter(mMatchesFilterBodyAdapter);
                break;
            case MATCH_FILTER_GAME_MODE:
                mMatchesFilterBodyAdapter = new MatchesFilterBodyAdapter(getActivity(), mMatchesFilterBodyModelList, false, FilterType.MATCH_FILTER_GAME_MODE, this);
                mHeadingTextView.setText(getActivity().getText(R.string.game_mode));
                mFilterRecyclerView.setLayoutManager(mFiltersBodyLinearLayoutManager);
                mFilterRecyclerView.setAdapter(mMatchesFilterBodyAdapter);
                break;
            case MATCH_FILTER_RESULT:
                mMatchesFilterBodyAdapter = new MatchesFilterBodyAdapter(getActivity(), mMatchesFilterBodyModelList, false, FilterType.MATCH_FILTER_RESULT, this);
                mHeadingTextView.setText(getActivity().getText(R.string.result));
                mFilterRecyclerView.setLayoutManager(mFiltersBodyLinearLayoutManager);
                mFilterRecyclerView.setAdapter(mMatchesFilterBodyAdapter);
                break;
            case MATCH_FILTER_SIDE:
                mMatchesFilterBodyAdapter = new MatchesFilterBodyAdapter(getActivity(), mMatchesFilterBodyModelList, false, FilterType.MATCH_FILTER_SIDE, this);
                mHeadingTextView.setText(getActivity().getText(R.string.side));
                mFilterRecyclerView.setLayoutManager(mFiltersBodyLinearLayoutManager);
                mFilterRecyclerView.setAdapter(mMatchesFilterBodyAdapter);
                break;
        }
        mCloseDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });
        return mFilterView;
    }

    @Override
    public void onItemChecked(int position, String value, FilterType filterType, boolean isSingleChoiceFilter, boolean isSelected) {
        switch (mMatchFilterType) {
            case MATCH_FILTER_HERO_PLAYED:
                if (isSelected) {
                    mMatchFilterResultModel.setHeroPlayedId(Integer.parseInt(value));
                } else {
                    mMatchFilterResultModel.setHeroPlayedId(-1);
                }
                for (MatchesFilterBodyModel matchesFilterBodyModel : mMatchesFilterBodyModelList) {
                    matchesFilterBodyModel.setSelected(false);
                }
                mMatchesFilterBodyModelList.get(position).setSelected(isSelected);
                mMatchesFilterBodyAdapter.notifyDataSetChanged();
                break;
            case MATCH_FILTER_LOBBY_TYPE:
                if (isSelected) {
                    mMatchFilterResultModel.setLobbyType(Integer.parseInt(value));
                } else {
                    mMatchFilterResultModel.setLobbyType(-1);
                }
                for (MatchesFilterBodyModel matchesFilterBodyModel : mMatchesFilterBodyModelList) {
                    matchesFilterBodyModel.setSelected(false);
                }
                mMatchesFilterBodyModelList.get(position).setSelected(isSelected);
                mMatchesFilterBodyAdapter.notifyDataSetChanged();
                break;
            case MATCH_FILTER_ALLIED_HERO:
                if (isSelected) {
                    mMatchFilterResultModel.getAlliedHeroIdList().add(Integer.parseInt(value));
                } else {
                    mMatchFilterResultModel.getAlliedHeroIdList().remove(Integer.valueOf(Integer.parseInt(value)));
                }
                mMatchesFilterBodyModelList.get(position).setSelected(isSelected);
                mMatchesFilterBodyAdapter.notifyDataSetChanged();
                break;
            case MATCH_FILTER_AGAINST_HERO:
                if (isSelected) {
                    mMatchFilterResultModel.getAgainstHeroIdList().add(Integer.parseInt(value));
                } else {
                    mMatchFilterResultModel.getAgainstHeroIdList().remove(Integer.valueOf(Integer.parseInt(value)));
                }
                mMatchesFilterBodyModelList.get(position).setSelected(isSelected);
                mMatchesFilterBodyAdapter.notifyDataSetChanged();
                break;
            case MATCH_FILTER_GAME_MODE:
                if (isSelected) {
                    mMatchFilterResultModel.setGameMode(Integer.parseInt(value));
                } else {
                    mMatchFilterResultModel.setGameMode(-1);
                }
                for (MatchesFilterBodyModel matchesFilterBodyModel : mMatchesFilterBodyModelList) {
                    matchesFilterBodyModel.setSelected(false);
                }
                mMatchesFilterBodyModelList.get(position).setSelected(isSelected);
                mMatchesFilterBodyAdapter.notifyDataSetChanged();
                break;
            case MATCH_FILTER_RESULT:
                if (isSelected) {
                    mMatchFilterResultModel.setWinCondition(Integer.parseInt(value));
                } else {
                    mMatchFilterResultModel.setWinCondition(-1);
                }
                for (MatchesFilterBodyModel matchesFilterBodyModel : mMatchesFilterBodyModelList) {
                    matchesFilterBodyModel.setSelected(false);
                }
                mMatchesFilterBodyModelList.get(position).setSelected(isSelected);
                mMatchesFilterBodyAdapter.notifyDataSetChanged();
                break;
            case MATCH_FILTER_SIDE:
                if (isSelected) {
                    mMatchFilterResultModel.setIsRadiant(Integer.parseInt(value));
                } else {
                    mMatchFilterResultModel.setIsRadiant(-1);
                }
                for (MatchesFilterBodyModel matchesFilterBodyModel : mMatchesFilterBodyModelList) {
                    matchesFilterBodyModel.setSelected(false);
                }
                mMatchesFilterBodyModelList.get(position).setSelected(isSelected);
                mMatchesFilterBodyAdapter.notifyDataSetChanged();
                break;
        }
        mUserMatchesFilterDialogFragmentInterface.onFiltersChanged(mMatchFilterResultModel);
    }

    public interface UserMatchesFilterDialogFragmentInterface {

        void onFiltersChanged(MatchFilterResultModel matchFilterResultModel);
    }
}
