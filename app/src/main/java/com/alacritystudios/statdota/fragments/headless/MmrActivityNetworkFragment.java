package com.alacritystudios.statdota.fragments.headless;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.alacritystudios.statdota.constants.StatDotaApiConstants;
import com.alacritystudios.statdota.models.HighMmrPubStarsModel;
import com.alacritystudios.statdota.rest.ApiClient;
import com.alacritystudios.statdota.rest.ApiInterface;
import com.alacritystudios.statdota.utils.LoggerUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Headless fragment to persist data fetched from network calls.
 */

public class MmrActivityNetworkFragment extends Fragment {
    private final String TAG = LoggerUtil.makeLogTag(MmrActivityNetworkFragment.class);

    private List<HighMmrPubStarsModel> mHighMmrPubStarsModelList;

    private Call<List<HighMmrPubStarsModel>> mHighMmrPubStarsModelListCall;

    private MmrActivityNetworkFragment.MmrActivityNetworkFragmentInterface mMmrActivityNetworkFragmentInterface;

    public static MmrActivityNetworkFragment getInstance() {
        MmrActivityNetworkFragment mmrActivityNetworkFragment = new MmrActivityNetworkFragment();
        Bundle bundle = new Bundle();
        mmrActivityNetworkFragment.setArguments(bundle);
        return mmrActivityNetworkFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MmrActivityNetworkFragment.MmrActivityNetworkFragmentInterface) {
            mMmrActivityNetworkFragmentInterface = (MmrActivityNetworkFragment.MmrActivityNetworkFragmentInterface) context;
        } else {
            throw new ClassCastException("MmrActivity must implement MmrActivityNetworkFragment");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMmrActivityNetworkFragmentInterface = null;
    }

    /**
     * Fetches the records from the backend.
     *
     * @param offset - The number of records to skip.
     * @param count  - The count of the results.
     * @param query  - The teams to query.
     */
    public void fetchHighMmrPubStarsListFromBackend(int offset, int count, String query) {
        Log.i(TAG, "fetchHighMmrPubStarsListFromBackend()");
        if (mHighMmrPubStarsModelListCall != null) {
            mHighMmrPubStarsModelListCall.cancel();
        }
        Retrofit retrofit = ApiClient.getClient(StatDotaApiConstants.STAT_DOTA_BASE_URL);
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        mHighMmrPubStarsModelListCall = apiInterface.getMmrRatings(offset, count, query);
        mHighMmrPubStarsModelListCall.enqueue(new Callback<List<HighMmrPubStarsModel>>() {

            @Override
            public void onResponse(Call<List<HighMmrPubStarsModel>> call, Response<List<HighMmrPubStarsModel>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    mMmrActivityNetworkFragmentInterface.onFetchTeamsListSuccessCallback(response.body());
                } else {
                    mMmrActivityNetworkFragmentInterface.onFetchTeamsListFailureCallback(false);
                }
            }

            @Override
            public void onFailure(Call<List<HighMmrPubStarsModel>> call, Throwable t) {
                if (call.isCanceled()) {
                    mMmrActivityNetworkFragmentInterface.onFetchTeamsListFailureCallback(true);
                } else {
                    mMmrActivityNetworkFragmentInterface.onFetchTeamsListFailureCallback(false);
                }
            }
        });
    }

    public interface MmrActivityNetworkFragmentInterface {

        void onFetchTeamsListSuccessCallback(List<HighMmrPubStarsModel> teamsModelList);

        void onFetchTeamsListFailureCallback(boolean isCancelled);
    }
}
