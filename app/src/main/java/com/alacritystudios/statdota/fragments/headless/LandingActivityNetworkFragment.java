package com.alacritystudios.statdota.fragments.headless;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.alacritystudios.statdota.constants.StatDotaApiConstants;
import com.alacritystudios.statdota.constants.TwitchApiConstants;
import com.alacritystudios.statdota.models.HomeFragmentWrapperModel;
import com.alacritystudios.statdota.models.MatchFilterResultModel;
import com.alacritystudios.statdota.models.PlayerSearchResultModel;
import com.alacritystudios.statdota.models.PubMatchesModel;
import com.alacritystudios.statdota.models.RecentLeagueMatchModel;
import com.alacritystudios.statdota.models.SearchResultModel;
import com.alacritystudios.statdota.models.TeamDetailsModel;
import com.alacritystudios.statdota.models.TeamsModel;
import com.alacritystudios.statdota.models.TwitchClipsModel;
import com.alacritystudios.statdota.models.TwitchStreamsModel;
import com.alacritystudios.statdota.models.UserFriendsModel;
import com.alacritystudios.statdota.models.UserHeroSummaryModel;
import com.alacritystudios.statdota.models.UserMatchSummaryModel;
import com.alacritystudios.statdota.models.UserStatsModel;
import com.alacritystudios.statdota.rest.ApiClient;
import com.alacritystudios.statdota.rest.ApiInterface;
import com.alacritystudios.statdota.utils.LoggerUtil;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Headless fragment to make all network calls for LandingActivity.
 */

public class LandingActivityNetworkFragment extends Fragment {

    private HomeFragmentWrapperModel mHomeFragmentWrapperModel;
    private SearchResultModel mSearchResultModel;
    private UserStatsModel mUserStatsModel;
    private List<UserMatchSummaryModel> mUserMatchSummaryList;
    private List<UserHeroSummaryModel> mUserHeroSummaryList;
    private List<UserFriendsModel> mUserFriendSummaryList;
    private TwitchStreamsModel mTwitchStreamsModel;
    private LandingActivityNetworkFragmentInterface mLandingActivityNetworkFragmentInterface;
    private final String TAG = LoggerUtil.makeLogTag(getClass());

    private Call<HomeFragmentWrapperModel> mHomeFragmentStatsCall;
    private Call<SearchResultModel> mSearchQueryCall;
    private Call<UserStatsModel> mUserStatsModelCall;
    private Call<List<UserMatchSummaryModel>> mUserMatchSummaryListCall;
    private Call<List<UserHeroSummaryModel>> mUserHeroSummaryListCall;
    private Call<List<UserFriendsModel>> mUserFriendSummaryListCall;
    private Call<List<PubMatchesModel>> mRecentPubMatchModelListCall;
    private Call<List<RecentLeagueMatchModel>> mRecentLeagueMatchModelListCall;
    private Call<TwitchStreamsModel> mTwitchStreamsCall;
    private Call<TeamDetailsModel> mTeamDetailsModelCall;

    public static LandingActivityNetworkFragment getInstance() {
        LandingActivityNetworkFragment landingActivityNetworkFragment = new LandingActivityNetworkFragment();
        Bundle bundle = new Bundle();
        landingActivityNetworkFragment.setArguments(bundle);
        return landingActivityNetworkFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof LandingActivityNetworkFragmentInterface) {
            mLandingActivityNetworkFragmentInterface = (LandingActivityNetworkFragmentInterface) context;
        } else {
            throw new ClassCastException("Landing Activity must implement LandingActivityNetworkFragmentInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mLandingActivityNetworkFragmentInterface = null;
    }

    /**
     * Returns the home fragment stats. If they are absent then we fetch them from the backend again.
     */
    public void fetchHomeFragmentStats() {
        Log.i(TAG, "fetchHomeFragmentStats()");
        if (mHomeFragmentWrapperModel != null) {
            if (mLandingActivityNetworkFragmentInterface != null)
                mLandingActivityNetworkFragmentInterface.onHomeFragmentQuerySuccessCallBack(mHomeFragmentWrapperModel);
        } else {
            fetchHomeFragmentStatsFromBackEnd();
        }
    }

    /**
     * Fetch home fragment stats from the backend.
     */
    public void fetchHomeFragmentStatsFromBackEnd() {
        Log.i(TAG, "fetchHomeFragmentStatsFromBackEnd()");
        Retrofit retrofit = ApiClient.getClient(StatDotaApiConstants.STAT_DOTA_BASE_URL);
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        if (mHomeFragmentStatsCall != null) {
            mHomeFragmentStatsCall.cancel();
        }
        mHomeFragmentStatsCall = apiInterface.getHomeFragmentData();
        mHomeFragmentStatsCall.enqueue(new Callback<HomeFragmentWrapperModel>() {

            @Override
            public void onResponse(Call<HomeFragmentWrapperModel> call, Response<HomeFragmentWrapperModel> response) {
                if (response.isSuccessful()) {
                    mHomeFragmentWrapperModel = response.body();
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onHomeFragmentQuerySuccessCallBack(mHomeFragmentWrapperModel);
                }
            }

            @Override
            public void onFailure(Call<HomeFragmentWrapperModel> call, Throwable t) {
                mHomeFragmentWrapperModel = null;
                if (call.isCanceled()) {
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onHomeFragmentQueryFailureCallBack(true);
                } else {
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onHomeFragmentQueryFailureCallBack(false);
                }
            }
        });
    }

    /**
     * Fetch search query results from the backend.
     */
    public void performSearchQuery(String query) {
        Log.i(TAG, "performSearchQuery()");
        if (!query.isEmpty()) {
            Retrofit apiClient = ApiClient.getClient(StatDotaApiConstants.STAT_DOTA_BASE_URL);
            ApiInterface apiInterface = apiClient.create(ApiInterface.class);
            if (mSearchQueryCall != null) {
                mSearchQueryCall.cancel();
            }
            mSearchQueryCall = apiInterface.performSearch(query);
            mSearchQueryCall.enqueue(new Callback<SearchResultModel>() {

                @Override
                public void onResponse(Call<SearchResultModel> call, Response<SearchResultModel> response) {
                    mSearchResultModel = response.body();
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onSearchQuerySuccessCallBack(mSearchResultModel);
                }

                @Override
                public void onFailure(Call<SearchResultModel> call, Throwable t) {
                    mSearchResultModel = null;
                    if (call.isCanceled()) {
                        if (mLandingActivityNetworkFragmentInterface != null)
                            mLandingActivityNetworkFragmentInterface.onSearchQueryFailureCallBack(true);
                    } else {
                        if (mLandingActivityNetworkFragmentInterface != null)
                            mLandingActivityNetworkFragmentInterface.onSearchQueryFailureCallBack(false);
                    }
                }
            });
        } else {
            if (mSearchQueryCall != null) {
                mSearchQueryCall.cancel();
            }
            mSearchResultModel = new SearchResultModel();
            mSearchResultModel.setPlayersSearchResultModelList(new ArrayList<PlayerSearchResultModel>());
            mSearchResultModel.setTeamsSearchResultModelList(new ArrayList<TeamsModel>());
            if (mLandingActivityNetworkFragmentInterface != null)
                mLandingActivityNetworkFragmentInterface.onSearchQuerySuccessCallBack(mSearchResultModel);
        }
    }

    /**
     * Returns the user stats. If they are absent then we fetch them from the API again.
     */
    public void fetchUserStats(String accountId) {
        Log.i(TAG, "fetchUserStats()");
        if (mUserStatsModel != null) {
            if (mLandingActivityNetworkFragmentInterface != null)
                mLandingActivityNetworkFragmentInterface.onUserStatsQuerySuccessCallback(mUserStatsModel);
        } else {
            fetchUserStatsFromBackend(accountId);
        }
    }

    /**
     * Fetch user stats from the backend.
     */
    public void fetchUserStatsFromBackend(String accountId) {
        Log.i(TAG, "fetchUserStatsFromBackend()");
        Retrofit retrofit = ApiClient.getClient(StatDotaApiConstants.STAT_DOTA_BASE_URL);
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        if (mUserStatsModelCall != null) {
            mUserStatsModelCall.cancel();
        }
        mUserStatsModelCall = apiInterface.getUserStatsInfo(accountId);
        mUserStatsModelCall.enqueue(new Callback<UserStatsModel>() {

            @Override
            public void onResponse(Call<UserStatsModel> call, Response<UserStatsModel> response) {
                if (response.isSuccessful()) {
                    mUserStatsModel = response.body();
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onUserStatsQuerySuccessCallback(mUserStatsModel);
                }
            }

            @Override
            public void onFailure(Call<UserStatsModel> call, Throwable t) {
                if (call.isCanceled()) {
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onUserStatsQueryFailureCallback(true);
                } else {
                    Log.d(TAG, t.getLocalizedMessage());
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onUserStatsQueryFailureCallback(false);
                }
            }
        });
    }

    /**
     * Fetch user matches from the backend.
     */
    public void fetchUserMatchesFromBackend(String accountId, int limit, final int offset, MatchFilterResultModel matchFilterResultModel) {
        Log.i(TAG, "fetchUserMatchesFromBackend()");
        Retrofit retrofit = ApiClient.getClient(StatDotaApiConstants.STAT_DOTA_BASE_URL);
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        if (mUserMatchSummaryListCall != null) {
            mUserMatchSummaryListCall.cancel();
        }
        mUserMatchSummaryListCall = apiInterface.getPlayerMatches(accountId, limit, offset, matchFilterResultModel.getHeroPlayedId(), matchFilterResultModel.getWinCondition(), matchFilterResultModel.getIsRadiant(), matchFilterResultModel.getGameMode(), matchFilterResultModel.getLobbyType(), matchFilterResultModel.getAlliedHeroIdList(), matchFilterResultModel.getAgainstHeroIdList());
        mUserMatchSummaryListCall.enqueue(new Callback<List<UserMatchSummaryModel>>() {

            @Override
            public void onResponse(Call<List<UserMatchSummaryModel>> call, Response<List<UserMatchSummaryModel>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (offset == 0) {
                        mUserMatchSummaryList = response.body();
                    } else {
                        mUserMatchSummaryList.addAll(response.body());
                    }
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onUserMatchesQuerySuccessCallback(mUserMatchSummaryList);
                } else {
                    Log.d(TAG, "Received null response body from backend");
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onUserMatchesQueryFailureCallback(false);
                }
            }

            @Override
            public void onFailure(Call<List<UserMatchSummaryModel>> call, Throwable t) {
                if (call.isCanceled()) {
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onUserMatchesQueryFailureCallback(true);
                } else {
                    t.printStackTrace();
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onUserMatchesQueryFailureCallback(false);
                }
            }
        });
    }

    /**
     * Returns the user hero stats. If they are absent then we fetch them from the API again.
     */
    public void fetchUserHeroes(String accountId) {
        Log.i(TAG, "fetchUserStats()");
        if (mUserHeroSummaryList != null) {
            if (mLandingActivityNetworkFragmentInterface != null)
                mLandingActivityNetworkFragmentInterface.onUserHeroesQuerySuccessCallback(mUserHeroSummaryList);
        } else {
            fetchUserHeroesFromBackend(accountId);
        }
    }

    /**
     * Fetch user heroes from the backend.
     */
    public void fetchUserHeroesFromBackend(String accountId) {
        Log.i(TAG, "fetchUserMatchesFromBackend()");
        Retrofit retrofit = ApiClient.getClient(StatDotaApiConstants.STAT_DOTA_BASE_URL);
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        if (mUserHeroSummaryListCall != null) {
            mUserHeroSummaryListCall.cancel();
        }
        mUserHeroSummaryListCall = apiInterface.getUserHeroSummary(accountId);
        mUserHeroSummaryListCall.enqueue(new Callback<List<UserHeroSummaryModel>>() {

            @Override
            public void onResponse(Call<List<UserHeroSummaryModel>> call, Response<List<UserHeroSummaryModel>> response) {
                if (response.isSuccessful() && response.body() != null) {

                    mUserHeroSummaryList = response.body();
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onUserHeroesQuerySuccessCallback(mUserHeroSummaryList);
                } else {
                    Log.d(TAG, "Received null response body from backend");
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onUserMatchesQueryFailureCallback(false);
                }
            }

            @Override
            public void onFailure(Call<List<UserHeroSummaryModel>> call, Throwable t) {
                if (call.isCanceled()) {
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onUserHeroesQueryFailureCallback(true);
                } else {
                    t.printStackTrace();
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onUserHeroesQueryFailureCallback(false);
                }
            }
        });
    }

    /**
     * Returns the user friends stats. If they are absent then we fetch them from the API again.
     */
    public void fetchUserFriends(String accountId) {
        Log.i(TAG, "fetchUserFriends()");
        if (mUserFriendSummaryList != null) {
            if (mLandingActivityNetworkFragmentInterface != null)
                mLandingActivityNetworkFragmentInterface.onUserFriendsQuerySuccessCallback(mUserFriendSummaryList);
        } else {
            fetchUserFriendsFromBackend(accountId);
        }
    }

    /**
     * Fetch user friends from the backend.
     */
    public void fetchUserFriendsFromBackend(String accountId) {
        Log.i(TAG, "fetchUserFriendsFromBackend()");
        Retrofit retrofit = ApiClient.getClient(StatDotaApiConstants.STAT_DOTA_BASE_URL);
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        if (mUserFriendSummaryListCall != null) {
            mUserFriendSummaryListCall.cancel();
        }
        mUserFriendSummaryListCall = apiInterface.getUserFriends(accountId);
        mUserFriendSummaryListCall.enqueue(new Callback<List<UserFriendsModel>>() {

            @Override
            public void onResponse(Call<List<UserFriendsModel>> call, Response<List<UserFriendsModel>> response) {
                if (response.isSuccessful() && response.body() != null) {

                    mUserFriendSummaryList = response.body();
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onUserFriendsQuerySuccessCallback(mUserFriendSummaryList);
                } else {
                    Log.d(TAG, "Received null response body from backend");
                    mLandingActivityNetworkFragmentInterface.onUserFriendsQueryFailureCallback(false);
                }
            }

            @Override
            public void onFailure(Call<List<UserFriendsModel>> call, Throwable t) {
                if (call.isCanceled()) {
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onUserFriendsQueryFailureCallback(true);
                } else {
                    t.printStackTrace();
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onUserFriendsQueryFailureCallback(false);
                }
            }
        });
    }

    /**
     * Fetch recent pub matches from the backend.
     */
    public void fetchRecentPubMatchesFromBackend(int limit, final int offset) {
        Log.i(TAG, "fetchRecentPubMatchesFromBackend()");
        Retrofit retrofit = ApiClient.getClient(StatDotaApiConstants.STAT_DOTA_BASE_URL);
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        if (mRecentPubMatchModelListCall != null) {
            mRecentPubMatchModelListCall.cancel();
        }
        mRecentPubMatchModelListCall = apiInterface.getRecentPubMatches(limit, offset);
        mRecentPubMatchModelListCall.enqueue(new Callback<List<PubMatchesModel>>() {

            @Override
            public void onResponse(Call<List<PubMatchesModel>> call, Response<List<PubMatchesModel>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onRecentPubMatchesQuerySuccessCallback(response.body());
                } else {
                    Log.d(TAG, "Received null response body from backend");
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onRecentPubMatchesQueryFailureCallback(false);
                }
            }

            @Override
            public void onFailure(Call<List<PubMatchesModel>> call, Throwable t) {
                if (call.isCanceled()) {
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onRecentPubMatchesQueryFailureCallback(true);
                } else {
                    t.printStackTrace();
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onRecentPubMatchesQueryFailureCallback(false);
                }
            }
        });
    }

    /**
     * Fetch recent league matches from the backend.
     */
    public void fetchRecentLeagueMatchesFromBackend(int limit, final int offset) {
        Log.i(TAG, "fetchRecentLeagueMatchesFromBackend()");
        Retrofit retrofit = ApiClient.getClient(StatDotaApiConstants.STAT_DOTA_BASE_URL);
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        if (mRecentLeagueMatchModelListCall != null) {
            mRecentLeagueMatchModelListCall.cancel();
        }
        mRecentLeagueMatchModelListCall = apiInterface.getRecentLeagueMatches(limit, offset);
        mRecentLeagueMatchModelListCall.enqueue(new Callback<List<RecentLeagueMatchModel>>() {

            @Override
            public void onResponse(Call<List<RecentLeagueMatchModel>> call, Response<List<RecentLeagueMatchModel>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onRecentLeagueMatchesQuerySuccessCallback(response.body());
                } else {
                    Log.d(TAG, "Received null response body from backend");
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onRecentLeagueMatchesQueryFailureCallback(false);
                }
            }

            @Override
            public void onFailure(Call<List<RecentLeagueMatchModel>> call, Throwable t) {
                if (call.isCanceled()) {
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onRecentLeagueMatchesQueryFailureCallback(true);
                } else {
                    t.printStackTrace();
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onRecentLeagueMatchesQueryFailureCallback(false);
                }
            }
        });
    }

    /**
     * Returns the twitch streams. If they are absent then we fetch them from the API again.
     */
    public void fetchTwitchStreams() {
        Log.i(TAG, "fetchTwitchStreams()");
        if (mTwitchStreamsModel != null) {
            mLandingActivityNetworkFragmentInterface.onTwitchStreamsFragmentQuerySuccessCallBack(mTwitchStreamsModel);
        } else {
            fetchTwitchStreamsFromTwitchAPI();
        }
    }

    /**
     * Fetch home fragment stats from the backend.
     */
    public void fetchTwitchStreamsFromTwitchAPI() {
        Log.i(TAG, "fetchTwitchStreamsFromTwitchAPI()");
        Retrofit retrofit = ApiClient.getClient(TwitchApiConstants.TWITCH_BASE_URL);
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        if (mTwitchStreamsCall != null) {
            mTwitchStreamsCall.cancel();
        }
        mTwitchStreamsCall = apiInterface.getLiveStreams(TwitchApiConstants.TWITCH_API_KEY, TwitchApiConstants.GAME_TYPE_DOTA_2);
        mTwitchStreamsCall.enqueue(new Callback<TwitchStreamsModel>() {

            @Override
            public void onResponse(Call<TwitchStreamsModel> call, Response<TwitchStreamsModel> response) {
                if (response.isSuccessful()) {
                    mTwitchStreamsModel = response.body();
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onTwitchStreamsFragmentQuerySuccessCallBack(mTwitchStreamsModel);
                }
            }

            @Override
            public void onFailure(Call<TwitchStreamsModel> call, Throwable t) {
                mTwitchStreamsModel = null;
                if (call.isCanceled()) {
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onTwitchStreamsFragmentQueryFailureCallBack(true);
                } else {
                    Log.d(TAG, t.getLocalizedMessage());
                    if (mLandingActivityNetworkFragmentInterface != null)
                        mLandingActivityNetworkFragmentInterface.onTwitchStreamsFragmentQueryFailureCallBack(false);
                }
            }
        });
    }

    /**
     * Fetches the team details from the backend.
     *
     * @param teamId - The team Id of the team who's details are to be fetched.
     */
    public void fetchTeamDetailsFromBackend(long teamId) {
        Log.i(TAG, "fetchTeamDetailsFromBackend()");
        if (mTeamDetailsModelCall != null) {
            mTeamDetailsModelCall.cancel();
        }
        Retrofit retrofit = ApiClient.getClient(StatDotaApiConstants.STAT_DOTA_BASE_URL);
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        mTeamDetailsModelCall = apiInterface.getTeamDetails(teamId);
        mTeamDetailsModelCall.enqueue(new Callback<TeamDetailsModel>() {

            @Override
            public void onResponse(Call<TeamDetailsModel> call, Response<TeamDetailsModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (mLandingActivityNetworkFragmentInterface != null) {
                        mLandingActivityNetworkFragmentInterface.onFetchTeamDetailsSuccessCallback(response.body());
                    }
                } else {
                    if (mLandingActivityNetworkFragmentInterface != null) {
                        mLandingActivityNetworkFragmentInterface.onFetchTeamDetailsFailureCallback(false);
                    }
                }
            }

            @Override
            public void onFailure(Call<TeamDetailsModel> call, Throwable t) {
                if (call.isCanceled()) {
                    if (mLandingActivityNetworkFragmentInterface != null) {
                        mLandingActivityNetworkFragmentInterface.onFetchTeamDetailsFailureCallback(true);
                    }
                } else {
                    if (mLandingActivityNetworkFragmentInterface != null) {
                        mLandingActivityNetworkFragmentInterface.onFetchTeamDetailsFailureCallback(false);
                    }
                }
            }
        });
    }

    public interface LandingActivityNetworkFragmentInterface {

        void onHomeFragmentQuerySuccessCallBack(HomeFragmentWrapperModel homeFragmentWrapperModel);

        void onHomeFragmentQueryFailureCallBack(boolean isCancelled);

        void onSearchQuerySuccessCallBack(SearchResultModel searchResultModel);

        void onSearchQueryFailureCallBack(boolean isCancelled);

        void onUserStatsQuerySuccessCallback(UserStatsModel userStatsModel);

        void onUserStatsQueryFailureCallback(boolean isCancelled);

        void onUserMatchesQuerySuccessCallback(List<UserMatchSummaryModel> userMatchSummaryModelList);

        void onUserMatchesQueryFailureCallback(boolean isCancelled);

        void onUserHeroesQuerySuccessCallback(List<UserHeroSummaryModel> userHeroSummaryModelList);

        void onUserHeroesQueryFailureCallback(boolean isCancelled);

        void onUserFriendsQuerySuccessCallback(List<UserFriendsModel> userFriendsModelList);

        void onUserFriendsQueryFailureCallback(boolean isCancelled);

        void onRecentPubMatchesQuerySuccessCallback(List<PubMatchesModel> pubMatchesModelList);

        void onRecentPubMatchesQueryFailureCallback(boolean isCancelled);

        void onRecentLeagueMatchesQuerySuccessCallback(List<RecentLeagueMatchModel> recentLeagueMatchModelList);

        void onRecentLeagueMatchesQueryFailureCallback(boolean isCancelled);

        void onTwitchStreamsFragmentQuerySuccessCallBack(TwitchStreamsModel twitchStreamsModel);

        void onTwitchStreamsFragmentQueryFailureCallBack(boolean isCancelled);

        void onTwitchClipsFragmentQuerySuccessCallBack(List<TwitchClipsModel> twitchClipsModelList);

        void onTwitchClipsFragmentQueryFailureCallBack(boolean isCancelled);

        void onFetchTeamDetailsSuccessCallback(TeamDetailsModel teamDetailsModel);

        void onFetchTeamDetailsFailureCallback(boolean isCancelled);
    }
}
