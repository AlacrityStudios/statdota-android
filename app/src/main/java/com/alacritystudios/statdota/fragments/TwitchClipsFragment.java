package com.alacritystudios.statdota.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.adapters.TwitchClipsAdapter;
import com.alacritystudios.statdota.fragments.base.BaseFragment;
import com.alacritystudios.statdota.models.TwitchClipsModel;
import com.alacritystudios.statdota.utils.LoggerUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment to display twitch top clips of the week.
 */

public class TwitchClipsFragment extends BaseFragment implements BaseFragment.BaseFragmentImpl {

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayout mPlaceholderLayout;
    private LinearLayout mErrorLayout;
    private RecyclerView mTwitchClipsRecyclerView;

    private TwitchClipsAdapter mTwitchClipsAdapter;
    private LinearLayoutManager mClipsLinearLayoutManager;
    private TwitchClipsFragment.TwitchClipsFragmentInterface mTwitchClipsFragmentInterface;

    private int mPageNumber = 0;

    public static TwitchClipsFragment newInstance() {
        TwitchClipsFragment twitchClipsFragment = new TwitchClipsFragment();
        Bundle bundle = new Bundle();
        twitchClipsFragment.setArguments(bundle);
        return twitchClipsFragment;
    }

    @Override
    public void initialiseUI() {
        mSwipeRefreshLayout = (SwipeRefreshLayout) mFragmentView.findViewById(R.id.twitch_clips_fragment_swipe_refresh_layout);
        mPlaceholderLayout = (LinearLayout) mFragmentView.findViewById(R.id.twitch_clips_fragment_placeholder_layout);
        mErrorLayout = (LinearLayout) mFragmentView.findViewById(R.id.twitch_clips_fragment_error_layout);
        mTwitchClipsRecyclerView = (RecyclerView) mFragmentView.findViewById(R.id.recycler_view_twitch_clips);
        setupLiveClipRecyclerView();
        setupSwipeRefreshLayout();
        mTwitchClipsFragmentInterface.fetchTwitchFragmentClips(mPageNumber * 25, 25);
    }

    @Override
    public void setListeners() {

    }

    @Override
    public void setContextAndTags() {
        mContext = getActivity();
        TAG = LoggerUtil.makeLogTag(getClass());
    }

    @Override
    public void initialiseOtherComponents() {
        mTwitchClipsAdapter = new TwitchClipsAdapter(mContext, new ArrayList<TwitchClipsModel>());
        mClipsLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_twitch_clips, container, false);
        setContextAndTags();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
        return mFragmentView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SearchFragment.SearchFragmentListenerInterface) {
            mTwitchClipsFragmentInterface = (TwitchClipsFragment.TwitchClipsFragmentInterface) context;
        } else {
            throw new ClassCastException("LandingActivity must implement TwitchClipsFragmentInterface.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mTwitchClipsFragmentInterface = null;
    }

    /**
     * Setup the swipe refresh layout behaviour.
     */
    private void setupSwipeRefreshLayout() {
        Log.i(TAG, "setupSwipeRefreshLayout()");
        mPlaceholderLayout.setVisibility(View.GONE);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                mPageNumber = 0;
                mTwitchClipsFragmentInterface.fetchTwitchFragmentClips(mPageNumber * 25, 25);
            }
        });
    }

    /**
     * Setup the matches recycler view.
     */
    private void setupLiveClipRecyclerView() {
        Log.i(TAG, "setupLiveClipRecyclerView()");
        mTwitchClipsRecyclerView.setLayoutManager(mClipsLinearLayoutManager);
        mTwitchClipsRecyclerView.setAdapter(mTwitchClipsAdapter);
    }


    /**
     * Success case of fetching clips.
     *
     * @param twitchClipsModelList - List of twitch clips to display.
     */
    public void onFetchResultSuccess(List<TwitchClipsModel> twitchClipsModelList) {
        Log.i(TAG, "onFetchResultSuccess()");
        mTwitchClipsAdapter.setmTwitchClipsModelList(twitchClipsModelList);
        mTwitchClipsAdapter.notifyDataSetChanged();
        if (mTwitchClipsAdapter.getmTwitchClipsModelList().size() == 0) {
            mTwitchClipsRecyclerView.setVisibility(View.GONE);
            mErrorLayout.setVisibility(View.GONE);
            mPlaceholderLayout.setVisibility(View.VISIBLE);
        } else {
            mTwitchClipsRecyclerView.setVisibility(View.VISIBLE);
            mErrorLayout.setVisibility(View.GONE);
            mPlaceholderLayout.setVisibility(View.GONE);
        }
        mSwipeRefreshLayout.setRefreshing(false);
    }

    /**
     * Failure case of fetching clips.
     */
    public void onFetchResultFailure(boolean isCancelled) {
        Log.i(TAG, "onFetchResultFailure()");
        mTwitchClipsAdapter.setmTwitchClipsModelList(new ArrayList<TwitchClipsModel>());
        mTwitchClipsAdapter.notifyDataSetChanged();
        if (!isCancelled) {
            mTwitchClipsRecyclerView.setVisibility(View.GONE);
            mErrorLayout.setVisibility(View.VISIBLE);
            mPlaceholderLayout.setVisibility(View.GONE);
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    public interface TwitchClipsFragmentInterface {
        void fetchTwitchFragmentClips(int offset, int count);
    }
}