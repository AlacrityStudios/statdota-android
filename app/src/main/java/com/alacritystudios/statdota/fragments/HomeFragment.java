package com.alacritystudios.statdota.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.activities.HeroActivity;
import com.alacritystudios.statdota.activities.MmrActivity;
import com.alacritystudios.statdota.activities.TeamActivity;
import com.alacritystudios.statdota.adapters.HeroStatsSummaryAdapter;
import com.alacritystudios.statdota.adapters.NotablePlayersSummaryAdapter;
import com.alacritystudios.statdota.adapters.PubMatchesSummaryAdapter;
import com.alacritystudios.statdota.adapters.RecentLeagueMatchesSummaryAdapter;
import com.alacritystudios.statdota.adapters.TeamsSummaryAdapter;
import com.alacritystudios.statdota.adapters.TwitchClipsSummaryAdapter;
import com.alacritystudios.statdota.fragments.base.BaseFragment;
import com.alacritystudios.statdota.models.HeroDetails;
import com.alacritystudios.statdota.models.HeroStatsModel;
import com.alacritystudios.statdota.models.HighMmrPubStarsModel;
import com.alacritystudios.statdota.models.HomeFragmentWrapperModel;
import com.alacritystudios.statdota.models.ProPlayersModel;
import com.alacritystudios.statdota.models.PubMatchesModel;
import com.alacritystudios.statdota.models.RecentLeagueMatchModel;
import com.alacritystudios.statdota.models.TeamDetailsModel;
import com.alacritystudios.statdota.models.TeamsModel;
import com.alacritystudios.statdota.models.TwitchClipsModel;
import com.alacritystudios.statdota.sql.StatDotaDatabaseHelper;
import com.alacritystudios.statdota.utils.ComponentUtil;
import com.alacritystudios.statdota.utils.LoggerUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Home fragment for entire application. Shows various trending parameters.
 */

public class HomeFragment extends BaseFragment implements BaseFragment.BaseFragmentImpl, TeamsSummaryAdapter.TeamsSummaryAdapterListener {

    private Toolbar mToolbar;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayout mPlaceholderLayout;
    private NestedScrollView mContentScrollView;
    private RecyclerView mProTeamsRecyclerView;
    private RecyclerView mHighMmrPubStarsRecyclerView;
    private RecyclerView mPopularHeroesRecyclerView;
    private RecyclerView mRecentPubMatchesRecyclerView;
    private RecyclerView mRecentLeagueMatchesRecyclerView;
    private RecyclerView mPopularClipsRecyclerView;
    private TextView mSeeAllHeroesTextView;
    private TextView mSeeAllTeamsTextView;
    private TextView mSeeAllHighMmrPubStarsTextView;
    private TextView mSeeAllRecentPubMatchesTextView;
    private TextView mSeeAllRecentLeagueMatchesTextView;
    private TextView mSeeAllPopularTwitchClipsTextView;

    private Drawable mRecentLeagueMatchesPlaceholderDrawable;

    private TeamsSummaryAdapter mTeamsSummaryAdapter;
    private NotablePlayersSummaryAdapter mNotablePlayersSummaryAdapter;
    private PubMatchesSummaryAdapter mPubMatchesSummaryAdapter;
    private RecentLeagueMatchesSummaryAdapter mRecentLeagueMatchesSummaryAdapter;
    private HeroStatsSummaryAdapter mHeroStatsSummaryAdapter;
    private TwitchClipsSummaryAdapter mTwitchClipsSummaryAdapter;
    private LinearLayoutManager mTeamsSummaryLinearLayoutManager;
    private LinearLayoutManager mHighMmrPubStarsSummaryLinearLayoutManager;
    private LinearLayoutManager mRecentLeagueMatchesSummaryLinearLayoutManager;
    private LinearLayoutManager mPubMatchesSummaryLinearLayoutManager;
    private LinearLayoutManager mHeroStatsSummaryLinearLayoutManager;
    private LinearLayoutManager mTwitchClipsSummaryLinearLayoutManager;

    private HomeFragmentInterface mHomeFragmentInterface;

    public static HomeFragment newInstance() {
        HomeFragment homeFragment = new HomeFragment();
        Bundle bundle = new Bundle();
        homeFragment.setArguments(bundle);
        return homeFragment;
    }

    @Override
    public void initialiseUI() {
        mToolbar = (Toolbar) mFragmentView.findViewById(R.id.home_fragment_toolbar);
        mSwipeRefreshLayout = (SwipeRefreshLayout) mFragmentView.findViewById(R.id.home_fragment_swipe_refresh_layout);
        mPlaceholderLayout = (LinearLayout) mFragmentView.findViewById(R.id.home_fragment_placeholder_layout);
        mContentScrollView = (NestedScrollView) mFragmentView.findViewById(R.id.home_fragment_content_scroll_view);
        mProTeamsRecyclerView = (RecyclerView) mFragmentView.findViewById(R.id.pro_teams_recycler_view);
        mHighMmrPubStarsRecyclerView = (RecyclerView) mFragmentView.findViewById(R.id.high_mmr_pub_stars_recycler_view);
        mPopularHeroesRecyclerView = (RecyclerView) mFragmentView.findViewById(R.id.popular_heroes_recycler_view);
        mRecentLeagueMatchesRecyclerView = (RecyclerView) mFragmentView.findViewById(R.id.recent_league_matches_recycler_view);
        mRecentPubMatchesRecyclerView = (RecyclerView) mFragmentView.findViewById(R.id.recent_pub_matches_recycler_view);
        mPopularClipsRecyclerView = (RecyclerView) mFragmentView.findViewById(R.id.popular_clips_recycler_view);
        mSeeAllHeroesTextView = (TextView) mFragmentView.findViewById(R.id.see_all_heroes_text_view);
        mSeeAllTeamsTextView = (TextView) mFragmentView.findViewById(R.id.see_all_teams_text_view);
        mSeeAllHighMmrPubStarsTextView = (TextView) mFragmentView.findViewById(R.id.see_all_high_mmr_pub_stars_text_view);
        mSeeAllRecentLeagueMatchesTextView = (TextView) mFragmentView.findViewById(R.id.see_all_recent_league_matches_text_view);
        mSeeAllRecentPubMatchesTextView = (TextView) mFragmentView.findViewById(R.id.see_all_recent_pub_matches_text_view);
        mSeeAllPopularTwitchClipsTextView = (TextView) mFragmentView.findViewById(R.id.see_all_popular_twitch_clips_text_view);
        setupToolbar();
        setupSwipeRefreshLayout();
        setupRecyclerViews();
        mSwipeRefreshLayout.setRefreshing(true);
        mHomeFragmentInterface.fetchHomeFragmentStats();
    }

    @Override
    public void setListeners() {
        mSeeAllHeroesTextView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, HeroActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
        mSeeAllTeamsTextView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, TeamActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
        mSeeAllHighMmrPubStarsTextView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, MmrActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
        mSeeAllRecentLeagueMatchesTextView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                mHomeFragmentInterface.onRecentLeagueMatchesRequest();
            }
        });
        mSeeAllRecentPubMatchesTextView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                mHomeFragmentInterface.onRecentPubMatchesRequest();
            }
        });
    }

    @Override
    public void setContextAndTags() {
        mContext = getActivity();
        TAG = LoggerUtil.makeLogTag(getClass());
    }

    @Override
    public void initialiseOtherComponents() {
        mRecentLeagueMatchesPlaceholderDrawable = ActivityCompat.getDrawable(mContext, R.drawable.ic_dota);
        DrawableCompat.wrap(mRecentLeagueMatchesPlaceholderDrawable).setTint(ActivityCompat.getColor(mContext, R.color.colorDefaultMedium));
        mTeamsSummaryAdapter = new TeamsSummaryAdapter(mContext, new ArrayList<TeamsModel>(), this);
        mNotablePlayersSummaryAdapter = new NotablePlayersSummaryAdapter(mContext, new ArrayList<ProPlayersModel>());
        mRecentLeagueMatchesSummaryAdapter = new RecentLeagueMatchesSummaryAdapter(mContext, new ArrayList<RecentLeagueMatchModel>(), mRecentLeagueMatchesPlaceholderDrawable);
        mPubMatchesSummaryAdapter = new PubMatchesSummaryAdapter(mContext, new ArrayList<PubMatchesModel>());
        mHeroStatsSummaryAdapter = new HeroStatsSummaryAdapter(mContext, new ArrayList<HeroStatsModel>());
        mTwitchClipsSummaryAdapter = new TwitchClipsSummaryAdapter(mContext, new ArrayList<TwitchClipsModel>());
        mTeamsSummaryLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        mHighMmrPubStarsSummaryLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        mRecentLeagueMatchesSummaryLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        mPubMatchesSummaryLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        mTwitchClipsSummaryLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        mHeroStatsSummaryLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_home, container, false);
        setContextAndTags();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
        return mFragmentView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof HomeFragment.HomeFragmentInterface) {
            mHomeFragmentInterface = (HomeFragment.HomeFragmentInterface) context;
        } else {
            throw new ClassCastException("LandingActivity must implement HomeFragmentInterface.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mHomeFragmentInterface = null;
    }

    @Override
    public void onItemTouch(long teamId) {
        mHomeFragmentInterface.fetchTeamDetails(teamId);
    }

    /**
     * Sets up the toolbar for the activity.
     */
    private void setupToolbar() {
        Log.i(TAG, "setupToolbar()");
        mToolbar.setTitleTextAppearance(mContext, R.style.ToolbarTitleAppearance);
        mToolbar.setTitle(R.string.home);
        ((AppCompatActivity) mContext).setSupportActionBar(mToolbar);
    }

    /**
     * Setup the swipe refresh layout behaviour.
     */
    private void setupSwipeRefreshLayout() {
        Log.i(TAG, "setupSwipeRefreshLayout()");
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                mHomeFragmentInterface.refreshHomeFragmentStats();
            }
        });
    }

    /**
     * Sets up the recycler views for the activity.
     */
    private void setupRecyclerViews() {
        Log.i(TAG, "setupRecyclerViews()");
        mProTeamsRecyclerView.setLayoutManager(mTeamsSummaryLinearLayoutManager);
        mProTeamsRecyclerView.setAdapter(mTeamsSummaryAdapter);
        mHighMmrPubStarsRecyclerView.setLayoutManager(mHighMmrPubStarsSummaryLinearLayoutManager);
        mHighMmrPubStarsRecyclerView.setAdapter(mNotablePlayersSummaryAdapter);
        mPopularHeroesRecyclerView.setLayoutManager(mHeroStatsSummaryLinearLayoutManager);
        mPopularHeroesRecyclerView.setAdapter(mHeroStatsSummaryAdapter);
        mRecentLeagueMatchesRecyclerView.setLayoutManager(mRecentLeagueMatchesSummaryLinearLayoutManager);
        mRecentLeagueMatchesRecyclerView.setAdapter(mRecentLeagueMatchesSummaryAdapter);
        mRecentPubMatchesRecyclerView.setLayoutManager(mPubMatchesSummaryLinearLayoutManager);
        mRecentPubMatchesRecyclerView.setAdapter(mPubMatchesSummaryAdapter);
        mPopularClipsRecyclerView.setLayoutManager(mTwitchClipsSummaryLinearLayoutManager);
        mPopularClipsRecyclerView.setAdapter(mTwitchClipsSummaryAdapter);
    }

    /**
     * Success case of fetching stats.
     *
     * @param homeFragmentWrapperModel
     */
    public void onFetchResultSuccess(HomeFragmentWrapperModel homeFragmentWrapperModel) {
        Log.i(TAG, "onFetchResultSuccess()");
        mTeamsSummaryAdapter.setmTeamsModelList(homeFragmentWrapperModel.getTeams());
        mTeamsSummaryAdapter.notifyDataSetChanged();
        mNotablePlayersSummaryAdapter.setmProPlayersModelList(homeFragmentWrapperModel.getProPlayersModels());
        mNotablePlayersSummaryAdapter.notifyDataSetChanged();
        mHeroStatsSummaryAdapter.setmHeroStatsModelList(homeFragmentWrapperModel.getHeroStats());
        mHeroStatsSummaryAdapter.notifyDataSetChanged();
        mRecentLeagueMatchesSummaryAdapter.setmRecentLeagueMatchModelList(homeFragmentWrapperModel.getRecentLeagueMatchModels());
        mRecentLeagueMatchesSummaryAdapter.notifyDataSetChanged();
        new PopulateHeroDetailsInPubMatches().execute(homeFragmentWrapperModel.getPubMatches());
        mTwitchClipsSummaryAdapter.setmTwitchClipsModelList(homeFragmentWrapperModel.getTwitchClips());
        mTwitchClipsSummaryAdapter.notifyDataSetChanged();
        mContentScrollView.setVisibility(View.VISIBLE);
        mPlaceholderLayout.setVisibility(View.GONE);
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    /**
     * Failure case of fetching stats.
     */
    public void onFetchResultFailure() {
        Log.i(TAG, "onFetchResultFailure()");
        mTeamsSummaryAdapter.setmTeamsModelList(new ArrayList<TeamsModel>());
        mTeamsSummaryAdapter.notifyDataSetChanged();
        mNotablePlayersSummaryAdapter.setmProPlayersModelList(new ArrayList<ProPlayersModel>());
        mNotablePlayersSummaryAdapter.notifyDataSetChanged();
        mHeroStatsSummaryAdapter.setmHeroStatsModelList(new ArrayList<HeroStatsModel>());
        mHeroStatsSummaryAdapter.notifyDataSetChanged();
        mRecentLeagueMatchesSummaryAdapter.setmRecentLeagueMatchModelList(new ArrayList<RecentLeagueMatchModel>());
        mRecentLeagueMatchesSummaryAdapter.notifyDataSetChanged();
        mPubMatchesSummaryAdapter.setmPubMatchesModelList(new ArrayList<PubMatchesModel>());
        mPubMatchesSummaryAdapter.notifyDataSetChanged();
        mTwitchClipsSummaryAdapter.setmTwitchClipsModelList(new ArrayList<TwitchClipsModel>());
        mTwitchClipsSummaryAdapter.notifyDataSetChanged();
        mContentScrollView.setVisibility(View.GONE);
        mPlaceholderLayout.setVisibility(View.VISIBLE);
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    /**
     * To be called when the query succeeds.
     *
     * @param teamDetailsModel - The resulting model details to be shown.
     */
    public void onTeamDetailsQuerySuccess(TeamDetailsModel teamDetailsModel) {
        Log.i(TAG, "onTeamDetailsQuerySuccess()");
        TeamDetailsDialogFragment teamDetailsDialogFragment = TeamDetailsDialogFragment.getInstance(teamDetailsModel);
        teamDetailsDialogFragment.show(((AppCompatActivity)mContext).getSupportFragmentManager(), "dialog");
    }

    /**
     * To be called when the query fails.
     */
    public void onTeamDetailsQueryFailure(boolean isCancelled) {
        Log.i(TAG, "onTeamDetailsQueryFailure()");
        if (!isCancelled) {
            ComponentUtil.createAndShowSnackBar(mContext, mFragmentView, mContext.getString(R.string.fragment_teams_error_message));
        }
    }

    public class PopulateHeroDetailsInPubMatches extends AsyncTask<List<PubMatchesModel>, Void, List<PubMatchesModel>> {
        @Override
        protected void onPostExecute(List<PubMatchesModel> pubMatchesModels) {
            mPubMatchesSummaryAdapter.setmPubMatchesModelList(pubMatchesModels);
            mPubMatchesSummaryAdapter.notifyDataSetChanged();
        }

        @Override
        protected List<PubMatchesModel> doInBackground(List<PubMatchesModel>[] lists) {
            StatDotaDatabaseHelper statDotaDatabaseHelper = StatDotaDatabaseHelper.getInstance(mContext);
            for (PubMatchesModel pubMatchesModel: lists[0]) {
                pubMatchesModel.setmRadiantHeroDetails(new ArrayList<HeroDetails>());
                pubMatchesModel.setmDireHeroDetails(new ArrayList<HeroDetails>());
                String[] radiantTeamHeroes = pubMatchesModel.getRadiantTeam().split(",");
                String[] direTeamHeroes = pubMatchesModel.getDireTeam().split(",");
                for(int i = 0; i < radiantTeamHeroes.length; i++) {
                    pubMatchesModel.getmRadiantHeroDetails().add(statDotaDatabaseHelper.fetchHeroDetails(Long.parseLong(radiantTeamHeroes[i])));
                    pubMatchesModel.getmDireHeroDetails().add(statDotaDatabaseHelper.fetchHeroDetails(Long.parseLong(direTeamHeroes[i])));
                }
            }
            return lists[0];
        }
    }

    public interface HomeFragmentInterface {

        void fetchHomeFragmentStats();

        void refreshHomeFragmentStats();

        void fetchTeamDetails(long teamId);

        void onRecentPubMatchesRequest();

        void onRecentLeagueMatchesRequest();
    }
}
