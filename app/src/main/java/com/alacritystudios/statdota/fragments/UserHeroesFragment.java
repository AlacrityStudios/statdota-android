package com.alacritystudios.statdota.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.adapters.UserHeroSummaryAdapter;
import com.alacritystudios.statdota.constants.BundleConstants;
import com.alacritystudios.statdota.constants.StatDotaApiConstants;
import com.alacritystudios.statdota.fragments.base.BaseFragment;
import com.alacritystudios.statdota.models.UserHeroSummaryModel;
import com.alacritystudios.statdota.rest.ApiClient;
import com.alacritystudios.statdota.rest.ApiInterface;
import com.alacritystudios.statdota.utils.LoggerUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


/**
 * Displays crucial stats about the user's hero performances.
 */

public class UserHeroesFragment extends BaseFragment implements BaseFragment.BaseFragmentImpl {

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayout mPlaceholderLayout;
    private LinearLayout mErrorLayout;
    private RecyclerView mHeroesSummaryRecyclerView;

    private UserHeroSummaryAdapter mUserHeroSummaryAdapter;
    private LinearLayoutManager mUserHeroListLinearLayoutManager;

    private UserHeroesFragment.UserHeroesFragmentInterface mUserHeroesFragmentInterface;
    private HeroesSortOrder mSortOrder;
    private long mAccountId;
    private boolean mIsUserAccount;

    public enum HeroesSortOrder {
        heroName,
        mostPlayed,
        highestWinrate
    }

    public static UserHeroesFragment newInstance(long accountId, boolean isUserAccount) {
        UserHeroesFragment userHeroesFragment = new UserHeroesFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(BundleConstants.ACCOUNT_ID, accountId);
        bundle.putBoolean(BundleConstants.IS_USER_ACCOUNT, isUserAccount);
        userHeroesFragment.setArguments(bundle);
        return userHeroesFragment;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.hero_list_sort_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void initialiseUI() {
        setHasOptionsMenu(true);
        mHeroesSummaryRecyclerView = (RecyclerView) mFragmentView.findViewById(R.id.recycler_view_user_heroes_summary);
        mSwipeRefreshLayout = (SwipeRefreshLayout) mFragmentView.findViewById(R.id.fragment_user_heroes_swipe_refresh_layout);
        mPlaceholderLayout = (LinearLayout) mFragmentView.findViewById(R.id.user_heroes_fragment_placeholder_layout);
        mErrorLayout = (LinearLayout) mFragmentView.findViewById(R.id.user_heroes_fragment_error_layout);
        mHeroesSummaryRecyclerView.setLayoutManager(mUserHeroListLinearLayoutManager);
        mHeroesSummaryRecyclerView.setAdapter(mUserHeroSummaryAdapter);
        setupSwipeRefreshLayout();
        mSwipeRefreshLayout.setRefreshing(true);
        mUserHeroesFragmentInterface.fetchHeroFragmentStats(String.valueOf(mAccountId));
    }

    @Override
    public void setListeners() {

    }

    @Override
    public void setContextAndTags() {
        mContext = getActivity();
        TAG = LoggerUtil.makeLogTag(getClass());
    }

    @Override
    public void initialiseOtherComponents() {
        mSortOrder = HeroesSortOrder.heroName;
        mAccountId = getArguments().getLong(BundleConstants.ACCOUNT_ID);
        mIsUserAccount = getArguments().getBoolean(BundleConstants.IS_USER_ACCOUNT);
        mUserHeroSummaryAdapter = new UserHeroSummaryAdapter(new ArrayList<UserHeroSummaryModel>(), mContext);
        mUserHeroListLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_user_heroes, container, false);
        setContextAndTags();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
        return mFragmentView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof UserHeroesFragmentInterface) {
            mUserHeroesFragmentInterface = (UserHeroesFragmentInterface) context;
        } else {
            throw new ClassCastException("LandingActivity must implement UserHeroesFragmentInterface.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mUserHeroesFragmentInterface = null;
    }

    /**
     * Setup the swipe refresh layout behaviour.
     */
    private void setupSwipeRefreshLayout() {
        Log.i(TAG, "setupSwipeRefreshLayout()");
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                mUserHeroesFragmentInterface.refreshHeroFragmentStats(String.valueOf(mAccountId));
            }
        });
    }

    /**
     *
     * @param userHeroSummaryModels
     * @return
     */
    public List<UserHeroSummaryModel> sortRecords(List<UserHeroSummaryModel> userHeroSummaryModels) {
        if (mSortOrder == HeroesSortOrder.heroName) {
            Collections.sort(userHeroSummaryModels, new Comparator<UserHeroSummaryModel>() {

                @Override
                public int compare(UserHeroSummaryModel o1, UserHeroSummaryModel o2) {
                    return o1.getHeroDetails().getLocalizedName().compareTo(o2.getHeroDetails().getLocalizedName());
                }
            });
        } else if(mSortOrder == HeroesSortOrder.mostPlayed) {
            Collections.sort(userHeroSummaryModels, new Comparator<UserHeroSummaryModel>() {

                @Override
                public int compare(UserHeroSummaryModel o1, UserHeroSummaryModel o2) {
                    return o2.getGames().compareTo(o1.getGames());
                }
            });
        }else if(mSortOrder == HeroesSortOrder.highestWinrate) {
            Collections.sort(userHeroSummaryModels, new Comparator<UserHeroSummaryModel>() {

                @Override
                public int compare(UserHeroSummaryModel o1, UserHeroSummaryModel o2) {
                    return ((Integer)(o2.getGames() == 0 ? 0 : o2.getWin() * 100/o2.getGames())).compareTo((Integer)(o1.getGames() == 0 ? 0 : o1.getWin() * 100/o1.getGames()));
                }
            });
        }
        return userHeroSummaryModels;
    }

    /**
     * To be called when the query succeeds.
     * @param userMatchSummaryModelList - The resulting list to be shown.
     */
    public void onUserHeroesQuerySuccess(List<UserHeroSummaryModel> userMatchSummaryModelList) {
        Log.i(TAG, "onUserHeroesQuerySuccess()");
        userMatchSummaryModelList = sortRecords(userMatchSummaryModelList);
        mUserHeroSummaryAdapter.setmHeroSummaryList(userMatchSummaryModelList);
        mUserHeroSummaryAdapter.notifyDataSetChanged();
        if (userMatchSummaryModelList.size() == 0) {
            mHeroesSummaryRecyclerView.setVisibility(View.GONE);
            mErrorLayout.setVisibility(View.GONE);
            mPlaceholderLayout.setVisibility(View.VISIBLE);
        } else {
            mHeroesSummaryRecyclerView.setVisibility(View.VISIBLE);
            mErrorLayout.setVisibility(View.GONE);
            mPlaceholderLayout.setVisibility(View.GONE);
        }
        mSwipeRefreshLayout.setRefreshing(false);
    }

    /**
     * To be called when the query fails.
     */
    public void onUserHeroesQueryFailure(boolean isCancelled) {
        Log.i(TAG, "onUserHeroesQueryFailure()");
        mUserHeroSummaryAdapter.setmHeroSummaryList(new ArrayList<UserHeroSummaryModel>());
        mUserHeroSummaryAdapter.notifyDataSetChanged();
        if(!isCancelled) {
            mHeroesSummaryRecyclerView.setVisibility(View.GONE);
            mErrorLayout.setVisibility(View.VISIBLE);
            mPlaceholderLayout.setVisibility(View.GONE);
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    public interface UserHeroesFragmentInterface {

        void fetchHeroFragmentStats(String accountId);

        void refreshHeroFragmentStats(String accountId);
    }
}
