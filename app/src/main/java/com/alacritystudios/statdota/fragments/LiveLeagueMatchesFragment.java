package com.alacritystudios.statdota.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.fragments.base.BaseFragment;

/**
 * Fragment to display all the live league matches.
 */

public class LiveLeagueMatchesFragment extends BaseFragment implements BaseFragment.BaseFragmentImpl {

    public static LiveLeagueMatchesFragment newInstance() {
        LiveLeagueMatchesFragment liveLeagueMatchesFragment = new LiveLeagueMatchesFragment();
        Bundle bundle = new Bundle();
        liveLeagueMatchesFragment.setArguments(bundle);
        return liveLeagueMatchesFragment;
    }

    @Override
    public void initialiseUI() {

    }

    @Override
    public void setListeners() {

    }

    @Override
    public void setContextAndTags() {

    }

    @Override
    public void initialiseOtherComponents() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_live_league_matches, container, false);
        setContextAndTags();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
        return mFragmentView;
    }
}
