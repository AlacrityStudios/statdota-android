package com.alacritystudios.statdota.fragments;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.adapters.RecentLeagueMatchesListAdapter;
import com.alacritystudios.statdota.fragments.base.BaseFragment;
import com.alacritystudios.statdota.models.RecentLeagueMatchModel;
import com.alacritystudios.statdota.utils.ComponentUtil;
import com.alacritystudios.statdota.utils.LoggerUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment to display all the recently completed league matches.
 */

public class RecentLeagueMatchesFragment extends BaseFragment implements BaseFragment.BaseFragmentImpl {

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayout mPlaceholderLayout;
    private LinearLayout mErrorLayout;
    private RecyclerView mRecentLeagueMatchListRecyclerView;
    private RecentLeagueMatchesListAdapter mRecentLeagueMatchesListAdapter;

    private Drawable mPlaceholderDrawable;
    private LinearLayoutManager mRecentLeagueMatchListLinearLayoutManager;
    private List<RecentLeagueMatchModel> mRecentLeagueMatchModelList;

    private int mPageNumber = 0;
    private boolean mLoading = true;

    private RecentLeagueMatchesFragment.RecentLeagueMatchesFragmentInterface mRecentLeagueMatchesFragmentInterface;

    public static RecentLeagueMatchesFragment newInstance() {
        RecentLeagueMatchesFragment recentLeagueMatchesFragment = new RecentLeagueMatchesFragment();
        Bundle bundle = new Bundle();
        recentLeagueMatchesFragment.setArguments(bundle);
        return recentLeagueMatchesFragment;
    }

    @Override
    public void initialiseUI() {
        mRecentLeagueMatchListRecyclerView = (RecyclerView) mFragmentView.findViewById(R.id.recycler_view_matches);
        mSwipeRefreshLayout = (SwipeRefreshLayout) mFragmentView.findViewById(R.id.recent_league_matches_swipe_refresh_layout);
        mPlaceholderLayout = (LinearLayout) mFragmentView.findViewById(R.id.recent_league_matches_fragment_placeholder_layout);
        mErrorLayout = (LinearLayout) mFragmentView.findViewById(R.id.recent_league_matches_fragment_error_layout);
        setupMatchesRecyclerView();
        setupSwipeRefreshLayout();
        mSwipeRefreshLayout.setRefreshing(true);
        mRecentLeagueMatchesFragmentInterface.fetchRecentLeagueMatches(20, mPageNumber * 20);
    }

    @Override
    public void setListeners() {
        mRecentLeagueMatchListRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (!recyclerView.canScrollVertically(1) && dy != 0) {
                    mPageNumber++;
                    mSwipeRefreshLayout.setRefreshing(true);
                    mRecentLeagueMatchesFragmentInterface.fetchRecentLeagueMatches(20, mPageNumber * 20);
                    mLoading = true;
                }
            }
        });
    }

    @Override
    public void setContextAndTags() {
        mContext = getActivity();
        TAG = LoggerUtil.makeLogTag(getClass());
    }

    @Override
    public void initialiseOtherComponents() {
        mPageNumber = 0;
        mRecentLeagueMatchModelList = new ArrayList<>();
        mPlaceholderDrawable = ActivityCompat.getDrawable(mContext, R.drawable.ic_dota);
        DrawableCompat.wrap(mPlaceholderDrawable).setTint(ActivityCompat.getColor(mContext, R.color.colorDefaultMedium));
        mRecentLeagueMatchesListAdapter = new RecentLeagueMatchesListAdapter(mRecentLeagueMatchModelList, mContext, mPlaceholderDrawable);
        mRecentLeagueMatchListLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_recent_league_matches, container, false);
        setContextAndTags();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
        return mFragmentView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof RecentLeagueMatchesFragment.RecentLeagueMatchesFragmentInterface) {
            mRecentLeagueMatchesFragmentInterface = (RecentLeagueMatchesFragment.RecentLeagueMatchesFragmentInterface) context;
        } else {
            throw new ClassCastException("LandingActivity must implement RecentLeagueMatchesFragmentInterface.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mRecentLeagueMatchesFragmentInterface = null;
    }

    /**
     * Setup the matches recycler view.
     */
    private void setupMatchesRecyclerView() {
        Log.i(TAG, "setupMatchesRecyclerView()");
        mRecentLeagueMatchListRecyclerView.setLayoutManager(mRecentLeagueMatchListLinearLayoutManager);
        mRecentLeagueMatchListRecyclerView.setAdapter(mRecentLeagueMatchesListAdapter);
    }

    /**
     * Setup the swipe refresh layout behaviour.
     */
    private void setupSwipeRefreshLayout() {
        Log.i(TAG, "setupSwipeRefreshLayout()");
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                mRecentLeagueMatchesListAdapter.getmRecentLeagueMatchList().clear();
                mPageNumber = 0;
                mSwipeRefreshLayout.setRefreshing(true);
                mRecentLeagueMatchesFragmentInterface.fetchRecentLeagueMatches(20, mPageNumber * 20);
            }
        });
    }

    /**
     * To be called when the query succeeds.
     *
     * @param recentLeagueMatchModelList - The resulting list to be shown.
     */
    public void onRecentLeagueMatchesQuerySuccess(List<RecentLeagueMatchModel> recentLeagueMatchModelList) {
        Log.i(TAG, "onRecentLeagueMatchesQuerySuccess()");
        mRecentLeagueMatchesListAdapter.getmRecentLeagueMatchList().addAll(recentLeagueMatchModelList);
        mRecentLeagueMatchesListAdapter.notifyDataSetChanged();
        if (recentLeagueMatchModelList.size() == 0) {
            mRecentLeagueMatchListRecyclerView.setVisibility(View.GONE);
            mErrorLayout.setVisibility(View.GONE);
            mPlaceholderLayout.setVisibility(View.VISIBLE);
        } else {
            mRecentLeagueMatchListRecyclerView.setVisibility(View.VISIBLE);
            mErrorLayout.setVisibility(View.GONE);
            mPlaceholderLayout.setVisibility(View.GONE);
        }
        mLoading = false;
        mSwipeRefreshLayout.setRefreshing(false);
    }

    /**
     * To be called when the query fails.
     */
    public void onRecentLeagueMatchesQueryFailure(boolean isCancelled) {
        Log.i(TAG, "onRecentLeagueMatchesQueryFailure()");
        if (!isCancelled && mRecentLeagueMatchesListAdapter.getmRecentLeagueMatchList().size() != 0) {
            ComponentUtil.createAndShowSnackBar(mContext, mSwipeRefreshLayout, mContext.getString(R.string.fragment_matches_error_message));
            mRecentLeagueMatchListRecyclerView.setVisibility(View.VISIBLE);
            mErrorLayout.setVisibility(View.GONE);
            mPlaceholderLayout.setVisibility(View.GONE);
            mSwipeRefreshLayout.setRefreshing(false);
        } else if (!isCancelled && mRecentLeagueMatchesListAdapter.getmRecentLeagueMatchList().size() == 0) {
            mRecentLeagueMatchListRecyclerView.setVisibility(View.GONE);
            mErrorLayout.setVisibility(View.VISIBLE);
            mPlaceholderLayout.setVisibility(View.GONE);
            mSwipeRefreshLayout.setRefreshing(false);
        }
        mPageNumber--;
        mLoading = false;
    }

    public interface RecentLeagueMatchesFragmentInterface {

        void fetchRecentLeagueMatches(int limit, int offset);

    }
}
