package com.alacritystudios.statdota.fragments.headless;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.alacritystudios.statdota.constants.StatDotaApiConstants;
import com.alacritystudios.statdota.models.HomeFragmentWrapperModel;
import com.alacritystudios.statdota.models.MatchFilterResultModel;
import com.alacritystudios.statdota.models.PubMatchesModel;
import com.alacritystudios.statdota.models.RecentLeagueMatchModel;
import com.alacritystudios.statdota.models.SearchResultModel;
import com.alacritystudios.statdota.models.TeamDetailsModel;
import com.alacritystudios.statdota.models.TwitchStreamsModel;
import com.alacritystudios.statdota.models.UserFriendsModel;
import com.alacritystudios.statdota.models.UserHeroSummaryModel;
import com.alacritystudios.statdota.models.UserMatchSummaryModel;
import com.alacritystudios.statdota.models.UserStatsModel;
import com.alacritystudios.statdota.rest.ApiClient;
import com.alacritystudios.statdota.rest.ApiInterface;
import com.alacritystudios.statdota.utils.LoggerUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Headless fragment to make all network calls.
 */

public class AccountActivityHeadlessFragment extends Fragment {

    private UserStatsModel mUserStatsModel;
    private List<UserMatchSummaryModel> mUserMatchSummaryList;
    private List<UserHeroSummaryModel> mUserHeroSummaryList;
    private List<UserFriendsModel> mUserFriendSummaryList;
    private AccountActivityHeadlessFragment.AccountActivityHeadlessFragmentInterface mAccountActivityHeadlessFragmentInterface;
    private final String TAG = LoggerUtil.makeLogTag(getClass());

    private Call<UserStatsModel> mUserStatsModelCall;
    private Call<List<UserMatchSummaryModel>> mUserMatchSummaryListCall;
    private Call<List<UserHeroSummaryModel>> mUserHeroSummaryListCall;
    private Call<List<UserFriendsModel>> mUserFriendSummaryListCall;
    private Call<List<PubMatchesModel>> mRecentPubMatchModelListCall;
    private Call<List<RecentLeagueMatchModel>> mRecentLeagueMatchModelListCall;

    public static AccountActivityHeadlessFragment getInstance() {
        AccountActivityHeadlessFragment accountActivityHeadlessFragment = new AccountActivityHeadlessFragment();
        Bundle bundle = new Bundle();
        accountActivityHeadlessFragment.setArguments(bundle);
        return accountActivityHeadlessFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AccountActivityHeadlessFragment.AccountActivityHeadlessFragmentInterface) {
            mAccountActivityHeadlessFragmentInterface = (AccountActivityHeadlessFragment.AccountActivityHeadlessFragmentInterface) context;
        } else {
            throw new ClassCastException("Account Activity must implement AccountActivityHeadlessFragmentInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mAccountActivityHeadlessFragmentInterface = null;
    }

    /**
     * Fetch user stats from the backend.
     */
    public void fetchUserStatsFromBackend(String accountId) {
        Log.i(TAG, "fetchUserStatsFromBackend()");
        Retrofit retrofit = ApiClient.getClient(StatDotaApiConstants.STAT_DOTA_BASE_URL);
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        if (mUserStatsModelCall != null) {
            mUserStatsModelCall.cancel();
        }
        mUserStatsModelCall = apiInterface.getUserStatsInfo(accountId);
        mUserStatsModelCall.enqueue(new Callback<UserStatsModel>() {

            @Override
            public void onResponse(Call<UserStatsModel> call, Response<UserStatsModel> response) {
                if (response.isSuccessful()) {
                    mUserStatsModel = response.body();
                    if (mAccountActivityHeadlessFragmentInterface != null)
                        mAccountActivityHeadlessFragmentInterface.onUserStatsQuerySuccessCallback(mUserStatsModel);
                }
            }

            @Override
            public void onFailure(Call<UserStatsModel> call, Throwable t) {
                if (call.isCanceled()) {
                    if (mAccountActivityHeadlessFragmentInterface != null)
                        mAccountActivityHeadlessFragmentInterface.onUserStatsQueryFailureCallback(true);
                } else {
                    Log.d(TAG, t.getLocalizedMessage());
                    if (mAccountActivityHeadlessFragmentInterface != null)
                        mAccountActivityHeadlessFragmentInterface.onUserStatsQueryFailureCallback(false);
                }
            }
        });
    }

    /**
     * Fetch user matches from the backend.
     */
    public void fetchUserMatchesFromBackend(String accountId, int limit, final int offset, MatchFilterResultModel matchFilterResultModel) {
        Log.i(TAG, "fetchUserMatchesFromBackend()");
        Retrofit retrofit = ApiClient.getClient(StatDotaApiConstants.STAT_DOTA_BASE_URL);
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        if (mUserMatchSummaryListCall != null) {
            mUserMatchSummaryListCall.cancel();
        }
        mUserMatchSummaryListCall = apiInterface.getPlayerMatches(accountId, limit, offset, matchFilterResultModel.getHeroPlayedId(), matchFilterResultModel.getWinCondition(), matchFilterResultModel.getIsRadiant(), matchFilterResultModel.getGameMode(), matchFilterResultModel.getLobbyType(), matchFilterResultModel.getAlliedHeroIdList(), matchFilterResultModel.getAgainstHeroIdList());
        mUserMatchSummaryListCall.enqueue(new Callback<List<UserMatchSummaryModel>>() {

            @Override
            public void onResponse(Call<List<UserMatchSummaryModel>> call, Response<List<UserMatchSummaryModel>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (offset == 0) {
                        mUserMatchSummaryList = response.body();
                    } else {
                        mUserMatchSummaryList.addAll(response.body());
                    }
                    if (mAccountActivityHeadlessFragmentInterface != null)
                        mAccountActivityHeadlessFragmentInterface.onUserMatchesQuerySuccessCallback(mUserMatchSummaryList);
                } else {
                    Log.d(TAG, "Received null response body from backend");
                    if (mAccountActivityHeadlessFragmentInterface != null)
                        mAccountActivityHeadlessFragmentInterface.onUserMatchesQueryFailureCallback(false);
                }
            }

            @Override
            public void onFailure(Call<List<UserMatchSummaryModel>> call, Throwable t) {
                if (call.isCanceled()) {
                    if (mAccountActivityHeadlessFragmentInterface != null)
                        mAccountActivityHeadlessFragmentInterface.onUserMatchesQueryFailureCallback(true);
                } else {
                    t.printStackTrace();
                    if (mAccountActivityHeadlessFragmentInterface != null)
                        mAccountActivityHeadlessFragmentInterface.onUserMatchesQueryFailureCallback(false);
                }
            }
        });
    }

    /**
     * Returns the user hero stats. If they are absent then we fetch them from the API again.
     */
    public void fetchUserHeroes(String accountId) {
        Log.i(TAG, "fetchUserStats()");
        if (mUserHeroSummaryList != null) {
            if (mAccountActivityHeadlessFragmentInterface != null)
                mAccountActivityHeadlessFragmentInterface.onUserHeroesQuerySuccessCallback(mUserHeroSummaryList);
        } else {
            fetchUserHeroesFromBackend(accountId);
        }
    }

    /**
     * Fetch user heroes from the backend.
     */
    public void fetchUserHeroesFromBackend(String accountId) {
        Log.i(TAG, "fetchUserMatchesFromBackend()");
        Retrofit retrofit = ApiClient.getClient(StatDotaApiConstants.STAT_DOTA_BASE_URL);
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        if (mUserHeroSummaryListCall != null) {
            mUserHeroSummaryListCall.cancel();
        }
        mUserHeroSummaryListCall = apiInterface.getUserHeroSummary(accountId);
        mUserHeroSummaryListCall.enqueue(new Callback<List<UserHeroSummaryModel>>() {

            @Override
            public void onResponse(Call<List<UserHeroSummaryModel>> call, Response<List<UserHeroSummaryModel>> response) {
                if (response.isSuccessful() && response.body() != null) {

                    mUserHeroSummaryList = response.body();
                    if (mAccountActivityHeadlessFragmentInterface != null)
                        mAccountActivityHeadlessFragmentInterface.onUserHeroesQuerySuccessCallback(mUserHeroSummaryList);
                } else {
                    Log.d(TAG, "Received null response body from backend");
                    if (mAccountActivityHeadlessFragmentInterface != null)
                        mAccountActivityHeadlessFragmentInterface.onUserMatchesQueryFailureCallback(false);
                }
            }

            @Override
            public void onFailure(Call<List<UserHeroSummaryModel>> call, Throwable t) {
                if (call.isCanceled()) {
                    if (mAccountActivityHeadlessFragmentInterface != null)
                        mAccountActivityHeadlessFragmentInterface.onUserHeroesQueryFailureCallback(true);
                } else {
                    t.printStackTrace();
                    if (mAccountActivityHeadlessFragmentInterface != null)
                        mAccountActivityHeadlessFragmentInterface.onUserHeroesQueryFailureCallback(false);
                }
            }
        });
    }

    /**
     * Returns the user friends stats. If they are absent then we fetch them from the API again.
     */
    public void fetchUserFriends(String accountId) {
        Log.i(TAG, "fetchUserFriends()");
        if (mUserFriendSummaryList != null) {
            if (mAccountActivityHeadlessFragmentInterface != null)
                mAccountActivityHeadlessFragmentInterface.onUserFriendsQuerySuccessCallback(mUserFriendSummaryList);
        } else {
            fetchUserFriendsFromBackend(accountId);
        }
    }

    /**
     * Fetch user friends from the backend.
     */
    public void fetchUserFriendsFromBackend(String accountId) {
        Log.i(TAG, "fetchUserFriendsFromBackend()");
        Retrofit retrofit = ApiClient.getClient(StatDotaApiConstants.STAT_DOTA_BASE_URL);
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        if (mUserFriendSummaryListCall != null) {
            mUserFriendSummaryListCall.cancel();
        }
        mUserFriendSummaryListCall = apiInterface.getUserFriends(accountId);
        mUserFriendSummaryListCall.enqueue(new Callback<List<UserFriendsModel>>() {

            @Override
            public void onResponse(Call<List<UserFriendsModel>> call, Response<List<UserFriendsModel>> response) {
                if (response.isSuccessful() && response.body() != null) {

                    mUserFriendSummaryList = response.body();
                    if (mAccountActivityHeadlessFragmentInterface != null)
                        mAccountActivityHeadlessFragmentInterface.onUserFriendsQuerySuccessCallback(mUserFriendSummaryList);
                } else {
                    Log.d(TAG, "Received null response body from backend");
                    mAccountActivityHeadlessFragmentInterface.onUserFriendsQueryFailureCallback(false);
                }
            }

            @Override
            public void onFailure(Call<List<UserFriendsModel>> call, Throwable t) {
                if (call.isCanceled()) {
                    if (mAccountActivityHeadlessFragmentInterface != null)
                        mAccountActivityHeadlessFragmentInterface.onUserFriendsQueryFailureCallback(true);
                } else {
                    t.printStackTrace();
                    if (mAccountActivityHeadlessFragmentInterface != null)
                        mAccountActivityHeadlessFragmentInterface.onUserFriendsQueryFailureCallback(false);
                }
            }
        });
    }

    public interface AccountActivityHeadlessFragmentInterface {

        void onUserStatsQuerySuccessCallback(UserStatsModel userStatsModel);

        void onUserStatsQueryFailureCallback(boolean isCancelled);

        void onUserMatchesQuerySuccessCallback(List<UserMatchSummaryModel> userMatchSummaryModelList);

        void onUserMatchesQueryFailureCallback(boolean isCancelled);

        void onUserHeroesQuerySuccessCallback(List<UserHeroSummaryModel> userHeroSummaryModelList);

        void onUserHeroesQueryFailureCallback(boolean isCancelled);

        void onUserFriendsQuerySuccessCallback(List<UserFriendsModel> userFriendsModelList);

        void onUserFriendsQueryFailureCallback(boolean isCancelled);

    }
}
