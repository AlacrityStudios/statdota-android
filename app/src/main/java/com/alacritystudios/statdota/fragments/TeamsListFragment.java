package com.alacritystudios.statdota.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.adapters.TeamsListAdapter;
import com.alacritystudios.statdota.fragments.base.BaseFragment;
import com.alacritystudios.statdota.models.TeamDetailsModel;
import com.alacritystudios.statdota.models.TeamsModel;
import com.alacritystudios.statdota.models.UserMatchSummaryModel;
import com.alacritystudios.statdota.utils.ApplicationUtils;
import com.alacritystudios.statdota.utils.ComponentUtil;
import com.alacritystudios.statdota.utils.LoggerUtil;
import com.lapism.searchview.SearchEditText;
import com.lapism.searchview.SearchView;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment to display the list of active Dota 2 teams.
 */

public class TeamsListFragment extends BaseFragment implements BaseFragment.BaseFragmentImpl, TeamsListAdapter.TeamsListAdapterListener {

    private Toolbar mTeamsListFragmentToolbar;
    private SearchView mTeamsListFragmentSearchView;
    private SwipeRefreshLayout mTeamsListFragmentSwipeRefreshLayout;
    private LinearLayout mTeamsListFragmentErrorLayout;
    private LinearLayout mTeamsListFragmentPlaceholderLayout;
    private RecyclerView mTeamsListRecyclerView;

    private Drawable mPlaceholderDrawable;
    private LinearLayoutManager mTeamsListLinearLayoutManager;
    private TeamsListAdapter mTeamsListAdapter;
    private TeamsListFragmentInterface mTeamsListFragmentInterface;

    private int mPageNumber = 0;
    private int mPreviousTotal = 0;
    private boolean mLoading = true;
    private int mVisibleThreshold = 19;
    private int mVisibleItemCount;
    private int mTotalItemCount;
    private int mFirstVisibleItem;

    public static TeamsListFragment getInstance() {
        TeamsListFragment teamsListFragment = new TeamsListFragment();
        Bundle bundle = new Bundle();
        teamsListFragment.setArguments(bundle);
        return teamsListFragment;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.teams_list_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                ((Activity) mContext).onBackPressed();
                break;
            case R.id.menu_search:
                mTeamsListFragmentSearchView.open(true);

        }
        return true;
    }

    @Override
    public void initialiseUI() {
        setHasOptionsMenu(true);
        mTeamsListFragmentToolbar = mFragmentView.findViewById(R.id.teams_list_fragment_toolbar);
        mTeamsListFragmentSearchView = mFragmentView.findViewById(R.id.teams_list_fragment_search_view);
        mTeamsListRecyclerView = mFragmentView.findViewById(R.id.teams_list_recycler_view);
        mTeamsListFragmentSwipeRefreshLayout = mFragmentView.findViewById(R.id.teams_list_fragment_swipe_refresh_layout);
        mTeamsListFragmentPlaceholderLayout = mFragmentView.findViewById(R.id.teams_list_fragment_placeholder_layout);
        mTeamsListFragmentErrorLayout = mFragmentView.findViewById(R.id.teams_list_fragment_error_layout);
        setupToolbar();
        setupTeamsListRecyclerView();
        mTeamsListFragmentSwipeRefreshLayout.setRefreshing(true);
        mTeamsListFragmentInterface.onFetchTeamsList(mPageNumber * 20, 20, "");
    }

    @Override
    public void setListeners() {
        mTeamsListFragmentSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mPageNumber = 0;
                mTeamsListAdapter.getmTeamsModelList().clear();
                mTeamsListFragmentInterface.onFetchTeamsList(mPageNumber * 20, 20, newText);
                return false;
            }
        });
        mTeamsListFragmentSearchView.setOnOpenCloseListener(new SearchView.OnOpenCloseListener() {

            @Override
            public boolean onClose() {
                mTeamsListFragmentToolbar.setVisibility(View.VISIBLE);
                return false;
            }

            @Override
            public boolean onOpen() {
                mTeamsListFragmentToolbar.setVisibility(View.GONE);
                return false;
            }
        });
        mTeamsListFragmentSearchView.setOnMenuClickListener(new SearchView.OnMenuClickListener() {

            @Override
            public void onMenuClick() {
                Log.i(TAG, "onMenuClick()");
            }
        });
        mTeamsListRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

//                mVisibleItemCount = mTeamsListRecyclerView.getChildCount();
//                mTotalItemCount = mTeamsListLinearLayoutManager.getItemCount();
//                mFirstVisibleItem = mTeamsListLinearLayoutManager.findFirstVisibleItemPosition();
//
//                if (mLoading) {
//                    if (mTotalItemCount > mPreviousTotal) {
//                        mLoading = false;
//                        mPreviousTotal = mTotalItemCount;
//                    }
//                }
//                if (! mLoading && (mTotalItemCount - mVisibleItemCount) <= (mFirstVisibleItem + mVisibleThreshold)) {
//                    mPageNumber++;
//                    mTeamsListFragmentSwipeRefreshLayout.setRefreshing(true);
//                    mTeamsListFragmentInterface.onFetchTeamsList(mPageNumber * 20, 20, "");
//                    mLoading = true;
//                }
                if (!recyclerView.canScrollVertically(1) && dy != 0) {
                    mPageNumber++;
                    mTeamsListFragmentSwipeRefreshLayout.setRefreshing(true);
                    mTeamsListFragmentInterface.onFetchTeamsList(mPageNumber * 20, 20, "");
                    mLoading = true;
                }
            }
        });

        mTeamsListFragmentSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                mPageNumber = 0;
                mTeamsListAdapter.getmTeamsModelList().clear();
                mTeamsListFragmentInterface.onFetchTeamsList(mPageNumber * 20, 20, "");
            }
        });
    }

    @Override
    public void setContextAndTags() {
        TAG = LoggerUtil.makeLogTag(getClass());
        mContext = getActivity();
    }

    @Override
    public void initialiseOtherComponents() {
        mPageNumber = 0;
        mPreviousTotal = 0;
        mPlaceholderDrawable = ActivityCompat.getDrawable(mContext, R.drawable.ic_dota);
        DrawableCompat.wrap(mPlaceholderDrawable).setTint(ActivityCompat.getColor(mContext, R.color.colorDefaultMedium));
        mTeamsListAdapter = new TeamsListAdapter(new ArrayList<TeamsModel>(), mContext, mPlaceholderDrawable, this);
        mTeamsListLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_teams_list, container, false);
        setContextAndTags();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
        return mFragmentView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TeamsListFragmentInterface) {
            mTeamsListFragmentInterface = (TeamsListFragmentInterface) context;
        } else {
            throw new ClassCastException("TeamActivity must implement TeamsListFragmentInterface.");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mTeamsListFragmentInterface = null;
    }

    @Override
    public void onItemTouch(long teamId) {
        mTeamsListFragmentInterface.fetchTeamDetails(teamId);
    }

    /**
     * Sets up the toolbar for the fragment.
     */
    private void setupToolbar() {
        Log.i(TAG, "setupToolbar()");
        mTeamsListFragmentToolbar.setTitleTextAppearance(mContext, R.style.ToolbarTitleAppearance);
        mTeamsListFragmentToolbar.setTitle(R.string.teams);
        mTeamsListFragmentToolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                ((Activity) mContext).onBackPressed();
            }
        });
        ((AppCompatActivity) mContext).setSupportActionBar(mTeamsListFragmentToolbar);
        ((AppCompatActivity) mContext).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) mContext).getSupportActionBar().setTitle(R.string.teams);
        LinearLayout linearLayout = mTeamsListFragmentSearchView.findViewById(com.lapism.searchview.R.id.linearLayout);
        ViewGroup.LayoutParams params = linearLayout.getLayoutParams();
        params.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 48, getResources().getDisplayMetrics());
        params.width = LinearLayout.LayoutParams.MATCH_PARENT;
        linearLayout.setLayoutParams(params);
        ImageView imageView =  mTeamsListFragmentSearchView.findViewById(com.lapism.searchview.R.id.imageView_arrow_back);
        //imageView.setVisibility(View.GONE);
        mTeamsListFragmentSearchView.findViewById(com.lapism.searchview.R.id.view_divider).setVisibility(View.GONE);
        SearchEditText searchEditText = mTeamsListFragmentSearchView.findViewById(com.lapism.searchview.R.id.searchEditText_input);
        searchEditText.setPadding(0,0,0,0);

        CardView.LayoutParams cardViewParams = new CardView.LayoutParams(
                CardView.LayoutParams.MATCH_PARENT,
                CardView.LayoutParams.WRAP_CONTENT
        );
        cardViewParams.setMargins(0, 0, 0, 0);
        mTeamsListFragmentSearchView.findViewById(com.lapism.searchview.R.id.cardView).setLayoutParams(cardViewParams);
        mTeamsListFragmentSearchView.setArrowOnly(true);
    }

    /**
     * Setup the teams list recycler view.
     */
    private void setupTeamsListRecyclerView() {
        Log.i(TAG, "setupTeamsListRecyclerView()");
        mTeamsListRecyclerView.setLayoutManager(mTeamsListLinearLayoutManager);
        mTeamsListRecyclerView.setAdapter(mTeamsListAdapter);
    }

    /**
     * To be called when the query succeeds.
     * @param teamsModelList - The resulting list to be shown.
     */
    public void onTeamsQuerySuccess(List<TeamsModel> teamsModelList) {
        Log.i(TAG, "onTeamsQuerySuccess() Fetched " + teamsModelList.size() + " items");
        mTeamsListAdapter.getmTeamsModelList().addAll(teamsModelList);
        mTeamsListAdapter.notifyDataSetChanged();
        if (mTeamsListAdapter.getmTeamsModelList().size() == 0) {
            mTeamsListRecyclerView.setVisibility(View.GONE);
            mTeamsListFragmentErrorLayout.setVisibility(View.GONE);
            mTeamsListFragmentPlaceholderLayout.setVisibility(View.VISIBLE);
        } else {
            mTeamsListRecyclerView.setVisibility(View.VISIBLE);
            mTeamsListFragmentErrorLayout.setVisibility(View.GONE);
            mTeamsListFragmentPlaceholderLayout.setVisibility(View.GONE);
        }
        mLoading = false;
        mTeamsListFragmentSwipeRefreshLayout.setRefreshing(false);
    }

    /**
     * To be called when the query fails.
     */
    public void onTeamsQueryFailure(boolean isCancelled) {
        Log.i(TAG, "onTeamsQueryFailure() isCancelled: " + isCancelled);
        if(!isCancelled && mTeamsListAdapter.getmTeamsModelList().size() != 0) {
            ComponentUtil.createAndShowSnackBar(mContext, mTeamsListFragmentSwipeRefreshLayout, mContext.getString(R.string.fragment_teams_error_message));
            mTeamsListRecyclerView.setVisibility(View.VISIBLE);
            mTeamsListFragmentErrorLayout.setVisibility(View.GONE);
            mTeamsListFragmentPlaceholderLayout.setVisibility(View.GONE);
            mTeamsListFragmentSwipeRefreshLayout.setRefreshing(false);
        } else if (!isCancelled && mTeamsListAdapter.getmTeamsModelList().size() == 0){
            mTeamsListRecyclerView.setVisibility(View.GONE);
            mTeamsListFragmentErrorLayout.setVisibility(View.VISIBLE);
            mTeamsListFragmentPlaceholderLayout.setVisibility(View.GONE);
            mTeamsListFragmentSwipeRefreshLayout.setRefreshing(false);
        }
        mPageNumber--;
        mLoading = false;
    }

    /**
     * To be called when the query succeeds.
     *
     * @param teamDetailsModel - The resulting model details to be shown.
     */
    public void onTeamDetailsQuerySuccess(TeamDetailsModel teamDetailsModel) {
        Log.i(TAG, "onTeamDetailsQuerySuccess()");
        TeamDetailsDialogFragment teamDetailsDialogFragment = TeamDetailsDialogFragment.getInstance(teamDetailsModel);
        teamDetailsDialogFragment.show(((AppCompatActivity)mContext).getSupportFragmentManager(), "dialog");
    }

    /**
     * To be called when the query fails.
     */
    public void onTeamDetailsQueryFailure(boolean isCancelled) {
        Log.i(TAG, "onTeamDetailsQueryFailure()");
        if (!isCancelled) {
            ComponentUtil.createAndShowSnackBar(mContext, mFragmentView, mContext.getString(R.string.fragment_teams_error_message));
        }
    }

    public interface TeamsListFragmentInterface {

        void onFetchTeamsList(int offset, int count, String query);

        void fetchTeamDetails(long teamId);
    }
}
