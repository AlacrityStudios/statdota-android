package com.alacritystudios.statdota.fragments.headless;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.alacritystudios.statdota.constants.StatDotaApiConstants;
import com.alacritystudios.statdota.models.HeroStatsModel;
import com.alacritystudios.statdota.rest.ApiClient;
import com.alacritystudios.statdota.rest.ApiInterface;
import com.alacritystudios.statdota.utils.LoggerUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A headless fragment to store fetches hero results.
 */

public class HeroNetworkFragment extends Fragment {

    private List<HeroStatsModel> mHeroStatsModelList;
    private HeroNetworkFragmentInterface mHeroNetworkFragmentInterface;
    private final String TAG = LoggerUtil.makeLogTag(HeroNetworkFragment.class);

    private Call<List<HeroStatsModel>> mHeroStatsListCall;

    public static HeroNetworkFragment getInstance() {
        HeroNetworkFragment heroNetworkFragment = new HeroNetworkFragment();
        Bundle bundle = new Bundle();
        heroNetworkFragment.setArguments(bundle);
        return heroNetworkFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof HeroNetworkFragmentInterface) {
            mHeroNetworkFragmentInterface = (HeroNetworkFragmentInterface) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement HeroNetworkFragment.HeroNetworkFragmentInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mHeroNetworkFragmentInterface = null;
    }

    /**
     * Returns the hero performance stats. If they are absent then we fetch them from the backend again.
     */
    public void fetchHeroPerformanceStats() {
        Log.i(TAG, "fetchHeroPerformanceStats()");
        if (mHeroStatsModelList != null) {
            mHeroNetworkFragmentInterface.onFetchHeroPerformanceStatsListSuccessCallback(mHeroStatsModelList);
        } else {
            fetchHeroPerformanceStatsFromBackEnd();
        }
    }

    /**
     * Fetch hero performance stats from the backend.
     */
    public void fetchHeroPerformanceStatsFromBackEnd() {
        Log.i(TAG, "fetchHeroPerformanceStatsFromBackEnd()");
        Retrofit retrofit = ApiClient.getClient(StatDotaApiConstants.STAT_DOTA_BASE_URL);
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        if (mHeroStatsListCall != null) {
            mHeroStatsListCall.cancel();
        }
        mHeroStatsListCall = apiInterface.getHeroStats();
        mHeroStatsListCall.enqueue(new Callback<List<HeroStatsModel>>() {

            @Override
            public void onResponse(Call<List<HeroStatsModel>> call, Response<List<HeroStatsModel>> response) {
                if (response.isSuccessful()) {
                    mHeroStatsModelList = response.body();
                    mHeroNetworkFragmentInterface.onFetchHeroPerformanceStatsListSuccessCallback(mHeroStatsModelList);
                }
            }

            @Override
            public void onFailure(Call<List<HeroStatsModel>> call, Throwable t) {
                if (! call.isCanceled()) {
                    mHeroStatsModelList = null;
                    mHeroNetworkFragmentInterface.onFetchHeroPerformanceStatsListFailureCallback(true);
                } else {
                    mHeroStatsModelList = null;
                    mHeroNetworkFragmentInterface.onFetchHeroPerformanceStatsListFailureCallback(false);
                }
            }
        });
    }

    public interface HeroNetworkFragmentInterface {

        void onFetchHeroPerformanceStatsListSuccessCallback(List<HeroStatsModel> heroStatsModels);

        void onFetchHeroPerformanceStatsListFailureCallback(boolean isCancelled);
    }
}
