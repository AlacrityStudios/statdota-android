package com.alacritystudios.statdota.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.components.HeroStatsMarkerView;
import com.alacritystudios.statdota.models.HeroStatsModel;
import com.alacritystudios.statdota.utils.ApiUtils;
import com.alacritystudios.statdota.utils.ApplicationUtils;
import com.bumptech.glide.Glide;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.IMarker;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * A Dialog Fragment implementation to show hero details from any other fragment/ activity.
 */

public class HeroStatsDialogFragment extends DialogFragment{

    private ImageView mHeroIconImageView;
    private TextView mHeroNameTextView;
    private LinearLayout mHeroRole1LinearLayout;
    private LinearLayout mHeroRole2LinearLayout;
    private LinearLayout mHeroRole3LinearLayout;
    private TextView mHeroRole1TextView;
    private TextView mHeroRole2TextView;
    private TextView mHeroRole3TextView;
    private BarChart mHeroStatsBarChart;
    private TextView mPrimaryAttributeTextView;
    private TextView mAttackTypeTextView;
    private TextView mProjectileSpeedTextView;
    private TextView mCmEnabledTextView;
    private TextView mMovementSpeedTextView;
    private TextView mTurnRateTextView;
    private TextView mAttackRateTextView;
    private TextView mAttackRangeTextView;
    private TextView mCloseDialogTextView;

    private HeroStatsModel mHeroStatsModel;

    public void setmHeroStatsModel(HeroStatsModel mHeroStatsModel) {
        this.mHeroStatsModel = mHeroStatsModel;
    }

    public static HeroStatsDialogFragment getInstance(HeroStatsModel heroStatsModel) {

        HeroStatsDialogFragment heroStatsDialogFragment = new HeroStatsDialogFragment();
        heroStatsDialogFragment.setmHeroStatsModel(heroStatsModel);
        return heroStatsDialogFragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_hero_stats_details_item, container, false);
        mCloseDialogTextView = view.findViewById(R.id.close_dialog_button);
        mHeroIconImageView = (ImageView) view.findViewById(R.id.hero_image_icon_image_view);
        mHeroNameTextView = (TextView) view.findViewById(R.id.hero_name_text_view);
        mHeroRole1TextView = (TextView) view.findViewById(R.id.hero_role_1_text_view);
        mHeroRole2TextView = (TextView) view.findViewById(R.id.hero_role_2_text_view);
        mHeroRole3TextView = (TextView) view.findViewById(R.id.hero_role_3_text_view);
        mHeroRole1LinearLayout = (LinearLayout) view.findViewById(R.id.hero_role_1_linear_layout);
        mHeroRole2LinearLayout = (LinearLayout) view.findViewById(R.id.hero_role_2_linear_layout);
        mHeroRole3LinearLayout = (LinearLayout) view.findViewById(R.id.hero_role_3_linear_layout);
        mPrimaryAttributeTextView = (TextView) view.findViewById(R.id.primary_attr_text_view);
        mAttackTypeTextView = (TextView) view.findViewById(R.id.attack_type_text_view);
        mProjectileSpeedTextView = (TextView) view.findViewById(R.id.projectile_speed_text_view);
        mCmEnabledTextView = (TextView) view.findViewById(R.id.cm_enabled_text_view);
        mMovementSpeedTextView = (TextView) view.findViewById(R.id.movement_speed_text_view);
        mTurnRateTextView = (TextView) view.findViewById(R.id.turn_rate_text_view);
        mAttackRateTextView = (TextView) view.findViewById(R.id.attack_rate_text_view);
        mAttackRangeTextView = (TextView) view.findViewById(R.id.attack_range_text_view);
        mHeroStatsBarChart = (BarChart) view.findViewById(R.id.hero_stats_bar_chart);
        
        mHeroNameTextView.setText(ApplicationUtils.makePlaceholderString(mHeroStatsModel.getLocalizedName(), "-"));
        try {
            mHeroRole1TextView.setText(ApplicationUtils.makePlaceholderString(mHeroStatsModel.getRoles().get(0), "-"));
            mHeroRole1LinearLayout.setVisibility(View.VISIBLE);
        } catch (Exception ex) {
            mHeroRole1LinearLayout.setVisibility(View.GONE);
        }
        try {
            mHeroRole2TextView.setText(ApplicationUtils.makePlaceholderString(mHeroStatsModel.getRoles().get(1), "-"));
            mHeroRole2LinearLayout.setVisibility(View.VISIBLE);
        } catch (Exception ex) {
            mHeroRole2LinearLayout.setVisibility(View.GONE);
        }
        try {
            mHeroRole3TextView.setText(ApplicationUtils.makePlaceholderString(mHeroStatsModel.getRoles().get(2), "-"));
            mHeroRole3LinearLayout.setVisibility(View.VISIBLE);
        } catch (Exception ex) {
            mHeroRole3LinearLayout.setVisibility(View.GONE);
        }
        mPrimaryAttributeTextView.setText(ApplicationUtils.makePlaceholderString(mHeroStatsModel.getPrimaryAttr(), "-"));
        mAttackTypeTextView.setText(ApplicationUtils.makePlaceholderString(mHeroStatsModel.getAttackType(), "-"));
        mProjectileSpeedTextView.setText(ApplicationUtils.makePlaceholderFloatString(mHeroStatsModel.getProjectileSpeed() != null ? mHeroStatsModel.getProjectileSpeed().floatValue() : 0f, "-"));
        mCmEnabledTextView.setText(mHeroStatsModel.getCmEnabled() != null? String.valueOf(mHeroStatsModel.getCmEnabled()): String.valueOf(false));
        mMovementSpeedTextView.setText(ApplicationUtils.makePlaceholderFloatString(mHeroStatsModel.getMoveSpeed() != null ? mHeroStatsModel.getMoveSpeed().floatValue() : 0f, "-"));
        mTurnRateTextView.setText(ApplicationUtils.makePlaceholderFloatString(mHeroStatsModel.getTurnRate() != null ? mHeroStatsModel.getTurnRate().floatValue() : 0f, "-"));
        mAttackRateTextView.setText(ApplicationUtils.makePlaceholderFloatString(mHeroStatsModel.getAttackRate() != null ? mHeroStatsModel.getAttackRate().floatValue() : 0f, "-"));
        mAttackRangeTextView.setText(ApplicationUtils.makePlaceholderFloatString(mHeroStatsModel.getAttackRange() != null ? mHeroStatsModel.getAttackRange().floatValue() : 0f, "-"));
        Glide.with(getActivity().getApplicationContext())
                .load(ApiUtils.getHeroImageUrl(mHeroStatsModel.getName()))
                .centerCrop()
                .into(mHeroIconImageView);

        setChartData();
        mCloseDialogTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        return view;
    }

    /**
     * Sets the chart data to various charts
     */
    public void setChartData() {
        List<BarEntry> winRateEntries = new ArrayList<>();
        if(mHeroStatsModel.getProPick() != null && mHeroStatsModel.getProPick() != 0) {
            winRateEntries.add(new BarEntry(0f, (mHeroStatsModel.getProWin() != null? mHeroStatsModel.getProWin().floatValue() : 0f) * 100 / mHeroStatsModel.getProPick()));
        } else {
            winRateEntries.add(new BarEntry(0f, 0f));
        }
        if(mHeroStatsModel.get1000Pick() != null && mHeroStatsModel.get1000Pick() != 0) {
            winRateEntries.add(new BarEntry(1f, (mHeroStatsModel.get1000Win() != null? mHeroStatsModel.get1000Win().floatValue() : 0f) * 100 / mHeroStatsModel.get1000Pick()));
        } else {
            winRateEntries.add(new BarEntry(1f, 0f));
        }
        if(mHeroStatsModel.get2000Pick() != null && mHeroStatsModel.get2000Pick() != 0) {
            winRateEntries.add(new BarEntry(2f, (mHeroStatsModel.get2000Win() != null? mHeroStatsModel.get2000Win().floatValue() : 0f) * 100 / mHeroStatsModel.get2000Pick()));
        } else {
            winRateEntries.add(new BarEntry(2f, 0f));
        }
        if(mHeroStatsModel.get3000Pick() != null && mHeroStatsModel.get3000Pick() != 0) {
            winRateEntries.add(new BarEntry(3f, (mHeroStatsModel.get3000Win() != null? mHeroStatsModel.get3000Win().floatValue() : 0f) * 100 / mHeroStatsModel.get3000Pick()));
        } else {
            winRateEntries.add(new BarEntry(3f, 0f));
        }
        if(mHeroStatsModel.get4000Pick() != null && mHeroStatsModel.get4000Pick() != 0) {
            winRateEntries.add(new BarEntry(4f, (mHeroStatsModel.get4000Win() != null? mHeroStatsModel.get4000Win().floatValue() : 0f) * 100 / mHeroStatsModel.get4000Pick()));
        } else {
            winRateEntries.add(new BarEntry(4f, 0f));
        }
        if(mHeroStatsModel.get5000Pick() != null && mHeroStatsModel.get5000Pick() != 0) {
            winRateEntries.add(new BarEntry(5f, (mHeroStatsModel.get5000Win() != null? mHeroStatsModel.get5000Win().floatValue() : 0f) * 100 / mHeroStatsModel.get5000Pick()));
        } else {
            winRateEntries.add(new BarEntry(5f, 0f));
        }

        Description description = new Description();
        description.setText("");
        BarDataSet winRateBarDataSet = new BarDataSet(winRateEntries, "");
        winRateBarDataSet.setValueTextColor(ActivityCompat.getColor(getActivity(), R.color.colorBlackText));
        winRateBarDataSet.setValueTextSize(10);
        winRateBarDataSet.setValueTypeface(Typeface.create("sans-serif-light", 1));
        winRateBarDataSet.setValueFormatter(new IValueFormatter() {

            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                return String.format("%.01f", value) + "%";
            }
        });
        BarData winRateBarData = new BarData(winRateBarDataSet);
        XAxis xAxis = mHeroStatsBarChart.getXAxis();
        xAxis.setDrawGridLines(false);
        // xAxis.setAvoidFirstLastClipping(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawAxisLine(false);
        xAxis.setTextColor(ActivityCompat.getColor(getActivity(), R.color.colorBlackText));
        xAxis.setTextSize(10);
        xAxis.setTypeface(Typeface.create("sans-serif-light", 1));
        xAxis.setValueFormatter(new IAxisValueFormatter() {

            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                if(value == 0f) {
                    return getActivity().getString(R.string.pro).toUpperCase();
                } else if (value == 1f) {
                    return getActivity().getString(R.string._1_thousand_mmr).toUpperCase();
                } else if (value == 2f) {
                    return getActivity().getString(R.string._2_thousand_mmr).toUpperCase();
                } else if (value == 3f) {
                    return getActivity().getString(R.string._3_thousand_mmr).toUpperCase();
                } else if (value == 4f) {
                    return getActivity().getString(R.string._4_thousand_mmr).toUpperCase();
                } else if (value == 5f) {
                    return getActivity().getString(R.string._5_thousand_mmr).toUpperCase();
                } else {
                    return getActivity().getString(R.string._5_thousand_mmr).toUpperCase();
                }
            }
        });

        YAxis axisLeft = mHeroStatsBarChart.getAxisLeft();
        axisLeft.setAxisMaximum(100f);
        axisLeft.setAxisMinimum(0f);
        axisLeft.setDrawGridLines(false);
        axisLeft.setDrawAxisLine(false);
        axisLeft.setTextColor(ActivityCompat.getColor(getActivity(), R.color.colorBlackText));
        axisLeft.setTextSize(10);
        axisLeft.setTypeface(Typeface.create("sans-serif-light", 1));
        YAxis axisRight = mHeroStatsBarChart.getAxisRight();
        axisRight.setEnabled(false);
        IMarker marker = new HeroStatsMarkerView(getActivity(), R.layout.layout_hero_stats_marker_view, mHeroStatsModel);
        mHeroStatsBarChart.setMarker(marker);
        mHeroStatsBarChart.getLegend().setEnabled(false);
        mHeroStatsBarChart.setDescription(description);
        mHeroStatsBarChart.setData(winRateBarData);
        mHeroStatsBarChart.setPinchZoom(false);
        mHeroStatsBarChart.disableScroll();
        mHeroStatsBarChart.setScaleEnabled(false);
        mHeroStatsBarChart.setDoubleTapToZoomEnabled(false);
        //mHeroStatsBarChart.setTouchEnabled(false);
        mHeroStatsBarChart.animateXY(1000, 1000);
    }
}
