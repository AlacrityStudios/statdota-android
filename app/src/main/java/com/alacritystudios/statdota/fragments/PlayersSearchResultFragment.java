package com.alacritystudios.statdota.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.adapters.PlayerSearchResultAdapter;
import com.alacritystudios.statdota.fragments.base.BaseFragment;
import com.alacritystudios.statdota.models.PlayerSearchResultModel;
import com.alacritystudios.statdota.utils.LoggerUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment to display the players search results.
 */

public class PlayersSearchResultFragment extends BaseFragment implements BaseFragment.BaseFragmentImpl {

    private RecyclerView mHeroStatsRecyclerView;
    private SwipeRefreshLayout mPlayerSearchResultsSwipeRefreshLayout;
    private LinearLayout mPlayerSearchResultsPlaceholderLayout;
    private TextView mPlayerSearchResultsPlaceholderMessage;

    private List<PlayerSearchResultModel> mPlayerSearchResultModelList;
    private LinearLayoutManager mPlayersLinearLayoutManager;
    private PlayerSearchResultAdapter mPlayerSearchResultAdapter;


    public static PlayersSearchResultFragment newInstance() {
        PlayersSearchResultFragment playersSearchResultFragment = new PlayersSearchResultFragment();
        Bundle bundle = new Bundle();
        playersSearchResultFragment.setArguments(bundle);
        return playersSearchResultFragment;
    }

    @Override
    public void initialiseUI() {
        mHeroStatsRecyclerView = mFragmentView.findViewById(R.id.recycler_view_players_search_results);
        mPlayerSearchResultsPlaceholderLayout = mFragmentView.findViewById(R.id.players_search_results_placeholder_layout);
        mPlayerSearchResultsPlaceholderMessage = mFragmentView.findViewById(R.id.players_search_results_placeholder_error_message);
        mHeroStatsRecyclerView.setLayoutManager(mPlayersLinearLayoutManager);
        mHeroStatsRecyclerView.setAdapter(mPlayerSearchResultAdapter);
        mPlayerSearchResultsPlaceholderLayout.setVisibility(View.GONE);
    }

    @Override
    public void setListeners() {

    }

    @Override
    public void setContextAndTags() {
        mContext = getActivity();
        TAG = LoggerUtil.makeLogTag(getClass());
    }

    @Override
    public void initialiseOtherComponents() {
        mPlayerSearchResultModelList = new ArrayList<>();
        mPlayerSearchResultAdapter = new PlayerSearchResultAdapter(mPlayerSearchResultModelList, mContext);
        mPlayersLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_players_search_results, container, false);
        setContextAndTags();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
        return mFragmentView;
    }

    /**
     * Called when data is available to be shown.
     */
    public void onDataFetchSuccess(List<PlayerSearchResultModel> playerSearchResultModelList) {
        Log.i(TAG, "onDataFetchSuccess()");
        mPlayerSearchResultModelList = playerSearchResultModelList;
        mPlayerSearchResultAdapter.setmPlayerSearchResultModelList(mPlayerSearchResultModelList);
        mPlayerSearchResultAdapter.notifyDataSetChanged();
        if (mPlayerSearchResultModelList.size() == 0) {
            mPlayerSearchResultsPlaceholderMessage.setText(mContext.getString(R.string.no_records_to_be_shown));
            mHeroStatsRecyclerView.setVisibility(View.GONE);
            mPlayerSearchResultsPlaceholderLayout.setVisibility(View.VISIBLE);
        } else {
            mHeroStatsRecyclerView.setVisibility(View.VISIBLE);
            mPlayerSearchResultsPlaceholderLayout.setVisibility(View.GONE);
        }
    }

    /**
     * Called when data is not available to be shown.
     */
    public void onDataFetchFailure(boolean isCancelled) {
        Log.i(TAG, "onDataFetchFailure()");
        mPlayerSearchResultModelList = new ArrayList<>();
        mPlayerSearchResultAdapter.setmPlayerSearchResultModelList(mPlayerSearchResultModelList);
        mPlayerSearchResultAdapter.notifyDataSetChanged();
        if (! isCancelled) {
            mPlayerSearchResultsPlaceholderMessage.setText(mContext.getString(R.string.fragment_search_error_message));
            mHeroStatsRecyclerView.setVisibility(View.GONE);
            mPlayerSearchResultsPlaceholderLayout.setVisibility(View.VISIBLE);
        }
    }
}
