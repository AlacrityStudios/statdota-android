package com.alacritystudios.statdota.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.adapters.RecentPubMatchesListAdapter;
import com.alacritystudios.statdota.fragments.base.BaseFragment;
import com.alacritystudios.statdota.models.HeroDetails;
import com.alacritystudios.statdota.models.PubMatchesModel;
import com.alacritystudios.statdota.sql.StatDotaDatabaseHelper;
import com.alacritystudios.statdota.utils.ComponentUtil;
import com.alacritystudios.statdota.utils.LoggerUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment to display all the recently completed high MMR matches.
 */

public class RecentPubMatchesFragment extends BaseFragment implements BaseFragment.BaseFragmentImpl {

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayout mPlaceholderLayout;
    private LinearLayout mErrorLayout;
    private RecyclerView mRecentPubMatchListRecyclerView;
    private RecentPubMatchesListAdapter mRecentPubMatchesListAdapter;

    private LinearLayoutManager mRecentPubMatchListLinearLayoutManager;
    private List<PubMatchesModel> mPubMatchesModelList;

    private int mPageNumber = 0;
    private boolean mLoading = true;

    private RecentPubMatchesFragment.RecentPubMatchesFragmentInterface mRecentPubMatchesFragmentInterface;

    public static RecentPubMatchesFragment newInstance() {
        RecentPubMatchesFragment recentPubMatchesFragment = new RecentPubMatchesFragment();
        Bundle bundle = new Bundle();
        recentPubMatchesFragment.setArguments(bundle);
        return recentPubMatchesFragment;
    }

    @Override
    public void initialiseUI() {
        mRecentPubMatchListRecyclerView = (RecyclerView) mFragmentView.findViewById(R.id.recycler_view_recent_pub_matches);
        mSwipeRefreshLayout = (SwipeRefreshLayout) mFragmentView.findViewById(R.id.recent_pub_matches_swipe_refresh_layout);
        mPlaceholderLayout = (LinearLayout) mFragmentView.findViewById(R.id.recent_pub_matches_fragment_placeholder_layout);
        mErrorLayout = (LinearLayout) mFragmentView.findViewById(R.id.recent_pub_matches_fragment_error_layout);
        setupMatchesRecyclerView();
        setupSwipeRefreshLayout();
        mSwipeRefreshLayout.setRefreshing(true);
        mRecentPubMatchesFragmentInterface.fetchRecentPubMatches(20, mPageNumber * 20);
    }

    @Override
    public void setListeners() {
        mRecentPubMatchListRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (!recyclerView.canScrollVertically(1) && dy != 0) {
                    mPageNumber++;
                    mSwipeRefreshLayout.setRefreshing(true);
                    mRecentPubMatchesFragmentInterface.fetchRecentPubMatches(20, mPageNumber * 20);
                    mLoading = true;
                }
            }
        });
    }

    @Override
    public void setContextAndTags() {
        mContext = getActivity();
        TAG = LoggerUtil.makeLogTag(getClass());
    }

    @Override
    public void initialiseOtherComponents() {
        mPageNumber = 0;
        mPubMatchesModelList = new ArrayList<>();
        mRecentPubMatchesListAdapter = new RecentPubMatchesListAdapter(mContext, mPubMatchesModelList);
        mRecentPubMatchListLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_recent_pub_matches, container, false);
        setContextAndTags();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
        return mFragmentView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof RecentPubMatchesFragment.RecentPubMatchesFragmentInterface) {
            mRecentPubMatchesFragmentInterface = (RecentPubMatchesFragment.RecentPubMatchesFragmentInterface) context;
        } else {
            throw new ClassCastException("LandingActivity must implement RecentPubMatchesFragmentInterface.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mRecentPubMatchesFragmentInterface = null;
    }

    /**
     * Setup the matches recycler view.
     */
    private void setupMatchesRecyclerView() {
        Log.i(TAG, "setupMatchesRecyclerView()");
        mRecentPubMatchListRecyclerView.setLayoutManager(mRecentPubMatchListLinearLayoutManager);
        mRecentPubMatchListRecyclerView.setAdapter(mRecentPubMatchesListAdapter);
    }

    /**
     * Setup the swipe refresh layout behaviour.
     */
    private void setupSwipeRefreshLayout() {
        Log.i(TAG, "setupSwipeRefreshLayout()");
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                mRecentPubMatchesListAdapter.getmPubMatchesModelList().clear();
                mPageNumber = 0;
                mSwipeRefreshLayout.setRefreshing(true);
                mRecentPubMatchesFragmentInterface.fetchRecentPubMatches(20, mPageNumber * 20);
            }
        });
    }

    /**
     * To be called when the query succeeds.
     *
     * @param pubMatchesModelList - The resulting list to be shown.
     */
    public void onRecentPubMatchesQuerySuccess(List<PubMatchesModel> pubMatchesModelList) {
        Log.i(TAG, "onRecentPubMatchesQuerySuccess()");
        new PopulateHeroDetailsInPubMatches().execute(pubMatchesModelList);
        if (pubMatchesModelList.size() == 0) {
            mRecentPubMatchListRecyclerView.setVisibility(View.GONE);
            mErrorLayout.setVisibility(View.GONE);
            mPlaceholderLayout.setVisibility(View.VISIBLE);
        } else {
            mRecentPubMatchListRecyclerView.setVisibility(View.VISIBLE);
            mErrorLayout.setVisibility(View.GONE);
            mPlaceholderLayout.setVisibility(View.GONE);
        }
        mLoading = false;
        mSwipeRefreshLayout.setRefreshing(false);
    }

    /**
     * To be called when the query fails.
     */
    public void onRecentPubMatchesQueryFailure(boolean isCancelled) {
        Log.i(TAG, "onRecentPubMatchesQueryFailure()");
        if (!isCancelled && mRecentPubMatchesListAdapter.getmPubMatchesModelList().size() != 0) {
            ComponentUtil.createAndShowSnackBar(mContext, mSwipeRefreshLayout, mContext.getString(R.string.fragment_matches_error_message));
            mRecentPubMatchListRecyclerView.setVisibility(View.VISIBLE);
            mErrorLayout.setVisibility(View.GONE);
            mPlaceholderLayout.setVisibility(View.GONE);
            mSwipeRefreshLayout.setRefreshing(false);
        } else if (!isCancelled && mRecentPubMatchesListAdapter.getmPubMatchesModelList().size() == 0) {
            mRecentPubMatchListRecyclerView.setVisibility(View.GONE);
            mErrorLayout.setVisibility(View.VISIBLE);
            mPlaceholderLayout.setVisibility(View.GONE);
            mSwipeRefreshLayout.setRefreshing(false);
        }
        mPageNumber--;
        mLoading = false;
    }

    public class PopulateHeroDetailsInPubMatches extends AsyncTask<List<PubMatchesModel>, Void, List<PubMatchesModel>> {
        @Override
        protected void onPostExecute(List<PubMatchesModel> pubMatchesModels) {
            mRecentPubMatchesListAdapter.getmPubMatchesModelList().addAll(pubMatchesModels);
            mRecentPubMatchesListAdapter.notifyDataSetChanged();
        }

        @Override
        protected List<PubMatchesModel> doInBackground(List<PubMatchesModel>[] lists) {
            StatDotaDatabaseHelper statDotaDatabaseHelper = StatDotaDatabaseHelper.getInstance(mContext);
            for (PubMatchesModel pubMatchesModel: lists[0]) {
                pubMatchesModel.setmRadiantHeroDetails(new ArrayList<HeroDetails>());
                pubMatchesModel.setmDireHeroDetails(new ArrayList<HeroDetails>());
                String[] radiantTeamHeroes = pubMatchesModel.getRadiantTeam().split(",");
                String[] direTeamHeroes = pubMatchesModel.getDireTeam().split(",");
                for(int i = 0; i < radiantTeamHeroes.length; i++) {
                    pubMatchesModel.getmRadiantHeroDetails().add(statDotaDatabaseHelper.fetchHeroDetails(Long.parseLong(radiantTeamHeroes[i])));
                    pubMatchesModel.getmDireHeroDetails().add(statDotaDatabaseHelper.fetchHeroDetails(Long.parseLong(direTeamHeroes[i])));
                }
            }
            return lists[0];
        }
    }

    public interface RecentPubMatchesFragmentInterface {

        void fetchRecentPubMatches(int limit, int offset);

    }
}












