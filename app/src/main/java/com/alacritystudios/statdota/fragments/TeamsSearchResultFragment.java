package com.alacritystudios.statdota.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.fragments.base.BaseFragment;

/**
 * Fragment to display the teams search results.
 */

public class TeamsSearchResultFragment extends BaseFragment implements BaseFragment.BaseFragmentImpl {


    public static TeamsSearchResultFragment newInstance() {
        TeamsSearchResultFragment teamsSearchResultFragment = new TeamsSearchResultFragment();
        Bundle bundle = new Bundle();
        teamsSearchResultFragment.setArguments(bundle);
        return teamsSearchResultFragment;
    }

    @Override
    public void initialiseUI() {

    }

    @Override
    public void setListeners() {

    }

    @Override
    public void setContextAndTags() {

    }

    @Override
    public void initialiseOtherComponents() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_teams_search_results, container, false);
        setContextAndTags();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
        return mFragmentView;
    }
}
