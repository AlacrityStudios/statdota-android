package com.alacritystudios.statdota.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.adapters.HighMmrPubStarsListAdapter;
import com.alacritystudios.statdota.fragments.base.BaseFragment;
import com.alacritystudios.statdota.models.HighMmrPubStarsModel;
import com.alacritystudios.statdota.utils.ComponentUtil;
import com.alacritystudios.statdota.utils.LoggerUtil;
import com.lapism.searchview.SearchEditText;
import com.lapism.searchview.SearchView;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment to display all high mmr players.
 */

public class HighMmrPubStarsListFragment extends BaseFragment implements BaseFragment.BaseFragmentImpl {
    
    private Toolbar mHighMmrPubStarsListFragmentToolbar;
    private SearchView mHighMmrPubStarsListFragmentSearchView;
    private SwipeRefreshLayout mHighMmrPubStarsListFragmentSwipeRefreshLayout;
    private LinearLayout mHighMmrPubStarsListFragmentErrorLayout;
    private LinearLayout mHighMmrPubStarsListFragmentPlaceholderLayout;
    private RecyclerView mHighMmrPubStarsListRecyclerView;

    private Drawable mPlaceholderDrawable;
    private LinearLayoutManager mHighMmrPubStarsListLinearLayoutManager;
    private HighMmrPubStarsListAdapter mHighMmrPubStarsListAdapter;
    private HighMmrPubStarsListFragment.HighMmrPubStarsListFragmentInterface mHighMmrPubStarsListFragmentInterface;

    private int mPageNumber = 0;
    private int mPreviousTotal = 0;
    private boolean mLoading = true;
    private int mVisibleThreshold = 19;
    private int mVisibleItemCount;
    private int mTotalItemCount;
    private int mFirstVisibleItem;

    public static HighMmrPubStarsListFragment getInstance() {
        HighMmrPubStarsListFragment highMmrPubStarsListFragment = new HighMmrPubStarsListFragment();
        Bundle bundle = new Bundle();
        highMmrPubStarsListFragment.setArguments(bundle);
        return highMmrPubStarsListFragment;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.high_mmr_pub_stars_list_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                ((Activity) mContext).onBackPressed();
                break;
            case R.id.menu_search:
                mHighMmrPubStarsListFragmentSearchView.open(true);

        }
        return true;
    }

    @Override
    public void initialiseUI() {
        setHasOptionsMenu(true);
        mHighMmrPubStarsListFragmentToolbar = mFragmentView.findViewById(R.id.high_mmr_pub_stars_list_fragment_toolbar);
        mHighMmrPubStarsListFragmentSearchView = mFragmentView.findViewById(R.id.high_mmr_pub_stars_list_fragment_search_view);
        mHighMmrPubStarsListRecyclerView = mFragmentView.findViewById(R.id.high_mmr_pub_stars_list_recycler_view);
        mHighMmrPubStarsListFragmentSwipeRefreshLayout = mFragmentView.findViewById(R.id.high_mmr_pub_stars_list_fragment_swipe_refresh_layout);
        mHighMmrPubStarsListFragmentPlaceholderLayout = mFragmentView.findViewById(R.id.high_mmr_pub_stars_list_fragment_placeholder_layout);
        mHighMmrPubStarsListFragmentErrorLayout = mFragmentView.findViewById(R.id.high_mmr_pub_stars_list_fragment_error_layout);
        setupToolbar();
        setupHighMmrPubStarsListRecyclerView();
        mHighMmrPubStarsListFragmentSwipeRefreshLayout.setRefreshing(true);
        mHighMmrPubStarsListFragmentInterface.onFetchHighMmrPubStarsList(mPageNumber * 20, 20, "");
    }

    @Override
    public void setListeners() {
        mHighMmrPubStarsListFragmentSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mPageNumber = 0;
                mHighMmrPubStarsListAdapter.getmHighMmrPubStarsModelList().clear();
                mHighMmrPubStarsListFragmentInterface.onFetchHighMmrPubStarsList(mPageNumber * 20, 20, newText);
                return false;
            }
        });
        mHighMmrPubStarsListFragmentSearchView.setOnOpenCloseListener(new SearchView.OnOpenCloseListener() {

            @Override
            public boolean onClose() {
                mHighMmrPubStarsListFragmentToolbar.setVisibility(View.VISIBLE);
                return false;
            }

            @Override
            public boolean onOpen() {
                mHighMmrPubStarsListFragmentToolbar.setVisibility(View.GONE);
                return false;
            }
        });
        mHighMmrPubStarsListFragmentSearchView.setOnMenuClickListener(new SearchView.OnMenuClickListener() {

            @Override
            public void onMenuClick() {
                Log.i(TAG, "onMenuClick()");
            }
        });
        mHighMmrPubStarsListRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!recyclerView.canScrollVertically(1) && dy != 0) {
                    mPageNumber++;
                    mHighMmrPubStarsListFragmentSwipeRefreshLayout.setRefreshing(true);
                    mHighMmrPubStarsListFragmentInterface.onFetchHighMmrPubStarsList(mPageNumber * 20, 20, "");
                    mLoading = true;
                }
            }
        });

        mHighMmrPubStarsListFragmentSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                mPageNumber = 0;
                mHighMmrPubStarsListAdapter.getmHighMmrPubStarsModelList().clear();
                mHighMmrPubStarsListFragmentInterface.onFetchHighMmrPubStarsList(mPageNumber * 20, 20, "");
            }
        });
    }

    @Override
    public void setContextAndTags() {
        TAG = LoggerUtil.makeLogTag(getClass());
        mContext = getActivity();
    }

    @Override
    public void initialiseOtherComponents() {
        mPageNumber = 0;
        mPreviousTotal = 0;
        mPlaceholderDrawable = ActivityCompat.getDrawable(mContext, R.drawable.ic_dota);
        DrawableCompat.wrap(mPlaceholderDrawable).setTint(ActivityCompat.getColor(mContext, R.color.colorDefaultMedium));
        mHighMmrPubStarsListAdapter = new HighMmrPubStarsListAdapter(new ArrayList<HighMmrPubStarsModel>(), mContext, mPlaceholderDrawable);
        mHighMmrPubStarsListLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_high_mmr_pub_stars_list, container, false);
        setContextAndTags();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
        return mFragmentView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof HighMmrPubStarsListFragment.HighMmrPubStarsListFragmentInterface) {
            mHighMmrPubStarsListFragmentInterface = (HighMmrPubStarsListFragment.HighMmrPubStarsListFragmentInterface) context;
        } else {
            throw new ClassCastException("PlayerActivity must implement HighMmrPubStarsListFragmentInterface.");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHighMmrPubStarsListFragmentInterface = null;
    }

    /**
     * Sets up the toolbar for the fragment.
     */
    private void setupToolbar() {
        Log.i(TAG, "setupToolbar()");
        mHighMmrPubStarsListFragmentToolbar.setTitleTextAppearance(mContext, R.style.ToolbarTitleAppearance);
        mHighMmrPubStarsListFragmentToolbar.setTitle(R.string.high_mmr_stars);
        mHighMmrPubStarsListFragmentToolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                ((Activity) mContext).onBackPressed();
            }
        });
        ((AppCompatActivity) mContext).setSupportActionBar(mHighMmrPubStarsListFragmentToolbar);
        ((AppCompatActivity) mContext).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) mContext).getSupportActionBar().setTitle(R.string.high_mmr_stars);
        LinearLayout linearLayout = mHighMmrPubStarsListFragmentSearchView.findViewById(com.lapism.searchview.R.id.linearLayout);
        ViewGroup.LayoutParams params = linearLayout.getLayoutParams();
        params.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 48, getResources().getDisplayMetrics());
        params.width = LinearLayout.LayoutParams.MATCH_PARENT;
        linearLayout.setLayoutParams(params);
        ImageView imageView =  mHighMmrPubStarsListFragmentSearchView.findViewById(com.lapism.searchview.R.id.imageView_arrow_back);
        //imageView.setVisibility(View.GONE);
        mHighMmrPubStarsListFragmentSearchView.findViewById(com.lapism.searchview.R.id.view_divider).setVisibility(View.GONE);
        SearchEditText searchEditText = mHighMmrPubStarsListFragmentSearchView.findViewById(com.lapism.searchview.R.id.searchEditText_input);
        searchEditText.setPadding(0,0,0,0);

        CardView.LayoutParams cardViewParams = new CardView.LayoutParams(
                CardView.LayoutParams.MATCH_PARENT,
                CardView.LayoutParams.WRAP_CONTENT
        );
        cardViewParams.setMargins(0, 0, 0, 0);
        mHighMmrPubStarsListFragmentSearchView.findViewById(com.lapism.searchview.R.id.cardView).setLayoutParams(cardViewParams);
        mHighMmrPubStarsListFragmentSearchView.setArrowOnly(true);
    }

    /**
     * Setup the highMmrPubStars list recycler view.
     */
    private void setupHighMmrPubStarsListRecyclerView() {
        Log.i(TAG, "setupHighMmrPubStarsListRecyclerView()");
        mHighMmrPubStarsListRecyclerView.setLayoutManager(mHighMmrPubStarsListLinearLayoutManager);
        mHighMmrPubStarsListRecyclerView.setAdapter(mHighMmrPubStarsListAdapter);
    }

    /**
     * To be called when the query succeeds.
     * @param highMmrPubStarsModelList - The resulting list to be shown.
     */
    public void onHighMmrPubStarsQuerySuccess(List<HighMmrPubStarsModel> highMmrPubStarsModelList) {
        Log.i(TAG, "onHighMmrPubStarsQuerySuccess() Fetched " + highMmrPubStarsModelList.size() + " items");
        mHighMmrPubStarsListAdapter.getmHighMmrPubStarsModelList().addAll(highMmrPubStarsModelList);
        mHighMmrPubStarsListAdapter.notifyDataSetChanged();
        if (mHighMmrPubStarsListAdapter.getmHighMmrPubStarsModelList().size() == 0) {
            mHighMmrPubStarsListRecyclerView.setVisibility(View.GONE);
            mHighMmrPubStarsListFragmentErrorLayout.setVisibility(View.GONE);
            mHighMmrPubStarsListFragmentPlaceholderLayout.setVisibility(View.VISIBLE);
        } else {
            mHighMmrPubStarsListRecyclerView.setVisibility(View.VISIBLE);
            mHighMmrPubStarsListFragmentErrorLayout.setVisibility(View.GONE);
            mHighMmrPubStarsListFragmentPlaceholderLayout.setVisibility(View.GONE);
        }
        mLoading = false;
        mHighMmrPubStarsListFragmentSwipeRefreshLayout.setRefreshing(false);
    }

    /**
     * To be called when the query fails.
     */
    public void onHighMmrPubStarsQueryFailure(boolean isCancelled) {
        Log.i(TAG, "onHighMmrPubStarsQueryFailure() isCancelled: " + isCancelled);
        if(!isCancelled && mHighMmrPubStarsListAdapter.getmHighMmrPubStarsModelList().size() != 0) {
            ComponentUtil.createAndShowSnackBar(mContext, mHighMmrPubStarsListFragmentSwipeRefreshLayout, mContext.getString(R.string.fragment_high_mmr_players_error_message));
            mHighMmrPubStarsListRecyclerView.setVisibility(View.VISIBLE);
            mHighMmrPubStarsListFragmentErrorLayout.setVisibility(View.GONE);
            mHighMmrPubStarsListFragmentPlaceholderLayout.setVisibility(View.GONE);
            mHighMmrPubStarsListFragmentSwipeRefreshLayout.setRefreshing(false);
        } else if (!isCancelled && mHighMmrPubStarsListAdapter.getmHighMmrPubStarsModelList().size() == 0){
            mHighMmrPubStarsListRecyclerView.setVisibility(View.GONE);
            mHighMmrPubStarsListFragmentErrorLayout.setVisibility(View.VISIBLE);
            mHighMmrPubStarsListFragmentPlaceholderLayout.setVisibility(View.GONE);
            mHighMmrPubStarsListFragmentSwipeRefreshLayout.setRefreshing(false);
        }
        mPageNumber--;
        mLoading = false;
    }

    public interface HighMmrPubStarsListFragmentInterface {

        void onFetchHighMmrPubStarsList(int offset, int count, String query);
    }
}
