package com.alacritystudios.statdota.interfaces;

/**
 * Listener to listen to filter item removal and update the view.
 */

public interface FilterItemRemovalListener {

    void removeItemAt(int position);
}
