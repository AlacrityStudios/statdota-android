package com.alacritystudios.statdota.interfaces;

/**
 * Listener to listen to item clicks on twitch live streams.
 */

public interface TwitchLiveStreamsItemClickListener {
    void onItemClick(String streamUrl);
}
