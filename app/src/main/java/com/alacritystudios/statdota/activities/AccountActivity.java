package com.alacritystudios.statdota.activities;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.activities.base.BaseActivity;
import com.alacritystudios.statdota.adapters.ViewPagerAdapter;
import com.alacritystudios.statdota.constants.BundleConstants;
import com.alacritystudios.statdota.constants.FragmentTagConstants;
import com.alacritystudios.statdota.constants.SteamWebApiConstants;
import com.alacritystudios.statdota.fragments.UserFriendsFragment;
import com.alacritystudios.statdota.fragments.UserHeroesFragment;
import com.alacritystudios.statdota.fragments.UserMatchesFragment;
import com.alacritystudios.statdota.fragments.UserStatsFragment;
import com.alacritystudios.statdota.fragments.headless.AccountActivityHeadlessFragment;
import com.alacritystudios.statdota.fragments.headless.AccountActivityHeadlessFragment;
import com.alacritystudios.statdota.models.MatchFilterResultModel;
import com.alacritystudios.statdota.models.PubMatchesModel;
import com.alacritystudios.statdota.models.RecentLeagueMatchModel;
import com.alacritystudios.statdota.models.TeamDetailsModel;
import com.alacritystudios.statdota.models.TwitchStreamsModel;
import com.alacritystudios.statdota.models.UserFriendsModel;
import com.alacritystudios.statdota.models.UserHeroSummaryModel;
import com.alacritystudios.statdota.models.UserMatchSummaryModel;
import com.alacritystudios.statdota.models.UserStatsModel;
import com.alacritystudios.statdota.utils.LoggerUtil;
import com.alacritystudios.statdota.utils.PreferenceUtil;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import java.util.List;

public class AccountActivity extends BaseActivity implements BaseActivity.BaseActivityImpl, AccountActivityHeadlessFragment.AccountActivityHeadlessFragmentInterface, UserMatchesFragment.UserMatchesFragmentInterface, UserHeroesFragment.UserHeroesFragmentInterface, UserFriendsFragment.UserFriendsFragmentInterface {

    private Toolbar mToolbar;
    private SmartTabLayout mAccountActivitySmartTabLayout;
    private ViewPager mAccountActivityViewPager;

    private ViewPagerAdapter mViewPagerAdapter;
    private UserMatchesFragment mUserMatchesFragment;
    private UserStatsFragment mUserStatsFragment;
    private UserHeroesFragment mUserHeroesFragment;
    private UserFriendsFragment mUserFriendsFragment;

    private long mAccountId;

    private AccountActivityHeadlessFragment mAccountActivityHeadlessFragment;

    @Override
    public void initialiseUI() {
        mToolbar = (Toolbar) findViewById(R.id.account_activity_toolbar);
        mAccountActivitySmartTabLayout = (SmartTabLayout) findViewById(R.id.account_activity_smart_tab_layout);
        mAccountActivityViewPager = (ViewPager) findViewById(R.id.account_activity_view_pager);
        setupToolbar();
        setupAccountActivityHeadlessFragment();
        setupViewPager(mAccountId);
    }

    @Override
    public void setListeners() {
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void setContextAndTags() {
        TAG = LoggerUtil.makeLogTag(getClass());
        mContext = AccountActivity.this;
    }

    @Override
    public void initialiseOtherComponents() {
        mAccountId = getIntent().getLongExtra(BundleConstants.ACCOUNT_ID, 0L);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        setContextAndTags();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
    }

    @Override
    public void onUserMatchesQuerySuccessCallback(List<UserMatchSummaryModel> userMatchSummaryModelList) {
        mUserMatchesFragment.onUserMatchesQuerySuccess(userMatchSummaryModelList);
    }

    @Override
    public void onUserMatchesQueryFailureCallback(boolean isCancelled) {
        mUserMatchesFragment.onUserMatchesQueryFailure(isCancelled);
    }

    @Override
    public void fetchUserMatches(String accountId, int limit, int offset, MatchFilterResultModel matchFilterResultModel) {
        mAccountActivityHeadlessFragment.fetchUserMatchesFromBackend(accountId, limit, offset, matchFilterResultModel);
    }

    @Override
    public void onUserStatsQuerySuccessCallback(UserStatsModel userStatsModel) {

    }

    @Override
    public void onUserStatsQueryFailureCallback(boolean isCancelled) {

    }

    @Override
    public void fetchHeroFragmentStats(String accountId) {
        mAccountActivityHeadlessFragment.fetchUserHeroes(accountId);
    }

    @Override
    public void refreshHeroFragmentStats(String accountId) {
        mAccountActivityHeadlessFragment.fetchUserHeroesFromBackend(accountId);
    }

    @Override
    public void onUserHeroesQuerySuccessCallback(List<UserHeroSummaryModel> userHeroSummaryModelList) {
        mUserHeroesFragment.onUserHeroesQuerySuccess(userHeroSummaryModelList);
    }

    @Override
    public void onUserHeroesQueryFailureCallback(boolean isCancelled) {
        mUserHeroesFragment.onUserHeroesQueryFailure(isCancelled);
    }

    @Override
    public void fetchFriendFragmentStats(String accountId) {
        mAccountActivityHeadlessFragment.fetchUserFriends(accountId);
    }

    @Override
    public void refreshFriendFragmentStats(String accountId) {
        mAccountActivityHeadlessFragment.fetchUserFriendsFromBackend(accountId);
    }

    @Override
    public void onUserFriendsQuerySuccessCallback(List<UserFriendsModel> userFriendsModelList) {
        mUserFriendsFragment.onUserFriendsQuerySuccess(userFriendsModelList);
    }

    @Override
    public void onUserFriendsQueryFailureCallback(boolean isCancelled) {
        mUserFriendsFragment.onUserFriendsQueryFailure(isCancelled);
    }

    /**
     * Sets up the toolbar for the activity.
     */
    private void setupToolbar() {
        Log.i(TAG, "setupToolbar()");
        mToolbar.setTitleTextAppearance(mContext, R.style.ToolbarTitleAppearance);
        ((AppCompatActivity) mContext).setSupportActionBar(mToolbar);
        ((AppCompatActivity) mContext).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /**
     * Setup the hero network fragment.
     */
    private void setupAccountActivityHeadlessFragment() {
        Log.i(TAG, "setupAccountActivityHeadlessFragment()");
        mAccountActivityHeadlessFragment = (AccountActivityHeadlessFragment) getSupportFragmentManager().findFragmentByTag(FragmentTagConstants.ACCOUNT_ACTIVITY_NETWORK_FRAGMENT);

        if (mAccountActivityHeadlessFragment == null) {
            mAccountActivityHeadlessFragment = AccountActivityHeadlessFragment.getInstance();
            getSupportFragmentManager().beginTransaction().add(mAccountActivityHeadlessFragment, FragmentTagConstants.ACCOUNT_ACTIVITY_NETWORK_FRAGMENT).commit();
        }
    }

    /**
     * Sets up the view pager.
     */
    private void setupViewPager(long accountId) {
        Log.i(TAG, "setupViewPager()");
        mViewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        mUserStatsFragment = UserStatsFragment.newInstance(accountId, false);
        mUserMatchesFragment = UserMatchesFragment.newInstance(accountId, false);
        mUserHeroesFragment = UserHeroesFragment.newInstance(accountId, false);
        mUserFriendsFragment = UserFriendsFragment.newInstance(accountId, false);
        mViewPagerAdapter.addFragment(mUserStatsFragment, mContext.getString(R.string.stats));
        mViewPagerAdapter.addFragment(mUserMatchesFragment, mContext.getString(R.string.matches));
        mViewPagerAdapter.addFragment(mUserHeroesFragment, mContext.getString(R.string.heroes));
        mViewPagerAdapter.addFragment(mUserFriendsFragment, mContext.getString(R.string.friends));
        mAccountActivityViewPager.setOffscreenPageLimit(3);
        mAccountActivityViewPager.setAdapter(mViewPagerAdapter);
        mAccountActivitySmartTabLayout.setViewPager(mAccountActivityViewPager);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}
