package com.alacritystudios.statdota.activities.base;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

/**
 * Provides basic implementations for an activity
 */

public class BaseActivity extends AppCompatActivity {

    protected String TAG;
    protected Context mContext;

    public interface BaseActivityImpl {

        void initialiseUI();

        void setListeners();

        void setContextAndTags();

        void initialiseOtherComponents();
    }
}
