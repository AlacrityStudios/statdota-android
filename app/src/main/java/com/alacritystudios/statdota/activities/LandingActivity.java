package com.alacritystudios.statdota.activities;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.activities.base.BaseActivity;
import com.alacritystudios.statdota.constants.FragmentTagConstants;
import com.alacritystudios.statdota.fragments.HomeFragment;
import com.alacritystudios.statdota.fragments.MatchesFragment;
import com.alacritystudios.statdota.fragments.ProfileFragment;
import com.alacritystudios.statdota.fragments.RecentLeagueMatchesFragment;
import com.alacritystudios.statdota.fragments.RecentPubMatchesFragment;
import com.alacritystudios.statdota.fragments.SearchFragment;
import com.alacritystudios.statdota.fragments.TwitchClipsFragment;
import com.alacritystudios.statdota.fragments.TwitchFragment;
import com.alacritystudios.statdota.fragments.TwitchStreamsFragment;
import com.alacritystudios.statdota.fragments.UserFriendsFragment;
import com.alacritystudios.statdota.fragments.UserHeroesFragment;
import com.alacritystudios.statdota.fragments.UserMatchesFragment;
import com.alacritystudios.statdota.fragments.headless.LandingActivityNetworkFragment;
import com.alacritystudios.statdota.models.HomeFragmentWrapperModel;
import com.alacritystudios.statdota.models.MatchFilterResultModel;
import com.alacritystudios.statdota.models.PubMatchesModel;
import com.alacritystudios.statdota.models.RecentLeagueMatchModel;
import com.alacritystudios.statdota.models.SearchResultModel;
import com.alacritystudios.statdota.models.TeamDetailsModel;
import com.alacritystudios.statdota.models.TwitchClipsModel;
import com.alacritystudios.statdota.models.TwitchStreamsModel;
import com.alacritystudios.statdota.models.UserFriendsModel;
import com.alacritystudios.statdota.models.UserHeroSummaryModel;
import com.alacritystudios.statdota.models.UserMatchSummaryModel;
import com.alacritystudios.statdota.models.UserStatsModel;
import com.alacritystudios.statdota.utils.LoggerUtil;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.BottomBarTab;
import com.roughike.bottombar.OnTabSelectListener;

import java.util.List;

public class LandingActivity extends BaseActivity implements BaseActivity.BaseActivityImpl, LandingActivityNetworkFragment.LandingActivityNetworkFragmentInterface,
        SearchFragment.SearchFragmentListenerInterface, HomeFragment.HomeFragmentInterface, UserMatchesFragment.UserMatchesFragmentInterface, UserHeroesFragment.UserHeroesFragmentInterface, UserFriendsFragment.UserFriendsFragmentInterface,
        RecentPubMatchesFragment.RecentPubMatchesFragmentInterface, RecentLeagueMatchesFragment.RecentLeagueMatchesFragmentInterface, TwitchClipsFragment.TwitchClipsFragmentInterface, TwitchStreamsFragment.TwitchStreamsFragmentInterface {

    private BottomBar mBottomBar;

    private FragmentTransaction mFragmentTransaction;
    private HomeFragment mHomeFragment;
    private SearchFragment mSearchFragment;
    private ProfileFragment mProfileFragment;
    private MatchesFragment mMatchesFragment;
    private TwitchFragment mTwitchFragment;
    private LandingActivityNetworkFragment mLandingActivityNetworkFragment;

    @Override
    public void initialiseUI() {
        mBottomBar = (BottomBar) findViewById(R.id.landing_activity_bottom_bar);
        setupLandingActivityNetworkFragment();
        setupBottomBar();
    }

    @Override
    public void setListeners() {
        mBottomBar.setOnTabSelectListener(new OnTabSelectListener() {

            @Override
            public void onTabSelected(@IdRes int tabId) {
                switch (tabId) {
                    case R.id.menu_home:
                        mHomeFragment = (HomeFragment) getSupportFragmentManager().findFragmentByTag(FragmentTagConstants.HOME_FRAGMENT_TAG);
                        if (mHomeFragment == null) {
                            mHomeFragment = HomeFragment.newInstance();
                        }
                        mFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        mFragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
                        mFragmentTransaction.replace(R.id.landing_activity_container_frame_layout, mHomeFragment, FragmentTagConstants.HOME_FRAGMENT_TAG);
                        mFragmentTransaction.commit();
                        break;
                    case R.id.menu_search:
                        mSearchFragment = SearchFragment.newInstance();
                        mFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        mFragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
                        mFragmentTransaction.replace(R.id.landing_activity_container_frame_layout, mSearchFragment, FragmentTagConstants.SEARCH_FRAGMENT_TAG);
                        //mFragmentTransaction.addToBackStack(FragmentTagConstants.SEARCH_FRAGMENT_TAG);
                        mFragmentTransaction.commit();
                        break;
                    case R.id.menu_profile:
                        mProfileFragment = (ProfileFragment) getSupportFragmentManager().findFragmentByTag(FragmentTagConstants.PROFILE_FRAGMENT_TAG);
                        if (mProfileFragment == null) {
                            mProfileFragment = ProfileFragment.newInstance();
                        }
                        mProfileFragment = ProfileFragment.newInstance();
                        mFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        mFragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
                        mFragmentTransaction.replace(R.id.landing_activity_container_frame_layout, mProfileFragment, FragmentTagConstants.PROFILE_FRAGMENT_TAG);
                        // mFragmentTransaction.addToBackStack(FragmentTagConstants.PROFILE_FRAGMENT_TAG);
                        mFragmentTransaction.commit();
                        break;
                    case R.id.menu_league_matches:
                        mMatchesFragment = MatchesFragment.newInstance();
                        mFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        mFragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
                        mFragmentTransaction.replace(R.id.landing_activity_container_frame_layout, mMatchesFragment, FragmentTagConstants.LEAGUE_MATCHES_FRAGMENT_TAG);
                        //mFragmentTransaction.addToBackStack(FragmentTagConstants.LEAGUE_MATCHES_FRAGMENT_TAG);
                        mFragmentTransaction.commit();
                        break;
                    case R.id.menu_twitch:
                        mTwitchFragment = TwitchFragment.newInstance();
                        mFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        mFragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
                        mFragmentTransaction.replace(R.id.landing_activity_container_frame_layout, mTwitchFragment, FragmentTagConstants.TWITCH_FRAGMENT_TAG);
                        //mFragmentTransaction.addToBackStack(FragmentTagConstants.TWITCH_FRAGMENT_TAG);
                        mFragmentTransaction.commit();
                        break;
                }
            }
        });
    }

    @Override
    public void setContextAndTags() {
        mContext = LandingActivity.this;
        TAG = LoggerUtil.makeLogTag(getClass());
    }

    @Override
    public void initialiseOtherComponents() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        setContextAndTags();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
    }

    @Override
    public void onHomeFragmentQuerySuccessCallBack(HomeFragmentWrapperModel homeFragmentWrapperModel) {
        mHomeFragment.onFetchResultSuccess(homeFragmentWrapperModel);
    }

    @Override
    public void onHomeFragmentQueryFailureCallBack(boolean isCancelled) {
        mHomeFragment.onFetchResultFailure();
    }

    @Override
    public void onSearchQuerySuccessCallBack(SearchResultModel searchResultModel) {
        mSearchFragment.onDataFetchSuccess(searchResultModel.getPlayersSearchResultModelList());
    }

    @Override
    public void onSearchQueryFailureCallBack(boolean isCancelled) {
        mSearchFragment.onDataFetchFailure(isCancelled);
    }


    @Override
    public void fetchHomeFragmentStats() {
        mLandingActivityNetworkFragment.fetchHomeFragmentStats();
    }

    @Override
    public void refreshHomeFragmentStats() {
        mLandingActivityNetworkFragment.fetchHomeFragmentStatsFromBackEnd();
    }

    @Override
    public void onRecentPubMatchesRequest() {
        mBottomBar.selectTabAtPosition(3);
    }

    @Override
    public void onRecentLeagueMatchesRequest() {
        mBottomBar.selectTabAtPosition(3);
    }

    @Override
    public void performSearch(String query) {
        mLandingActivityNetworkFragment.performSearchQuery(query);
    }

    @Override
    public void onUserMatchesQuerySuccessCallback(List<UserMatchSummaryModel> userMatchSummaryModelList) {
        if (mProfileFragment != null)
            mProfileFragment.onUserMatchesQuerySuccessCallback(userMatchSummaryModelList);
    }

    @Override
    public void onUserMatchesQueryFailureCallback(boolean isCancelled) {
        if (mProfileFragment != null)
            mProfileFragment.onUserMatchesQueryFailureCallback(isCancelled);
    }

    @Override
    public void fetchUserMatches(String accountId, int limit, int offset, MatchFilterResultModel matchFilterResultModel) {
        mLandingActivityNetworkFragment.fetchUserMatchesFromBackend(accountId, limit, offset, matchFilterResultModel);
    }

    @Override
    public void onUserStatsQuerySuccessCallback(UserStatsModel userStatsModel) {

    }

    @Override
    public void onUserStatsQueryFailureCallback(boolean isCancelled) {

    }

    @Override
    public void fetchHeroFragmentStats(String accountId) {
        mLandingActivityNetworkFragment.fetchUserHeroes(accountId);
    }

    @Override
    public void refreshHeroFragmentStats(String accountId) {
        mLandingActivityNetworkFragment.fetchUserHeroesFromBackend(accountId);
    }

    @Override
    public void onUserHeroesQuerySuccessCallback(List<UserHeroSummaryModel> userHeroSummaryModelList) {
        if (mProfileFragment != null)
            mProfileFragment.onUserHeroesQuerySuccessCallback(userHeroSummaryModelList);
    }

    @Override
    public void onUserHeroesQueryFailureCallback(boolean isCancelled) {
        if (mProfileFragment != null)
            mProfileFragment.onUserHeroesQueryFailureCallback(isCancelled);
    }

    @Override
    public void fetchFriendFragmentStats(String accountId) {
        mLandingActivityNetworkFragment.fetchUserFriends(accountId);
    }

    @Override
    public void refreshFriendFragmentStats(String accountId) {
        mLandingActivityNetworkFragment.fetchUserFriendsFromBackend(accountId);
    }

    @Override
    public void onUserFriendsQuerySuccessCallback(List<UserFriendsModel> userFriendsModelList) {
        if (mProfileFragment != null)
            mProfileFragment.onUserFriendsQuerySuccessCallback(userFriendsModelList);
    }

    @Override
    public void onUserFriendsQueryFailureCallback(boolean isCancelled) {
        if (mProfileFragment != null)
            mProfileFragment.onUserFriendsQueryFailureCallback(isCancelled);
    }

    @Override
    public void fetchRecentPubMatches(int limit, int offset) {
        mLandingActivityNetworkFragment.fetchRecentPubMatchesFromBackend(limit, offset);
    }

    @Override
    public void onRecentPubMatchesQuerySuccessCallback(List<PubMatchesModel> pubMatchesModelList) {
        if (mMatchesFragment != null)
            mMatchesFragment.onRecentPubMatchesQuerySuccess(pubMatchesModelList);
    }

    @Override
    public void onRecentPubMatchesQueryFailureCallback(boolean isCancelled) {
        if (mMatchesFragment != null)
            mMatchesFragment.onRecentPubMatchesQueryFailure(isCancelled);
    }

    @Override
    public void fetchRecentLeagueMatches(int limit, int offset) {
        mLandingActivityNetworkFragment.fetchRecentLeagueMatchesFromBackend(limit, offset);
    }

    @Override
    public void onRecentLeagueMatchesQuerySuccessCallback(List<RecentLeagueMatchModel> recentLeagueMatchModelList) {
        if (mMatchesFragment != null)
            mMatchesFragment.onRecentLeagueMatchesQuerySuccess(recentLeagueMatchModelList);
    }

    @Override
    public void onRecentLeagueMatchesQueryFailureCallback(boolean isCancelled) {
        if (mMatchesFragment != null)
            mMatchesFragment.onRecentLeagueMatchesQueryFailure(isCancelled);
    }

    @Override
    public void fetchTwitchFragmentClips(int offset, int count) {

    }

    @Override
    public void fetchTwitchFragmentLiveStreams() {
        mLandingActivityNetworkFragment.fetchTwitchStreams();
    }

    @Override
    public void refreshTwitchFragmentLiveStreams() {
        mLandingActivityNetworkFragment.fetchHomeFragmentStatsFromBackEnd();
    }

    @Override
    public void onTwitchClipsFragmentQuerySuccessCallBack(List<TwitchClipsModel> twitchClipsModelList) {
        if (mTwitchFragment != null)
            mTwitchFragment.onTopTwitchClipsQuerySuccess(twitchClipsModelList);
    }

    @Override
    public void onTwitchClipsFragmentQueryFailureCallBack(boolean isCancelled) {
        if (mTwitchFragment != null)
            mTwitchFragment.onTopTwitchClipsQueryFailure(isCancelled);
    }

    @Override
    public void onTwitchStreamsFragmentQuerySuccessCallBack(TwitchStreamsModel twitchStreamsModel) {
        if (mTwitchFragment != null)
            mTwitchFragment.onLiveTwitchStreamsQuerySuccess(twitchStreamsModel);
    }

    @Override
    public void onTwitchStreamsFragmentQueryFailureCallBack(boolean isCancelled) {
        if (mTwitchFragment != null)
            mTwitchFragment.onLiveTwitchStreamsQueryFailure(isCancelled);
    }

    @Override
    public void fetchTeamDetails(long teamId) {
        mLandingActivityNetworkFragment.fetchTeamDetailsFromBackend(teamId);
    }

    @Override
    public void onFetchTeamDetailsSuccessCallback(TeamDetailsModel teamDetailsModel) {
        if (mHomeFragment != null)
            mHomeFragment.onTeamDetailsQuerySuccess(teamDetailsModel);
    }

    @Override
    public void onFetchTeamDetailsFailureCallback(boolean isCancelled) {
        if (mHomeFragment != null)
            mHomeFragment.onTeamDetailsQueryFailure(isCancelled);
    }

    /**
     * Sets up the bottom bar for the activity.
     */
    private void setupBottomBar() {
        Log.i(TAG, "setupBottomBar()");
        for (int i = 0; i < mBottomBar.getTabCount(); i++) {
            BottomBarTab tab = mBottomBar.getTabAtPosition(i);
            tab.setGravity(Gravity.CENTER);
            View icon = tab.findViewById(com.roughike.bottombar.R.id.bb_bottom_bar_icon);
            icon.setPadding(0, icon.getPaddingTop(), 0, icon.getPaddingTop());
            try {
                icon.setLayoutParams(new LinearLayout.LayoutParams((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32, getResources().getDisplayMetrics()), (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32, getResources().getDisplayMetrics())));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            View title = tab.findViewById(com.roughike.bottombar.R.id.bb_bottom_bar_title);
            title.setVisibility(View.GONE);
        }
    }

    /**
     * Setup the hero network fragment.
     */
    private void setupLandingActivityNetworkFragment() {
        Log.i(TAG, "setupLandingActivityNetworkFragment()");
        mLandingActivityNetworkFragment = (LandingActivityNetworkFragment) getSupportFragmentManager().findFragmentByTag(FragmentTagConstants.LANDING_ACTIVITY_NETWORK_FRAGMENT);

        if (mLandingActivityNetworkFragment == null) {
            mLandingActivityNetworkFragment = LandingActivityNetworkFragment.getInstance();
            getSupportFragmentManager().beginTransaction().add(mLandingActivityNetworkFragment, FragmentTagConstants.LANDING_ACTIVITY_NETWORK_FRAGMENT).commit();
        }
    }
}
