package com.alacritystudios.statdota.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.activities.base.BaseActivity;
import com.alacritystudios.statdota.constants.FragmentTagConstants;
import com.alacritystudios.statdota.fragments.HeroPerformanceStatsListFragment;
import com.alacritystudios.statdota.fragments.headless.HeroNetworkFragment;
import com.alacritystudios.statdota.models.HeroStatsModel;
import com.alacritystudios.statdota.utils.LoggerUtil;

import java.util.List;

public class HeroActivity extends BaseActivity implements BaseActivity.BaseActivityImpl, HeroNetworkFragment.HeroNetworkFragmentInterface, HeroPerformanceStatsListFragment.HeroPerformanceStatsListFragmentInterface {

    private Toolbar mToolbar;
    private HeroPerformanceStatsListFragment mHeroPerformanceStatsListFragment;
    private HeroNetworkFragment mHeroNetworkFragment;

    @Override
    public void initialiseUI() {
        mToolbar = findViewById(R.id.hero_activity_toolbar);
        setupToolbar();
        setupHeroNetworkFragment();
        setupHeroPerformanceStatsFragment();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    public void setListeners() {

    }

    @Override
    public void setContextAndTags() {
        mContext = HeroActivity.this;
        TAG = LoggerUtil.makeLogTag(getClass());
    }

    @Override
    public void initialiseOtherComponents() {

    }

    @Override
    public void onFetchHeroPerformanceStatsListSuccessCallback(List<HeroStatsModel> heroStatsModels) {
        mHeroPerformanceStatsListFragment.onDataFetchSuccess(heroStatsModels);
    }

    @Override
    public void onFetchHeroPerformanceStatsListFailureCallback(boolean isCancelled) {
        mHeroPerformanceStatsListFragment.onDataFetchFailure(isCancelled);
    }

    @Override
    public void fetchHeroPerformanceStats() {
        mHeroNetworkFragment.fetchHeroPerformanceStats();
    }

    @Override
    public void refreshHeroPerformanceStats() {
        mHeroNetworkFragment.fetchHeroPerformanceStatsFromBackEnd();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hero);
        setContextAndTags();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
    }

    /**
     * Setup the custom toolbar.
     */
    private void setupToolbar() {
        Log.i(TAG, "setupToolbar()");
        mToolbar.setTitleTextAppearance(mContext, R.style.ToolbarTitleAppearance);
        mToolbar.setTitle(R.string.heroes);
        ((AppCompatActivity) mContext).setSupportActionBar(mToolbar);
    }

    /**
     * Setup the hero network fragment.
     */
    private void setupHeroNetworkFragment() {
        Log.i(TAG, "setupToolbar()");
        mHeroNetworkFragment = (HeroNetworkFragment) getSupportFragmentManager().findFragmentByTag(FragmentTagConstants.HERO_NETWORK_FRAGMENT);

        if (mHeroNetworkFragment == null) {
            mHeroNetworkFragment = HeroNetworkFragment.getInstance();
            getSupportFragmentManager().beginTransaction().add(mHeroNetworkFragment, FragmentTagConstants.HERO_NETWORK_FRAGMENT).commit();
        }
    }

    /**
     * Setup the hero network fragment.
     */
    private void setupHeroPerformanceStatsFragment() {
        Log.i(TAG, "setupHeroPerformanceStatsFragment()");
        mHeroPerformanceStatsListFragment = (HeroPerformanceStatsListFragment) getSupportFragmentManager().findFragmentByTag(FragmentTagConstants.HERO_PERFORMANCE_STATS_LIST_FRAGMENT);

        if (mHeroPerformanceStatsListFragment == null) {
            mHeroPerformanceStatsListFragment = HeroPerformanceStatsListFragment.getInstance();
            getSupportFragmentManager().beginTransaction().add(R.id.hero_activity_container_layout, mHeroPerformanceStatsListFragment, FragmentTagConstants.HERO_PERFORMANCE_STATS_LIST_FRAGMENT).commit();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}
