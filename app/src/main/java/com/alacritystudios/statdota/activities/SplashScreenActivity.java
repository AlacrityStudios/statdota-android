package com.alacritystudios.statdota.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.activities.base.BaseActivity;
import com.alacritystudios.statdota.constants.ApplicationConstants;
import com.alacritystudios.statdota.constants.StatDotaApiConstants;
import com.alacritystudios.statdota.models.ApplicationMetadataModel;
import com.alacritystudios.statdota.rest.ApiClient;
import com.alacritystudios.statdota.rest.ApiInterface;
import com.alacritystudios.statdota.sql.StatDotaDatabaseHelper;
import com.alacritystudios.statdota.utils.ComponentUtil;
import com.alacritystudios.statdota.utils.LoggerUtil;
import com.alacritystudios.statdota.utils.PreferenceUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class SplashScreenActivity extends BaseActivity implements BaseActivity.BaseActivityImpl {

    private ConstraintLayout mRootLayout;
    private ImageView mApplicationLogoImageView;
    private LinearLayout mMetadataInfoLayout;
    private ProgressBar mProgressBar;

    @Override
    public void initialiseUI() {
        mRootLayout = (ConstraintLayout) findViewById(R.id.activity_root_layout);
        mApplicationLogoImageView = (ImageView) findViewById(R.id.application_logo_image_view);
        mMetadataInfoLayout = (LinearLayout) findViewById(R.id.meta_data_info_container_layout);
        mProgressBar = (ProgressBar) findViewById(R.id.meta_data_progress_bar);
        mMetadataInfoLayout.setVisibility(View.GONE);
        checkMetadataValidity();
    }


    @Override
    public void setListeners() {
    }

    @Override
    public void setContextAndTags() {
        mContext = SplashScreenActivity.this;
        TAG = LoggerUtil.makeLogTag(getClass());
    }

    @Override
    public void initialiseOtherComponents() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_spash_screen);
        setContextAndTags();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * Check when metadata was last updated and update it.
     */
    private void checkMetadataValidity() {
        Log.i(TAG, "checkMetadataValidity()");
        long lastModificationDate = PreferenceUtil.getMetadataModificationDatePreference(mContext);
        long timeElapsed = System.currentTimeMillis() - lastModificationDate;
        if (timeElapsed > ApplicationConstants.weekInMillis) {
            mMetadataInfoLayout.setVisibility(View.VISIBLE);
            fetchApplicationMetadataFromBackEnd();
        } else {
            Intent intent = new Intent(SplashScreenActivity.this, LandingActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
        }
    }

    /**
     * Fetch hero and item details and store them on disk for future referencing.
     */
    public void fetchApplicationMetadataFromBackEnd() {
        Log.i(TAG, "fetchApplicationMetadataFromBackEnd()");
        Retrofit retrofit = ApiClient.getClient(StatDotaApiConstants.STAT_DOTA_BASE_URL);
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<ApplicationMetadataModel> applicationMetadataCall = apiInterface.getApplicationMetadata();
        applicationMetadataCall.enqueue(new Callback<ApplicationMetadataModel>() {

            @Override
            public void onResponse(Call<ApplicationMetadataModel> call, Response<ApplicationMetadataModel> response) {
                if (response.isSuccessful()) {
                    new SaveMetadataAsyncTask().execute(response.body());
                } else {
                    mMetadataInfoLayout.setVisibility(View.GONE);
                    mProgressBar.setVisibility(View.GONE);
                    ComponentUtil.createAndShowSnackBar(mContext, mMetadataInfoLayout, mContext.getString(R.string.connection_error_message));
                }
            }

            @Override
            public void onFailure(Call<ApplicationMetadataModel> call, Throwable t) {
                mMetadataInfoLayout.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.GONE);
                ComponentUtil.createAndShowSnackBar(mContext, mMetadataInfoLayout, mContext.getString(R.string.connection_error_message));
            }
        });
    }

    public class SaveMetadataAsyncTask extends AsyncTask<ApplicationMetadataModel, Void, Void> {

        @Override
        protected void onPostExecute(Void aVoid) {
            PreferenceUtil.setMetadataModificationDatePreference(mContext, System.currentTimeMillis());
            Intent intent = new Intent(SplashScreenActivity.this, LandingActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
        }

        @Override
        protected Void doInBackground(ApplicationMetadataModel... applicationMetadataModels) {
            StatDotaDatabaseHelper statDotaDatabaseHelper = StatDotaDatabaseHelper.getInstance(getApplicationContext());
            statDotaDatabaseHelper.saveHeroDetails(applicationMetadataModels[0].getHeroDetailsList());
            statDotaDatabaseHelper.saveItemDetails(applicationMetadataModels[0].getItemDetailsList());
            return null;
        }
    }

}