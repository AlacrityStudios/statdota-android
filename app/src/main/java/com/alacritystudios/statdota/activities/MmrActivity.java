package com.alacritystudios.statdota.activities;

import android.os.Bundle;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.activities.base.BaseActivity;
import com.alacritystudios.statdota.constants.FragmentTagConstants;
import com.alacritystudios.statdota.fragments.HighMmrPubStarsListFragment;
import com.alacritystudios.statdota.fragments.headless.MmrActivityNetworkFragment;
import com.alacritystudios.statdota.fragments.headless.TeamActivityNetworkFragment;
import com.alacritystudios.statdota.models.HighMmrPubStarsModel;
import com.alacritystudios.statdota.utils.LoggerUtil;

import java.util.List;

public class MmrActivity  extends BaseActivity implements BaseActivity.BaseActivityImpl, HighMmrPubStarsListFragment.HighMmrPubStarsListFragmentInterface, MmrActivityNetworkFragment.MmrActivityNetworkFragmentInterface {

    private MmrActivityNetworkFragment.MmrActivityNetworkFragmentInterface mMmrActivityNetworkFragmentInterface;
    private MmrActivityNetworkFragment mMmrActivityNetworkFragment;
    private HighMmrPubStarsListFragment mHighMmrPubStarsListFragment;

    @Override
    public void initialiseUI() {
        mMmrActivityNetworkFragment = (MmrActivityNetworkFragment) getSupportFragmentManager().findFragmentByTag(FragmentTagConstants.MMR_ACTIVITY_NETWORK_FRAGMENT);
        if (mMmrActivityNetworkFragment == null) {
            mMmrActivityNetworkFragment = MmrActivityNetworkFragment.getInstance();
            getSupportFragmentManager().beginTransaction().add(mMmrActivityNetworkFragment, FragmentTagConstants.MMR_ACTIVITY_NETWORK_FRAGMENT).commit();
        }
        mHighMmrPubStarsListFragment = (HighMmrPubStarsListFragment) getSupportFragmentManager().findFragmentByTag(FragmentTagConstants.HIGH_MMR_PUB_STARS_LIST_FRAGMENT);
        if (mHighMmrPubStarsListFragment == null) {
            mHighMmrPubStarsListFragment = HighMmrPubStarsListFragment.getInstance();
            getSupportFragmentManager().beginTransaction().add(R.id.mmr_activity_container_layout, mHighMmrPubStarsListFragment, FragmentTagConstants.HIGH_MMR_PUB_STARS_LIST_FRAGMENT).commit();
        }
    }

    @Override
    public void setListeners() {

    }

    @Override
    public void setContextAndTags() {
        TAG = LoggerUtil.makeLogTag(getClass());
        mContext = MmrActivity.this;
    }

    @Override
    public void initialiseOtherComponents() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mmr);
        setContextAndTags();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
    }

    @Override
    public void onFetchHighMmrPubStarsList(int offset, int count, String query) {
        mMmrActivityNetworkFragment.fetchHighMmrPubStarsListFromBackend(offset, count, query);
    }

    @Override
    public void onFetchTeamsListSuccessCallback(List<HighMmrPubStarsModel> highMmrPubStarsModelList) {
        mHighMmrPubStarsListFragment.onHighMmrPubStarsQuerySuccess(highMmrPubStarsModelList);
    }

    @Override
    public void onFetchTeamsListFailureCallback(boolean isCancelled) {
        mHighMmrPubStarsListFragment.onHighMmrPubStarsQueryFailure(isCancelled);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}
