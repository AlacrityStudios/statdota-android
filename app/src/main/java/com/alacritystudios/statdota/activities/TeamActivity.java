package com.alacritystudios.statdota.activities;

import android.os.Bundle;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.activities.base.BaseActivity;
import com.alacritystudios.statdota.constants.FragmentTagConstants;
import com.alacritystudios.statdota.fragments.TeamDetailsDialogFragment;
import com.alacritystudios.statdota.fragments.TeamsListFragment;
import com.alacritystudios.statdota.fragments.headless.TeamActivityNetworkFragment;
import com.alacritystudios.statdota.models.TeamDetailsModel;
import com.alacritystudios.statdota.models.TeamsModel;
import com.alacritystudios.statdota.utils.LoggerUtil;

import java.util.List;

public class TeamActivity extends BaseActivity implements BaseActivity.BaseActivityImpl,
        TeamsListFragment.TeamsListFragmentInterface, TeamActivityNetworkFragment.TeamActivityNetworkFragmentInterface {

    private TeamActivityNetworkFragment.TeamActivityNetworkFragmentInterface mTeamActivityNetworkFragmentInterface;
    private TeamActivityNetworkFragment mTeamActivityNetworkFragment;
    private TeamsListFragment mTeamsListFragment;

    @Override
    public void initialiseUI() {
        mTeamActivityNetworkFragment = (TeamActivityNetworkFragment) getSupportFragmentManager().findFragmentByTag(FragmentTagConstants.TEAM_ACTIVITY_NETWORK_FRAGMENT);
        if(mTeamActivityNetworkFragment == null) {
            mTeamActivityNetworkFragment = TeamActivityNetworkFragment.getInstance();
            getSupportFragmentManager().beginTransaction().add(mTeamActivityNetworkFragment, FragmentTagConstants.TEAM_ACTIVITY_NETWORK_FRAGMENT).commit();
        }
        mTeamsListFragment = (TeamsListFragment) getSupportFragmentManager().findFragmentByTag(FragmentTagConstants.TEAMS_LIST_FRAGMENT);
        if(mTeamsListFragment == null) {
            mTeamsListFragment = TeamsListFragment.getInstance();
            getSupportFragmentManager().beginTransaction().add(R.id.team_activity_container_layout ,mTeamsListFragment, FragmentTagConstants.TEAMS_LIST_FRAGMENT).commit();
        }
    }

    @Override
    public void setListeners() {

    }

    @Override
    public void setContextAndTags() {
        TAG = LoggerUtil.makeLogTag(getClass());
        mContext = TeamActivity.this;
    }

    @Override
    public void initialiseOtherComponents() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);
        setContextAndTags();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
    }

    @Override
    public void onFetchTeamsList(int offset, int count, String query) {
        mTeamActivityNetworkFragment.fetchTeamsListFromBackend(offset, count, query);
    }

    @Override
    public void onFetchTeamsListSuccessCallback(List<TeamsModel> teamsModelList) {
        mTeamsListFragment.onTeamsQuerySuccess(teamsModelList);
    }

    @Override
    public void onFetchTeamsListFailureCallback(boolean isCancelled) {
        mTeamsListFragment.onTeamsQueryFailure(isCancelled);
    }

    @Override
    public void fetchTeamDetails(long teamId) {
        mTeamActivityNetworkFragment.fetchTeamDetailsFromBackend(teamId);
    }

    @Override
    public void onFetchTeamDetailsSuccessCallback(TeamDetailsModel teamDetailsModel) {
        mTeamsListFragment.onTeamDetailsQuerySuccess(teamDetailsModel);
    }

    @Override
    public void onFetchTeamDetailsFailureCallback(boolean isCancelled) {
        mTeamsListFragment.onTeamDetailsQueryFailure(isCancelled);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}
