package com.alacritystudios.statdota.activities;

import android.graphics.drawable.Drawable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alacritystudios.statdota.R;
import com.alacritystudios.statdota.activities.base.BaseActivity;
import com.alacritystudios.statdota.adapters.TeamsListAdapter;
import com.alacritystudios.statdota.constants.StatDotaApiConstants;
import com.alacritystudios.statdota.models.TeamsModel;
import com.alacritystudios.statdota.rest.ApiClient;
import com.alacritystudios.statdota.rest.ApiInterface;
import com.alacritystudios.statdota.utils.LoggerUtil;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class TeamsListActivity extends BaseActivity implements BaseActivity.BaseActivityImpl {

    private Toolbar mToolbar;
    private RecyclerView mTeamsListRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayout mPlaceholderLayout;
    private TextView mPlaceholderMessage;

    private List<TeamsModel> mTeamsModelList;
    private LinearLayoutManager mTeamsListLinearLayoutManager;
    private TeamsListAdapter mTeamsListAdapter;
    private Drawable mPlaceholderDrawable;

    private int mPageNumber = 0;
    private int mPreviousTotal = 0;
    private boolean mLoading = true;
    private int mVisibleThreshold = 5;
    private int mVisibleItemCount;
    private int mTotalItemCount;
    private int mFirstVisibleItem;

    @Override
    public void initialiseUI() {
        mToolbar = findViewById(R.id.teams_list_activity_toolbar);
        mTeamsListRecyclerView = findViewById(R.id.teams_list_recycler_view);
        mSwipeRefreshLayout = findViewById(R.id.teams_list_activity_swipe_refresh_layout);
        mPlaceholderLayout = findViewById(R.id.teams_list_activity_placeholder_layout);
        mPlaceholderMessage = findViewById(R.id.teams_list_activity_placeholder_error_message);
        mTeamsListRecyclerView.setLayoutManager(mTeamsListLinearLayoutManager);
        mTeamsListRecyclerView.setAdapter(mTeamsListAdapter);
        setupToolbar();
        setupSwipeRefreshLayout();
        fetchTeamsList(10, 10 * mPageNumber);
    }

    @Override
    public void setListeners() {
        mTeamsListRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                mVisibleItemCount = mTeamsListRecyclerView.getChildCount();
                mTotalItemCount = mTeamsListLinearLayoutManager.getItemCount();
                mFirstVisibleItem = mTeamsListLinearLayoutManager.findFirstVisibleItemPosition();

                if (mLoading) {
                    if (mTotalItemCount > mPreviousTotal) {
                        mLoading = false;
                        mPreviousTotal = mTotalItemCount;
                    }
                }
                if (!mLoading && (mTotalItemCount - mVisibleItemCount) <= (mFirstVisibleItem + mVisibleThreshold)) {
                    fetchTeamsList(10, mPageNumber * 10);
                    mLoading = true;
                }
            }
        });
    }

    @Override
    public void setContextAndTags() {
        mContext = TeamsListActivity.this;
        TAG = LoggerUtil.makeLogTag(getClass());
    }

    @Override
    public void initialiseOtherComponents() {
        mPageNumber = 0;
        mPreviousTotal = 0;
        mTeamsModelList = new ArrayList<>();
        mPlaceholderDrawable = ActivityCompat.getDrawable(mContext, R.drawable.ic_dota);
        DrawableCompat.wrap(mPlaceholderDrawable).setTint(ActivityCompat.getColor(mContext, R.color.colorDefaultMedium));
       // mTeamsListAdapter = new TeamsListAdapter(mTeamsModelList, mContext, mPlaceholderDrawable);
        mTeamsListLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teams_list);
        setContextAndTags();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    /**
     * Setup the custom toolbar.
     */
    private void setupToolbar() {
        Log.i(TAG, "setupToolbar()");
        ((AppCompatActivity) mContext).setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.teams);
    }

    /**
     * Setup the swipe refresh layout behaviour.
     */
    private void setupSwipeRefreshLayout() {
        Log.i(TAG, "setupSwipeRefreshLayout()");
        mPlaceholderLayout.setVisibility(View.GONE);
        mPlaceholderMessage.setText(R.string.fragment_heroes_error_message);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                mTeamsModelList.clear();
                mTeamsListAdapter.notifyDataSetChanged();
                mPageNumber = 0;
                fetchTeamsList(10, mPageNumber * 10);
            }
        });
    }

    /**
     * Fetch hero stats.
     */
    private void fetchTeamsList(int count, int offset) {
        Log.i(TAG, "fetchTeamsList()");
        mPageNumber++;
        if (! mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(true);
        }
        Retrofit retrofit = ApiClient.getClient(StatDotaApiConstants.STAT_DOTA_BASE_URL);
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<List<TeamsModel>> teams = apiInterface.getTeams(offset, count, "");
        teams.enqueue(new Callback<List<TeamsModel>>() {

            @Override
            public void onResponse(Call<List<TeamsModel>> call, Response<List<TeamsModel>> response) {
                if (response.isSuccessful()) {
                    List<TeamsModel> teamsModelList = response.body();
                    mTeamsModelList.addAll(teamsModelList);
                    mTeamsListAdapter.notifyDataSetChanged();
                    if (mSwipeRefreshLayout.isRefreshing()) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        if (mTeamsModelList.size() == 0) {
                            mTeamsListRecyclerView.setVisibility(View.GONE);
                            mPlaceholderLayout.setVisibility(View.VISIBLE);
                        } else {
                            mTeamsListRecyclerView.setVisibility(View.VISIBLE);
                            mPlaceholderLayout.setVisibility(View.GONE);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<TeamsModel>> call, Throwable t) {
                if (mSwipeRefreshLayout.isRefreshing()) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    if (mTeamsModelList.size() == 0) {
                        mTeamsListRecyclerView.setVisibility(View.GONE);
                        mPlaceholderLayout.setVisibility(View.VISIBLE);
                    } else {
                        mTeamsListRecyclerView.setVisibility(View.VISIBLE);
                        mPlaceholderLayout.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}
